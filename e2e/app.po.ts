import { browser, element, by } from 'protractor';

export class ManagerEventosFrontendPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('eventos-root h1')).getText();
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsientoManualComponent } from './asiento-manual/asiento-manual.component';
import { AdminAuthGuard } from './authguards/AdminAuthGuard';
import { AdministracionAuthGuard } from './authguards/AdministracionAuthGuard';
import { ComprasAuthGuard } from './authguards/ComprasAuthGuard';
import { GerenciaAuthGuard } from './authguards/GerenciaAuthGuard';
import { GestorPreciosAG } from './authguards/GestorPreciosAG';
import { VentasAuthGuar } from './authguards/VentasAuthGuard';
import { BalanceCuentasComponent } from './balance-cuentas/balance-cuentas.component';
import { CajaComponent } from './caja/caja.component';
import { GestionChequeComponent } from './cheque/gestion-cheque/gestion-cheque.component';
import { GestionChequeraComponent } from './cheque/gestion-chequera/gestion-chequera.component';
import { ListaChequeraComponent } from './cheque/lista-chequera/lista-chequera.component';
import { ListaOrdenCobranzasComponent } from './clientes/cobros/listado/ordenes-cobranza.component';
import { ListaClientesComponent } from './clientes/lista-clientes/lista-clientes.component';
import { ListaComisionistasComponent } from './contratos/comisionistas/lista-comisionistas/lista-comisionistas.component';
import { ListaLiquidacionComisionesComponent } from './contratos/liquidacion-comision/listado/listado-liquidaciones.component';
import { ListaContratoComponent } from './contratos/lista-contrato/lista-contrato.component';
import { GestionEmpleadosComponent } from './empleados/gestion-empleados/gestion-empleados.component';
import { ListaEmpleadosComponent } from './empleados/lista-empleados/lista-empleados.component';
import { GestionPlanDeCuentasComponent } from './gestion-planDeCuentas/gestion-planDeCuentas.component';
import { GestionarCentroCostoComponent } from './gestionar-centro-costo/gestionar-centro-costo.component';
import { HomeComponent } from './home/home.component';
import { ListaAsientosComponent } from './lista-asientos/lista-asientos.component';
import { ListaCentrosCostoComponent } from './lista-centros-costo/lista-centros-costo.component';
import { LoginComponent } from './login/login.component';
import { ParametrosComponent } from './parametricos/parametros/parametros.component';
import { PreciosAdicionalesComponent } from './precios/adicionales/precios-adicionales/precios-adicionales.component';
import { PreciosBebidasComponent } from './precios/bebidas/precios-bebidas/precios-bebidas.component';
import { HomeListadosPreciosComponent } from './precios/home-listados-precios/home-listados-precios.component';
import { ImpresionAlquilerComponent } from './precios/precios-alquiler/impresion-alquiler/impresion-alquiler.component';
import { ListadoPreciosAlquilerComponent } from './precios/precios-alquiler/listado-precios-alquiler/listado-precios-alquiler.component';
import { ImpresionMenuComponent } from './precios/precios-menu/impresion-menu/impresion-menu.component';
import { ListadoPreciosMenuComponent } from './precios/precios-menu/listado-precios-menu/listado-precios-menu.component';
import { VistaListadosComponent } from './precios/vita-listados/vista-listados.component';
import { GestionarProductoComponent } from './producto/gestionar-productos/gestionar-producto.component';
import { ListaProductosComponent } from './producto/lista-productos/lista-productos.component';
import { FacturasComponent } from './proveedores/facturas/listado/facturas.component';
import { GestionProveedoresComponent } from './proveedores/gestion-proveedores/gestion-proveedores.component';
import { ListaProveedoresComponent } from './proveedores/lista-proveedores/lista-proveedores.component';
import { OrdenPagosComponent } from './proveedores/ordenPago/listado/ordenes-pago.component';
import { CambiarPasswordComponent } from './recuperar-password/cambiar-password/cambiar-password.component';
import { OlvidoPasswordComponent } from './recuperar-password/olvido-password/olvido-password.component';
import { PanelControlComponent } from './reportes/panel-control/panel-control.component';
import { TotalesGastosComponent } from './reportes/totales-gastos/totales-gastos.component';
import { TotalesProductoComponent } from './reportes/totales-producto/totales-producto.component';
import { GestionarSalonesComponent } from './salon/gestionar-salones/gestionar-salones.component';
import { ListaSalonesComponent } from './salon/lista-salones/lista-salones.component';









const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'admin/reportes/panel-control', component: PanelControlComponent, canActivate: [GerenciaAuthGuard] },
  { path: 'admin/reportes/totales-productos', component: TotalesProductoComponent, canActivate: [GerenciaAuthGuard] },
  { path: 'admin/reportes/totales-gastos', component: TotalesGastosComponent, canActivate: [GerenciaAuthGuard] },
  { path: 'admin/alta-centro', component: GestionarCentroCostoComponent, canActivate: [AdminAuthGuard] },
  { path: 'admin/gestion-planCuentas', component: GestionPlanDeCuentasComponent, canActivate: [AdminAuthGuard] },
  { path: 'admin/lista-asientos', component: ListaAsientosComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/asiento-manual', component: AsientoManualComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/editar-centro/:codigoCentroCosto', component: GestionarCentroCostoComponent, canActivate: [AdminAuthGuard] },
  { path: 'admin/editar-asiento/:numeroAsiento', component: AsientoManualComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/lista-centros', component: ListaCentrosCostoComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/balance-cuentas', component: BalanceCuentasComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/chequeras/nuevo', component: GestionChequeraComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/chequeras/:id', component: GestionChequeraComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/chequeras', component: ListaChequeraComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/parametricos', component: ParametrosComponent, canActivate: [AdminAuthGuard] },

  { path: 'admin/precios/home', component: HomeListadosPreciosComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/vistas/:codigo', component: VistaListadosComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/listado/002', component: ListadoPreciosAlquilerComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/listado/002/imprimir', component: ImpresionAlquilerComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/listado/001', component: ListadoPreciosMenuComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/listado/001/imprimir', component: ImpresionMenuComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/listado/003', component: PreciosBebidasComponent, canActivate: [GestorPreciosAG] },
  { path: 'admin/precios/listado/adic', component: PreciosAdicionalesComponent, canActivate: [GestorPreciosAG] },

  { path: 'caja/resumen', component: CajaComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'clientes/nuevo', component: ListaClientesComponent, canActivate: [VentasAuthGuar] },
  { path: 'clientes/editar/:id', component: ListaClientesComponent, canActivate: [VentasAuthGuar] },
  { path: 'clientes/lista-clientes', component: ListaClientesComponent, canActivate: [VentasAuthGuar] },
  { path: 'clientes/alta-contrato', component: ListaContratoComponent, canActivate: [VentasAuthGuar] },
  { path: 'clientes/lista-contratos', component: ListaContratoComponent, canActivate: [VentasAuthGuar] },
  { path: 'clientes/ver-contrato/:id', component: ListaContratoComponent, canActivate: [VentasAuthGuar] },
  { path: 'clientes/lista-cobros', component: ListaOrdenCobranzasComponent, canActivate: [VentasAuthGuar] },



  { path: 'comisionistas/lista-comisionistas', component: ListaComisionistasComponent, canActivate: [AdminAuthGuard] },
  { path: 'comisionistas/nuevo', component: ListaComisionistasComponent, canActivate: [AdminAuthGuard] },
  { path: 'comisionistas/ver-comisionista/:id', component: ListaComisionistasComponent, canActivate: [AdminAuthGuard] },
  { path: 'comisionistas/nueva-liquidacion', component: ListaLiquidacionComisionesComponent, canActivate: [AdminAuthGuard] },
  { path: 'comisionistas/lista-liquidaciones', component: ListaLiquidacionComisionesComponent, canActivate: [AdminAuthGuard] },
  { path: 'comisionistas/editar-liquidacion/:id', component: ListaLiquidacionComisionesComponent, canActivate: [AdminAuthGuard] },

  { path: 'proveedores/nuevo', component: GestionProveedoresComponent, canActivate: [ComprasAuthGuard] },
  { path: 'proveedores/editar/:id', component: GestionProveedoresComponent, canActivate: [ComprasAuthGuard] },
  { path: 'proveedores/lista-proveedores', component: ListaProveedoresComponent, canActivate: [ComprasAuthGuard] },
  { path: 'proveedores/facturas', component: FacturasComponent, canActivate: [ComprasAuthGuard] },
  { path: 'proveedores/ordenesPago', component: OrdenPagosComponent, canActivate: [ComprasAuthGuard] },

  { path: 'empleados/lista-empleados', component: ListaEmpleadosComponent, canActivate: [AdminAuthGuard] },
  { path: 'empleados/nuevo', component: GestionEmpleadosComponent, canActivate: [AdminAuthGuard] },
  { path: 'empleados/editar/:id', component: GestionEmpleadosComponent, canActivate: [AdminAuthGuard] },

  { path: 'salones/lista-salones', component: ListaSalonesComponent, canActivate: [AdminAuthGuard] },
  { path: 'salones/nuevo', component: GestionarSalonesComponent, canActivate: [AdminAuthGuard] },
  { path: 'salones/editar/:id', component: GestionarSalonesComponent, canActivate: [AdminAuthGuard] },

  { path: 'productos/lista-productos', component: ListaProductosComponent, canActivate: [AdminAuthGuard] },
  { path: 'productos/nuevo', component: GestionarProductoComponent, canActivate: [AdminAuthGuard] },
  { path: 'productos/editar/:id', component: GestionarProductoComponent, canActivate: [AdminAuthGuard] },

  { path: 'login', component: LoginComponent },
  { path: 'public/olvido-su-password', component: OlvidoPasswordComponent },
  { path: 'public/cambiar-password/:email/:token', component: CambiarPasswordComponent, pathMatch: 'full' },
  { path: 'admin/cheque/nuevo', component: GestionChequeComponent, canActivate: [AdministracionAuthGuard] },
  { path: 'admin/cheque/:id', component: GestionChequeComponent, canActivate: [AdministracionAuthGuard] }
  /* TODO { path: '**', component: PageNotFoundComponent } */

];
@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
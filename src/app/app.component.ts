import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MenuItem } from 'primeng/primeng';
import { AdminAuthGuard } from './authguards/AdminAuthGuard';
import { AdministracionAuthGuard } from './authguards/AdministracionAuthGuard';
import { ComprasAuthGuard } from './authguards/ComprasAuthGuard';
import { GerenciaAuthGuard } from './authguards/GerenciaAuthGuard';
import { GestorPreciosAG } from './authguards/GestorPreciosAG';
import { VentasAuthGuar } from './authguards/VentasAuthGuard';
import { CookieServiceHelper } from './common-services/cookieServiceHelper.service';
import { SessionComponent } from './session-component.component';

@Component({
	selector: 'catering-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent extends SessionComponent implements OnInit {
	title: string = 'Luviam Catering';
	public _menuItems: MenuItem[];
	constructor(cookieService: CookieServiceHelper,
		private router: Router,
		private adminAG: AdminAuthGuard,
		private gerenciaAG: GerenciaAuthGuard,
		private comprasAG: ComprasAuthGuard,
		private ativoAG: AdministracionAuthGuard,
		private gestorPrecios: GestorPreciosAG,
		private ventasAG: VentasAuthGuar) {
		super();
		let windwUrl = window["cambioClave"];
		console.log(windwUrl);

		if (!this.isUserLogged() && windwUrl.indexOf("public/") < 0) {
			this.router.navigate(["login"]);
		}
	}

	get menuItems() {
		return this._menuItems && this._menuItems.length > 0 ? this._menuItems : this.generarMenu();
	}
	ngOnInit() {
		this.generarMenu();

	}

	private generarMenu() {
		this._menuItems = [{
			label: 'Admin. Maestros',
			visible: this.adminAG.esVisible(),
			items: [

				{ label: 'Gestión Plan de Cuentas', routerLink: ['admin/gestion-planCuentas'] },
				{ label: 'Gestionar Centros', routerLink: ['admin/lista-centros'] },
				{ label: 'Salones', routerLink: ['salones/lista-salones'] },
				{ label: 'Parametros', routerLink: ['admin/parametricos'] },
				{ label: 'Productos', routerLink: ['productos/lista-productos'] },




			]
		},
		{
			label: 'Precios',
			visible: this.gestorPrecios.esVisible(),
			items: [

				{ label: 'Precios', icon: 'fa fa-dollar', routerLink: ['admin/precios/home'] },

			]
		},
		{
			label: 'Reportes',
			visible: this.gerenciaAG.esVisible(),
			items: [

				{ label: 'Panel de Control', routerLink: ['admin/reportes/panel-control'] },
				{ label: 'Totales de Productos', routerLink: ['admin/reportes/totales-productos'] },
				{ label: 'Totales de Gastos', routerLink: ['admin/reportes/totales-gastos'] },

			]
		},
		{
			label: 'Caja',
			visible: this.ativoAG.esVisible(),
			items: [
				{ label: 'Ver caja', routerLink: ['caja/resumen'] },
				{ label: 'Lista de Asientos', routerLink: ['admin/lista-asientos'] },
				{ label: 'Asiento Manual', routerLink: ['admin/asiento-manual'] },
				{ label: 'Resumen Cuentas', routerLink: ['admin/balance-cuentas'] },
				{ label: 'Gestión de Cheques', routerLink: ['admin/chequeras'] }


			]
		},
		{
			label: 'Clientes',
			visible: this.ventasAG.esVisible(),
			items: [
				{ label: 'Nuevo', routerLink: ['clientes/nuevo'] },
				{ label: 'Ver todos', routerLink: ['clientes/lista-clientes'] },
				{ label: 'Nuevo Contrato', routerLink: ['clientes/alta-contrato'] },
				{ label: 'Contratos', routerLink: ['clientes/lista-contratos'] },
				{ label: 'Cobros', routerLink: ['clientes/lista-cobros'] },
			]
		},
		{
			label: 'Proveedores',
			visible: this.comprasAG.esVisible(),
			items: [
				{ label: 'Nuevo', routerLink: ['proveedores/nuevo'] },
				{ label: 'Ver todos', routerLink: ['proveedores/lista-proveedores'] },
				{ label: 'Facturas', routerLink: ['proveedores/facturas'] },
				{ label: 'Ordenes de Pago', routerLink: ['proveedores/ordenesPago'] }

			]
		},
		{
			label: 'Comisionistas',
			visible: this.adminAG.esVisible(),
			items: [
				{ label: 'Nuevo', routerLink: ['comisionistas/nuevo'] },
				{ label: 'Ver todos', routerLink: ['comisionistas/lista-comisionistas'] },
				{ label: 'Nueva Liquidación', routerLink: ['comisionistas/nueva-liquidacion'] },
				{ label: 'Ver Liquidaciones', routerLink: ['comisionistas/lista-liquidaciones'] },
			]
		},
		{
			label: 'Empleados',
			visible: this.adminAG.esVisible(),
			items: [
				{ label: 'Nuevo', routerLink: ['empleados/nuevo'] },
				{ label: 'Ver todos', routerLink: ['empleados/lista-empleados'] }

			]
		},

		];
		return this._menuItems;
	}
	logout() {
		this.clearCredentials();
		this._menuItems = [];
		this.router.navigate(["login"]);
	}
	get username() {
		return this.getUserFullName();
	}
}
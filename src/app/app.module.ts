import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
//External modules and providers
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AngularFittextModule } from 'angular-fittext';
import { environment } from 'environments/environment';
import { DragulaModule } from 'ng2-dragula';
import { CookieModule, CookieOptionsProvider, CookieService } from 'ngx-cookie';
//Internal Modules
import { AppRoutingModule } from './app-routing.module';
//Components
import { AppComponent } from './app.component';
import { AsientoManualComponent } from './asiento-manual/asiento-manual.component';
import { AsientoService } from './asiento-manual/service/asientoService.service';
import { AdminAuthGuard } from './authguards/AdminAuthGuard';
import { AdministracionAuthGuard } from './authguards/AdministracionAuthGuard';
import { AdminPreciosAG } from './authguards/AdminPreciosAG';
import { ComprasAuthGuard } from './authguards/ComprasAuthGuard';
import { GerenciaAuthGuard } from './authguards/GerenciaAuthGuard';
import { GestorPreciosAG } from './authguards/GestorPreciosAG';
import { VentasAuthGuar } from './authguards/VentasAuthGuard';
import { BalanceCuentasComponent } from './balance-cuentas/balance-cuentas.component';
import { CajaComponent } from './caja/caja.component';
import { CajaService } from './caja/service/cajaService.service';
import { GestionChequeComponent } from './cheque/gestion-cheque/gestion-cheque.component';
import { GestionChequeraComponent } from './cheque/gestion-chequera/gestion-chequera.component';
import { ListaChequeraComponent } from './cheque/lista-chequera/lista-chequera.component';
import { ChequesPorTipoPipe } from './cheque/pipes/cheques.pipes';
import { ChequeService } from './cheque/service/cheque.service';
import { ChequeraService } from './cheque/service/chequera.service';
import { GestionOrdenCobranzaComponent } from './clientes/cobros/gestion/gestion-oc.component';
import { ListaOrdenCobranzasComponent } from './clientes/cobros/listado/ordenes-cobranza.component';
import { OrdenCobranzaService } from './clientes/cobros/service/OrdenCobranzaService.service';
import { GestionClienteComponent } from './clientes/gestion-cliente/gestion-cliente.component';
import { GestionContactoComponent } from './clientes/gestion-contacto/gestion-contacto.component';
import { ListaClientesComponent } from './clientes/lista-clientes/lista-clientes.component';
import { ClienteService } from './clientes/service/ClienteService';
import { VistaClienteComponent } from './clientes/vista-cliente/vista-cliente.component';
import { AutosizeTextComponent } from './common-components/autosize-text/autosize-text.component';
import { DescriptivoLabelComponent } from './common-components/descriptivo-selector/descriptivo-label.component';
import { DescriptivoSelectorComponent } from './common-components/descriptivo-selector/descriptivo-selector.component';
import { FiltroComponent } from './common-components/filtro/filtro.component';
import { CentroCostoService } from './common-services/centroCostoService.service';
import { CookieServiceHelper } from './common-services/cookieServiceHelper.service';
import { CuentasService } from './common-services/cuentasService.service';
import { DescriptivosService } from './common-services/descriptivos.service';
import { PlanCuentaService } from './common-services/planCuentaService.service';
import { TokenService } from './common-services/token.service';
import { MessagesComponent } from './common-utils/messages/messages.component';
import { CentroByCodigo } from './common-utils/pipes/centroByCodigo.pipe';
import { SocialLinkComponent } from './common-utils/social-link/social-link.component';
import { AltaContratoComponent } from './contratos/alta-contrato/alta-contrato.component';
import { GestionComisionistaesComponent } from './contratos/comisionistas/gestion-comisionista/gestion-comisionista.component';
import { ListaComisionistasComponent } from './contratos/comisionistas/lista-comisionistas/lista-comisionistas.component';
import { ContratoDatosCabeceraComponent } from './contratos/contrato-datos-cabecera/contrato-datos-cabecera.component';
import { ContratoDatosEventoComponent } from './contratos/contrato-datos-evento/contrato-datos-evento.component';
import { ContratoDetalleComisionesComponent } from './contratos/contrato-detalle-comisionies/contrato-detalle-comisiones.component';
import { ContratoDetalleServiciosComponent } from './contratos/contrato-detalle-servicios/contrato-detalle-servicios.component';
import { ContratoVisualizacionComponent } from './contratos/contrato-visualizacion/contrato-visualizacion.component';
import { GestionLiquidacionComisionesComponent } from './contratos/liquidacion-comision/gestion/gestion-liquidacion.component';
import { ListaLiquidacionComisionesComponent } from './contratos/liquidacion-comision/listado/listado-liquidaciones.component';
import { LiquidacionComisionesService } from './contratos/liquidacion-comision/service/LiquidacionComisionesService.service';
import { ListaContratoComponent } from './contratos/lista-contrato/lista-contrato.component';
import { ComisionistaService } from './contratos/service/comisionista.service';
import { ContratosService } from './contratos/service/contratos.service';
import { GestionEmpleadosComponent } from './empleados/gestion-empleados/gestion-empleados.component';
import { ListaEmpleadosComponent } from './empleados/lista-empleados/lista-empleados.component';
import { EmpleadosService } from './empleados/service/EmpleadosService';
import { EstadoCuentaComponent } from './estado-cuenta/estado-cuenta.component';
import { GestionPlanDeCuentasComponent } from './gestion-planDeCuentas/gestion-planDeCuentas.component';
import { GestionarCentroCostoComponent } from './gestionar-centro-costo/gestionar-centro-costo.component';
import { GlobalInjector } from './GlobalInjector';
import { HomeComponent } from './home/home.component';
import { HomeService } from './home/service/home.service';
import { ListaAsientosComponent } from './lista-asientos/lista-asientos.component';
import { ListaCentrosCostoComponent } from './lista-centros-costo/lista-centros-costo.component';
import { LoadingPanelComponent } from './loading-panel/loading-panel.component';
import { LoginComponent } from './login/login.component';
//Providers
import { LoginService } from './login/service/login.service';
import { NgBootstrapModule } from './ng-bootstrap.module';
import { GestionAgrupadorComponent } from './parametricos/agrupador/gestion-agrupador/gestion-agrupador.component';
import { ListaAgrupadorComponent } from './parametricos/agrupador/lista-agrupador/lista-agrupador.component';
import { AgrupadorService } from './parametricos/agrupador/service/agrupador.service';
import { GestionCategoriaComponent } from './parametricos/categoria/gestion-categoria/gestion-categoria.component';
import { ListaCategoriaComponent } from './parametricos/categoria/lista-categoria/lista-categoria.component';
import { CategoriaService } from './parametricos/categoria/service/categoria.service';
import { GestionEscalaComisionesComponent } from './parametricos/escalas-comision/gestion-escala-comision/gestion-escala-comisiones.component';
import { ListaEscalaComisionesComponent } from './parametricos/escalas-comision/lista-escala-comision/lista-escala-comisiones.component';
import { EscalaComisionesService } from './parametricos/escalas-comision/service/escala-comisiones.service';
import { GestionGrupoProductoComponent } from './parametricos/grupo-producto/gestion-grupo-producto/gestion-grupo-producto.component';
import { ListaGrupoProductoComponent } from './parametricos/grupo-producto/lista-grupo-producto/lista-grupo-producto.component';
import { GrupoProductoService } from './parametricos/grupo-producto/service/grupo-producto.service';
import { GestionListaPrecioComponent } from './parametricos/lista-precios/gestion-lista-precios/gestion-lista-precios.component';
import { ListaListaPrecioComponent } from './parametricos/lista-precios/lista-lista-precios/lista-lista-precios.component';
import { ListaPrecioService } from './parametricos/lista-precios/service/lista-precios.service';
import { GestionMarcaComponent } from './parametricos/marca/gestion-marca/gestion-marca.component';
import { ListaMarcaComponent } from './parametricos/marca/lista-marca/lista-marca.component';
import { MarcaService } from './parametricos/marca/service/marca.service';
import { GestionParametricoComponent } from './parametricos/parametrico/gestion-parametrico/gestion-parametrico.component';
import { ListaParametricoComponent } from './parametricos/parametrico/lista-parametrico/lista-parametrico.component';
import { ParametrosComponent } from './parametricos/parametros/parametros.component';
import { ParametricoService } from './parametricos/services/parametricos.service';
import { GestionTipoComisionistaComponent } from './parametricos/tipo-comisionista/gestion-tipo-comisionista/gestion-tipo-comisionista.component';
import { ListaTipoComisionistaComponent } from './parametricos/tipo-comisionista/lista-tipo-comisionista/lista-tipo-comisionista.component';
import { TipoComisionistaService } from './parametricos/tipo-comisionista/service/tipo-comisionista.service';
import { GestionTipoEventoComponent } from './parametricos/tipo-evento/gestion-tipo-evento/gestion-tipo-evento.component';
import { ListaTipoEventoComponent } from './parametricos/tipo-evento/lista-tipo-evento/lista-tipo-evento.component';
import { PrecioFiltradoPipe } from './pipes/precios-filtrados.pipe';
import { ProductoCategoriaPipe } from './pipes/producto.pipe';
import { SanitizeHtmlPipe } from './pipes/SanitizeHtmlPipe.pipe';
import { TipoAlquilerPipe } from './pipes/tipo-alquiler.pipe';
import { ConflictosAdicionalesComponent } from './precios/adicionales/conflictos/conflictos-adicionales.component';
import { GestionPrecioAdicionalesComponent } from './precios/adicionales/gestion-precio-adicionales/gestion-precio-adicionales.component';
import { ImpresionListaAdicionalessComponent } from './precios/adicionales/impresion-lista-adicionales/impresion-lista-adicionales.component';
import { NuevoPrecioAdicionalesComponent } from './precios/adicionales/nuevo-precio-adicionales/nuevo-precio-adicionales.component';
import { PreciosAdicionalesComponent } from './precios/adicionales/precios-adicionales/precios-adicionales.component';
import { ConflictosBebidasComponent } from './precios/bebidas/conflictos/conflictos-bebidas.component';
import { GestionPrecioBebidaComponent } from './precios/bebidas/gestion-precio-bebida/gestion-precio-bebida.component';
import { ImpresionListaBebidasComponent } from './precios/bebidas/impresion-lista-bebidas/impresion-lista-bebidas.component';
import { NuevoPrecioBebidaComponent } from './precios/bebidas/nuevo-precio-bebida/nuevo-precio-bebida.component';
import { PreciosBebidasComponent } from './precios/bebidas/precios-bebidas/precios-bebidas.component';
import { HomeListadosPreciosComponent } from './precios/home-listados-precios/home-listados-precios.component';
import { PreciosAdicionalesService } from './precios/precios-adicionales.service';
import { PreciosAlquilerService } from './precios/precios-alquiler.service';
import { ConflictosAlquilerComponent } from './precios/precios-alquiler/conflictos-alquiler/conflictos-alquiler.component';
import { ImpresionAlquilerComponent } from './precios/precios-alquiler/impresion-alquiler/impresion-alquiler.component';
import { ListadoPreciosAlquilerComponent } from './precios/precios-alquiler/listado-precios-alquiler/listado-precios-alquiler.component';
import { NuevoPrecioAlquilerComponent } from './precios/precios-alquiler/nuevo-precio-alquiler/nuevo-precio-alquiler.component';
import { PreciosBebidasService } from './precios/precios-bebidas.service';
import { PreciosMenuService } from './precios/precios-menu.service';
import { ConflictosMenuComponent } from './precios/precios-menu/conflictos-menu/conflictos-menu.component';
import { ImpresionMenuComponent } from './precios/precios-menu/impresion-menu/impresion-menu.component';
import { ListadoPreciosMenuComponent } from './precios/precios-menu/listado-precios-menu/listado-precios-menu.component';
import { NuevoPrecioMenuComponent } from './precios/precios-menu/nuevo-precio-menu/nuevo-precio-menu.component';
import { VistaListadosComponent } from './precios/vita-listados/vista-listados.component';
import { PrimeNgModule } from './prime-ng.module';
import { GestionarProductoComponent } from './producto/gestionar-productos/gestionar-producto.component';
import { ListaProductosComponent } from './producto/lista-productos/lista-productos.component';
import { ProductoService } from './producto/service/producto.service';
import { GestionFacturaComponent } from './proveedores/facturas/gestion/gestion-factura.component';
import { FacturasComponent } from './proveedores/facturas/listado/facturas.component';
import { FacturaService } from './proveedores/facturas/service/FacturaService.service';
import { GestionProveedoresComponent } from './proveedores/gestion-proveedores/gestion-proveedores.component';
import { ListaProveedoresComponent } from './proveedores/lista-proveedores/lista-proveedores.component';
import { GestionOrdenPagoComponent } from './proveedores/ordenPago/gestion/gestion-op.component';
import { OrdenPagosComponent } from './proveedores/ordenPago/listado/ordenes-pago.component';
import { OrdenPagoService } from './proveedores/ordenPago/service/OrdenPagoService.service';
import { ProveedoresService } from './proveedores/service/ProveedoresService';
import { CambiarPasswordComponent } from './recuperar-password/cambiar-password/cambiar-password.component';
import { OlvidoPasswordComponent } from './recuperar-password/olvido-password/olvido-password.component';
import { CubiertoPromedioComponent } from './reportes/indicadores/grafico/cubierto-promedio/cubierto-promedio.component';
import { GraficoComponent } from './reportes/indicadores/grafico/grafico.component';
import { TotalGastoComponent } from './reportes/indicadores/grafico/total-gasto/total-gasto.component';
import { IndicadoresComponent } from './reportes/indicadores/indicadores.component';
import { PanelControlComponent } from './reportes/panel-control/panel-control.component';
import { IndicadoresService } from './reportes/services/indicadores.service';
import { ReporteService } from './reportes/services/reportes.service';
import { TotalesGastosComponent } from './reportes/totales-gastos/totales-gastos.component';
import { TotalesProductoComponent } from './reportes/totales-producto/totales-producto.component';
import { GestionarSalonesComponent } from './salon/gestionar-salones/gestionar-salones.component';
import { ListaSalonesComponent } from './salon/lista-salones/lista-salones.component';
import { SelectorSalonComponent } from './salon/selector-salon/selector-salon.component';
import { SalonService } from './salon/service/salon.service';











export function jwtOptionsFactory(tokenService) {
  return {
    tokenGetter: () => {
      return tokenService.getAsyncToken();
    },
    whitelistedDomains: environment.whitelistedDomains,
    authScheme: ''
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    LoadingPanelComponent,
    DescriptivoSelectorComponent,
    DescriptivoLabelComponent,
    OlvidoPasswordComponent,
    CambiarPasswordComponent,
    GestionarCentroCostoComponent,
    ListaCentrosCostoComponent,
    AsientoManualComponent,
    CentroByCodigo,
    ListaAsientosComponent,
    CajaComponent,
    GestionClienteComponent,
    ListaClientesComponent,
    GestionProveedoresComponent,
    ListaProveedoresComponent,
    BalanceCuentasComponent,
    GestionPlanDeCuentasComponent,
    GestionChequeraComponent,
    ListaChequeraComponent,
    FacturasComponent,
    GestionFacturaComponent,
    OrdenPagosComponent,
    GestionOrdenPagoComponent,
    GestionChequeComponent,
    ChequesPorTipoPipe,
    ListaEmpleadosComponent,
    GestionEmpleadosComponent,
    AltaContratoComponent,
    DescriptivoLabelComponent,
    DescriptivoSelectorComponent,
    ProductoCategoriaPipe,
    SanitizeHtmlPipe,
    PrecioFiltradoPipe,
    ListaSalonesComponent,
    GestionarSalonesComponent,
    ListaProductosComponent,
    ContratoDatosCabeceraComponent,
    VistaClienteComponent,
    ContratoDatosEventoComponent,
    ContratoDetalleServiciosComponent,
    ContratoVisualizacionComponent,
    ListaContratoComponent,
    EstadoCuentaComponent,
    SelectorSalonComponent,
    MessagesComponent,
    SocialLinkComponent,
    GestionarProductoComponent,
    ListaTipoEventoComponent,
    GestionTipoEventoComponent,
    ListaCategoriaComponent,
    GestionCategoriaComponent,
    ParametrosComponent,
    GestionContactoComponent,
    GestionOrdenCobranzaComponent,
    ListaOrdenCobranzasComponent,
    TotalesProductoComponent,
    IndicadoresComponent,
    GraficoComponent,
    CubiertoPromedioComponent,
    PanelControlComponent,
    CubiertoPromedioComponent,
    ListaTipoComisionistaComponent,
    GestionTipoComisionistaComponent,
    GestionParametricoComponent,
    ListaParametricoComponent,
    TotalesProductoComponent,
    ListaComisionistasComponent,
    GestionComisionistaesComponent,
    ContratoDetalleComisionesComponent,
    GestionLiquidacionComisionesComponent,
    ListaLiquidacionComisionesComponent,
    ListaEscalaComisionesComponent,
    GestionEscalaComisionesComponent,
    TotalesGastosComponent,
    AutosizeTextComponent,
    TotalGastoComponent,
    HomeListadosPreciosComponent,
    PreciosBebidasComponent,
    GestionPrecioBebidaComponent,
    NuevoPrecioBebidaComponent,
    ListaMarcaComponent,
    GestionMarcaComponent,
    ConflictosAdicionalesComponent,
    ConflictosBebidasComponent,
    FiltroComponent,
    ImpresionListaBebidasComponent,
    GestionGrupoProductoComponent,
    ListaGrupoProductoComponent,
    ListaListaPrecioComponent,
    GestionListaPrecioComponent,
    GestionPrecioAdicionalesComponent,
    PreciosAdicionalesComponent,
    ImpresionListaAdicionalessComponent,
    NuevoPrecioAdicionalesComponent,
    VistaListadosComponent,
    ListadoPreciosMenuComponent,
    NuevoPrecioMenuComponent,
    ListaAgrupadorComponent,
    GestionAgrupadorComponent,
    ImpresionMenuComponent,
    ConflictosMenuComponent,
    ListadoPreciosAlquilerComponent,
    NuevoPrecioAlquilerComponent,
    ConflictosAlquilerComponent,
    ImpresionAlquilerComponent,
    TipoAlquilerPipe





  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    PrimeNgModule,
    NgBootstrapModule,
    CookieModule.forRoot(),
    HttpClientModule,
    DragulaModule.forRoot(),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [TokenService]
      }
    }),
    HttpClientModule,
    AngularFittextModule
  ],
  providers: [
    GerenciaAuthGuard,
    VentasAuthGuar,
    AdministracionAuthGuard,
    AdminAuthGuard,
    ComprasAuthGuard,
    GestorPreciosAG,
    LoginService,
    HomeService,
    DescriptivosService,
    CentroCostoService,
    AsientoService,
    CuentasService,
    ClienteService,
    ProveedoresService,
    PlanCuentaService,
    CajaService,
    ChequeraService,
    ChequeService,
    FacturaService,
    OrdenPagoService,
    EmpleadosService,
    SalonService,
    ProductoService,
    HttpModule,
    CookieServiceHelper,
    CookieService,
    CookieOptionsProvider,
    TokenService,
    ContratosService,
    ParametricoService,
    OrdenCobranzaService,
    ReporteService,
    IndicadoresService,
    CategoriaService,
    TipoComisionistaService,
    ReporteService,
    ComisionistaService,
    LiquidacionComisionesService,
    EscalaComisionesService,
    PreciosBebidasService,
    MarcaService,
    GrupoProductoService,
    ListaPrecioService,
    PreciosAdicionalesService,
    AgrupadorService,
    PreciosMenuService,
    PreciosAlquilerService,
    AdminPreciosAG



  ],
  bootstrap: [AppComponent]
})


export class AppModule {
  constructor(private injector: Injector) {
    GlobalInjector.InjectorInstance = this.injector;
  }
}

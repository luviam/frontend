import { AutoComplete } from 'primeng/primeng';
import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { AsientoService } from './service/asientoService.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { DescriptivosService } from '../common-services/descriptivos.service';
import { Seleccionable } from '../model/Seleccionable';
import { CentroCostoService } from '../common-services/centroCostoService.service';
import { CentroCosto } from '../model/CentroCosto';
import { CuentaConfig } from '../model/ConfigCuenta';
import { Descriptivo } from '../model/Descriptivo';
import { OperacionContable } from '../model/OperacionContable';
import { AsientoContable } from '../model/AsientoContable';
import { SessionComponent } from '../session-component.component';
import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';

@Component({
	selector: 'asiento-manual',
	templateUrl: 'asiento-manual.component.html',
	styleUrls: ['./asiento-manual.component.less'],
})

export class AsientoManualComponent extends SessionComponent implements OnInit {
	public titulo: string = "Asiento Manual";
	public asiento: AsientoContable = new AsientoContable(null, null, null,
		this.getCurrentUser().user.nombre + " " + this.getCurrentUser().user.apellido, new Date());
	public centros: CentroCosto[] = [];
	public centrosSugeridos: CentroCosto[] = [];

	public cuentasSugeridas  : Descriptivo[] = [];
	public editable: boolean = false;
	public finalizado: boolean = false;
	constructor(cookieService :CookieServiceHelper,
		private descriptivosService: DescriptivosService,
		private centroCostro: CentroCostoService,
		private confirmationService: ConfirmationService,
		private asientoService: AsientoService,
		private router: Router,
		private route: ActivatedRoute) {
		super();
	}

	@ViewChildren('autoFocus') items: QueryList<AutoComplete>;

	ngOnInit() {
		this.finalizado = false;

		this.addLoadingCount();
		this.centroCostro.getAllCentros().then((res) => {
			this.centros = res;
			this.susLoadingCount();
			this.route.params.subscribe(params => {

				let numeroAsiento: number = <number>params['numeroAsiento'];
				let $this = this;
				if (numeroAsiento) {
					$this.titulo = "Asiento - N° ";
					$this.addLoadingCount();
					$this.asientoService.findByNumero(numeroAsiento).then((asiento) => {
						$this.susLoadingCount();
						$this.asiento = asiento;
						$this.titulo += asiento.numeroAsiento + "";
						$this.asiento.operaciones.forEach(o =>{
							if(o.debe)	o.debe = Number(o.debe.toFixed(2));
							if(o.haber) o.haber = Number(o.haber.toFixed(2));
						})
						$this.editable = true;
					}).catch($this.errorHandler)

				} else {
					$this.editable = true;
					$this.asiento = new AsientoContable(null, null, null,
						$this.getCurrentUser().user.nombre + " " + $this.getCurrentUser().user.apellido, new Date());
				}
			});
		}).catch(this.errorHandler)




	}
	public getCuentas(codigo: string) {
		if (!this.centros || this.centros.length === 0) {
			return [];
		}
		let centro = this.centros.filter(c => c.codigo === codigo)[0];
		return centro ? centro.cuentas : [];
	}
	public cambioCentro(event: any, operacion: OperacionContable) {
		operacion.cuenta = null;
	}
	public agregarNuevaOperacion() {
		this.asiento.operaciones.push(new OperacionContable());
		let $this = this;
		setTimeout(()=>{
			$this.items.last.inputEL.nativeElement.focus();
		},0)
		
	}
	public balanceValido() {

		let state : boolean = true;
		if(!this.asiento){
			this.error("ERROR INTERNO");
			return false;
		}
		if(this.asiento.operaciones.length <= 1){
			state = false;
			this.error("Ingrese más de una operación");
		}
		if(!this.asiento.isCentrosBalanceados()){
			state = false;
			this.error("El balance de cuentas de cada centro debe ser 0");
		}	
		if(this.asiento.diferencia !== 0){
			state = false;
			this.error("El balance del asiento debe ser 0");
		}
		if(!this.sinOperacionesNulas()){
			state = false;
			this.error("No debe haber operaciones sin valor");
		}
		if(!this.operacionCompleta()){
			state = false;
			this.error("Revise las operaciones indicadas y verifique que todas estén completas.");
		}
		return state;
		
	}
	public operacionCompleta() {
		return !this.asiento.operaciones.some(op => !op.centroCosto || !op.cuenta || !op.descripcion);
	}
	public sinOperacionesNulas() {
		return !this.asiento.operaciones.some(op => op.debe === 0 && op.haber === 0);
	}
	
	public guardarAsiento() {
		this.addLoadingCount();
		let $this = this;
		if (this.balanceValido()) {
			this.asiento.estado = new Descriptivo("P", "Pendiente");
			this.asiento.operaciones.forEach(o =>{
				if(o.debe) o.debe = Number(o.debe.toFixed(2));
				if(o.haber) o.haber = Number(o.haber.toFixed(2));
			})
			this.asientoService.guardar(this.asiento).then((res) => {
				$this.success('Se registró el asiento exitosamente.' );
				setTimeout(() => {
					$this.router.navigate(["admin/lista-asientos"]);
				}, 3000)
				this.finalizado = true;
				this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.susLoadingCount();
		}
	}

	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Asiento",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {

				this.router.navigate(['admin/lista-asientos']);
			}
		});
	}
	public keyPressHandler(event: any) {
		if (event.charCode === 13 && this.editable) {
			this.guardarAsiento();
			event.preventDefault();
		}else if( event.charCode === 43){
			this.agregarNuevaOperacion();
			event.preventDefault();
			

		}
	}

	public filtrarCentros(event: any) {
		this.centrosSugeridos = this.centros.filter(c => c.contieneTexto(event.query));
	}

	public filtrarCuentas(op :OperacionContable, event: any) {
		let cuentas = this.getCuentas(op.centroCosto.codigo);
		if(!cuentas || cuentas.length === 0){
			this.cuentasSugeridas = [];
		}else{
			this.cuentasSugeridas = cuentas.filter(c => c.codigo.toUpperCase().includes(event.query.toUpperCase()) ||
														c.descripcion.toUpperCase().includes(event.query.toUpperCase())	)
		}
		
	}
	

	public autoselectCuenta(op: OperacionContable) {
		if (!op.cuenta || !op.cuenta.codigo) {

			if (this.cuentasSugeridas.length === 1) {
				op.cuenta = this.cuentasSugeridas[0];
			} else {
				op.cuenta = null;
			}

		}
	}
	public autoselectCentro(op: OperacionContable) {
		if (!op.centroCosto || !op.centroCosto.codigo) {

			if (this.centrosSugeridos.length === 1) {
				op.centroCosto = this.centrosSugeridos[0];
			} else {
				op.centroCosto = null;
			}

		}
	}
}
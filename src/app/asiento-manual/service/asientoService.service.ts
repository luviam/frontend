import { RequestOptions } from '@angular/http';
import { CentroCosto } from '../../model/CentroCosto';
import { OperacionContable } from '../../model/OperacionContable';
import { Descriptivo } from '../../model/Descriptivo';
import { AsientoContable } from '../../model/AsientoContable';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';


@Injectable()
export class AsientoService extends ServicioAbstract{
  
    
    public getAllCabeceras(filtro : any): Promise<any>{
        let desde = moment(filtro.fechaDesde).format("YYYYMMDD HH:mm")
        let hasta = moment(filtro.fechaHasta).format("YYYYMMDD HH:mm")
        if(filtro.fechaDesde > filtro.fechaHasta){
            return Promise.reject({"mensaje":"Fecha Desde debe ser menor a la Fecha Hasta"});
        }
        return this.http.get(this.getApiURL() + 'asientos/cabeceras/' + desde + "/" + hasta).toPromise().then(this.parseCabeceras, this.handleError);

    }

    public parseCabeceras(respuesta : any) : AsientoContable[]{
        let res= respuesta;
        let asientos : any[] = res.respuesta? res.respuesta : [];
        
        return asientos.map(a => new AsientoContable(a.id,a.numeroAsiento,a.descripcion,a.responsable,new Date(a.fecha),new Descriptivo(a.estado,a.estado)));
    }
    public findByNumero(numero : number) :Promise<any>{
        return this.http.get(this.getApiURL() + 'asientos/' +numero).toPromise().then(this.parseAsiento, this.handleError);
    
    }

    public parseAsiento(respuesta : any) : AsientoContable{
        let res= respuesta;
        let a : any = res.respuesta? res.respuesta : null;
        if(!a){
            return null;
        }
       let asiento : AsientoContable =  new AsientoContable(a.id,
                a.numeroAsiento,
                a.descripcion,
                a.responsable,
                new Date(a.fecha),new Descriptivo(a.estado,a.estado),
        
        a.operaciones.map(o => new OperacionContable(o.id,
                            new Descriptivo(o.centroCosto.codigo, o.centroCosto.descripcion),
                            new Descriptivo(o.cuenta.codigo, o.cuenta.descripcion),
                            o.descripcion,o.debe,o.haber,o.comprobante,new Date(o.fecha),
                            o.responsable,
                            o.estado? new Descriptivo(o.estado.codigo,o.estado.descripcion) : new Descriptivo("P","Pendiente"),
                            o.numeroAsiento
                        )));
                        return asiento;
    }

    public delete(asiento : AsientoContable) : Promise<any>{
        let params = new HttpParams();
       params = params.append('id', asiento.id+"")
        return this.http.delete(this.getApiURL() + 'asientos',  {params}).toPromise().then(this.handleOk, this.handleError);
    }
    public guardar(asiento : AsientoContable) : Promise<any>{
        
        let asientoAux: AsientoContable = asiento;
        if (asientoAux.id) {
            return this.http.post(this.getApiURL() + 'asientos', asientoAux).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'asientos', asientoAux).toPromise().then(this.handleOk, this.handleError);
        }
    }
    
   

    
}
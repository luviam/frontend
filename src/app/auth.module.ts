import { CookieService } from 'ngx-cookie';
import { Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { Http, RequestOptions, Response } from '@angular/http';
import { AppUser } from "./model/AppUser";
import { environment } from "environments/environment";

const API_URL = environment.apiUrl;
@NgModule({
	
})
export class AuthModule { }
export function getJwtHttp(http: Http, options: RequestOptions,
	cookieService: CookieService, router: Router) {
	let jwtOptions = {
		endPoint: API_URL + 'refresh-token',
		payload: { token: "refresh-token" },
		beforeSeconds: 3600,
		tokenName: 'token',
		refreshTokenGetter: (() => (<AppUser>cookieService.getObject('luviamUser')).token),
		tokenSetter: ((resp: Response): boolean | Promise<void> => {
			let res = resp;
				
			if (!res['token']) {
				cookieService.remove('luviamUser');
				console.log("Se ha perdido la sesion");
				router.navigate(["/login"]);
				return false;
			}
			let currentUser: AppUser = <AppUser>cookieService.getObject('luviamUser')
			currentUser.token = res['token'];
			cookieService.remove('luviamUser');
			cookieService.putObject('luviamUser', currentUser);

			return true;
		})
	};
	
}
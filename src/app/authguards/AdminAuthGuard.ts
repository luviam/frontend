import { AdministracionAuthGuard } from './AdministracionAuthGuard';
import { AppUser } from '../model/AppUser';
import { Usuario } from '../model/Usuario';
import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../session-component.component';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie'; 
import { RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate } from '@angular/router';


@Injectable()
export class AdminAuthGuard extends SessionComponent implements CanActivate {
    
    constructor( private router: Router, ) {
        super();
    }
    public getRoles() : string[] { return []};

    public esVisible() : boolean{
        let currentUser: AppUser = this.getCurrentUser();
        if (currentUser) {
            let user: Usuario = new Usuario(currentUser.user);
            let valid:boolean = user.tieneRol(["ROLE_ADMIN"].concat(this.getRoles()));
            return valid;
            
        }
    }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {

        let currentUser: AppUser = this.getCurrentUser();
        if (currentUser) {
            let user: Usuario = new Usuario(currentUser.user);
            let valid:boolean = user.tieneRol(["ROLE_ADMIN"].concat(this.getRoles()));

            if (!valid){
                this.router.navigate([""]);
            }

            return valid;
            
        }

        return false;
    }

}
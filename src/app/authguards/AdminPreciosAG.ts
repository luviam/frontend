import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AdminAuthGuard } from './AdminAuthGuard';


@Injectable()
export class AdminPreciosAG extends AdminAuthGuard {

    constructor(router: Router, ) {
        super(router);
    }

    public getRoles(): string[] {
        return ["ROLE_ADMIN_PRECIOS"];
    }

}
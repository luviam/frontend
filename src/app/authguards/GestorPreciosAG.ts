import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AdminPreciosAG } from './AdminPreciosAG';


@Injectable()
export class GestorPreciosAG extends AdminPreciosAG {

    constructor(router: Router, ) {
        super(router);
    }

    public getRoles(): string[] {
        return ["ROLE_GESTOR_PRECIOS"];
    }

}
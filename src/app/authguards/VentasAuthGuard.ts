import { AppUser } from '../model/AppUser';
import { Usuario } from '../model/Usuario';
import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../session-component.component';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie'; 
import { RouterStateSnapshot, ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { AdminAuthGuard } from './AdminAuthGuard';


@Injectable()
export class VentasAuthGuar extends AdminAuthGuard{
    
    constructor( router: Router, ) {
        super(router);
    }

    public getRoles(): string[]{
        return ["ROLE_VENTAS"];
    }

}
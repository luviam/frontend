import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { CentroCostoService } from '../common-services/centroCostoService.service';
import { CentroCosto } from '../model/CentroCosto';
import { CuentasService } from '../common-services/cuentasService.service';
import { CuentaTotal } from '../model/CuentaTotal';
import { SessionComponent } from '../session-component.component';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'balance-cuentas',
	templateUrl: 'balance-cuentas.component.html',
	styleUrls: ['./balance-cuentas.component.less'],
})

export class BalanceCuentasComponent extends SessionComponent {
	public titulo : string = "Resumen de Cuentas";
	public cuentas : CuentaTotal[] = [];
	public centros: CentroCosto[]= [];
	public filtro: any = { "fechaDesde": new Date(), "fechaHasta": new Date() , "codigoCuenta":null};
	constructor(cookieService :CookieServiceHelper,
		private cuentasService : CuentasService, private centroCostoService : CentroCostoService){
		super();
		
	}
	ngOnInit() {
		let $this = this;
		
		this.filtro.fechaDesde = moment().subtract(1, 'months').startOf('day').toDate();
		this.filtro.fechaHasta = moment().add(1,'days').startOf('day').toDate();
		this.cuentas = [];
		this.addLoadingCount();
		this.centroCostoService.getAllCentros().then((res) => {
			$this.centros = res;
			$this.centros.push(new CentroCosto(-1,"GRAL","General",[]));
			$this.susLoadingCount();
			
		}).catch(this.errorHandler)

	 }
	 get fechaMaxima() {
		var d = new Date();
		d.setHours(23, 59, 59, 0);
		return d;
	}

	public buscarBalance(){
		let $this = this;
		this.addLoadingCount();
		this.cuentasService.getTotales(this.filtro).then((res)=>{
			$this.cuentas = res;
			
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	 public nodeExpand(event : any){
		let $this = this;
		if(event.node){
			if(event.node.esCuentaAplicacion){
				this.addLoadingCount();
				this.filtro.cuenta = event.node;
				this.cuentasService.getOperaciones(this.filtro).then((res)=>{
					event.node.cuentasHijas  = res.map(o => {return new CuentaTotal(o.cuenta.id, o.cuenta.codigo,o.descripcion,true,o.debe,o.haber,null,o.fecha,true)});
					$this.susLoadingCount();
				}).catch(this.errorHandler);
			}
		}
	}
}
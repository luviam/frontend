import { Component } from '@angular/core';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { MenuItem } from 'primeng/primeng';
import { CuentasService } from '../common-services/cuentasService.service';
import { AltaTraspaso } from '../model/AltaTraspaso';
import { BasicEntity } from '../model/BasicEntity';
import { Descriptivo } from '../model/Descriptivo';
import { EstadoTraspaso } from '../model/EstadoTraspaso';
import { OperacionContable } from '../model/OperacionContable';
import { SessionComponent } from '../session-component.component';
import { CajaService } from './service/cajaService.service';


@Component({
	selector: 'caja',
	styleUrls: ['./caja.component.less'],
	templateUrl: 'caja.component.html'
})

export class CajaComponent extends SessionComponent {
	public operaciones: OperacionContable[] = [];
	public acciones: MenuItem[];
	public traspaso: AltaTraspaso = new AltaTraspaso();
	public displayTraspaso: boolean = false;
	public traspasos: EstadoTraspaso[] = [];
	public traspasoDestino: Descriptivo;

	public cajas: BasicEntity[] = [];
	public titulo: string = "Resumen de Caja"
	public filtro: any = { "fechaDesde": moment().subtract(20, 'days').toDate(), "fechaHasta": this.fechaMaxima, "cuenta": null, "descripcion": "" };

	public tituloTransaccion: string = "";
	public displayAperturaCierre: boolean = false;
	public esApertura: boolean = false;
	public montoCaja: number = 0;

	public estadoCajaId: number = 0;

	public saldoRealCaja: number = 0;

	constructor(
		private cuentasService: CuentasService, private cajaService: CajaService,
		private confirmationService: ConfirmationService) {
		super();
	}
	get fechaMaxima() {
		var d = new Date();
		d.setHours(23, 59, 59, 0);
		return d;
	}
	ngOnInit() {
		let $this = this;
		this.acciones = [{
			label: "", icon: "fa fa-bars", items: [{
				label: "Apertura de Caja",
				command: (event) => {
					this.esApertura = true;
					this.montoCaja = this.saldoRealCaja;
					this.tituloTransaccion = "Apertura de caja";
					this.displayAperturaCierre = true;
				}
			},
			{
				label: "Traspaso de Caja",
				command: (event) => {
					this.traspaso.idCajaOrigen = this.filtro.cuenta.codigo;
					this.traspaso.idCajaDestino = null;
					this.traspaso.monto = 0;
					this.traspaso.fecha = new Date();
					this.traspaso.descripcion = "";
					this.displayTraspaso = true;
				}
			},
			{
				label: "Cierre de Caja",
				command: (event) => {
					this.esApertura = false;
					this.montoCaja = this.saldoRealCaja;
					this.tituloTransaccion = "Cierre de caja";
					this.displayAperturaCierre = true;
				}
			}]
		}];

		this.addLoadingCount();
		this.cuentasService.getCallCajas().then((res) => {
			$this.cajas = res.map(r => new BasicEntity(r.id, r.id + "", r.centroCosto.codigo + " - " + r.nombre));
			$this.filtro.cuenta = $this.cajas[0];
			$this.buscarMovimientos();
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}

	public buscarMovimientos() {

		let $this = this;
		this.filtro.fechaDesde.setHours(0, 0, 0, 0);
		this.filtro.fechaHasta.setHours(23, 59, 59, 0);

		this.updateData();

	}

	public efectuarAperturaCierreCaja() {
		if (this.saldoRealCaja != 0 && this.saldoRealCaja != this.montoCaja) {
			this.confirmationService.confirm({
				header: "Confirmar Apertura/Cierre",
				message: 'La operación que usted esta por efectuar, registrará un asiento automático por la diferencia de saldo informada en caja. ¿Desea continuar?',
				accept: () => {

					this.confirmarAperturaCierreCaja();
				},
				reject: () => {
					this.montoCaja = this.saldoRealCaja;
				}

			});
		} else {
			this.confirmarAperturaCierreCaja();
		}
	}

	public confirmarAperturaCierreCaja() {
		const $this = this;
		this.addLoadingCount();
		let transaccionCaja: Promise<any> = undefined;
		if (this.esApertura) {
			transaccionCaja = this.cajaService.abrirCaja(this.filtro.cuenta.id, this.montoCaja)
		} else {
			transaccionCaja = this.cajaService.cerrarCaja(this.filtro.cuenta.id, this.montoCaja)
		}
		transaccionCaja.then(() => {
			this.susLoadingCount();
			$this.success($this.tituloTransaccion + " efectuado");
			this.displayAperturaCierre = false;
			$this.updateData();
		}).catch($this.errorHandler);
	}



	public updateData() {
		if (!this.filtro.cuenta) return;
		this.addLoadingCount();
		let $this = this;
		this.cuentasService.getOperaciones(this.filtro).then((res) => {
			this.addLoadingCount();

			$this.cuentasService.getSaldo($this.filtro.cuenta.codigo, this.filtro.fechaDesde).then((r) => {

				let saldo: number = 0;


				let data: OperacionContable[] = [new OperacionContable(-1, null, new Descriptivo("R", "Saldo a la Fecha"), "Saldo a la fecha", r, 0, null, this.filtro.fechaDesde, "sistema")];
				data = data.concat(res);
				data.forEach(o => {
					saldo = saldo + o.debe - o.haber;
					o.saldo = Number(saldo.toFixed(2));
				});
				$this.operaciones = [...data];
				$this.susLoadingCount();
			}).catch($this.errorHandler);
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.cajaService.getTraspasos(this.filtro.cuenta.codigo).then((res) => {
			$this.traspasos = res.filter(t => t.estado.codigo !== 'A');
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.cajaService.getEstadoCaja(this.filtro.cuenta.id).then((res) => {
			$this.estadoCajaId = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.cajaService.getSaldoCaja(this.filtro.cuenta.id).then((res) => {
			$this.saldoRealCaja = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	get cajasDestino() {
		if (!this.filtro.cuenta) return [];
		return this.cajas.filter(c => c.codigo != this.filtro.cuenta.codigo);
	}

	get saldoCaja() {
		let saldo: number = 0;

		this.operaciones.forEach(o => saldo += o.debe - o.haber);
		return saldo.toFixed(2);
	}

	public confirmarTraspaso(traspaso: EstadoTraspaso) {
		let $this = this;
		this.addLoadingCount();
		this.cajaService.confirmarTraspaso(traspaso).then((res) => {
			$this.susLoadingCount();
			$this.success("Traspaso Aceptado");
			$this.updateData();
		}).catch(this.errorHandler);
	}

	public rechazarTraspaso(traspaso: EstadoTraspaso) {
		let $this = this;
		if (traspaso.idCajaOrigen + "" === this.filtro.cuenta.codigo && traspaso.estado.codigo !== "A") {
			this.addLoadingCount();
			this.cajaService.cancelarTraspaso(traspaso).then((res) => {
				$this.susLoadingCount();
				$this.success("Traspaso Cancelado");
				$this.updateData();
			}).catch(this.errorHandler);
		} else {
			this.addLoadingCount();
			this.cajaService.rechazarTraspaso(traspaso).then((res) => {
				$this.susLoadingCount();
				$this.success("Traspaso Cancelado");
				$this.updateData();
			}).catch(this.errorHandler);
		}


	}

	public traspasoCaja() {

		let $this = this;
		if (this.traspasoValido()) {
			this.addLoadingCount();
			this.traspaso.idCajaDestino = Number(this.traspasoDestino.codigo);
			this.cajaService.generarTraspaso(this.traspaso).then((res) => {
				$this.displayTraspaso = false;
				$this.success("Traspaso registrado");
				$this.susLoadingCount();
				$this.buscarMovimientos();
			}).catch(this.errorHandler);
		}
	}
	public traspasoValido() {
		let noError: boolean = true;
		if (!this.traspasoDestino || !this.traspasoDestino.codigo) {
			this.error("Seleccione una caja de Destino");
			noError = false;
		}
		if (this.traspaso.monto <= 0) {
			noError = false; this.error("El Monto debe ser  mayor a 0");
		}
		if (!this.traspaso.fecha) {
			noError = false; this.error("La fecha no puede quedar vacia");
		}

		if (!this.traspaso.descripcion) {
			noError = false; this.error("Indique una Descripción");
		}

		return noError;
	}
}
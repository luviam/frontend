import { RequestOptions } from '@angular/http';
import { Descriptivo } from '../../model/Descriptivo';
import { EstadoTraspaso } from '../../model/EstadoTraspaso';
import { AltaTraspaso } from '../../model/AltaTraspaso';
import { BasicEntity } from '../../model/BasicEntity';
import { Cuenta } from '../../model/Cuenta';
import { ConvenioSalon } from '../../model/ConvenioSalon';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CajaService extends ServicioAbstract {

 

    public getTraspasos(idCuenta : number) : Promise<any>{
        return this.http.get(this.getApiURL() + "caja/traspasos/" + idCuenta)
        .toPromise()
        .then((response: any) => {
            let res = response;
            let cabeceras: BasicEntity[] = [];
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                cabeceras = res.respuesta.map(c => 
                    new EstadoTraspaso(c.id,
                                        c.descripcion,
                                        c.fechaOperacion ? new Date(c.fechaOperacion) : null,
                                        c.idCajaOrigen,
                                        c.origen,
                                        c.idCajaDestino,
                                        c.destino,
                                        c.responsable,
                                        new Descriptivo(c.estado.codigo,c.estado.descripcion),
                                        c.monto));
            }
            return cabeceras;

        }, this.handleError);
    }

    public rechazarTraspaso(traspaso : EstadoTraspaso): Promise<any>{
        
        return this.http.post(this.getApiURL() + 'caja/cancelarTraspaso',traspaso).toPromise()

    }

    public cancelarTraspaso(traspaso : EstadoTraspaso) : Promise<any>{
        let params = new HttpParams();
       params = params.append('id', traspaso.id+"")
        return this.http.delete(this.getApiURL() + "caja/traspaso", {params}).toPromise();
    }
    
    public confirmarTraspaso(traspaso : EstadoTraspaso): Promise<any>{
        
        return this.http.post(this.getApiURL() + 'caja/confirmarTraspaso',traspaso).toPromise();
        
    }

    public getEstadoCaja(cajaId : number): Promise<any>{
        
        return this.http.get(this.getApiURL() + 'caja/estado/'+cajaId).toPromise()
        .then((response: any) => {
            let res = response;
            return res.respuesta;
        }, this.handleError);;
        
    }


    public getSaldoCaja(cajaId : number): Promise<any>{
        
        return this.http.get(this.getApiURL() + 'caja/saldo/'+cajaId).toPromise()
        .then((response: any) => {
            let res = response;
            return res.respuesta;
        }, this.handleError);;
        
    }

    public abrirCaja(cajaId : number, monto:number): Promise<any>{
        
        return this.http.post(this.getApiURL() + 'caja/apertura/'+cajaId,{monto:monto}).toPromise()
        .then((response: any) => {
            let res = response;
            return res;
        }, this.handleError);;
        
    }

    public cerrarCaja(cajaId : number, monto:number): Promise<any>{
        
        return this.http.post(this.getApiURL() + 'caja/cierre/'+cajaId,{monto:monto}).toPromise()
        .then((response: any) => {
            let res = response;
            return res;
        }, this.handleError);
        
    }

    public generarTraspaso(traspaso :AltaTraspaso) : Promise<any>{
        return this.http.post(this.getApiURL() + 'caja/traspaso',traspaso).toPromise();
        
    }
}
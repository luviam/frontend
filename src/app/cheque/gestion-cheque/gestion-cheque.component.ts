import { Descriptivo } from '../../model/Descriptivo';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Pago } from '../../model/Pago';
import { ChequeraService } from '../service/chequera.service';
import { CuentaAplicacion } from '../../model/CuentaAplicacion';
import { ChequeService } from '../service/cheque.service';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';

import { SessionComponent } from '../../session-component.component';
import { Cheque } from '../../model/Cheque';
import { CuentasService } from '../../common-services/cuentasService.service';

@Component({
	selector: 'gestion-cheque',
	templateUrl: 'gestion-cheque.component.html',
	providers: [ChequeService],
	styleUrls: ['./gestion-cheque.component.less']
})

export class GestionChequeComponent extends SessionComponent implements OnInit {

	public estadoCheque : Descriptivo;
	constructor(
		private chequeService: ChequeService,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private router: Router,
		private cuentasService: CuentasService) {
		super();
	}

	public titulo: string = "Nuevo Cheque";
	public chequeEditado: Cheque;
	public cuentasBancarias: CuentaAplicacion[] = [];
	public tiposCheque: Descriptivo[] = [
		new Descriptivo("P", "Propios"),
		new Descriptivo("C", "De Terceros por Clientes"),
		new Descriptivo("O", "De Terceros por Otros"),
	];

	public estados: Descriptivo[] = [
		new Descriptivo("D", "Disponible"),
		new Descriptivo("E", "Entregado"),
		new Descriptivo("R", "Rechazado a cobrar"),
		new Descriptivo("C", "Rechazado cobrado"),
		new Descriptivo("A", "Anulado"),
	]

	ngOnInit() {
		let $this = this;
		this.route.params.subscribe(params => {

			let id: string = <string>params['id'];
			if (id) {
				this.addLoadingCount();
				this.chequeService.getById(parseInt(id)).then((chq) => {
					$this.chequeEditado = chq;
					$this.titulo = "Editando - " + $this.chequeEditado.descripcion;
					$this.estadoCheque = Descriptivo.fromData($this.chequeEditado.estado);
					$this.susLoadingCount();
					$this.editable = true;
				}).catch(this.errorHandler);
			} else {
				this.titulo = "Nuevo Cheque";
				$this.chequeEditado = new Cheque(null, null, null, new Descriptivo("P", "Propio"));
				$this.editable = true;
				$this.estadoCheque = new Descriptivo("D", "Disponible");
			}

			this.addLoadingCount();
			this.cuentasService.getAllBancarias().then((res: any[]) => {
				if (res && res.length > 0) {
					$this.cuentasBancarias = res;
				} else {
					$this.error("No hay cuentas de aplicación de subgrupo Bancos disponibles");
					$this.finalizado = true;
					setTimeout(() => {
						$this.router.navigate(["admin/chequeras"]);
					}, 3000)
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		});
	}

	public updateTipoCheque(event: any) {
		if (event.new) {
			this.chequeEditado.tipoCheque = event.new;
		} else {
			this.chequeEditado.tipoCheque = null;
		}
	}

	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				this.router.navigate(['admin/chequeras']);
			}
		});
	}

	public guardarCheque() {
		if (this.validarCheque()) {
			let $this = this;
			$this.addLoadingCount();
			this.chequeEditado.estado = this.estadoCheque;
			this.chequeService.guardar(this.chequeEditado).then((res) => {
				$this.susLoadingCount();
				this.success("El cheque se guardó exitosamente");
				setTimeout(() => {
					$this.router.navigate(["admin/chequeras"]);
				}, 3000)
			}).catch(this.errorHandler)
		}
	}

	public validarCheque(): boolean {
		var estado: boolean = true;
		if (this.estadoCheque.codigo !== 'A') {
			if (this.estadoCheque.codigo !== 'D' && (!this.chequeEditado.destino && !this.chequeEditado.pago)) {
				estado = estado && false;
				this.error("Debe indicar un destino para el cheque");
			}
			if (this.estadoCheque.codigo !== 'D' && !this.chequeEditado.importe) {
				estado = estado && false;
				this.error("Debe indicar un importe para el cheque");
			}
		}
		return estado;
	}
}
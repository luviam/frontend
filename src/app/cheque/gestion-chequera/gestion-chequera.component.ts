import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { CuentasService } from '../../common-services/cuentasService.service';
import { Chequera } from '../../model/Chequera';
import { CuentaAplicacion } from '../../model/CuentaAplicacion';
import { SecuenciaCheques } from '../../model/SecuenciaCheques';
import { SessionComponent } from '../../session-component.component';
import { ChequeraService } from '../service/chequera.service';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Descriptivo } from '../../model/Descriptivo';

@Component({
	selector: 'eventos-gestion-chequera',
	templateUrl: './gestion-chequera.component.html',
	styleUrls: ['./gestion-chequera.component.less']
})
export class GestionChequeraComponent extends SessionComponent {

	public chequera: Chequera;
	public cuentasBancarias: CuentaAplicacion[] = [];
	public cuentasBancariasSeleccionables: Descriptivo[] = [];
	public titulo: string = "";

	constructor(cookieService :CookieServiceHelper,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private router: Router,
		private cuentasService: CuentasService,
		private chequeraService: ChequeraService) {
		super();
	}

	ngOnInit() {
		this.finalizado = false;
		let $this = this;
		this.chequera = new Chequera();
		this.chequera.secuencias = [];
		this.chequera.secuencias.push(new SecuenciaCheques());

		this.addLoadingCount();
		this.cuentasService.getAllBancarias().then((res:any[]) => {
			if (res && res.length > 0) {
				$this.cuentasBancarias = res;
				$this.cuentasBancariasSeleccionables = res.map(r=>new Descriptivo(r.id+"", r.centroCosto.codigo + " - " + r.nombre));
			}else{
				$this.error("No hay cuentas de aplicación de subgrupo Bancos disponibles");
				$this.finalizado = true;
				setTimeout(() => {
					$this.router.navigate(["admin/chequeras"]);
				}, 3000)
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.route.params.subscribe(params => {

			let idChequera: string = <string>params['id'];
			if (idChequera) {
				this.addLoadingCount();
				this.chequeraService.getById(parseInt(idChequera)).then((chq) => {
					$this.chequera = chq;
					$this.titulo = "Editando - " + $this.chequera.descripcion;
					$this.susLoadingCount();
					$this.editable = true;
				}).catch(this.errorHandler);
			} else {
				this.titulo = "Nueva Chequera";
				$this.editable = true;
			}
		});
		console.log(this.chequera.secuencias);
	}

	public seleccionarCuenta($event){
		this.chequera.cuentaBancaria = this.cuentasBancarias.find(cb=>cb.id == $event.value.codigo);
	}

	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				this.router.navigate(['admin/chequeras']);
			}
		});
	}

	public guardarChequera() {
		let $this = this;
		if (this.chequeraValida(this.chequera)) {
			this.addLoadingCount();
			this.chequeraService.guardar(this.chequera).then((c) => {
				$this.success("La chequera fue guardada correctamente");
				$this.finalizado = true;
				setTimeout(() => {
					$this.router.navigate(["admin/chequeras"]);
				}, 1500)
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("Los datos de la chequera no son válidos");
		}
	}
	public chequeraValida(chq: Chequera): boolean {
		return chq.cuentaBancaria && chq.secuenciasValidas;
	}
	public agregarSecuencia() {
		if (!this.chequera.secuencias  || this.chequera.secuencias.length == 0 || this.chequera.secuenciasValidas) {
			this.chequera.secuencias.push(new SecuenciaCheques());
		} else {
			this.error("Debe completar todas las secuencias correctamente antes de agregar una nueva");
		}
	}
	public eliminarSecuencia(sec:SecuenciaCheques){
		let index:number = this.chequera.secuencias.findIndex(secuencia=>secuencia.id==sec.id);
		this.chequera.secuencias.splice(index,1);
	}

}
import { CalendarEvent } from '../../common-utils/CalendarEvent';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { Descriptivo } from '../../model/Descriptivo';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ChequeraService } from '../service/chequera.service';
import { Router } from '@angular/router';
import { Chequera } from '../../model/Chequera';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Cheque } from '../../model/Cheque';
import { ChequeService } from '../service/cheque.service';

@Component({
  selector: 'eventos-lista-chequera',
  templateUrl: './lista-chequera.component.html',
  styleUrls: ['./lista-chequera.component.less']
})
export class ListaChequeraComponent extends SessionComponent implements OnInit {
	  public listaChequeras: Chequera[] = [];
	  public listaCheques: Cheque[] = [];
	  public estados : Descriptivo[] = [];
	  public listaChequesAMostrar: Cheque[] = [];
	  public titulo: string = "Listado de chequeras"
	  public tiposSeleccionados: string[] =["P","C","O"];
	  @ViewChild("cal")
	  private calendario: any;
	  public eventos : CalendarEvent[] = [];
	  public options : any = {};
	  public headerConfig = {
		  left: 'prev, today, next',
		  center: 'title',
		  right: 'verListado, month,agendaWeek,agendaDay'
	  };
	  constructor(cookieService :CookieServiceHelper,
		private chequeraService: ChequeraService,
		private chequeService : ChequeService,
		private router: Router,
		private confirmationService: ConfirmationService) { super( 'listado') }

	ngOnInit() {
		this.options.customButtons = {
			verListado: {
				text: 'Ver Listado',
				icon: 'fa-hamburguer',
				click: r => {this.goTo('listado')}
			  }
		}
		
		this.getListado();
		this.getCheques();
		let $this = this;	
		this.addLoadingCount();
		this.descriptivoService.getAllEstados().then((t)=>{
			$this.estados =t;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}	

	public confirmarEliminarCheque(cheque : Cheque){
		this.confirmationService.confirm({
			header: "Va a eliminar el cheque N° " + cheque.numero + " del banco " + cheque.banco,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.chequeService.delete(cheque.id).then((res) => {
					$this.listaCheques.splice($this.listaCheques.indexOf(cheque),1);
					$this.updateSeleccionados();
					$this.susLoadingCount();
					$this.success("El cheque fue eliminado correctamente");
				}).catch($this.errorHandler);
			}
		});
	}
	public confirmarEliminar(chequera: Chequera) {
		this.confirmationService.confirm({
			header: "Va a eliminar la chequera " + chequera.descripcion,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.chequeraService.delete(chequera.id).then((res) => {
					$this.getListado();
					$this.getCheques();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
			}
		});
	}
	
	public generarEventos(cheques : Cheque[]){
		this.eventos = [];
		let $this = this;
		cheques.forEach(c => {
			
			$this.eventos.push( new CalendarEvent(
				c.id, c.descripcionCorta(), true, c.fechaPago,
				c.fechaPago, "evento evento_"+c.estado.codigo,
				 c
			));
		});
		this.eventos = [... this.eventos];
	}
	public eventClick(event : any, cal? : any){
		this.editarCheque(event.calEvent.fuente);
	}


	public getListado() {
		let $this = this;
		this.addLoadingCount();
		this.chequeraService.getAllList().then((res) => {
			$this.listaChequeras = [...res];
			
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	public getCheques(){
		let $this = this;
		this.addLoadingCount();
		this.chequeService.getAllList().then((res)=>{
			$this.listaCheques = [...res];
			$this.generarEventos($this.listaCheques);
			$this.updateSeleccionados();
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}
	public editarChequera(chequera: Chequera) {
		this.router.navigate(["admin/chequeras/" + chequera.id + ""]);
	}
	public nuevaChequera() {
		this.router.navigate(["admin/chequeras/nuevo"]);
	}
	public nuevoCheque() {
		this.router.navigate(["admin/cheque/nuevo"]);
	}
	public editarCheque(cheque : Cheque){
		this.router.navigate(["admin/cheque/" + cheque.id + ""]);
	}

	public updateSeleccionados(){
		let $this = this;
		this.listaChequesAMostrar = this.listaCheques.filter(c=> $this.tiposSeleccionados.indexOf(c.tipoCheque.codigo)> -1);
		this.listaChequesAMostrar = [...this.listaChequesAMostrar];
	}
	

}
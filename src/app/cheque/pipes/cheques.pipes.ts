
import { Pipe, PipeTransform } from '@angular/core';

import { Cheque } from '../../model/Cheque';

@Pipe({ name: 'chequesByTipo' })
export class ChequesPorTipoPipe implements PipeTransform {
  transform(cheques: Cheque[], tipo : string) {
    return cheques.filter(c => c.tipoCheque.codigo === tipo);
  }
}
import { Cheque } from '../../model/Cheque';
import { CuentaAplicacion } from '../../model/CuentaAplicacion';
import { SecuenciaCheques } from '../../model/SecuenciaCheques';
import { Descriptivo } from '../../model/Descriptivo';
import { RequestOptions } from '@angular/http';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ChequeService extends ServicioAbstract{
   


    public getAllList(): Promise<any> {
        return this.http.get(this.getApiURL() + "cheques/all")
            .toPromise()
            .then(this.parseCheques, this.handleError);

    }
    public getDisponibles(): Promise<Cheque[]> {
        return this.http.get(this.getApiURL() + "cheques/disponibles")
            .toPromise()
            .then(this.parseCheques, this.handleError);

    }

    public parseCheques(res : any) : Cheque[]{
            
            let cabeceras: Cheque[] = [];
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                cabeceras = res.respuesta.map( Cheque.fromData);
            }
            return cabeceras;

    }

    public getById(id: number): Promise<any> {
        return this.http.get(this.getApiURL() + "cheques/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cheque: Cheque;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    cheque = Cheque.fromData(res.respuesta);
                }
                return cheque;

            }, this.handleError);

    }
    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'cheques', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar(cheque: Cheque): Promise<any> {
        if (cheque.id) {
            return this.http.post(this.getApiURL() + 'cheques', cheque).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'cheques', cheque).toPromise().then(this.handleOk, this.handleError);
        }
    }

    
}

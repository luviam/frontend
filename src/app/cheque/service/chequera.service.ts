import { CuentaAplicacion } from '../../model/CuentaAplicacion';
import { SecuenciaCheques } from '../../model/SecuenciaCheques';
import { Descriptivo } from '../../model/Descriptivo';
import { RequestOptions } from '@angular/http';
import { Chequera } from '../../model/Chequera';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ChequeraService extends ServicioAbstract{
   


    public getAllList(): Promise<any> {
        return this.http.get(this.getApiURL() + "chequera/all")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cabeceras: Chequera[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    cabeceras = (<any[]>res.respuesta).map(chq=>new Chequera(chq.id, chq.cuentaBancaria, chq.secuencias, chq.esDiferido));
                }
                return cabeceras;

            }, this.handleError);

    }


    public getById(id: number): Promise<any> {
        return this.http.get(this.getApiURL() + "chequera/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let chequera: Chequera;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    chequera = this.parseChequera(res.respuesta);
                }
                return chequera;

            }, this.handleError);

    }
    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'chequera', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar(chequera: Chequera): Promise<any> {
        if (chequera.id) {
            return this.http.post(this.getApiURL() + 'chequera', chequera).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'chequera', chequera).toPromise().then(this.handleOk, this.handleError);
        }
    }

    private parseChequera(data: any) {

        return new Chequera(data.id, new CuentaAplicacion(data.cuentaBancaria.id, data.cuentaBancaria.codigo, data.cuentaBancaria.nombre, data.cuentaBancaria.centroCosto),
            this.parseSecuencias(data.secuencias), data.esDiferido);
    }

    private parseSecuencias(secuencias:any[]){
        return secuencias.map(sec=>new SecuenciaCheques(sec.id, sec.minNumeracion, sec.maxNumeracion));
    }
}
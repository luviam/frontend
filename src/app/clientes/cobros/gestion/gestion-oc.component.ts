import { EstadoIVA } from './../../../model/EstadoIVA';
import { CabeceraOrdenCobranza } from '../listado/shared/CabeceraOrdenCobranza';


import { CuotaServicio } from '../../../contratos/model/CuotaServicio';

import { Descriptivo } from '../../../model/Descriptivo';
import { ClienteService } from '../../service/ClienteService';

import { Cobro } from '../../../model/documentos/Cobro';
import { OrdenCobranzaService } from '../service/OrdenCobranzaService.service';


import { ItemOrdenCobranza } from '../../../model/documentos/ItemOrdenCobranza';

import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';

import { CuentaAplicacion } from '../../../model/CuentaAplicacion';
import { CuentasService } from '../../../common-services/cuentasService.service';

import { SessionComponent } from '../../../session-component.component';

import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CentroCostoService } from '../../../common-services/centroCostoService.service';

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cheque } from '../../../model/Cheque';
import { ChequeService } from '../../../cheque/service/cheque.service';
import { OrdenCobranza } from '../../../model/documentos/OrdenCobranza';
import { ContratosService } from '../../../contratos/service/contratos.service';
import { ContratoCabecera } from '../../lista-clientes/shared/ContratoCabecera';

@Component({
	selector: 'gestion-oc',
	styleUrls: ["./gestion-oc.component.less"],
	templateUrl: 'gestion-oc.component.html'
})

export class GestionOrdenCobranzaComponent extends SessionComponent {
	public eventos: ContratoCabecera[] = [];
	public clientes: Descriptivo[] = [];
	public cuotas: CuotaServicio[] = [];
	public _cliente: Descriptivo;
	
	private _cabeceraOrdenCobranza: CabeceraOrdenCobranza;
	public ordenCobranza: OrdenCobranza = new OrdenCobranza();
	private _soloSeleccionadosItems: boolean = false;
	private _soloSeleccionadosImpuestos: boolean = false;

	set soloSeleccionadosItems(val : boolean){
		this._soloSeleccionadosItems = val;
		this.filtrarItems();
	}
	get soloSeleccionadosItems() : boolean{
		return this._soloSeleccionadosItems;
	}

	set soloSeleccionadosImpuestos(val : boolean){
		this._soloSeleccionadosImpuestos = val;
		this.filtrarItems();
	}
	get soloSeleccionadosImpuestos() : boolean{
		return this._soloSeleccionadosImpuestos;
	}
	
	public editando: boolean = false;
	public centros: Descriptivo[] = [];
	public cuentasImputacion: CuentaAplicacion[] = [];
	public finalizado: boolean = false;
	public tiposComprobante: Descriptivo[] = [];
	public cuentas: any[] = [];
	public estadoIVA : EstadoIVA = new EstadoIVA();

	public cheques: Cheque[] = [];
	public chequesSugeridos: Cheque[] = [];

	private _items: ItemOrdenCobranza[] = [];
	public  itemsFiltrados: ItemOrdenCobranza[] = [];
	public impuestosFiltrados: ItemOrdenCobranza[] = [];
	private _impuestos: ItemOrdenCobranza[] = [];
	private tiposCobro: Descriptivo[] = [
		new Descriptivo("E", "Efectivo"),
		new Descriptivo("C", "Cheque"),
		new Descriptivo("T", "Transferencia"),
	]

	@Input()
	public editable: boolean = true;
	
	@Input()
	set contrato(val: ContratoCabecera) {
		if (!val || val.codigo === Descriptivo.TODOS().codigo) {
			this.ordenCobranza.contrato = null;
		} else {
			this.ordenCobranza.contrato = val;
			this.getCuotas(val);
			this.getEstadoIVA(val);
		}

	}
	get contrato(): ContratoCabecera {
		return this.ordenCobranza.contrato;
	}

	@Input()
	set cliente(val: Descriptivo) {
		if (!val || !val.codigo || val.codigo === Descriptivo.TODOS().codigo) {
			this._cliente = null;
		} else {
			this._cliente = val;
			this.getContraros(val);
		}
	}
	get cliente(): Descriptivo {
		return this._cliente;
	}


	@Input()
	set centroCosto(val: Descriptivo) {
		if (!val || val.codigo === Descriptivo.TODOS().codigo) {
			this.ordenCobranza.centroCosto = null;
		} else {
			this.ordenCobranza.centroCosto = val;
		}
	}

	get centroCosto(): Descriptivo {
		return this.ordenCobranza.centroCosto;
	}

	@Input()
	set cabeceraOrdenCobranza(cabecera: CabeceraOrdenCobranza) {
		this._cabeceraOrdenCobranza = cabecera;
		if (cabecera && cabecera.id) {
			this.items = this.impuestos = [];
			this.buscarOrdenCobranza(cabecera.id);
		} else {
			this.ordenCobranza = new OrdenCobranza();
			this.ordenCobranza.centroCosto = this.centroCosto || null;
			this.ordenCobranza.contrato = ContratoCabecera.fromData(this.contrato) || null;
			this.editable = true;
			this.ordenCobranza.fecha = new Date();
			this.soloSeleccionadosItems = false;
			this.soloSeleccionadosImpuestos = false;
		}
	}
	get cabeceraOrdenCobranza():CabeceraOrdenCobranza{
		return this._cabeceraOrdenCobranza;
	}

	@Input()
	public visible: boolean = true;

	@Output()
	public visibleChange = new EventEmitter();

	@Output()
	public onOrdenCobranzaGuardada = new EventEmitter();

	@Output()
	public onCancelar = new EventEmitter();


	constructor(
		private confirmationService: ConfirmationService,
		private descriptivosService: DescriptivosService,
		private contratoService: ContratosService,
		private ordenCobranzaService: OrdenCobranzaService,
		private cuentasService: CuentasService,
		private chequeService: ChequeService,
		private centroService: CentroCostoService,
		private clienteService: ClienteService) {
		super();
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		$this.actualizarCuentas();


		$this.addLoadingCount();
		$this.descriptivosService.getAllClientes().then((res) => {
			$this.clientes = res;
			$this.susLoadingCount();

		}).catch(this.errorHandler);


		//$this.addLoadingCount();
		//	$this.contratoService.getContratos().then((res) =>{
		//		$this.contratos = res;
		//		$this.susLoadingCount();
		//	}).catch($this.errorHandler);



		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	set items(val : ItemOrdenCobranza[]){
		this._items = val;
	}
	set impuestos(val: ItemOrdenCobranza[]){
		this._impuestos = val;
	}

	private filtrarItems() {
		if (this.soloSeleccionadosItems) {
			this.itemsFiltrados = this._items.filter(i => this.ordenCobranza.items.some(oi => oi.cuotaServicio.id === i.cuotaServicio.id));
		} else {
			this.itemsFiltrados = this._items;
		}
	}

	private filtrarImpuestos() {
		if (this.soloSeleccionadosImpuestos) {
			this.impuestosFiltrados = this._impuestos.filter(i => this.ordenCobranza.items.some(oi => oi.cuotaServicio.id === i.cuotaServicio.id));
		} else {
			this.impuestosFiltrados = this._impuestos;
		}
	}




	public actualizarCuentas() {
		let $this = this;
		this.addLoadingCount();
		$this.cuentasService.getCallCajas().then((res) => {
			$this.cuentas["E"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		$this.cuentasService.getAllBancarias().then((res) => {
			$this.cuentas["T"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		$this.cuentasService.getAllChequesClientes().then((res) => {
			$this.cuentas["C"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}
	public buscarOrdenCobranza(idOrdenCobranza: number) {
		let $this = this;
		this.addLoadingCount();
		$this.ordenCobranzaService.getById(idOrdenCobranza).then((res) => {
			$this.ordenCobranza = (res);
			$this.cliente = $this.ordenCobranza.contrato.cliente;
			
			$this.editable = $this.ordenCobranza.estado.codigo != "A";
			$this.soloSeleccionadosItems = $this.ordenCobranza.estado.codigo === "A" || $this.ordenCobranza.estado.codigo === "PP";
			$this.soloSeleccionadosImpuestos = $this.soloSeleccionadosItems;
			if($this.editable){
				$this.getCuotas($this.ordenCobranza.contrato);
			}else{
				$this.items = $this.ordenCobranza.itemsNeto;
				$this.impuestos = $this.ordenCobranza.impuestos;
			}
			$this.filtrarImpuestos();
			$this.filtrarItems();
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}



	public guardar() {
		let $this = this;
		if (this.validar()) {
			this.addLoadingCount();
			//this.ordenCobranza.cobros.filter(p => !p.centroCosto).forEach(p => p.centroCosto = this.ordenCobranza.centroCosto);
			this.ordenCobranzaService.guardar(this.ordenCobranza).then((res) => {
				$this.susLoadingCount();
				$this.ordenCobranza = new OrdenCobranza();
				$this.onOrdenCobranzaGuardada.emit();
			}).catch(this.errorHandler);
		}

	}
	public volver() {
		this.visible = false;
		this.visibleChange.emit(this.visible);
		this.onCancelar.emit();
	}
	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: "genConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				$this.volver();
			}
		});
	}

	public getCuotas(contrato: Descriptivo) {
		let $this = this;
		this.addLoadingCount();
		if(contrato && contrato.codigo){
			this.contratoService.getCuotas(contrato.codigo).then((res) => {
				$this.cuotas = res;
				if ($this.ordenCobranza.contrato && $this.ordenCobranza.contrato.codigo != contrato.codigo) {
					$this.ordenCobranza.items = [];
				}
				
				$this.items = $this.cuotas.filter(c => c.producto.categoria.codigo !== 'IMP').map(c => {
					var l = $this.ordenCobranza.items.filter(i => i.cuotaServicio.id === c.id)[0];
					return l || new ItemOrdenCobranza(null, c, c.saldo);
	
				});
				$this.impuestos = $this.cuotas.filter(c => c.producto.categoria.codigo === 'IMP').map(c => {
					var l = $this.ordenCobranza.items.filter(i => i.cuotaServicio.id === c.id)[0];
					return l || new ItemOrdenCobranza(null, c, c.saldo);
	
				});
				$this.ordenCobranza.itemsNeto.forEach(i=> {
					if(!$this._items.some(ii => ii.id === i.id)){
						$this._items.push(i);
					}
				})
				$this.ordenCobranza.impuestos.forEach(i=> {
					if(!$this._impuestos.some(ii => ii.id === i.id)){
						$this._impuestos.push(i);
					}
				})
				$this.filtrarImpuestos();
				$this.filtrarItems();
				$this.susLoadingCount();
	
	
	
			}).catch(this.errorHandler);
		}
		
	}

	
	public getContraros(cliente : Descriptivo){
		let $this = this;
		this.addLoadingCount();
		this.contratoService.getByClienteId(cliente.codigo).then((res) => {
			$this.eventos = res;
			if ($this.ordenCobranza.contrato && $this.ordenCobranza.contrato.cliente &&
				$this.ordenCobranza.contrato.cliente.codigo !== cliente.codigo) {
				if ($this.eventos.length === 1) {
					$this.ordenCobranza.contrato = $this.eventos[0];
				} else {
					$this.ordenCobranza.contrato = null;
				}
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	
	public agregarNuevoCobro() {
		this.ordenCobranza.addCobro(new Cobro(null, this.tiposCobro[0],
			this.ordenCobranza.centroCosto,
			null,
			this.ordenCobranza.total - this.ordenCobranza.totalCobros,
			null));
	}

	public getCuentas(cobro : Cobro){
		if (cobro && cobro.tipoOperacion && cobro.centroCosto)
			return this.cuentas[cobro.tipoOperacion.codigo].filter(c => cobro.centroCosto.codigo === c.centroCosto.codigo);
		return [];
	}


	public filtrarCheques(event: any) {

		this.chequesSugeridos = this.cheques.filter(c => c.descripcion.toUpperCase().includes(event.query.toUpperCase()));


	}
	public updateTipoCobro(oc: Cobro) {
		if (this.getCuentas(oc).length === 1) {
			oc.cuenta = this.getCuentas(oc)[0];
		}
		if (oc.tipoOperacion.codigo == "C") {
			if (!oc.comprobante) {
				oc.comprobante = new Cheque(
					null,
					"", "", Cheque.CLIENTE(), null, "", "", "", null, null,
					oc.monto, new Descriptivo("D", "Disponible"), "", false, null, null,
					this.ordenCobranza.fecha
				);
			}
		} else {
			oc.comprobante = null;
		}

	}

	public updateCheque(oc: Cobro) {
		if (oc.tipoOperacion.codigo !== "E" && oc.comprobante) {
			if (oc.comprobante.importe) {
				oc.monto = oc.comprobante.importe;
			}
			if (oc.tipoOperacion.codigo === 'P') {

				oc.cuenta = oc.comprobante.cuentaBancaria;
			}


		}
	}
	public autoselectCheque(oc: Cobro) {
		if (!oc.comprobante || !oc.comprobante.id) {


			if (this.chequesSugeridos.length == 1) {
				oc.comprobante = this.chequesSugeridos[0];

			} else {
				oc.comprobante = null;

			}

		}
	}
	public cobrosValidos() {

		return this.ordenCobranza.cobros.length === 0 || this.ordenCobranza.totalCobros === this.ordenCobranza.total;

	}
	public validar(): boolean {
		let estado: boolean = true;

		if(!this.ordenCobranza.centroCosto || !this.ordenCobranza.centroCosto.codigo){
			estado = false;
			this.error("Elija un Centro de Costo");
		}
		if (this.ordenCobranza.cobros.some(p => p.tipoOperacion.codigo == 'C' && !p.comprobante)) {
			estado = false;
			this.error("Debe completar todos los cheques");
		}
		if (this.ordenCobranza.cobros.some(p => !p.monto)) {
			estado = false;
			this.error("Debe completar todos los importes de cobros");
		}
		
		return estado;
	}

	public getChequeColumnStyle(): any {
		return { 'width': this.ordenCobranza.tieneCheques ? '40%' : '10%' };
	}

	public getEstadoIVA(val : ContratoCabecera){
		let $this = this;
		
		if(val && val.id){
			$this.addLoadingCount();
			this.contratoService.getEstadoIVA(val.id).then((r)=>{
				$this.estadoIVA = r;
				$this.susLoadingCount();
			}).catch(this.errorHandler);

			}
		
	}
}
import { ItemOrdenCobranza } from '../../../model/documentos/ItemOrdenCobranza';
import { SalonService } from '../../../salon/service/salon.service';
import { Descriptivo } from '../../../model/Descriptivo';
import { OrdenCobranzaService } from '../service/OrdenCobranzaService.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { CabeceraOrdenCobranza } from './shared/CabeceraOrdenCobranza';
import { FiltroCliente } from '../../filtros/FiltroCliente';

import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from '../../../session-component.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { ClienteService } from '../../service/ClienteService';
import { ContratosService } from '../../../contratos/service/contratos.service';

@Component({
	selector: 'ordenes-cobranza',
	styleUrls: ["./ordenes-cobranza.component.less"],
	templateUrl: 'ordenes-cobranza.component.html'
})

export class ListaOrdenCobranzasComponent extends SessionComponent {


	get cliente() { return this.filtro.cliente };

	get titulo() : string{
		if(this.ordenCobranzaEditada && this.ordenCobranzaEditada.numero){
			return "Cobro N° " + this.ordenCobranzaEditada.numero;
		}
		if(this.cliente && this.cliente.codigo && this.cliente.codigo !== Descriptivo.TODOS().codigo){
			return "Cobros de " + this.cliente.descripcion;
		}else{
			return "Cobros";
		}
		
	}
	public estadosSeleccionados: string[] = ["A", "P", "R", "PP"];
	
	private _vista: string = "lista-ocs";
	
	@Input()
	public filtro: FiltroCliente = new FiltroCliente();

	@Input()
	public mostrarFiltrosCliente : boolean = true;

	@Input()
	set vista(val:string){
		this._vista = val;
		this.vistaChange.emit(val);
	}

	get vista():string{
		return this._vista;
	}

	@Output()
	public vistaChange : EventEmitter<string> = new EventEmitter<string>();

	@Output()
	public docsActualizados :EventEmitter<boolean> = new EventEmitter<boolean>();

	public clientes : Descriptivo[] = [];
	public eventos : Descriptivo[] = [];
	public _ordenesCobranza: CabeceraOrdenCobranza[] = [];
	public _filtrados: CabeceraOrdenCobranza[] = [];
	public ordenCobranzaEditada: CabeceraOrdenCobranza = new CabeceraOrdenCobranza();
	public editandoOrdenCobranza: boolean = false;
	public _ordenSeleccionada : CabeceraOrdenCobranza = new CabeceraOrdenCobranza();
	get ordenSeleccionada(): CabeceraOrdenCobranza{
		return this._ordenSeleccionada;
	}
	set ordenSeleccionada(val :CabeceraOrdenCobranza) {
		this.itemsOrden = [];
		this._ordenSeleccionada = val;
		if(val && val.id){
			let $this = this;
			this.addLoadingCount();
			this.ordenCobranzaService.getItems(val.id).then((res)=>{
				$this.itemsOrden = res;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
	}
	public itemsOrden : ItemOrdenCobranza[] = [];
	public salones : Descriptivo[] = [];
	public horarios: Descriptivo[] =[
		
		new Descriptivo("D","Día"),
		new Descriptivo("N", "Noche"),
		Descriptivo.TODOS()
	];
	
	
	public centros: Descriptivo[] = [];

	
	constructor(cookieService :CookieServiceHelper,

		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private clienteService: ClienteService,
		private ordenCobranzaService : OrdenCobranzaService,
		private contratoService : ContratosService,
		private salonesService : SalonService) {
		super();
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		//this.filtro.fechaDesde = moment().subtract(3, 'months').startOf('day').toDate();
		this.filtro.fechaDesde = moment().startOf('year').toDate();
		this.filtro.fechaHasta = moment().add(1,'days').startOf('day').toDate();
		if(!this.filtro.centro){
			this.filtro.centro = Descriptivo.TODOS();
		}
		if(!this.filtro.salon){
			this.filtro.salon = Descriptivo.TODOS();
		}
		if(!this.filtro.horario){
			this.filtro.horario = Descriptivo.TODOS();
		}
		if(!this.filtro.cliente){
			this.filtro.cliente = Descriptivo.TODOS();
		}
		$this.addLoadingCount();
		$this.descriptivosService.getAllClientes().then((res) =>{
			$this.clientes = res;
			$this.clientes.push(Descriptivo.TODOS());
			if(!$this.filtro.cliente)
				$this.filtro.cliente = Descriptivo.TODOS();
			
			$this.susLoadingCount();
		}).catch($this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res;
			$this.centros.push(Descriptivo.TODOS());
			if(!$this.filtro.centro)
				$this.filtro.centro = Descriptivo.TODOS();
			$this.susLoadingCount();
			
		}).catch($this.errorHandler);
		$this.addLoadingCount();
		$this.descriptivosService.getAllSalones().then((res)=>{
			$this.salones = res;
			$this.salones.push(Descriptivo.TODOS());
			if($this.filtro.salon)
				$this.filtro.salon = Descriptivo.TODOS();
			$this.susLoadingCount();
		})
		$this.updateOrdenCobranzas();

	
		

	}
	public updateOrdenCobranzas() {
		let $this = this;
		this.filtro.fechaDesde.setHours(0,0,0,0);
		this.filtro.fechaHasta.setHours(23,59,59,0);
		if (this.filtro.cliente && this.filtro.centro) {
			$this.addLoadingCount();
			$this.clienteService.getCabecerasOrdenesCobro(this.filtro).then((res) => {
				$this._ordenesCobranza = res;
				$this.filtrar();
				if($this.ordenSeleccionada && !$this.ordenesCobranza.some(s=> s.id === $this.ordenSeleccionada.id)){
					$this.ordenSeleccionada = null;
				}
				$this.susLoadingCount();
			}).catch($this.errorHandler);
		}
	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			key:"opConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['clientes/lista-clientes']);
			}
		});
	}

	get saldo() {
		return this._ordenesCobranza.map(o => o.importeNeto).reduce((a,b)=> a+ b,0);
		
	}

	public pagable(op : CabeceraOrdenCobranza): boolean{
		return op.estado === "PP" && this.esAuditor;
	}
	public aprobable(op :CabeceraOrdenCobranza) : boolean{
		return op.estado === "P" && this.esAuditor;
	}

	public rechazable(op :CabeceraOrdenCobranza) : boolean{
		return op.estado === "P" && this.esAuditor;
	}

	public eliminable(op : CabeceraOrdenCobranza) : boolean{
		
		return op.estado !="A" || this.esAdministrador;
	}

	public esEditable(op : CabeceraOrdenCobranza) : boolean{
		return op.estado !="A";
	}

	public esVisualizable(op: CabeceraOrdenCobranza) : boolean{
		return !this.esEditable(op);
	}
	get ordenesCobranza() {
		return this._filtrados;
	}

	public updateTipoProveedor(event: any) {

	}

	public updateTipoCuenta(event: any) {

	}
	public editar(ordenCobranza: CabeceraOrdenCobranza) {
		this.ordenCobranzaEditada = ordenCobranza;
		this.editandoOrdenCobranza = true;
		this.vista = "ocs-gestion";
	}

	public pagar(ordenCobranza: CabeceraOrdenCobranza) {
		this.ordenCobranzaEditada = ordenCobranza;
		this.editandoOrdenCobranza = true;
		this.vista = "ocs-gestion";
	}

	public nuevaOrdenCobranza() {
		this.ordenCobranzaEditada = new CabeceraOrdenCobranza();
		this.editandoOrdenCobranza = true;
		this.vista = "ocs-gestion";
	}
	public onOrdenCobranzaGuardada() {
		this.editandoOrdenCobranza = false;
		this.success("Orden Guardada");
		this.updateOrdenCobranzas();
		this.vista = "ocs-lista";
		this.docsActualizados.emit(true);
	}

	public onEdicionCancelada(){
		this.vista = "ocs-lista";
		this.editandoOrdenCobranza = false;
	}
	public aprobar(ordenCobranza : CabeceraOrdenCobranza){
		let $this  = this;
		this.addLoadingCount();
		this.ordenCobranzaService.aprobarOrdenCobranza(ordenCobranza).then((res)=>{
			$this.success("Orden aprobada");
			$this.updateOrdenCobranzas();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}

	public rechazar(ordenCobranza : CabeceraOrdenCobranza){
		let $this  = this;
		this.addLoadingCount();
		this.ordenCobranzaService.rechazarOrdenCobranza(ordenCobranza).then((res)=>{
			$this.success("Orden Rechazada");
			$this.updateOrdenCobranzas();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}

	public eliminar(ordenCobranza : CabeceraOrdenCobranza){
		this.confirmationService.confirm({
			key:"opConf",
			header: "Va a eliminar la OC " + ordenCobranza.descripcion,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.ordenCobranzaService.eliminarOrdenCobranza(ordenCobranza).then((res)=>{
					$this.susLoadingCount();
					$this.ordenSeleccionada = new CabeceraOrdenCobranza();
					$this.success("Se borró la Orden correctamente");
					$this.ordenCobranzaEditada = null;
					$this.ordenSeleccionada = null;
					$this.updateOrdenCobranzas();
					$this.docsActualizados.emit(true);
				}).catch($this.errorHandler);
			}
		});
	
		
	}

	
	public filtrar(){
		this.addLoadingCount();
		this._filtrados = this._ordenesCobranza.filter(f => this.estadosSeleccionados.indexOf(f.estado) > -1);
		this.susLoadingCount();
	}
}
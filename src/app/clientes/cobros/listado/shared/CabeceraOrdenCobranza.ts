import { ContratoCabecera } from '../../../lista-clientes/shared/ContratoCabecera';
import { Descriptivo } from '../../../../model/Descriptivo';
import { ItemOrdenCobranza } from '../../../../model/documentos/ItemOrdenCobranza';
import { BasicEntity } from '../../../../model/BasicEntity';
import * as moment from 'moment';
export class CabeceraOrdenCobranza extends BasicEntity{
    constructor(public id? : number, 
                public fecha? : Date, 
                public numero? : string, 
                public responsable? : string,
                public descripcion? : string,
                public importeNeto? : number, 
                public ivaCobrado : number = 0,
                public estado? : string,
                public contrato? : ContratoCabecera){
                    super(id,numero,descripcion);
                }
    public static fromData(data : any){
        if(!data) return null;
        let o : CabeceraOrdenCobranza = new CabeceraOrdenCobranza(
            data.id,
            data.fecha? new Date(data.fecha): null,
            data.numero,
            data.responsable,
            data.descripcion,
            data.importeNeto?  data.importeNeto : 0,
            data.ivaCobrado? data.ivaCobrado : 0,
            data.estado,
            ContratoCabecera.fromData(data.contrato)
        )
        return o;
        
    }
    get fechaFormateada(){
        return moment(this.fecha).format('DD/MM/YYYY');
    }

    get cliente() : Descriptivo{
        return this.contrato.cliente || Descriptivo.SIN_ASIGNAR();
    }

    get total(){
        return this.importeNeto + this.ivaCobrado;
    }   
}
import { Contrato } from '../../../contratos/model/Contrato';
import { ItemOrdenCobranza } from '../../../model/documentos/ItemOrdenCobranza';
import { OrdenCobranza } from '../../../model/documentos/OrdenCobranza';

import { Pago } from '../../../model/Pago';


import { RequestOptions, Http } from '@angular/http';

import { Descriptivo } from '../../../model/Descriptivo';
import { CabeceraOrdenCobranza } from '../listado/shared/CabeceraOrdenCobranza';
import { Injectable } from '@angular/core';


import { ServicioAbstract } from '../../../common-services/service.service';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable()
export class OrdenCobranzaService extends ServicioAbstract {
    
   

    public getByContrato(contrato : Contrato){
        return this.http.get(this.getApiURL() + 'ordenCobranza/contrato/' + contrato.id).toPromise().then(this.parseOrdenesCobranza, this.handleError);
    }

    public getCabecerasByContrato(contrato : Contrato){
        return this.http.get(this.getApiURL() + 'ordenCobranza/cabeceras/contrato/' + contrato.id).toPromise().then(this.parseCabecerasOrdenCobranza, this.handleError);
    }


    public aprobarOrdenCobranza(ordenCobranza: CabeceraOrdenCobranza): Promise<any> {
        
        let o : CabeceraOrdenCobranza = CabeceraOrdenCobranza.fromData(ordenCobranza);
        o.estado = "A";
        return this.cambioEstadoOrdenCobranza(o);
    }
    public rechazarOrdenCobranza(ordenCobranza: CabeceraOrdenCobranza): Promise<any> {
        let o : CabeceraOrdenCobranza = CabeceraOrdenCobranza.fromData(ordenCobranza);
        o.estado = "R";
        return this.cambioEstadoOrdenCobranza(o);
    }

    public eliminarOrdenCobranza(ordenCobranza : CabeceraOrdenCobranza) : Promise<any>{
        let params = new HttpParams();
       params = params.append('id',ordenCobranza.id+"")
        return this.http.delete(this.getApiURL() + 'ordenCobranza', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    private cambioEstadoOrdenCobranza(ordenCobranza: CabeceraOrdenCobranza): Promise<any> {
        return this.http.post(this.getApiURL() + "ordenCobranza/cambioEstado", ordenCobranza).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(idordenCobranza: number): Promise<any> {
        return this.http.get(this.getApiURL() + 'ordenCobranza/' + idordenCobranza).toPromise().then(this.parseOrdenCobranza, this.handleError);
    }

    public guardar(ordenCobranza: OrdenCobranza): Promise<any> {
        ordenCobranza.importeNeto = ordenCobranza.totalItems;
        if (ordenCobranza.id) {
           return this.http.post(this.getApiURL() + 'ordenCobranza', ordenCobranza).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'ordenCobranza', ordenCobranza).toPromise().then(this.handleOk, this.handleError);
        }
        
    }

    public getItems(idOrden : number) : Promise<ItemOrdenCobranza[]>{
        return this.http.get(this.getApiURL() + 'ordenCobranza/items/' + idOrden).toPromise().then(this.parseItems, this.handleError);
    }

    public parseItems(response: any) : ItemOrdenCobranza[]{
        let res = response.respuesta;
        if(res){
            return res.map(i => ItemOrdenCobranza.fromData(i));
        }
        return [];
    }

    
    public parseOrdenCobranza(response: any): OrdenCobranza {
        let res = response;
        let ordenCobranza : OrdenCobranza = null;
        return  OrdenCobranza.fromData(res.respuesta);
    }

    public parseOrdenesCobranza(response: any): OrdenCobranza[] {
        let res = response;
        let cabeceras: OrdenCobranza[] = [];
        if (res !== null && res.respuesta !== null) {
            cabeceras = res.respuesta.map(c => OrdenCobranza.fromData(c));
        }
        return cabeceras;

    }
    public parseCabecerasOrdenCobranza(response: any): CabeceraOrdenCobranza[] {
        let res = response;
        let cabeceras: CabeceraOrdenCobranza[] = [];
        if (res !== null && res.respuesta !== null) {
            cabeceras = res.respuesta.map(c => CabeceraOrdenCobranza.fromData(c));
        }
        return cabeceras;

    }
    
}
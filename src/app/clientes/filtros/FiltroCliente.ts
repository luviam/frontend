
import { Descriptivo } from '../../model/Descriptivo';
export class FiltroCliente{
    constructor(public cliente ?: Descriptivo,
                public fechaDesde? : Date,
                public fechaHasta? : Date,
                public centro? :Descriptivo,
                public salon? :Descriptivo,
                public horario? :Descriptivo){
                    
    }
}
import { Contacto } from '../../model/Contacto';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { ConvenioSalon } from '../../model/ConvenioSalon';

import { Descriptivo } from '../../model/Descriptivo';
import { Cliente } from '../../model/Cliente';
import { ClienteService } from '../service/ClienteService';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CentroCostoService } from '../../common-services/centroCostoService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'gestion-cliente',
	styleUrls: ["./gestion-cliente.component.less"],
	templateUrl: 'gestion-cliente.component.html'
})

export class GestionClienteComponent extends SessionComponent {
	
	private _cliente: Cliente = new Cliente();
	public tiposCliente: Descriptivo[] = [];
	public tipoIVA: Descriptivo[] = [];
	
	@Input()
	public editable: boolean = false;
	
	@Input()
	set cliente(val :Cliente){
		this._cliente = val;
		this.titulo = val && val.id? "Editando a " + val.nombreCompleto : "Nuevo Cliente";
		this.clienteChange.emit(this.cliente);
	}

	@Output()
	public onCancelar : EventEmitter<boolean> = new EventEmitter(true);

	@Output()
	public onGuardar : EventEmitter<boolean> = new EventEmitter(true);

	get cliente():Cliente{
		return this._cliente;
	}

	@Output()
	public clienteChange :EventEmitter<Cliente> = new EventEmitter<Cliente>();

	public finalizado: boolean = false;
	public titulo : string ="";
	public displayContactoDialog : boolean = false;
	public editandoContacto : boolean = false;
	public contactoEditado: Contacto = new Contacto();
	public tituloContacto: string = "";
	constructor(cookieService :CookieServiceHelper,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private clienteService: ClienteService) {
		super();
	}



	ngOnInit() {
		this.finalizado = false;
		let $this = this;
		this.addLoadingCount();
		this.descriptivosService.getAllTiposCliente().then((res) => {
			if (res) {
				$this.tiposCliente = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllTipoIVA().then((res) => {
			if (res) {
				$this.tipoIVA = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		
	}
	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
		  key: "conf",
		  header: "Cancelar Edición",
		  message: 'Se perderán los cambios realizado. Desea continuar?',
		  accept: () => {
			$this.onCancelar.emit(true);
		  }
		});
	  }

	public updateTipoCliente(event : any){
		
		if(event.value.codigo === "SA" && !this.cliente.convenio){
			this.cliente.convenio = new ConvenioSalon();
		}
	}

	public guardarCliente(){
		let $this = this;
		if(this.clienteValido(this.cliente)){
			this.addLoadingCount();
			if(this.cliente.tipoCliente.codigo != 'SA'){
				this.cliente.convenio = null;
			}
			if(this.cliente.tipoCliente.codigo != 'SO'){
				this.cliente.agasajados = null;
				this.cliente.nombreFirmante = null;
				this.cliente.dniFirmante = null;
			}
			this.clienteService.guardar(this.cliente).then((c)=>{
				$this.success("El cliente fue guardado correctamente");
				$this.finalizado = true;
				setTimeout(() => {
					$this.onGuardar.emit(true);
				  }, 3000)
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}else{
			this.error("El cliente no es valido");
		}
	}


	public clienteValido(cliente :Cliente) : boolean{
		let estado = true;
		if(!this.cliente.tipoCliente || !this.cliente.tipoCliente.codigo){
			this.error("Complete el Tipo de Cliente");
			estado = false;
		}
		if(!this.cliente.esSocial){
			if(!this.cliente.cuit){
				this.error("Complete el CUIT");
				estado = false;
			}
			if(!this.cliente.iva || !this.cliente.iva.codigo){
				this.error("Complete la Categoría de IVA");
				estado = false;
			}
			if(!this.cliente.razonSocial){
				this.error("Complete la Razón social");
				estado = false;
			}
		}
		return estado;
	}

	public editarContacto(contacto : Contacto){
		this.tituloContacto = "Editando a " + contacto.nombre;
		this.editandoContacto = true;
		this.contactoEditado = contacto;
		this.displayContactoDialog = true;
	}

	public nuevoContacto(){
		this.tituloContacto = "Nuevo Contacto";
		this.editandoContacto = false;
		this.contactoEditado = new Contacto();
		this.displayContactoDialog = true;
	}

	public deleteContacto(contacto: Contacto){
		let $this = this;
		this.confirmationService.confirm({
		  key: "conf",
		  header: "Borrar contacto",
		  message: 'Se borrará el contacto ' + contacto.nombre + '. Desea continuar?',
		  accept: () => {
			$this.cliente.borrarContacto(contacto);
		  }
		});
	  
		
		
	}
	public guardarContacto(event : Contacto){
		this.cliente.setContacto(event);
		this.displayContactoDialog = false;
		this.editandoContacto = false;
		this.contactoEditado = new Contacto();
		
			
	}
}
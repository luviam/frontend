import { CustomValidators } from '../../custom-validators.class';
import { FormBuilder } from '@angular/forms';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Contacto } from '../../model/Contacto';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';


@Component({
	selector: 'gestion-contacto',
	templateUrl: 'gestion-contacto.component.html',
	styles:["gestion-contacto.component.less"]
})

export class GestionContactoComponent extends SessionComponent {

	private _contacto:Contacto;
	public contactoForm: FormGroup;
	@Input()
	public editable : boolean = true;
	
	@Input()
	set contacto(val : Contacto){
		
		this._contacto = val;
		this.modelContactoToForm(val);
		this.contactoChange.emit(this.contacto);
	}
	get contacto(): Contacto{
		return this._contacto;
	}

	@Output()
	public contactoChange : EventEmitter<Contacto> = new EventEmitter<Contacto>();

	@Output()
	public onContactoGuardado : EventEmitter<Contacto> = new EventEmitter<Contacto>();

	@Output()
	public onCancelar : EventEmitter<boolean> = new EventEmitter<boolean>();
	
	constructor(cookieService : CookieServiceHelper,
				private confirmationService : ConfirmationService,
				private fb: FormBuilder){
		super();
		this.contactoForm = this.fb.group({
			'relacionContacto': ['', CustomValidators.required],
			'nombre': ['', CustomValidators.required],
			'email': ['', [CustomValidators.email]],
			'telefono': [''],
			'celular': [''],
			'facebook': [''],
			'instagram': [''],
			'numeroDocumento': ['']
		});
	}
	ngOnInit() { 

	}

	public guardarContacto(){
		this._contacto = this.formContactoToModel();
		if(this.validarContacto()){
			this.onContactoGuardado.emit(this.contacto);
		}
	}

	public validarContacto(): boolean{

		return true;
	}
	
	private formContactoToModel(): Contacto{
		let formValue = this.contactoForm.value;
		return Contacto.fromData(formValue);
	}

	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
		  key: "confContacto",
		  header: "Cancelar Edición",
		  message: 'Se perderán los cambios realizados. Desea continuar?',
		  accept: () => {
			$this.onCancelar.emit(true);
		  }
		});
	  }
	private modelContactoToForm(contacto: Contacto){
		if(contacto){
			this.contactoForm.patchValue(
				{
					relacionContacto: contacto.relacionContacto,
					nombre: contacto.nombre,
					email: contacto.email,
					telefono: contacto.telefono,
					celular: contacto.celular,
					facebook: contacto.facebook,
					instagram: contacto.instagram,
					numeroDocumento: contacto.numeroDocumento
				}
			);
		}
		
	}
}

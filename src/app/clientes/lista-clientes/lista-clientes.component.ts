import { FiltroCliente } from '../filtros/FiltroCliente';
import { Menu, MenuItem } from 'primeng/primeng';


import { Cliente } from '../../model/Cliente';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from '../../session-component.component';
import { ClienteService } from '../service/ClienteService';
import { Component, OnInit, ViewChild } from '@angular/core';

import { ListaClientes } from './shared/lista-clientes.model';
import { Contacto } from '../../model/Contacto';


@Component({
	selector: 'lista-clientes',
	templateUrl: 'lista-clientes.component.html',
	providers: [ClienteService]
})

export class ListaClientesComponent extends SessionComponent implements OnInit {
	@ViewChild("menu")
	private menu : Menu;
	public items: MenuItem[];

	listaClientes: ListaClientes[] = [];

	titulo: string = "Listado de Clientes"
	tituloDocumentos : string ="Eventos";
	private filtro: any = {}
	public clienteSeleccionado : ListaClientes;
	public clienteEditado : Cliente;
	public gestionCliente: boolean = false;
	public editable: boolean = true;

	constructor(
		private clienteService: ClienteService,
		private router: Router,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService, ) { super('listaClientes') }

	ngOnInit() {
		this.items =[
			{label: 'Editar', icon: 'fa-pencil', command: (event)=>{
				this.editarCliente(this.clienteSeleccionado);
			}},
			{label: 'Eventos', icon: 'fa-file-o', command: (event)=>{
				this.mostrarContratos(this.clienteSeleccionado);
			}},
			{label: 'Cobros', icon: 'fa-money', command: (event)=>{
				this.mostrarPagos(this.clienteSeleccionado);
			}}
		];

		if(this.esAdministrador){
			this.items.push(
				{label: 'Eliminar', icon: 'fa-trash', command: (event)=>{
					this.confirmarEliminar(this.clienteSeleccionado);
				}}
			);
		}
		this.getListado();
		let $this = this;
		if ($this.route.snapshot.url.some(u => u.path === "nuevo")) {
			this.nuevoCliente();
		} else {
			$this.route.params.subscribe(params => {
				let idCliente: string = <string>params['id'];
				if (idCliente) {
					$this.editarCliente(new ListaClientes(Number(idCliente)))
				}


			});
		}

	}
	public confirmarEliminar(cliente: ListaClientes) {
		this.confirmationService.confirm({
			header: "Va a eliminar al Cliente " + cliente.descripcion,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.clienteService.delete(cliente.id).then((res) => {
					$this.getListado();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
			}
		});
	}

	public getListado() {
		let $this = this;
		this.addLoadingCount();
		this.clienteService.getAllList(this.filtro).then((res) => {
			$this.listaClientes = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	public editarCliente(cliente: ListaClientes) {
		let $this = this;

		this.goTo("cliente-gestion");
		this.addLoadingCount();
		this.clienteService.getById(cliente.id).then(c => {
			$this.clienteEditado = c;
			$this.gestionCliente = true;
			$this.titulo = "Editando a " + $this.clienteEditado.nombreCliente;
			$this.susLoadingCount();
		}).catch(this.errorHandler);



	}

	public onCancelarEdicionCliente(event: any) {
		this.gestionCliente = false;
		this.clienteEditado = null;
		this.titulo = "Lista de Clientes";
		this.router.navigate(['clientes/lista-clientes']);
		this.goTo( "listaClientes");
	}

	public onDocsActualizados(event : any){
		this.onCancelarEdicionCliente(event);
		this.getListado();
	}

	public clienteGuardado(cliente: Cliente) {
		this.gestionCliente = false;
		this.clienteEditado = null;
		this.getListado();
		this.titulo = "Lista de Clientes";
		this.goTo( "listaClientes");
		this.router.navigate(['clientes/lista-clientes']);
	}
	public nuevoCliente() {
		this.clienteEditado = new Cliente();
		this.gestionCliente = true;
		this.titulo = "Nuevo Cliente";
		this.goTo( "cliente-gestion");
	}

	trackByFn(index :number, item : Contacto){
		return item.id;
	}

	public mostrarAcciones(event:any,cliente: ListaClientes){
		this.clienteSeleccionado = cliente;
		this.menu.show(event);
	}
	public mostrarContratos(cliente :ListaClientes){
		
		let $this = this;
		this.addLoadingCount();
		this.clienteService.getById(cliente.id).then((c)=>{
			$this.goTo( "contratos-lista");
			$this.clienteEditado = c;
			$this.tituloDocumentos = "Eventos de " + c.nombreCliente;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		
	}
	public mostrarPagos(cliente :ListaClientes){
		
		let $this = this;
		$this.filtro = new FiltroCliente(
			cliente		
		)
		$this.goTo( "ocs-lista");
		$this.tituloDocumentos = "Cobros de " + cliente.nombreCliente;	
	}
	public ocultarVolver():boolean{
		return !this.currentView.includes('-lista');
	}
}
import { Cliente } from './../../../model/Cliente';
import { Descriptivo } from '../../../model/Descriptivo';

import { ContratoCabecera } from './ContratoCabecera';
export class CabeceraCliente extends Descriptivo{

	constructor(

		public id? : number,
		public nombreCliente? : string,
		public nombreContacto? : string,
		public telefono? : string,
		public celular? : string,
		public email? : string,
		public razonSocial? : string,
		public cuit? : string,
		public tipoCliente? : Descriptivo,
		public activo? : boolean,
		public facebook? : string,
		public instagram? : string,
		public contratos? : ContratoCabecera[],
	){
		super(id +"", nombreCliente);
	}

	public static fromData(data : any) : CabeceraCliente{
		let o : CabeceraCliente  = new CabeceraCliente(
		 data.id,
		 data.nombreCliente,
		 data.nombreContacto,
		 data.telefono,
		 data.celular,
		 data.email,
		 data.razonSocial,
		 data.cuit,
		 Descriptivo.fromData(data.tipoCliente),
		 data.activo,
		 data.facebook,
		 data.instagram,
		 data.contratos?  data.contratos.map(a => ContratoCabecera.fromData(a)) : [],	);
		return o; 

	}
	public esSocial():boolean{
		return this.tipoCliente && this.tipoCliente.codigo === Cliente.SOCIAL;
	}

}
import { Contrato } from './../../../contratos/model/Contrato';
import { Descriptivo } from './../../../model/Descriptivo';
import { Cliente } from '../../../model/Cliente';

import * as moment from 'moment';


export class ContratoCabecera extends Descriptivo{
	public lookUp: string;
	public fechaContratacionStr : string;
	public fechaEventoStr : string;
	constructor(

		public id? : number,
		public estado? : Descriptivo,
		public codigoHorario? : string,
		public fechaContratacion? : Date,
		public fechaEvento? : Date,
		public locacion? : string,
		public tipoEvento? : string,
		public idCliente? : number,
		public cliente? : Descriptivo,
		public total? : number,
		public cobros? : number,
		public nombreVendedor? : string
	){
		super(id+"", "");
		this.fechaEventoStr  = this.fechaEvento? moment(this.fechaEvento).format("DD/MM/YYYY") : "SD";
		this.fechaContratacionStr  = this.fechaContratacion? moment(this.fechaContratacion).format("DD/MM/YYYY") : "SD";
		
		var _horario : string = this.codigoHorario =="D" ? "Día" : "Noche";
		this.descripcion =  this.locacion + " - " + this.fechaEventoStr + " - " + _horario;

		this.lookUp = this.descripcion+ "/" + cliente.descripcion + "/" +this.tipoEvento + "#"+id;
	}

	public static fromData(data : any) : ContratoCabecera{
		if(!data) return null; 
		let o : ContratoCabecera  = new ContratoCabecera(
		 data.id,
		 Descriptivo.fromData(data.estado),
		 data.codigoHorario,
		 data.fechaContratacion? new Date(data.fechaContratacion) : null,
		 data.fechaEvento? new Date(data.fechaEvento) : null,
		 data.locacion,
		 data.tipoEvento,
		 data.idCliente,
		 data.cliente? Descriptivo.fromData(data.cliente) : Descriptivo.SIN_ASIGNAR(),
		 data.total,
		 data.cobros,	
		 data.nombreVendedor);
		return o; 

	}
	
	get saldo(): number{
		return this.total - this.cobros;
	}
	public descripcionCorta(): string{

		return this.cliente.descripcion + " - " + this.locacion;
	}

	public esAprobado():boolean{
		return this.estado && this.estado.codigo === "A";
	}

	public esCerrado():boolean{
		return this.estado && this.estado.codigo === "L";
	}

}
import { Cliente } from './../../../model/Cliente';
import { StringUtils } from '../../../common-utils/string-utils.class';
import { Contacto } from '../../../model/Contacto';
import { Descriptivo } from '../../../model/Descriptivo';
export class ListaClientes extends Descriptivo {
	constructor(
		public id?: number,
		public nombreCliente?: string,
		public telefono?: string,
		public celular?: string,
		public email?: string,
		public razonSocial?: string,
		public tipoCliente: Descriptivo = new Descriptivo(),
		public facebook?: string,
		public instagram?: string,
		public contactos: Contacto[] = [],
	) {
		super(id + "", nombreCliente);
	}

	public static fromData(data: any): ListaClientes {
		if (!data) return null;
		let o: ListaClientes = new ListaClientes(
			data.id,
			data.nombreCliente,
			data.telefono,
			data.celular,
			data.email,
			data.razonSocial,
			Descriptivo.fromData(data.tipoCliente),
			data.facebook,
			data.instagram,
			data.contactos ? data.contactos.map(c => Contacto.fromData(c)) : [], );
		return o;

	}

	get contactosStr(){
        let str : string ="";
        this.contactos.forEach(c =>str += " " +  StringUtils.getSinTildes(c.nombre) +
                                        " " +   StringUtils.getSinTildes(c.email) + 
                                        " " +   StringUtils.getSinTildes(c.telefono) + 
                                        " " +   StringUtils.getSinTildes(c.numeroDocumento));
        return str;
	}
	public esSocial():boolean{
		return this.tipoCliente && this.tipoCliente.codigo === Cliente.SOCIAL;
	}

}
import { ListaClientes } from './../lista-clientes/shared/lista-clientes.model';
import { Descriptivo } from '../../model/Descriptivo';
import { CabeceraOrdenCobranza } from '../cobros/listado/shared/CabeceraOrdenCobranza';
import { FiltroCliente } from '../filtros/FiltroCliente';
import { Cliente } from '../../model/Cliente';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';


@Injectable()
export class ClienteService extends ServicioAbstract {

   

    public getCabecerasOrdenesCobro(filtro: FiltroCliente): Promise<CabeceraOrdenCobranza[]> {
          if (filtro.fechaDesde > filtro.fechaHasta) {
            return Promise.reject({ "mensaje": "Fecha Desde debe ser menor a la Fecha Hasta" });
        }

        let url = "ordenCobranza/cabeceras/";
        return this.http.post(this.getApiURL() + url,filtro).toPromise().then(this.parseCabecerasOrdenCobranza, this.handleError);
    }

   
    public getAllList(filtro: any): Promise<ListaClientes[]> {
        return this.http.get(this.getApiURL() + "cliente/cabecera")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cabeceras: ListaClientes[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    cabeceras = res.respuesta.map(r => ListaClientes.fromData(r));
                }
                return cabeceras;

            }, this.handleError);

    }
    


    public getById(id: number): Promise<Cliente> {
        return this.http.get(this.getApiURL() + "cliente/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cliente: Cliente;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    cliente = Cliente.fromData(res.respuesta);
                }
                return cliente;

            }, this.handleError);

    }
    public delete(id: any): Promise<any> {
        let params = new HttpParams();
        params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'cliente', { params }).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar(cliente: Cliente): Promise<any> {
        if (cliente.id) {
            return this.http.post(this.getApiURL() + 'cliente', cliente).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'cliente', cliente).toPromise().then(this.handleOk, this.handleError);
        }
    }
    public parseCabecerasOrdenCobranza(response: any): CabeceraOrdenCobranza[] {
        let res = response;
        let cabeceras: CabeceraOrdenCobranza[] = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            cabeceras = res.respuesta.map(c => CabeceraOrdenCobranza.fromData(c));
        }
        return cabeceras;

    }
}
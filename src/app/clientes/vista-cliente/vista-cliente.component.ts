
import { ClienteService } from '../service/ClienteService';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Cliente } from '../../model/Cliente';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'vista-cliente',
	templateUrl: 'vista-cliente.component.html',
	styleUrls: ['vista-cliente.component.less']
})

export class VistaClienteComponent extends SessionComponent {


	public esEditable: boolean = false;
	@Input()
	public cliente :Cliente;
	


	@Input()
	set editable(e: boolean) {
		this.esEditable = e;
	}
	constructor(
		private clienteService: ClienteService) {
		super()
	}
	ngOnInit() {

	}
}
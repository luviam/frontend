import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
	selector: 'autosize-text',
	templateUrl: 'autosize-text.component.html'
})

export class AutosizeTextComponent implements OnInit {
	@ViewChild("svg")
	private svg : any;

	public x0 : number = 100;

	public x1 :number;

	public y0: number = 30;

	public y1 : number;

	public size : string = "0 0 200 50";

	@Input()
	public text : string ="";

	ngOnInit() {
		
		let width = this.svg.nativeElement.parentElement.clientWidth;
		let height = this.svg.nativeElement.parentElement.clientHeight
		this.size = "0 0 " + width+ " " + height;
		this.x0 = width / 2; this.y0 = height / 2;
	 }
}
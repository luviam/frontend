import {Component} from '@angular/core';
	
	@Component({
	    selector: 'descriptivo-label',
	    template: '<ng-content></ng-content>'
	})
	export class DescriptivoLabelComponent {}
import { StringUtils } from '../../common-utils/string-utils.class';
import { Descriptivo } from '../../model/Descriptivo';
import { Component, OnInit, Input, Output, EventEmitter, forwardRef, ElementRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
  selector: 'descriptivo-selector',
  templateUrl: './descriptivo-selector.component.html',
  styleUrls: ['./descriptivo-selector.component.less'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DescriptivoSelectorComponent),
      multi: true
    }
  ]
})

export class DescriptivoSelectorComponent implements OnInit, ControlValueAccessor {
  

  private _data: Descriptivo[] = [];
  @Input()
  set data(val : Descriptivo[]){
    this._data = !val? [] : val;
    
    this._data = this._data.sort((a,b)=> a.descripcion.toLowerCase() > b.descripcion.toLowerCase()? 1: -1);
    
    
    if(val && val.length === 1){
      this.seleccionado = val[0];
      
    }
  }
  get data(): Descriptivo[]{
    return this._data;
  }
  @Input()
  appendTo : string = "";

  @Input()
  inputClass: string = "ui-g-12";

  @Input()
  labelClass: string = "ui-g-12";

  @Input()
  limpiable : boolean = true;

  @Input()
  nombre: string;

  @Input()
  autoselect : boolean = true;

 
  @Input()
  public editable : boolean = true;

  @Output() onSelect:EventEmitter<any> = new EventEmitter<any>();

 private onChangeCallback: (_: any) => void = () => {};

  @ViewChild('autocomplete') autocomplete;

  _seleccionado: any;

  disabled: boolean;

  recomendaciones: Descriptivo[];

  constructor() {
    this.recomendaciones = [];
    
  }
  select(event : Descriptivo){
   this.seleccionado = event;
  }

  clear(){
    this.seleccionado = null;
  }
  get seleccionado() {
    return this._seleccionado;
  }
  set seleccionado(val: Descriptivo) {
    if( val !== this._seleccionado){
      let prev = this._seleccionado;
      this._seleccionado = val;
      this.onChangeCallback(val);
      this.onSelect.emit({prev:prev,new:val});

    }
    
    //this.propagateChange(this._seleccionado);
  }

  public autoselectOpcion(){
    
    if(!this.seleccionado || !this.seleccionado.codigo){
        this.seleccionado = null;
    }
    if(this.autoselect && this.recomendaciones && this.recomendaciones.length ===1 ){
      this.seleccionado = this.recomendaciones[0];
    }
  }
  todos() {
        this.recomendaciones = [];
    
        setTimeout(() => {
          this.recomendaciones = this.data;
        }, 100)
      }

  ngOnInit() {
  }
  propagateChange = (_: any) => {  };

  writeValue(obj: Descriptivo): void {
    if (obj !== this._seleccionado) {
      this.seleccionado = obj;
    }
  }
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }
  registerOnTouched(fn: any): void { }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  

  selectItem(value : Descriptivo) {
    this.seleccionado = value;
  }
 
  get sinDatos() {
    return this.data === undefined || this.data.length == 0;
  }

  public onFocus(event : any){
    event.srcElement.select();
  }

  filtrar(event) {
    this.recomendaciones = this.data
      .filter(s => s.codigo.toLowerCase().includes(event.query.toLowerCase())
        || StringUtils.getSinTildes(s.descripcion).includes(StringUtils.getSinTildes(event.query)));
  }

}
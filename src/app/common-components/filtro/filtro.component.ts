import { Filtro } from './../../model/Filtro';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { state, style, animate, trigger, transition } from '@angular/animations';

@Component({
	selector: 'filtro',
	templateUrl: 'filtro.component.html',
	styleUrls: ["filtro.component.less"],
	
})

export class FiltroComponent implements OnInit {


	@Input()
	public filtro : Filtro;
	
	@Input()
	public verCompleto: boolean = true;

	@Output()
	public onBuscar: EventEmitter<Filtro> = new EventEmitter<Filtro>();
	ngOnInit() { 

	}

	public buscar(){
		this.onBuscar.emit(this.filtro);
	}

	public toggleFiltro(){
		this.verCompleto = !this.verCompleto;
	}
}
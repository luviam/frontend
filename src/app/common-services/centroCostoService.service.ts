import { Cuenta } from '../model/Cuenta';
import { CentroCostoConfig } from '../model/CentroCostoConfig';
import { ActivatedRoute } from '@angular/router';
import { CabceraCentro } from '../model/CabeceraCentro';
import { Descriptivo } from '../model/Descriptivo';
import { CentroCosto } from '../model/CentroCosto';
import { CuentaConfig } from '../model/ConfigCuenta';
import { ConfigComprobanteFiscal } from '../model/ConfigComprobanteFiscal';
import { Http, RequestOptions } from '@angular/http';

import { Injectable } from '@angular/core';
import { ServicioAbstract } from './service.service';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable()
export class CentroCostoService extends ServicioAbstract {


    private tipoComprobante: Descriptivo[] = [
        new Descriptivo("FA", "Factura A"),
        new Descriptivo("FB", "Factura B"),
        new Descriptivo("FC", "Factura C"),
        new Descriptivo("RE", "Recibo")
    ];
  

    public getAllCentros(): Promise<any> {

        return this.http.get(this.getApiURL() + "centro-costo/all").toPromise().then((response: any) => {
            let res = response;
            let cabeceras: CentroCosto[] = [];
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                return res.respuesta.map(c => 
                    new CentroCosto(c.id,c.codigo,c.descripcion, c.cuentasAplicacion.map(cc=> 
                            new Cuenta(cc.codigo,cc.nombre,cc.tipoCuenta, cc.activo,cc.id))
                        )
                     );
            }else{
                return [];
            }
        }).catch(this.handleError);
    }
    public getAllCentrosCabeceras(): Promise<any> {
        let $this: CentroCostoService = this;
        return this.http.get(this.getApiURL() + "centro-costo/cabeceras")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cabeceras: CabceraCentro[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    res.respuesta.forEach(eve => cabeceras.push($this.parseCabeceraCentro(eve)));
                }
                return cabeceras;

            }, this.handleError);

    }

    private parseCabeceraCentro(c: any) {
        let cabecera: CabceraCentro = new CabceraCentro(c.codigo, c.nombre, c.cuit, c.iibb, c.domicilioFiscal, c.id);
        return cabecera;
    }


    private getCuentasAplicacion(centro: CentroCosto): Descriptivo[] {
        return [];
        // return this.getCuentasAplicacionRec(this.centros.filter(c => c.codigo === centro.codigo)[0].cuentas, []);
    }
    private getCuentasAplicacionRec(cuentas: CuentaConfig[], result: Descriptivo[]) {

        cuentas.forEach(c => {
            if (c.leaf) {
                result.push(new Descriptivo(c.codigo, c.descripcion));
            } else {
                //this.getCuentasAplicacionRec(c.cuentasHijas, result);
            }
        });
        return result;
    }

    public getAllCentrosConfig(): Promise<any> {

        return Promise.resolve([]);

    }


    public guardar(centro: CentroCostoConfig) {
        let centroAux: CentroCostoConfig = centro;
        this.eliminarParent(centroAux.cuentas);
        if (centroAux.id) {
            return this.http.post(this.getApiURL() + 'centro-costo', centroAux).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'centro-costo', centroAux).toPromise().then(this.handleOk, this.handleError);
        }
    }


    public eliminar(id: any) {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'centro-costo', {params}).toPromise().then(this.handleOk, this.handleError);
    }



    public findByCodigo(codigoCentroCosto: string): Promise<CentroCostoConfig> {
        let $this = this;
        return this.http.get(this.getApiURL() + "centro-costo/" + codigoCentroCosto)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let centro: CentroCostoConfig;
                if (res !== null && res.respuesta !== null) {
                    centro = $this.parseCentroCosto(res.respuesta);
                }
                return centro;

            }, this.handleError);
    }

    private parseCentroCosto(c: any): CentroCostoConfig {

        let centro = new CentroCostoConfig(
            c.id,
            c.codigoCentro,
            c.descripcionCentro,
            c.razonSocial,
            c.tipoIIBB,
            c.iibb,
            c.cuit,
            c.domicilioFiscal,
            c.codigoPostal,
            c.provincia ? new Descriptivo(c.provincia.codigo, c.provincia.descripcion) : null,
            c.comprobantes ? c.comprobantes.map(e => new ConfigComprobanteFiscal(e.codigo, e.descripcion, e.activo, e.numeracion)) : [],
            c.cuentas.map(cc => new CuentaConfig(cc.codigo, cc.descripcion, cc.idParent, cc.id, cc.activo)),
            c.activo);
        return centro;
    }

    parsePlanRecursivo = (res: any): CuentaConfig[] => {
        let cuentas: CuentaConfig[];
        if (res) {
            cuentas = [];
            //res.forEach(c => cuentas.push(new CuentaConfig(c.id, c.codigo, c.descripcion, c.activo, this.parsePlanRecursivo(c.cuentasHijas))));
        }
        return cuentas;
    }


    private eliminarParent(cuentas: CuentaConfig[]): void {
        cuentas.forEach(cuenta => {
            cuenta.parent = null;
            if (cuenta.cuentasHijas.length > 0) {
                // this.eliminarParent(cuenta.cuentasHijas)
            }
        })

    }
}
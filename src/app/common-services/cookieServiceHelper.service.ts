import { AppUser } from '../model/AppUser';
import { CookieService } from 'ngx-cookie';
import { Injectable } from "@angular/core";

@Injectable()
export class CookieServiceHelper{

    constructor(private cookieService : CookieService){

    }

    public getToken() : string{
        return this.cookieService.getObject("luviamUser") ? (<AppUser>this.cookieService.getObject("luviamUser")).token : undefined;
    }
    
    public getUserApp() :AppUser{
        return this.cookieService.getObject("luviamUser") ? (<AppUser>this.cookieService.getObject("luviamUser")) : undefined;
    }

    public deleteUser() {
        this.cookieService.remove('luviamUser');
    }
}
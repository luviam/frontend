import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { CuentaConfig } from '../model/ConfigCuenta';
import { Cuenta } from '../model/Cuenta';
import { CuentaAplicacion } from '../model/CuentaAplicacion';
import { CuentaTotal } from '../model/CuentaTotal';
import { Descriptivo } from '../model/Descriptivo';
import { OperacionContable } from '../model/OperacionContable';
import { ServicioAbstract } from './service.service';



@Injectable()
export class CuentasService extends ServicioAbstract {

    private cuentas: CuentaTotal[] = [];

    public getSaldo(codigoCuenta: string, fecha: Date): Promise<number> {

        return this.http.get(this.getApiURL() + 'cuentas/saldo/' + codigoCuenta + "/" + fecha).toPromise().then((r: any) => r.respuesta, this.handleError);
    }
    public getOperaciones(filtro: any): Promise<any> {
        let desde = moment(filtro.fechaDesde).startOf('day').format("YYYYMMDD HH:mm");
        let hasta = moment(filtro.fechaHasta).endOf("day").format("YYYYMMDD HH:mm");
        if (filtro.fechaDesde > filtro.fechaHasta) {
            return Promise.reject({ "mensaje": "Fecha Desde debe ser menor a la Fecha Hasta" });
        }
        let idCentro = filtro.centro ? filtro.centro.id : -1;
        return this.http.get(this.getApiURL() + 'cuentas/operaciones/' + desde + "/" + hasta + "/" + filtro.cuenta.id + "/" + idCentro).toPromise().then(this.parseOperaciones, this.handleError);
    }

    public delete(id: number): Promise<any> {
        return this.http.delete(this.getApiURL() + 'cuentas/' + id).toPromise().then(() => { return; }, this.handleError);
    }

    public getTotales(filtro: any): Promise<any> {
        let desde = moment(filtro.fechaDesde).startOf('day').format("YYYYMMDD HH:mm")
        let hasta = moment(filtro.fechaHasta).endOf('day').format("YYYYMMDD HH:mm")
        if (filtro.fechaDesde > filtro.fechaHasta) {
            return Promise.reject({ "mensaje": "Fecha Desde debe ser menor a la Fecha Hasta" });
        }
        let idCentro = filtro.centro ? filtro.centro.id : -1;
        return this.http.get(this.getApiURL() + 'cuentas/totales/' + desde + "/" + hasta + "/" + idCentro + "/").toPromise().then(this.parseTotales, this.handleError);
    }

    public getCuentasDefault(): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/cuentasDefault/').toPromise()
            .then((response: any) => {
                let res = response;
                let cuentas: CuentaConfig[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    cuentas = res.respuesta.map(
                        (r) => new CuentaConfig(r.codigo,
                            r.descripcion,
                            r.idParent,
                            r.id,
                            true,
                            r.eliminable));
                    return cuentas;
                } else {
                    return [];
                }
            }).catch(this.handleError);
    }
    public parseTotales(res: any): CuentaTotal[] {
        const response = res;
        let operaciones: any[] = response.respuesta;

        let rubros: CuentaTotal[] = [];
        operaciones.forEach(o => {
            let r = rubros.find(c => c.id === o.rubroId);
            if (!r) {
                r = new CuentaTotal(o.rubroId, o.rubroCodigo, o.rubroNombre, false, 0, 0, 0, null, false, []);
                rubros.push(r);
            }

            let g = r.cuentasHijas.find(c => c.id === o.grupoId);
            if (!g) {
                g = new CuentaTotal(o.grupoId, o.grupoCodigo, o.grupoNombre, false, 0, 0, 0, null, false, []);
                r.agregarCuentaHija(g)
            }
            let sg = g.cuentasHijas.find(c => c.id === o.subGrupoId);
            if (!sg) {
                sg = new CuentaTotal(o.subGrupoId, o.subGrupoCodigo, o.subGrupoNombre, false, 0, 0, 0, null, false, []);
                g.agregarCuentaHija(sg);
            }
            sg.agregarCuentaHija(new CuentaTotal(o.cuentaId, o.cuentaCodigo, o.cuentaNombre, true, o.debe, o.haber, o.balance));
        })

        return rubros;
    }

    public parseOperaciones(res: any): OperacionContable[] {
        const response = res;
        let operaciones: any[] = response.respuesta;
        return operaciones.map(o => new OperacionContable(o.id,
            new Descriptivo(o.centroCosto.codigo, o.centroCosto.descripcion),
            new Descriptivo(o.cuenta.codigo, o.cuenta.descripcion),
            o.descripcion, o.debe, o.haber, o.comprobante, new Date(o.fecha),
            o.responsable,
            new Descriptivo(o.estado, o.estado),
            o.numeroAsiento,
            o.tipoGenerador
        ));
    }


    public findByCentro(codigoCentroCosto: string): Promise<any> {

        return Promise.resolve([]);
        //return this.http.get(this.getApiURL() + 'centroCosto/findByCodigo/'+codigoCentroCosto).toPromise().then(this.parseCentroCosto, this.handleError);            
    }

    public getAll(): Promise<any> {
        return this.http.get(this.getApiURL() + 'plan/plan-template').toPromise().then(this.parsePlan, this.handleError);
    }

    public getAllBancarias(): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/bancarias').toPromise().then(this.parseCuentas, this.handleError);
    }
    public getAllChequesClientes(): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/chequesClientes').toPromise().then(this.parseCuentas, this.handleError);
    }
    public getAllChequesOtros(): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/chequesOtros').toPromise().then(this.parseCuentas, this.handleError);
    }
    public getAllFacturacionByCentro(idCentro: number): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/facturacion/' + idCentro).toPromise().then(this.parseCuentas, this.handleError);
    }
    public getAllFacturacion(): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/facturacion/-1').toPromise().then(this.parseCuentas, this.handleError);
    }
    public getCallCajas(): Promise<any> {
        return this.http.get(this.getApiURL() + 'cuentas/cajas').toPromise().then(this.parseCuentas, this.handleError);
    }

    public guardarPlan(plan: Cuenta[]): Promise<any> {
        return this.http.post(this.getApiURL() + 'plan/guardar', plan).toPromise();

    }

    private parseCuentas = (res: any): CuentaAplicacion[] => {
        const response = res;
        let cuentas: CuentaAplicacion[] = [];
        if (response.respuesta) {
            cuentas = (<any[]>response.respuesta).map(ca => new CuentaAplicacion(ca.id, ca.codigo, ca.nombre, ca.centroCosto));
        }
        return cuentas;
    }



    private parsePlan = (res: any): CuentaConfig[] => {
        const response = res;
        let cuentas: CuentaConfig[];
        if (response.respuesta) {
            cuentas = this.parsePlanRecursivo(response.respuesta);
        }
        return cuentas;
    }

    private parsePlanRecursivo = (res: any): CuentaConfig[] => {
        let cuentas: CuentaConfig[];
        if (res) {
            cuentas = [];
            //cuentas = res.map(c => new CuentaConfig(c.id, c.codigo, c.descripcion, c.esActiva, this.parsePlanRecursivo(c.cuentasHijas),c.tipoCuenta, c)
            //);

        }
        return cuentas;
    }


}
import { HttpInterceptorHandler } from '@angular/common/http/src/interceptor';

import { HttpClient, HttpHandler, HttpXhrBackend } from '@angular/common/http';
import { Descriptivo } from '../model/Descriptivo';
import { ServicioAbstract } from './service.service';
import { Injectable, Injector } from '@angular/core';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class DescriptivosService extends ServicioAbstract {
  public static injector: Injector = Injector.create([{ provide: DescriptivosService, deps: [] }]);


  getAllTiposComision() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/tipos-comision");
  }

  getAllComisionistas() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/comisionistas");
  }

  getTipoComprobantesFacturables() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-comprobante-facturable");
  }
  getTipoComprobantes(): Promise<any> {
    return this.getDescriptivos("descriptivos/tipo-comprobante");
  }

  getCentrosCosto(): Promise<any>{
    return this.getDescriptivos("descriptivos/centro-costo");
  }

  getProductos(): Promise<any>{
    return this.getDescriptivos("descriptivos/productos");
  }
  getProvincias() : Promise<any>{
    
        return this.getDescriptivos("descriptivos/provincias");
  }

  getCajas() : Promise<any>{
    return this.getDescriptivos("descriptivos/cajas");
  }
  getAllTipoIVA() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-iva");
  }
  
  getAllTiposCliente() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-cliente");
  }

  getAllTiposProveedor() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-proveedor");
  }
  
  getAllTiposCuenta() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-cuenta-proveedor");
  }

  getAllTiposJornada() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-jornada");
  }

  getAllSectores() : Promise<any>{
    return this.getDescriptivos("descriptivos/sectores");
  }

  getAllTiposEmpleado() : Promise<any>{
    return this.getDescriptivos("descriptivos/tipo-empleado");
  }

  getAllUnidadesProducto() : Promise<any>{
    return this.getDescriptivos("descriptivos/unidades-producto");
  }

  getAllCategoriasProducto() : Promise<any>{
    return this.getDescriptivos("descriptivos/categorias-producto");
  }
  getProveedores() : Promise<any>{
    return this.getDescriptivos("descriptivos/proveedores");
  }

  getAllTiposEvento() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/tipos-evento");
  }
  getAllSalones() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/salones");
  }
  getAllEstados() : Promise<Descriptivo[]>{
    let estados: Descriptivo[] = [
      new Descriptivo("D", "Disponible"), 
      new Descriptivo("E", "Entregado"),
       new Descriptivo("R","Rechazado a cobrar"), 
       new Descriptivo("C", "Rechazado cobrado"),
       new Descriptivo("A", "Anulado"),
    ];
    return Promise.resolve(estados);
  }
  getAllClientes() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/clientes");
  }
  getAllGrupos() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/grupos");
  }
  getAllSubGrupos() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/subgrupos");
  }
  getAllRubros() : Promise<Descriptivo[]>{
    return this.getDescriptivos("descriptivos/rubros");
  }
}
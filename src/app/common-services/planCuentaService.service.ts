import { HttpClient } from '@angular/common/http';
import { Grupo } from './../model/Grupo';
import { Cuenta } from '../model/Cuenta';
import { Injectable } from '@angular/core';
import { ServicioAbstract } from './service.service';
@Injectable()
export class PlanCuentaService extends ServicioAbstract{

  
    public getPlanCuentas(): Promise<any> {
        return this.http.get(this.getApiURL() + 'plan/plan-cuentas').toPromise().then(this.parsePlan, this.handleError);
    }

    public getGruposByRubro(rubro: string): Promise<any> {
        return this.http.get(this.getApiURL() + 'plan/grupos/' + rubro).toPromise().then((r : any)=>{
            const response = r;
            let grupos: Grupo[];
            if (response.respuesta) {
                grupos = response.respuesta.map(a => Grupo.fromData(a));
            }
            return grupos;
        }, this.handleError);
    }

    public delete(tipo:string, id:number): Promise<any> {
        return this.http.delete(this.getApiURL() + 'plan/'+tipo+"/"+id).toPromise().then(()=>{return;}, this.handleError);
    }

    public guardarPlan(cuentas : Cuenta[]){
        let planCuenta = {"cuentas": cuentas};
        planCuenta.cuentas.forEach(c => {
            c.parent = null;
            c.cuentasHijas.forEach( cc=>{
                cc.parent = null;
                cc.cuentasHijas.forEach(ccc => {
                    ccc.parent = null;
                })
            })
        }
         );
        return this.http.post(this.getApiURL() + 'plan/guardar', planCuenta).toPromise().then(this.handleOk, this.handleError);
        
    }

    private parsePlan = (res: any): Cuenta[] => {
        const response = res;
        let cuentas: Cuenta[];
        if (response.respuesta) {
            cuentas = this.parsePlanRecursivo(response.respuesta);
        }
        return cuentas;
    }

    private parsePlanRecursivo = (res: any): Cuenta[] => {
        let cuentas: Cuenta[];
        if (res) {
            cuentas = [];
            cuentas = res.map(c => new Cuenta( c.codigo, c.descripcion,c.tipoCuenta ,
                                               c.esActiva, c.id, 
                                               this.parsePlanRecursivo(c.cuentasHijas), c, 
                                               c.eliminable)
            );
            
        }
        return cuentas;
    }

}
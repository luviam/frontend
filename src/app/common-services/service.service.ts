import { GlobalInjector } from './../GlobalInjector';

import { Descriptivo } from '../model/Descriptivo';

import { environment } from 'environments/environment';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

export abstract class ServicioAbstract {

    protected http: HttpClient;
    constructor() {
        this.http = GlobalInjector.InjectorInstance.get<HttpClient>(HttpClient);
    }
    getApiURL() {
        return environment.apiUrl;
    }

    public omitKeys(obj, keys) {
        var dup = {};
        for (var key in obj) {
            if (keys.indexOf(key) == -1) {
                dup[key] = obj[key];
            }
        }
        return dup;
    }
    getDescriptivos(urlServicio: string): Promise<Descriptivo[]> {
        let $this = this;
        return this.http.get(this.getApiURL() + urlServicio)
            .toPromise()
            .then(this.parseDescriptivos, this.handleError);
    }
    protected parseDescriptivos(res: any): Descriptivo[] {

        let descriptivos: Descriptivo[] = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            descriptivos = res.respuesta.map(d => Descriptivo.fromData(d));
        }
        return descriptivos;


    }
    protected handleOk = (response: any): string => {
        let res = response;
        return res.mensaje;
    }
    protected handleError(error: any): Promise<any> {
        return Promise.reject(error.error || error);
    }

}
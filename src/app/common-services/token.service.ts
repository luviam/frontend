import { Injectable } from '@angular/core';
import { CookieServiceHelper } from './cookieServiceHelper.service';

@Injectable()
export class TokenService {
    private cookieServiceHelper : CookieServiceHelper;
    constructor(cookieServiceHelper:CookieServiceHelper) { 
        this.cookieServiceHelper = cookieServiceHelper;
    }

    getAsyncToken(){
        return this.cookieServiceHelper.getToken();
    }

}
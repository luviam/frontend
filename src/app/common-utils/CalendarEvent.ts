import { Descriptivo } from '../model/Descriptivo';
import { ContratoCabecera } from '../clientes/lista-clientes/shared/ContratoCabecera';
export class CalendarEvent extends Descriptivo{

    public onClick : (r)=>{};
    constructor(
        public id? : number,
        public title? : string,
        public allDay : boolean = false,
        public start? :Date,
        public end? :Date,
        public className? : string,
        public fuente? :any
        

    ){
        super(id+"",title)
    }
}
export class DateUtils {

    static createDateFromHour(hour){
        if (hour && hour.indexOf(":")>0){
            var dateHour = new Date();
            dateHour.setHours(parseInt(hour.split(":")[0]));
            dateHour.setMinutes(parseInt(hour.split(":")[1]));
            return dateHour;
        }
        return undefined;
    }

}
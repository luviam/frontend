import { Descriptivo } from '../model/Descriptivo';
export class DescriptivoHelper {
    public static parseDescriptivo(res: any) : Descriptivo[] {
        let response = res;
        let descriptivos: Descriptivo[] = [];
        if (response !== null && response.respuesta !== null) {
            var $this = this;
            response.respuesta.forEach(e => descriptivos.push(new Descriptivo(e.codigo, e.descripcion)));
        }
        return descriptivos;
    }
}
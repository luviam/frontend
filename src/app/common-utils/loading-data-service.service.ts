import { Injectable, Injector, ReflectiveInjector } from '@angular/core';

@Injectable()
export class LoadingService {
    public loading: boolean = false;
    public loadingCount: number = 0;

    public static injector: Injector = Injector.create([{ provide: LoadingService, deps: [] }, {provide : Location, deps:[]}]);

    public resetLoadingCount(){
        this.loadingCount = 0;
        this.loading = false;

    }
    public addLoadingCount() {
        this.loadingCount++;
        if (this.loadingCount > 0) {
            this.loading = true;
        }
    }

    public susLoadingCount() {
        if (this.loadingCount > 0) {
            this.loadingCount--;
            if (this.loadingCount <= 0) {
                this.loading = false;
            }
        } else {
            this.loading = false;
        }
    }
}
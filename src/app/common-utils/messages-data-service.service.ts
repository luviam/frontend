import { LoadingService } from './loading-data-service.service';
import { Message } from 'primeng/primeng';
import { Injectable, Injector, ReflectiveInjector } from '@angular/core';

@Injectable()
export class MessagesService{
    public messages : Message[] = [];
    public loadingService : LoadingService;
    public _life : number = 3000;
    public static injector : Injector = Injector.create([{ provide: MessagesService, deps: [] }]);
    constructor(){
        this.loadingService = LoadingService.injector.get(LoadingService);
    }
    get life():number{
        return this._life;
    }
    set life(val : number){
        this._life = val;
    }
    public errorHandler = (error, msgs?:Message[]) => {
        console.log(error);
        let _error = {mensaje:""};
        if(typeof error === "string" || !error.mensaje){
            _error.mensaje = "Error Interno";
        
        }else{
            _error = error;
        }
        this.loadingService.resetLoadingCount();
        msgs = [];
        msgs.push({ severity: 'error', 
                detail: _error.mensaje });
        this.messages = [];
        this.messages.push({ severity: 'error', 
                detail: _error.mensaje });
                
    }
    public cleanMessages(){
        this.messages = [];
    }
    public success(mensaje?:string , titulo?:string){
        this.messages.push({ severity: 'success', 
        detail: mensaje, summary: titulo });
    }
    public error(mensaje?:string , titulo?:string){
        this.messages.push({ severity: 'error', 
        detail: mensaje, summary: titulo });
    }
    public info(mensaje?:string , titulo?:string){
        this.messages.push({ severity: 'info', 
        detail: mensaje, summary: titulo });
    }
    public warning(mensaje?:string , titulo?:string){
        this.messages.push({ severity: 'warning', 
        detail: mensaje, summary: titulo });
    }

}
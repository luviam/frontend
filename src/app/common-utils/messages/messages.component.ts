import { Message } from 'primeng/primeng';
import { MessagesService } from '../messages-data-service.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'messages',
	templateUrl: 'messages.component.html'
})

export class MessagesComponent implements OnInit {

	private messagesService : MessagesService;
	
	constructor(){
		this.messagesService = MessagesService.injector.get(MessagesService);
	}
	ngOnInit() { 

	}

	get life():number{
		return this.messagesService.life;

	}
	
	get messages(): Message[]{
		return this.messagesService.messages;
	}

	set messages(val : Message[]){
		this.messagesService.messages = val;
	}
}
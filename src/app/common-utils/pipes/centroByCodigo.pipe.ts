import { CentroCosto } from '../../model/CentroCosto';
import { PipeTransform, Pipe } from "@angular/core";

@Pipe({ name: 'centroByCodigo' })
export class CentroByCodigo implements PipeTransform {
  transform(centros: CentroCosto[], codigo:string) {
    return centros.filter(c => c.codigo ===codigo);
  }
}

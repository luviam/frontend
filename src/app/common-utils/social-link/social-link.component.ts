import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'social-link',
	templateUrl: 'social-link.component.html',
	styleUrls: ["social-link.component.less"]
})

export class SocialLinkComponent implements OnInit {
	
	public url : string;
	private _type : string = "F";
	private _baseUrl : string ="";
	@Input()
	public iconOnly:boolean = false;

	@Input()
	set type(val : string){
		
		if(val !== "F" && val !== "I"){
			console.log("Tipo de red invalido. Default Facebook");
			this._type= "F";
		}else{
			this._type = val;
		}
		this.updateUrl();
	} 

	get type(): string{
		return this._type;
	}
	@Input()
	set baseUrl(val : string){
		if(val){
			this._baseUrl = val.replace("@","").replace("#","");
			let parts : string[] = this._baseUrl.split("/",4);
			this._baseUrl = parts[parts.length-1];
			this.updateUrl();
		}else{this._baseUrl = ""}
		
		
	}

	get baseUrl():string{
		return this._baseUrl;
	}

	
	ngOnInit() { }

	private updateUrl(){
		if(this.type === "I"){
			this.url = "https://www.instagram.com/";
		}else{
			this.url = "https://www.facebook.com/";
		}
		this.url += this.baseUrl ;
	}
}
export class StringUtils {

    public static capitalize = (cadena:string)=>{
        return cadena.toLowerCase().replace(/\b\w/g, function(l){ return l.toUpperCase() });
    }

    public static isMailValid = (email:string)=>{
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

public static getSinTildes(original :string ) : string{
        let str :string = original ? original.toLowerCase() : "";
            str = str.replace(new RegExp(/[àáâãäå]/g),"a")
                .replace(new RegExp(/[èéêë]/g),"e")
                .replace(new RegExp(/[ìíîï]/g),"i")
                .replace(new RegExp(/[òóôõö]/g),"o")
                .replace(new RegExp(/[ùúûü]/g),"u");
        return str;
    }
}
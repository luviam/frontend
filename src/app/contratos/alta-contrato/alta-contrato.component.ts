import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';

import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { MenuItem } from 'primeng/primeng';

import { Descriptivo } from '../../model/Descriptivo';
import { CuotaServicio } from '../model/CuotaServicio';
import { ServicioContratado } from '../model/ServicioContratado';
import { Cliente } from '../../model/Cliente';

import { Empleado } from '../../model/Empleado';
import { Salon } from '../../model/Salon';

import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contrato } from '../model/Contrato';
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import * as moment from 'moment';
import { Producto } from '../../model/Producto';
import { ContratosService } from '../service/contratos.service';
import { Comision } from '../model/Comision';

@Component({
	selector: 'alta-contrato',
	templateUrl: 'alta-contrato.component.html',
	styleUrls: ['./alta-contrato.component.less'],
	providers:[ ContratosService]
})

export class AltaContratoComponent extends SessionComponent {
	public editable: boolean = true;
	private _contrato : Contrato = new Contrato();

	@Output()
	public onCancelar : EventEmitter<Boolean> = new EventEmitter<Boolean>();
	
	@Output()
	public onGuardar : EventEmitter<Contrato> = new EventEmitter<Contrato>();

	@Input()
	set  contrato (contrato: Contrato){
		if(contrato){
			this._contrato = contrato;
			
		}
		
		
	}

	get contrato(): Contrato{
		return this._contrato;
	}
	

	public activeIndex : number = 0;
	public pasos : MenuItem[] =[]; 
	
	
	get titulo():string{
		switch (this.activeIndex) {
			case 0:
					return "Datos de Cabecera";
			case 1:
					return "Servicios Contratados";
			case 2:
					return "Productos Adicionales";
			case 3:
					return "Comisiones"
			case 4:
					return "Confirmación"
			default:
				break;
		}
	}

	constructor(cookieService :CookieServiceHelper,
				public contratoService: ContratosService,
				public confirmService : ConfirmationService,
				private router: Router ) {
		super();
	}
	ngOnInit() {
		this.activeIndex = 0;
		this.pasos = [{
			label: 'Cliente',
			command: (event: any) => {
				this.activeIndex = 0;
			}
		},
		{
			label: 'Servicio Contratado',
			command: (event: any) => {
				this.activeIndex = 1;
			}
		},
		{
			label: 'Productos',
			command: (event: any) => {
				this.activeIndex = 2;
			}
		},
		{
			label: 'Comisiones',
			command: (event: any) => {
				this.activeIndex = 3;
			}
		},
		{
			label: 'Confirmación',
			command: (event: any) => {
				this.activeIndex = 4;
			}
		}
	];
		/*let $this = this;
		this.contratoService.getById(17).then((r)=>{
			$this.contrato = r;
			$this.activeIndex = 2;
			
		})*/;
		if(this.contrato.esAprobado){
			this.activeIndex = 2
		}

	}
	
	public cancelarContrato() {
		this.confirmService.confirm({
			key: "acConf",
		  header: "Cancelar Contrato",
		  message: 'Se perderán los cambios realizados. Desea continuar?',
		  accept: () => {
			this.onCancelar.emit(true);
		  }
		});
	  }
	public guardarContrato() {
		if(this.validarContrato()){
			this.contrato.servicios.forEach(s =>{
				s.planPago.forEach(p => {
					p.parent = null;
					if(!p.id && (!p.saldo || p.saldo == 0)){
						p.saldo = p.total;
					};
				});
				s.cantidad = s.producto.codigoUnidad === 'PORC'? s.cantidadStr / 100 : s.cantidadStr;
				
			})
			this.contrato.comisiones.forEach(c =>{
				c.totalAComisionar = c.updateTotalComision(this.contrato.getServicio(c.producto.codigo), this.contrato);
				if(c.id && (!c.saldoComision|| c.saldoComision === 0)){
					c.saldoComision = c.totalAComisionar;
				}
			})
			if(this.contrato.sadaic.total === 0){
				this.contrato.quitarServicio(this.contrato.sadaic);
			}
			if(this.contrato.garantia.total === 0){
				this.contrato.quitarServicio(this.contrato.garantia);
			}
			var _salon : string = this.contrato.salon? this.contrato.salon.descripcion : "SD";
			var fecha : string = this.contrato.fechaEvento? moment(this.contrato.fechaEvento).format("DD/MM/YYYY") : "SD";
			var _horario : string = this.contrato.horario? this.contrato.horario.descripcion : "SD;"
			this.contrato.descripcion =  _salon + " - " + fecha + " - " + _horario;
			this.contrato.totalCalculado = this.contrato.total;
			this.contrato.ipMonto = this.contrato.ipTotal;
			let $this = this;

			this.addLoadingCount();
			this.contratoService.guardar(this.contrato).then(r =>{
				$this.contrato = r;
				$this.contrato.servicios.forEach(s =>{
					s.planPago.forEach(p => p.parent = s);
				});
				$this.contrato.servicios = [...this.contrato.servicios];
				$this.susLoadingCount();
				$this.success("El contrato se guardó correctamente");
				this.onGuardar.emit(r);
			}).catch(this.errorHandler);

		}
	}
	public validarContrato(){
		return this.cabeceraIsValid &&
			this.eventoIsValid &&
			this.serviciosIsValid;
	}
	
	get stepIsValid():boolean{
		switch (this.activeIndex) {
			case 0:
					return this.cabeceraIsValid;
			case 1:
					return this.eventoIsValid;
			case 2:
					return this.serviciosIsValid;
			case 3:
					return this.comisionesIsValid;
			case 4:
					return false;
			default:
				break;
		}
		return false;
	}

	
	get cabeceraIsValid()  : boolean{
		return 	this.contrato &&
				this.contrato.cliente && this.contrato.cliente.codigo &&
				this.contrato.fechaContratacion && 
				this.contrato.numeroPresupuesto &&
				this.contrato.vendedor && this.contrato.vendedor.codigo != undefined;

	}
	get serviciosIsValid()  : boolean{
		return 	this.contrato != undefined &&
				this.contrato.cliente != undefined && this.contrato.cliente.codigo != undefined &&
				this.contrato.fechaContratacion != undefined && 
				this.contrato.vendedor != undefined;

	}
	get eventoIsValid() : boolean{
		return this.contrato != undefined &&
				this.contrato.baseInvitados > 0 &&
				this.contrato.tipoEvento != undefined && this.contrato.tipoEvento.codigo &&
				this.contrato.horario != undefined && this.contrato.horario.codigo != undefined &&
				this.contrato.salon != undefined && this.contrato.salon.codigo != undefined
				&& this.contrato.fechaEvento != undefined &&
				this.contrato.fechaContratacion <= this.contrato.fechaEvento;

	}

	get comisionesIsValid() : boolean{
		return !this.contrato.comisiones.some(c => c.porcentajeComision <= 0 || (!c.cobraAlInicio && !c.cobraAlLiquidar && !c.cobraPorCobro));
	}
}
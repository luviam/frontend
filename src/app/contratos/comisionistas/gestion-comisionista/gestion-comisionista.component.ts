import { Empleado } from '../../../model/Empleado';
import { Salon } from '../../../model/Salon';
import { EmpleadosService } from '../../../empleados/service/EmpleadosService';
import { SalonService } from '../../../salon/service/salon.service';
import { ComisionistaService } from '../../service/comisionista.service';
import { CentroCostoService } from '../../../common-services/centroCostoService.service';
import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { CentroCosto } from '../../../model/CentroCosto';
import { Descriptivo } from '../../../model/Descriptivo';
import { Comisionista } from '../../model/Comisionista';
import { MenuItem } from 'primeng/primeng';
import { SessionComponent } from '../../../session-component.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TipoComisionistaService } from '../../../parametricos/tipo-comisionista/service/tipo-comisionista.service';
import { TipoComisionista } from '../../../model/TipoComisionista';

@Component({
	selector: 'gestion-comisionista',
	styleUrls: ["./gestion-comisionista.component.less"],
	templateUrl: 'gestion-comisionista.component.html'
})

export class GestionComisionistaesComponent extends SessionComponent {
	public acciones: MenuItem[];
	private _comisionista: Comisionista;

	@Input()
	set comisionista(val: Comisionista) {
		if (val) {
			this._comisionista = val;
			this.comisionistaChange.emit();
		}
	}
	get comisionista(): Comisionista {
		return this._comisionista;
	}

	@Output()
	public comisionistaChange: EventEmitter<Comisionista> = new EventEmitter<Comisionista>();

	@Output()
	public onGuardar : EventEmitter<Comisionista> = new EventEmitter<Comisionista>();

	@Output()
	public onCancelar : EventEmitter<Boolean> = new EventEmitter<Boolean>();

	@Input()
	public editable: boolean = false;

	public vendedores: Empleado[] =[];
	public salones: Salon[] =[];

	private tiposComisionista: TipoComisionista[] = [];
	private tipoIVA: Descriptivo[] = [];
	
	public editando: boolean = false;
	public finalizado: boolean = false;
	public titulo: string = "";
	constructor(
		private confirmationService: ConfirmationService,
		private descriptivosService: DescriptivosService,
		private comisionistaService: ComisionistaService,
		private salonService:SalonService,
		private empleadoService:EmpleadosService,
		private tipoComisionistaService: TipoComisionistaService) {
		super("gestion-comisionista");
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		this.addLoadingCount();
		this.empleadoService.getAllList({tipoEmpleado: new Descriptivo(Empleado.VENDEDOR_CODIGO,"")}).then(r=>{
			$this.vendedores = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		this.addLoadingCount();
		this.salonService.getAll().then(r=>{
			$this.salones = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		

		this.addLoadingCount();
		this.tipoComisionistaService.getAll().then((res) => {
			if (res) {
				$this.tiposComisionista = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllTipoIVA().then((res) => {
			if (res) {
				$this.tipoIVA = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		

	}
	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				$this.onCancelar.emit(true);
				
			}
		});
	}

	public updateTipoComisionista(event: any) {

	}

	public updateTipoCuenta(event: any) {

	}

	public guardarComisionista() {
		let $this = this;
		if (this.comisionistaValido(this.comisionista)) {
			this.addLoadingCount();
			if (this.comisionista.tipoComisionista.codigo === TipoComisionista.SALON_CODIGO) {
				this.comisionista.vendedor = null;
			}else if (this.comisionista.tipoComisionista.codigo === TipoComisionista.VENDEDOR_CODIGO) {
				this.comisionista.salon = null;
			}else{
				this.comisionista.vendedor = null;
				this.comisionista.salon = null;
			}
			

			this.comisionistaService.guardar(this.comisionista).then((c) => {
				$this.success("El comisionista fue guardado correctamente");
				$this.finalizado = true;
				setTimeout(() => {
					$this.onGuardar.emit(c);
				}, 3000)
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El comisionista no es valido");
		}
	}
	

	public comisionistaValido(comisionista: Comisionista): boolean {
		return true;
	}

	
}
import { LiquidacionComisionesService } from './../../liquidacion-comision/service/LiquidacionComisionesService.service';
import { TipoComisionista } from '../../../model/TipoComisionista';
import { ComisionistaService } from '../../service/comisionista.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { FiltroComisionista } from '../../model/filtros/FiltroComisionista';
import { CabeceraComisionista } from '../../model/CabeceraComisionista';
import { SessionComponent } from '../../../session-component.component';
import { Comisionista } from '../../model/Comisionista';

import { MenuItem, Menu } from 'primeng/primeng';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Component, OnInit, ViewChild } from '@angular/core';



@Component({
	selector: 'lista-comisionistas',
	templateUrl: 'lista-comisionistas.component.html',
	styleUrls: ['lista-comisionistas.component.less']
})
export class ListaComisionistasComponent extends SessionComponent implements OnInit {
	@ViewChild("menu")
	private menu: Menu;
	public items: MenuItem[];
	public comisionistaSeleccionado: CabeceraComisionista;
	public comisionistaEditado: Comisionista;
	listaComisionistas: CabeceraComisionista[] = [];
	
	titulo: string = "Listado de Comisionistas"
	private filtro: FiltroComisionista = new FiltroComisionista();
	private filtroDocumentos: FiltroComisionista = new FiltroComisionista();
	public acciones: MenuItem[] = [];
	public tituloDocumentos: string = "";
	constructor(
		private comisionistaService: ComisionistaService,
		private liquidacionService : LiquidacionComisionesService,
		public route: ActivatedRoute,
		private confirmationService: ConfirmationService) { super( "lista-comisionistas") }

	ngOnInit() {
		this.items = [
			{
				label: 'Editar', icon: 'fa-pencil', command: (event) => {
					this.editarComisionista(this.comisionistaSeleccionado);
				}
			}
		];

		if (this.esAdministrador) {
			this.items.push(
				{
					label: 'Eliminar', icon: 'fa-trash', command: (event) => {
						this.confirmarEliminar(this.comisionistaSeleccionado);
					}
				}
			);
		}
		this.getListado();
		this.acciones = [{
			label: "", icon: "fa fa-bars", items: [
				{
					label: "Nuevo Comisionista",
					icon: 'fa-plus',
					command: (event) => {
						this.nuevoComisionista();
					}
				},
				{
					label: "Generar Liquidación Mensual",
					icon: 'fa-money',
					command: (event) => {
						this.generarMensuales();
					},
					disabled: !this.esAdministrador
				}
				]
		}];
		
		let url = this.route.snapshot.url.join('');
		if (url.includes("nuevo")) {
			this.nuevoComisionista();
		}
	}

	public confirmarEliminar(comisionista: CabeceraComisionista) {
		let $this = this;
		let mensaje : string;
		if(comisionista.tipoComisionista.codigo === TipoComisionista.VENDEDOR_CODIGO){
			mensaje = "Esta operación también eliminara al Empleado " + comisionista.nombre + " del sistema. Desea continuar?"
		}else{
			mensaje = 'Esta operación no puede deshacerse. Desea continuar? ';
		}
		this.confirmationService.confirm({
			key: "genConf",
			header: "Va a eliminar al Comisionista " + comisionista.nombre,
			message: mensaje,
			accept: () => {
				$this.addLoadingCount();
				$this.comisionistaService.delete(comisionista.id).then((res) => {
					$this.getListado();
					$this.success("El comisionista fue eliminado correctamente");
					$this.susLoadingCount();
				}).catch($this.errorHandler);
			}
		});
	}


	public cancelarEdicion() {
		this.titulo = "Lista de Comisionistas";
		this.volver();
	}
	public getListado() {
		let $this = this;
		this.addLoadingCount();
		this.comisionistaService.getAllList(this.filtro).then((res) => {
			$this.listaComisionistas = [...res];
			if ($this.comisionistaSeleccionado) {
				$this.comisionistaSeleccionado =
					$this.listaComisionistas.filter(p => p.id == this.comisionistaSeleccionado.id)[0];
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	public editarComisionista(comisionista: CabeceraComisionista) {
		let $this = this;
		this.addLoadingCount();
		this.comisionistaService.getById(comisionista.id).then(r=>{
			$this.comisionistaEditado = r;
			
			$this.goTo("gestion-comisionista");
			$this.titulo ="Editando a " + r.nombre;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}

	public comisionistaGuardado(c : Comisionista){
		this.getListado();
		this.titulo = "Lista de Comisionistas";
		this.volver();
	}

	public nuevoComisionista() {
		this.comisionistaEditado = new Comisionista();
		this.comisionistaEditado.activo = true;
		this.titulo = "Nuevo Comisionista";
		this.goTo("gestion-comisionista");
	}

	public mostrarAcciones(event: any, comisionista: CabeceraComisionista) {
		this.comisionistaSeleccionado = comisionista;
		this.menu.show(event);
	}

	public generarMensuales(){

		if(this.esAdministrador){
			let $this = this;
			this.addLoadingCount();
			this.liquidacionService.generarMensuales().then(r=>{
				$this.success("Se generaron las liquidaciones del mes");
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
	}
}

export class ListaProveedores {
	constructor(public id: number,
		public nombre: string,
		public tipoProveedor: string,
		public tipoCuenta: string,
		public contacto: string,
		public telefono : string,
		public celular : string,
		public email : string,
		public _saldo : number = 0) {
		
	}
	public static fromData(data : any) : ListaProveedores{
		if(!data) return null;

		let lp : ListaProveedores = 
			new ListaProveedores(
			data.id,
			data.nombre,
			data.descripcionTipoProveedor,
			data.descripcionTipoCuenta,
			data.contacto,
			data.telefono,
			data.celular,
			data.email,
			data.saldo);
		return lp;

	
	}
	get saldo(){
		return this._saldo || 0;
	}
	set saldo(n : number){
		if(n){
			this._saldo = n;
		}else{
			this._saldo = 0;
		}
	}
}
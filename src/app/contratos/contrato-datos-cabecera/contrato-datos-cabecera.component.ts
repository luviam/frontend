import { CabeceraCliente } from '../../clientes/lista-clientes/shared/CabeceraCliente';

import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Descriptivo } from '../../model/Descriptivo';
import { ClienteService } from '../../clientes/service/ClienteService';
import { EmpleadosService } from '../../empleados/service/EmpleadosService';
import { Empleado } from '../../model/Empleado';
import { Contrato } from '../model/Contrato';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ListaClientes } from '../../clientes/lista-clientes/shared/lista-clientes.model';

@Component({
	selector: 'contrato-datos-cabecera',
	templateUrl: 'contrato-datos-cabecera.component.html',
	styleUrls: ["contrato-datos-cabecera.component.less"]
})

export class ContratoDatosCabeceraComponent extends SessionComponent {


	public _contrato : Contrato;
	@Input()
	public editable : boolean = false;

	@Input()
	set contrato(c : Contrato){
		this._contrato = c;
		if(c.cliente){
			this._clienteSeleccionado = ListaClientes.fromData( c.cliente);
		}
	};

	get contrato():Contrato{
		return this._contrato;
	}

	

	@Output()
	public contratoChange : EventEmitter<Contrato> = new EventEmitter<Contrato>();
	
	@Input()
	public vendedores: Descriptivo[] = [];
	@Input()
	public clientes: ListaClientes[] = [];

	private _clienteSeleccionado : ListaClientes;

	constructor(cookieService :CookieServiceHelper,
				private empleadosService :EmpleadosService, 
				private clienteService : ClienteService) {
		super();
	}
	ngOnInit() {
		let $this = this;
		if(this.vendedores.length === 0){
			this.addLoadingCount();
			this.empleadosService.getAllList({tipoEmpleado: new Descriptivo(Empleado.VENDEDOR_CODIGO,"Vendedor")}).then((res)=>{
				$this.vendedores = res;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
		if(this.clientes.length === 0){
			this.addLoadingCount();
			this.clienteService.getAllList({}).then((res)=>{
				$this.clientes = res;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}

	 }

	 get clienteSeleccionado() :ListaClientes{
		 return this._clienteSeleccionado;
	 }
	 set clienteSeleccionado(c: ListaClientes) {
		if (c && c.id) {
			let $this = this;
			this._clienteSeleccionado = c;
			this.addLoadingCount();
			this.clienteService.getById(c.id).then((cli) => {
				$this.contrato.cliente = cli;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}else{
			this.contrato.cliente = null;
		}

	};
}

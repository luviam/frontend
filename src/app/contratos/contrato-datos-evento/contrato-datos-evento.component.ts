import { Producto } from '../../model/Producto';
import { ServicioContratado } from '../model/ServicioContratado';
import { SalonService } from '../../salon/service/salon.service';
import { Salon } from '../../model/Salon';
import { ProductoService } from '../../producto/service/producto.service';

import { DescriptivosService } from '../../common-services/descriptivos.service';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Descriptivo } from '../../model/Descriptivo';
import { Contrato } from '../model/Contrato';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
	selector: 'contrato-datos-evento',
	templateUrl: 'contrato-datos-evento.component.html',
	styleUrls: ["contrato-datos-evento.component.less"]

})

export class ContratoDatosEventoComponent extends SessionComponent {

	public _contrato: Contrato;
	@Input()
	public editable: boolean = false;

	@Input()
	set contrato(c: Contrato) {
		this._contrato = c;
	};

	get contrato(): Contrato {
		return this._contrato;
	}

	@Output()
	public contratoChange: EventEmitter<Contrato> = new EventEmitter<Contrato>();

	public tiposEvento: Descriptivo[] = [];
	public tiposHorario: Descriptivo[] = [new Descriptivo("D", "Día"), new Descriptivo("N", "Noche")];
	public tiposServicio: Descriptivo[] = [
		new Descriptivo("MENU", "Solo Menú"),
		new Descriptivo("SALON", "Solo Salón"),
		new Descriptivo("PACK", "Paquete")
	];

	public productos: Producto[] = [];
	public salones: Salon[] = [];

	public _salon: Producto;
	public _menu: Producto;


	public menuElegido: Producto;
	public bebidaElegida: Producto;

	public servicioSalon: ServicioContratado;
	public servicioMenu: ServicioContratado;
	public servicioBebida: ServicioContratado;

	get baseInvitados(): number {
		return this.contrato.baseInvitados;
	}
	set baseInvitados(base: number) {
		this.contrato.baseInvitados = base;
		this.contrato.adultos = base;
		this.updateCantidadesServicio(base);
	}

	public tipoServicioSeleccionado: Descriptivo;
	private _incluyeBebida: boolean = false;
	private _incluyeMenu: boolean = false;
	private _incluyeAlquiler: boolean = false;

	get incluyeMenu(): boolean {
		return this._incluyeMenu;
	}

	set incluyeMenu(state: boolean) {
		this._incluyeMenu = state;
		if (!state && this.servicioMenu) {
			this.contrato.quitarServicio(this.servicioMenu);
			
		}
	}
	get incluyeBebida(): boolean {
		return this._incluyeBebida;
	}

	set incluyeBebida(state: boolean) {
		this._incluyeBebida = state;
		if (!state && this.servicioBebida) {
			this.contrato.quitarServicio(this.servicioBebida);
		}
	}
	get incluyeAlquilerDisabled(): boolean {
		return !this.contrato || !this.contrato.salon || this.contrato.salon.codigo === Salon.OTRO;
	}
	get incluyeAlquiler(): boolean {
		return this._incluyeAlquiler;
	}
	set incluyeAlquiler(state: boolean) {
		this._incluyeAlquiler = state;
		if (!state) {
			if(this.servicioSalon){
				this.contrato.quitarServicio(this.servicioSalon);
				this.contrato.quitarComisiones([this.servicioSalon]);
			}
			
		} else{
			if(this.contrato.salon && this.contrato.salon.productoAlquiler){
				this.onSalonSeleccionado(this.contrato.salon);
			}
		}
	}

	constructor(
		
		private productoService: ProductoService,
		private salonService: SalonService) {
		super();
		this.servicioBebida = new ServicioContratado();
		this.servicioMenu = new ServicioContratado();
		this.servicioSalon = new ServicioContratado();
	}
	ngOnInit() {

		let $this = this;

		$this.addLoadingCount();
		$this.descriptivoService.getAllTiposEvento().then((res) => {
			$this.tiposEvento = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		$this.addLoadingCount();
		$this.productoService.getAll().then((res) => {
			$this.productos = res;
			$this.susLoadingCount();


			if ($this.contrato) {
				$this.servicioSalon = $this.contrato.getServiciosPack().filter(c => c.producto.categoria.codigo === Producto.ALQUILER)[0];
				$this.incluyeAlquiler = $this.servicioSalon != undefined;
				if(!$this.servicioSalon){
					$this.servicioSalon = new ServicioContratado(
						null, $this.contrato.fechaContratacion, null,
						[], 0, $this.contrato.baseInvitados, Producto.PACK.codigo, null, true
					);
				}
	
				$this.servicioMenu = $this.contrato.getServiciosPack().filter(c => c.producto.categoria.codigo === Producto.MENU)[0];
				$this.incluyeMenu = $this.servicioMenu != undefined;
				if(!$this.servicioMenu){
					$this.servicioMenu = new ServicioContratado(
						null, $this.contrato.fechaContratacion, null, [], 0, $this.contrato.baseInvitados, Producto.PACK.codigo, null, true
					);
				}
	
				$this.servicioBebida = $this.contrato.servicios.filter(c => c.producto.categoria.codigo === Producto.BEBIDA_BASE)[0]; 
				$this.incluyeBebida = $this.servicioBebida != undefined;
				if(!$this.servicioBebida){
					$this.servicioBebida = new ServicioContratado(
						null, $this.contrato.fechaContratacion, null, [], 0, $this.contrato.baseInvitados, Producto.BEBIDA_BASE, null, true
					);
				}
			}
			
			
			
			
		}).catch(this.errorHandler);

		$this.addLoadingCount();
		$this.salonService.getAll().then((res) => {
			$this.salones = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		if (!$this.contrato.horario) {
			$this.contrato.horario = $this.tiposHorario[1];
		}
		

	}
	public seleccionarMenu(event: any) {
		if (event.new && event.prev !== event.new) {
			this.servicioMenu.cantidad = this.servicioMenu.cantidad > 0? this.servicioMenu.cantidad : this.contrato.baseInvitados;
			this.servicioMenu.fechaContrato = this.servicioMenu.fechaContrato? this.servicioMenu.fechaContrato : this.contrato.fechaContratacion;
			this.contrato.quitarServicios([this.servicioMenu]);
			this.contrato.addServicio(this.servicioMenu);
		}


	}

	public seleccionarBebida(event: any) {
	if (event.new && event.prev !== event.new) {
			this.servicioBebida.cantidad = this.servicioBebida.cantidad > 0? this.servicioBebida.cantidad : this.contrato.baseInvitados;
			this.servicioBebida.fechaContrato = this.servicioBebida.fechaContrato? this.servicioBebida.fechaContrato : this.contrato.fechaContratacion;
			this.contrato.quitarServicios([this.servicioBebida]);
			this.contrato.addServicio(this.servicioBebida);
		}

	}

	private updateCantidadesServicio(base: number) {
		this.contrato.servicios.filter(s => s.grupo && s.producto && s.producto.codigoUnidad !== 'PORC').forEach(s => {
			s.cantidad = base;
		}
		)
	}

	public changeHorario(event: any) {
		if (event.chequed) {
			this.contrato.horario = this.tiposHorario[1];
		} else {
			this.contrato.horario = this.tiposHorario[0];
		}
	}

	public onSalonSeleccionado(salon: Salon) {
		if (salon) {
			if(!this.servicioSalon){
				this.servicioSalon = new ServicioContratado(
					null, this.contrato.fechaContratacion, null,
					[], 0, this.contrato.baseInvitados, Producto.PACK.codigo, null, true
				);
			}
		 	if (salon.codigo != Salon.OTRO) {

				if(this.servicioSalon.producto != salon.productoAlquiler){
					this.servicioSalon.producto = salon.productoAlquiler;
					if (salon.productoAlquiler.unidad.codigo === 'PORC') {
						this.servicioSalon.cantidad = 1;
					} else {
						this.servicioSalon.cantidad = this.contrato.baseInvitados;
					}
					this.servicioSalon.fechaContrato = this.contrato.fechaContratacion;
					this.contrato.quitarServicios([this.servicioSalon]);
					this.contrato.addServicio(this.servicioSalon);
				}
			} else {
				this.contrato.quitarServicios([this.servicioSalon]);
			}
		}

	}

	public esPaqueteArmable(): boolean {
		return !this.contrato.id || this.contrato.esEditable
	}
}
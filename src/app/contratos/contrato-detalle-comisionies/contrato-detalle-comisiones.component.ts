import { Comisionista } from './../model/Comisionista';

import { ComisionistaService } from './../service/comisionista.service';
import { Comision } from './../model/Comision';
import { ConfigComision } from './model/ConfigComision';
import { DescriptivosService } from './../../common-services/descriptivos.service';
import { Descriptivo } from '../../model/Descriptivo';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Contrato } from '../model/Contrato';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Producto } from '../../model/Producto';

@Component({
	selector: 'contrato-detalle-comisiones',
	templateUrl: 'contrato-detalle-comisiones.component.html',
	styleUrls: ["contrato-detalle-comisiones.component.less"]
})

export class ContratoDetalleComisionesComponent extends SessionComponent {

	public _contrato: Contrato;

	public centros: Descriptivo[] = [];
	@Input()
	public editable: boolean = false;

	@Input()
	set contrato(c: Contrato) {
		this._contrato = c;
	};

	get contrato(): Contrato {
		return this._contrato;
	}

	public tiposComision: Descriptivo[] = [];

	public configComision: ConfigComision = new ConfigComision();
	public mostrarConfigComision: boolean = false;
	public comisionistas: Comisionista[] = [];


	public productosSeleccionables: Producto[] = [];

	get productos(): Producto[] {
		return this.contrato.servicios.map(s => s.producto);
	}

	@Output()
	public contratoChange: EventEmitter<Contrato> = new EventEmitter<Contrato>();

	constructor(
		private comisionistasService: ComisionistaService,
		) {
		super();
	}
	ngOnInit() {
		let $this = this;

		this.addLoadingCount();
		this.descriptivoService.getAllTiposComision().then(r => {
			$this.tiposComision = r;
			$this.susLoadingCount();
			this.addLoadingCount();
			this.comisionistasService.getAll().then(r => {
				$this.comisionistas = r;
					this.calcularDefaults();
				
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}).catch(this.errorHandler);



		this.addLoadingCount();
		this.descriptivoService.getCentrosCosto().then(r => {
			$this.centros = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);




	}

	public comisionistaSeleccionado(seleccion: any) {
		let com: Comisionista = seleccion.new;
		if (com) {
			this.configComision.productos = this.contrato.comisiones
				.filter(c => c.comisionista.codigo == com.codigo).map(c =>
					this.productos.filter(p => p.codigo === c.producto.codigo)[0]);
			this.productosSeleccionables = this.productos.filter(p => !this.configComision.tieneProducto(p)
				&& com.tieneCategoria(p.categoria));

		} else {
			this.configComision.productos = [];
			this.productosSeleccionables = [];
		}
	}

	private calcularDefaults() {
		let productos: Producto[] = this.contrato.servicios.filter(s => !s.id).map(s => s.producto);
		if (this.contrato.salon.esComisionista) {
			let salon: Comisionista = this.comisionistas.filter(c => c.salon && c.salon.codigo === this.contrato.salon.codigo)[0];
			if (salon) {
				productos.filter(p => salon.tieneCategoria(p.categoria)).forEach(p =>
					this.contrato.addComision(new Comision(null, salon, p, 0, 0, 0, this.tiposComision.filter(t=>t.codigo==="T")[0], false, false, false,
						this.contrato.getServicio(p.codigo).centroCosto))
				);
			}
		}

		let vendedor: Comisionista = this.comisionistas.filter(c => c.vendedor && c.vendedor.codigo === this.contrato.vendedor.codigo)[0];
		if (vendedor) {
			
			productos.filter(p => vendedor.tieneCategoria(p.categoria)).forEach(p =>
				this.contrato.addComision(new Comision(null, vendedor, p, 0, 0, 0, this.tiposComision.filter(t=>t.codigo==="S")[0], false, false, false,
					this.contrato.getServicio(p.codigo).centroCosto))
			);
		}

	}

	public nuevaComision() {
		this.configComision = new ConfigComision();
		this.mostrarConfigComision = true;
	}

	public guardarConfigComision() {
		let $this = this;
		if (!this.configComision.comisionista || !this.configComision.comisionista.codigo) {
			this.error("Seleccione un Comisionista");
			return;
		}
		//Elimino los que se quitaron
		this.contrato.comisiones = this.contrato.comisiones
			.filter(c => this.configComision.comisionista.codigo != c.comisionista.codigo
				|| this.configComision.tieneProductoByComision(c));

		this.configComision.productos.forEach(p => {
			let c: Comision = $this.contrato.getComision(p.codigo, this.configComision.comisionista.codigo);
			if (!c) {
				$this.contrato.addComision(
					new Comision(null, this.configComision.comisionista, p, 0, 0, 0,
						null, false, false, false, this.contrato.getServicio(p.codigo).centroCosto)
				);
			}
		});
		this.mostrarConfigComision = false;
	}

	public quitarComision(del: Comision) {
		this.contrato.comisiones = this.contrato.comisiones
			.filter(c => c.comisionista.codigo !== del.comisionista.codigo
				|| c.producto.codigo !== del.producto.codigo);
	}


}



import { Producto } from './../../../model/Producto';
import { Comisionista } from './../../model/Comisionista';
import { Comision } from '../../model/Comision';
export class ConfigComision{

    constructor(public comisionista? :Comisionista,
                public productos :Producto[] = [] ){

                }

    public tieneProductoByComision(com : Comision,){
        return com && this.comisionista.codigo === com.comisionista.codigo &&
                 this.productos.some(p => p.codigo === com.producto.codigo);
    }

    get valid() : boolean{
        return true;
    }
    
    public tieneProducto(prod : Producto) : boolean{

        return prod && this.productos.some(p=> p.codigo === prod.codigo);
    }

}
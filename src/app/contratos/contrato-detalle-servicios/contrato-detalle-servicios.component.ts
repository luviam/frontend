import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Descriptivo } from '../../model/Descriptivo';
import { CuotaServicio } from '../model/CuotaServicio';
import { ServicioContratado } from '../model/ServicioContratado';
import { ServicioContratadoTotal } from '../model/ServicioContradoTotal';
import { TreeNode } from 'primeng/primeng';
import { ProductoService } from '../../producto/service/producto.service';
import { FormControl } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Contrato } from '../model/Contrato';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Producto } from '../../model/Producto';

import * as moment from 'moment';

@Component({
	selector: 'contrato-detalle-servicios',
	templateUrl: 'contrato-detalle-servicios.component.html',
	styleUrls: ["contrato-detalle-servicios.component.less"]
})

export class ContratoDetalleServiciosComponent extends SessionComponent {

	public _contrato: Contrato;

	@Input()
	public editable: boolean = false;

	@Input()
	set contrato(c: Contrato) {
		this._contrato = c;
	};

	get contrato(): Contrato {
		return this._contrato;
	}


	public fechaLimite: Date = new Date();
	public cantidadCuotas: number = 4;
	public centrosProducto: Descriptivo[] = [];
	public centroEditable: boolean = true;
	private _productos: Producto[] = [];
	public productosSinInternos: Producto[] = [];

	public garantiaSC: ServicioContratadoTotal = new ServicioContratadoTotal();
	public sadaicSC: ServicioContratadoTotal = new ServicioContratadoTotal();
	get productos(): Producto[] {

		if (this.contrato.esPaquete) {
			let p = [Producto.PACK];
			p.push(...this._productos);
			return p;
		} else {
			return this._productos;
		}
	}
	set productos(val: Producto[]) {
		this._productos = val;
	}
	public productoSeleccionado: Producto;

	public displayNuevoServicio: boolean = false;
	public nuevoServicioForm: FormGroup;
	@Output()
	public contratoChange: EventEmitter<Contrato> = new EventEmitter<Contrato>();

	constructor(
		private productoService: ProductoService,
		public confirmService: ConfirmationService) {
		super();
	}
	ngOnInit() {
		let $this = this;
		this.nuevoServicioForm = new FormGroup({
			'categoria': new FormControl(),
			'producto': new FormControl(),
			'valorUnitario': new FormControl(),
			'cantidad': new FormControl(1),
			'cuotas': new FormControl(1),
			'adultos': new FormControl(0),
			'adolescentes': new FormControl(0),
			'menores': new FormControl(0),
			'bebes': new FormControl(0),
			'centro': new FormControl(null),
			'fechaContrato': new FormControl(new Date()),
			'descripcion': new FormControl(),
		});

		this.fechaLimite = moment(this.contrato.fechaEvento).subtract(10, 'd').toDate();

		this.addLoadingCount();
		this.productoService.getAll().then(r => {

			$this.productos = r.filter(p => p.cuentas.some(c => c.habilitado)).sort((a, b) => { return a.nombre.toLowerCase().localeCompare(b.nombre.toLowerCase()) });
			$this.productosSinInternos = this.productos.filter(p => !p.esInterno());
			this.calcularPlanPagos($this.contrato.servicios.filter(s => s.planPago.length === 0), this.cantidadCuotas);

			$this.calcularDefaults();
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}

	private calcularDefaults() {
		this.garantiaSC = <ServicioContratadoTotal>this.contrato.getServicio(Producto.GARANTIA);
		if (!this.garantiaSC) {
			this.garantiaSC = new ServicioContratadoTotal(null, this.contrato.fechaContratacion, this.getProducto(Producto.GARANTIA), [], 0, 1, "INTERNOS");
			this.garantiaSC.setFechaEvento(this.contrato.fechaEvento);
			this.contrato.addServicio(this.garantiaSC);
		}

		this.sadaicSC = <ServicioContratadoTotal>this.contrato.getServicio(Producto.SADAIC);
		if (!this.sadaicSC) {
			this.sadaicSC = new ServicioContratadoTotal(null, this.contrato.fechaContratacion, this.getProducto(Producto.SADAIC), [], 0, 1, "INTERNOS");
			this.sadaicSC.setFechaEvento(this.contrato.fechaEvento);
			this.contrato.addServicio(this.sadaicSC);
		}

	}
	public getProducto(codigo: string): Producto {
		return this.productos.filter(p => p.codigo === codigo)[0] || null;
	}

	public updateVencimiento(cuota: CuotaServicio) {
		if (!this.contrato.esCongelado) {
			this.calcularCuotaIncremento(cuota);
		}
		if (!this.contrato.id) {
			cuota.fechaEstimadaPago = cuota.fechaVencimiento;
		}

	}
	public congeladoChangeGlobal(event) {
		this.congeladoChange(this.contrato.servicios, event);
	}

	public calcularCuotaIncremento(cuota: CuotaServicio) {
		let años: number = moment(cuota.fechaVencimiento).get('year') - moment(this.contrato.fechaContratacion).get("year");
		let meses: number = moment(cuota.fechaVencimiento).get("month") + (años * 12) - moment(this.contrato.fechaContratacion).get("month");
		cuota.incremento100 = Math.round(0.025 * meses * 1000) / 10;
	}
	public congeladoChange(servicios: ServicioContratado[], event: any) {
		if (!event) {
			servicios.filter(s => s.grupo !== "INTERNOS").forEach(s => {
				s.planPago.forEach(c => {
					this.calcularCuotaIncremento(c);
				});
			});

		} else {
			servicios.filter(s => s.grupo !== "INTERNOS").forEach(s => {
				s.planPago.forEach(c => {
					c.pIncremento = 0;
				});
			});
			let data: CuotaServicio;
		}


	}

	public calcularPlanPagos(servicios: ServicioContratado[], cantCuotas: number): ServicioContratado[] {

		servicios.filter(s => s.grupo !== "INTERNOS").forEach(s => {
			s.planPago = [];
			let total = s.cantidad;
			if (cantCuotas === 1) {
				s.agregarCuota(new CuotaServicio(
					null,
					s.producto,
					s.fechaContrato,
					moment(s.fechaContrato).add(10, 'd').toDate(),
					moment(s.fechaContrato).add(10, 'd').toDate(),
					s.cantidad,
					s.costoUnitario,
					s.total,
					0, false, s.descripcion, s.grupo
				));
			} else {
				let porcPrimerCuota = 0.4;

				let valCuota = (1 - porcPrimerCuota) / (cantCuotas - 1);
				let cantDias = moment(this.fechaLimite).diff(moment(s.fechaContrato), 'd');
				let diasCuota = cantCuotas > 2 ? Math.floor((cantDias) / (cantCuotas - 1)) : 0;
				let pivotCuota = porcPrimerCuota;
				let cantCuota = 0;
				for (let i = 1; i <= cantCuotas; i++) {
					let fechaLimite: Date;
					if (i === cantCuotas) {
						pivotCuota = 1 - porcPrimerCuota - (valCuota * (cantCuotas - 2));
						cantCuota = s.cantidad - s.planPago.reduce((a, b) => { return a + b.cantidad }, 0);
						fechaLimite = this.fechaLimite;
					} else if (i === 1) {
						fechaLimite = s.fechaContrato;
						cantCuota = s.cantidad === 1 ? 0.4 * s.cantidad : Math.floor(0.4 * s.cantidad);
						if (cantCuota === 0) cantCuota = 1;
					} else {
						cantCuota = s.cantidad === 1 ? pivotCuota * s.cantidad : Math.floor(pivotCuota * s.cantidad);
						if (cantCuota === 0) cantCuota = 1;
						fechaLimite = moment(s.fechaContrato).add(diasCuota * (i - 1), 'd').toDate();
					}
					cantCuota = Math.round(cantCuota * 100) / 100;

					s.agregarCuota(new CuotaServicio(
						null,
						s.producto,
						s.fechaContrato,
						fechaLimite,
						fechaLimite,
						cantCuota,
						s.costoUnitario,
						s.costoUnitario * cantCuota,
						0, false, s.descripcion, s.grupo
					));
					pivotCuota = valCuota;

				}
			}

		});
		this.congeladoChange(servicios, this.contrato.esCongelado);
		return servicios;
	}
	get total(): number {
		return this.contrato.total;
	}

	public recalcularServicio(servicio: ServicioContratado) {
		this.calcularPlanPagos([servicio], servicio.planPago.length);
	}
	public recalcularCuotas() {
		this.contrato.eliminarCuotas();
		this.contrato.servicios = [...this.calcularPlanPagos(this.contrato.servicios, this.cantidadCuotas)];
	}
	public categoriaSeleccionada(event: any) {
		let nuevo: Descriptivo = event.new;
		if (event.new !== event.prev) {
			this.nuevoServicioForm.patchValue({
				'producto': null,
				'descripcion': null
			});
		}

	}
	public productoChange(event: any) {
		this.productoSeleccionado = event.new;

		let val: number = 0;

		if (this.productoSeleccionado) {

			if (this.productoSeleccionado.codigo === Producto.PACK.codigo ||
				this.contrato.getServiciosPack()
					.some(s => s.grupo === Producto.PACK.codigo && s.producto.codigo === this.productoSeleccionado.codigo)) {
				this.centroEditable = false;
				this.contrato.getServiciosPack().forEach(s => {
					if (s.producto.unidad.codigo === 'PORC') {
						val += s.costoUnitario / this.contrato.baseInvitados;
					} else {
						val += s.costoUnitario;
					}
				});
				if(this.productoSeleccionado.codigo === Producto.PACK.codigo && this.contrato.packDivisible()){
					this.productoSeleccionado.divisible = true;
					let sp : ServicioContratado = this.contrato.getPackDivisible();
					this.productoSeleccionado.adolescenteProp = sp.producto.adolescenteProp;
					this.productoSeleccionado.bebeProp = sp.producto.bebeProp;
					this.productoSeleccionado.menorProp = sp.producto.menorProp;
				}

			} else {
				let s: ServicioContratado;
				s = this.contrato.getServicio(this.productoSeleccionado.codigo);
				val = s ? s.costoUnitario : 0;
				if (s) {
					val = s.costoUnitario;
					this.centroEditable = false;
				} else {
					this.centrosProducto = this.productoSeleccionado.centros;

					this.centroEditable = true;
				}
			}

		} else {
			this.centrosProducto = [];
		}


		this.nuevoServicioForm.patchValue({
			'valorUnitario': Math.round(val * 100) / 100
		})
	}
	public nuevoServicio(producto?: Producto) {
		this.nuevoServicioForm.patchValue({
			'categoria': null,
			'producto': producto ? producto : null,
			'valorUnitario': 0,
			'cantidad': 1,
			'cuotas': 1,
			'fechaContrato': this.contrato.fechaContratacion,
			'adultos': 0,
			'adolescentes': 0,
			'menores': 0,
			'bebes': 0,
			'descripcion': producto ? producto.descripcion : null
		})
		if (producto) {
			this.productoChange({ new: producto });
		}
		this.displayNuevoServicio = true;
	}
	public mostrarFechaContrato(servicio: any) {
		return !servicio.leaf || (this.contrato.fechaContratacion.getTime() != servicio.fechaContrato.getTime());
	}
	public agregarServicio() {
		let val = this.nuevoServicioForm.value;

		let serviciosActualizados: ServicioContratado[] = [];

		if (this.productoSeleccionado.codigo === Producto.PACK.codigo) {
			this.contrato.getServiciosPack().forEach(s => {
				let ser: ServicioContratado = new ServicioContratado(
					null,
					val.fechaContrato,
					s.producto,
					[],
					s.costoUnitario, null, Producto.PACK.codigo, val.centro);
				ser.descripcion = val.descripcion;
				serviciosActualizados.push(ser);
			});
		} else {


			let ser: ServicioContratado = new ServicioContratado(
				null,
				val.fechaContrato,
				val.producto,
				[],
				val.valorUnitario, null, null, val.centro);
			ser.descripcion = val.descripcion;
			
			serviciosActualizados.push(ser);
		}

		serviciosActualizados.forEach(s => this.actualizarCuotasServicio(val, s));
		this.contrato.servicios = [...this.contrato.servicios];
		this.displayNuevoServicio = false;
		this.contrato.ultimaModificacion = val.fechaContrato;


	}
	private cuotasPorCantidad(servicio: ServicioContratado, cantidad: number, porc: number, cuotas: number, desc?: string) {
		let servicioPivot: ServicioContratado = ServicioContratado.fromData(servicio);
		servicioPivot.cantidad = cantidad;
		servicioPivot.costoUnitario = servicioPivot.costoUnitario * porc / 100;
		servicioPivot = this.calcularPlanPagos([servicioPivot], cuotas)[0];
		if (desc) {
			servicioPivot.planPago.forEach(c => c.descripcion = desc);
		}
		let existente = this.contrato.getServicio(servicio.producto.codigo);
		if (existente) {
			existente.planPago.push(...servicioPivot.planPago);
		} else {
			this.contrato.addServicio(servicioPivot);
		}
	}
	private actualizarCuotasServicio(val: any, servicio: ServicioContratado) {

		if (this.productoSeleccionado.divisible) {

			let existente: ServicioContratado;
			if (val.adultos != 0) {

				this.cuotasPorCantidad(servicio, val.adultos, 100, val.cuotas)
			}
			if (val.adolescentes != 0) {
				this.cuotasPorCantidad(servicio, val.adolescentes, this.productoSeleccionado.adolescenteProp, val.cuotas, this.productoSeleccionado.descripcion + " - Adolesc.")
			}
			if (val.menores != 0) {
				this.cuotasPorCantidad(servicio, val.menores, this.productoSeleccionado.menorProp, val.cuotas, this.productoSeleccionado.descripcion + " - Menores");
			}

			if (val.bebes != 0) {
				this.cuotasPorCantidad(servicio, val.bebes, this.productoSeleccionado.bebeProp, val.cuotas, this.productoSeleccionado.descripcion + " - Bebes");
			}

		} else {
			servicio.cantidad = val.cantidad;
			servicio = this.calcularPlanPagos([servicio], val.cuotas)[0];
			let existente = this.contrato.getServicio(servicio.producto.codigo);
			if (existente) {
				existente.planPago.push(...servicio.planPago);
			} else {
				this.contrato.addServicio(servicio);
			}
		}


	}


	public agregarCuota(node: TreeNode) {
		if (!node.leaf) {
			let cuota: CuotaServicio = node.data;
			this.nuevoServicio(cuota.producto);

		}
	}
	public quitarServicio(node: TreeNode) {
		let $this = this;
		if (node.leaf) {
			let cuota: CuotaServicio = node.data;
			let servicio: ServicioContratado = cuota.parent.data;
			servicio.quitarCuota(cuota);
		} else {
			if (node.data.grupo == 'PACK') {
				this.confirmService.confirm({
					key: "acConf",
					header: "Eliminar Paquete",
					message: 'Va a eliminar el paquete completo. Desea continuar?',
					accept: () => {
						$this.contrato.quitarServicios($this.contrato.getServiciosPack());
						$this.contrato.quitarComisiones($this.contrato.getServiciosPack())
					}
				});
			} else {
				let servicio: ServicioContratado = node.data;
				this.contrato.quitarServicio(servicio);
				this.contrato.quitarComisiones([servicio])
			}

		}




	}

	get serviciosMuestra() {
		return this.contrato.serviciosContratados;
	}



	public esEliminable(servicio: ServicioContratado) {
		return !this.contrato.id || !servicio.id || (!this.contrato.esAprobado);
	}
}



import { CuotaServicio } from './../model/CuotaServicio';
import { Producto } from './../../model/Producto';
import { ServicioContratado } from './../model/ServicioContratado';
import { OrdenCobranzaService } from './../../clientes/cobros/service/OrdenCobranzaService.service';
import { CabeceraOrdenCobranza } from '../../clientes/cobros/listado/shared/CabeceraOrdenCobranza';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';

import { ServicioResumen } from './modelo/ServicioResumen';


import { Contrato } from '../model/Contrato';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { Descriptivo } from '../../model/Descriptivo';
import { Comision } from '../model/Comision';
import { Categoria } from '../../model/Categoria';

@Component({
	selector: 'contrato-visualizacion',
	templateUrl: 'contrato-visualizacion.component.html',
	styleUrls: ["contrato-visualizacion.component.less"]
})

export class ContratoVisualizacionComponent extends SessionComponent {
	constructor(
		private cobranzasService: OrdenCobranzaService) {
		super( 'visualizacion-contrato');
	}
	public _contrato: Contrato;
	public _verAgrupado: boolean = false;
	public comisiones: Comision[] = [];
	public cobranzas: CabeceraOrdenCobranza[] = [];
	public impuestosTotal: number = 0;
	public verCalculadoSobre : boolean = false;
	public totalMenu : number = 0;
	public importeCobranzas: number = 0;
	public totalCobranzas: number = 0;
	public ivaCobranzas: number = 0;
	public impuestos: ServicioResumen[] = [];
	get verAgrupado(): boolean {
		return this._verAgrupado;
	}
	@Input()
	public mostrarComisiones: boolean = true;

	@Input()
	set verAgrupado(b: boolean) {
		this._verAgrupado = b;
		if (this.contrato)
			this.updateServiciosMostrar();
	}

	@Input()
	public editable: boolean = false;

	@Input()
	set contrato(c: Contrato) {
		this._contrato = c;
		if (c) {
			this.updateServiciosMostrar();
			this.updateComisiones();
			this.updateCobranzas();
			this.updateTotalMenu()
		}

	};

	get contrato(): Contrato {
		return this._contrato;
	}

	public serviciosMostrar: ServicioResumen[] = [];


	ngOnInit() {
		if (this.contrato){
			this.updateServiciosMostrar();
			this.updateComisiones();
			this.updateCobranzas();
		}

	}

	private updateServiciosMostrar() {

		let servicios: ServicioResumen[] = [];
		if (this.verAgrupado) {
			servicios = this.agruparServicios(this.contrato);
		} else {
			this.contrato.serviciosContratados.filter(s => s.producto.imputaIVA && s.producto.codigo!= Producto.DEVOLUCION).forEach(cs => {
				let cant = cs.planPago.reduce((a, b) => { return a + b.cantidad }, 0);
				let head: ServicioResumen = new ServicioResumen(cs.id, cs.fechaContrato, cs.producto, cs.costoUnitario, cant, 0, cs.grupo, false);
				servicios.push(head);
				if (cs.planPago) {
					cs.planPago.forEach(ps => {
						let s: ServicioResumen = new ServicioResumen(ps.id, ps.fechaContrato,
							ps.producto, ps.costoUnitario, ps.cantidad, null, cs.grupo, true,
							ps.pIncremento,
							ps.fechaEstimadaPago, ps.fechaVencimiento,ps.descripcion,null,head);
						servicios.push(s);
						head.total += s.total;
					});

				}
			});
		}

		this.serviciosMostrar = [...servicios];
	}
	public mostrarFechaContrato(servicio: ServicioResumen) {
		return !servicio.leaf || (servicio.parent && servicio.parent.fechaContrato.getTime() != servicio.fechaContrato.getTime());
	}

	public mostrarDescripcion(servicio: ServicioResumen) {
		return !servicio.leaf || (servicio.parent && servicio.parent.descripcion != servicio.descripcion);
	}

	private agruparServicios(contrato: Contrato): ServicioResumen[] {
		let servicios: ServicioResumen[] = [];
		let menuPrincipal = this.contrato.getServiciosPack().filter(s => s.producto.categoria.codigo === "MENU")[0];
		if (menuPrincipal) {
			let nombrePaquete: string = "Paquete";
			let costoPaquete: number = 0;
			let total: number = 0;
			if (this.contrato.getServiciosPack().length > 1) {
				this.contrato.getServiciosPack().forEach(s => {
					nombrePaquete += " - " + s.producto.descripcion;
					costoPaquete += s.costoUnitario;

				})
			} else if (this.contrato.getServiciosPack().length === 1) {
				nombrePaquete = this.contrato.getServiciosPack()[0].descripcion;
				costoPaquete += this.contrato.getServiciosPack()[0].costoUnitario;
			}

			let paqueteProd: Producto = new Producto(null, Producto.PACK.codigo, nombrePaquete, menuPrincipal.producto.unidad, Categoria.PACK());
			let head: ServicioResumen = new ServicioResumen(
				null, this.contrato.fechaContratacion, paqueteProd, costoPaquete, menuPrincipal.cantidad, 0, Producto.PACK.codigo, false
			);
			servicios.push(head);
			menuPrincipal.planPago.forEach(p => {
				let s: ServicioResumen = new ServicioResumen(
					null, p.fechaContrato, paqueteProd, p.costoUnitario, p.cantidad, null, Producto.PACK.codigo, true, p.pIncremento, p.fechaEstimadaPago, p.fechaVencimiento, p.descripcion
				);
				let comp : CuotaServicio = contrato.getCuotasPack()
					.filter(pp=> pp.producto.codigo !== p.producto.codigo && p.grupo === Producto.PACK.codigo
						&& p.fechaContrato.getTime() === pp.fechaContrato.getTime() &&
						p.cantidad == pp.cantidad && (pp.descripcion === p.descripcion || 
							(p.descripcion === p.producto.descripcion && pp.descripcion === pp.producto.descripcion)))[0];

				if(comp){
					s.costoUnitario = p.costoUnitario + comp.costoUnitario;
					s.total = (p.pIncremento + 1) * s.costoUnitario * s.cantidad;
				
				}else{

				}
				if(p.descripcion === menuPrincipal.producto.descripcion){
					s.descripcion = nombrePaquete;
				}
				s.parent = head;
				servicios.push(s);
				head.total += s.total;
			})
		}
		this.contrato.serviciosContratados.filter(c => c.producto.imputaIVA && c.producto.codigo != Producto.DEVOLUCION 
				&& (c.grupo !== 'PACK')).forEach(cs => {
			let head: ServicioResumen = new ServicioResumen(cs.id, cs.fechaContrato, cs.producto, cs.costoUnitario, cs.cantidad, 0, cs.grupo, false);
			servicios.push(head);
			if (cs.planPago) {
				cs.planPago.filter(p => p.grupo !== Producto.PACK.codigo).forEach(ps => {
					let s: ServicioResumen = new ServicioResumen(ps.id, ps.fechaContrato,
						ps.producto, ps.costoUnitario, ps.cantidad, null, cs.grupo, true,
						ps.pIncremento,
						ps.fechaEstimadaPago, ps.fechaVencimiento, ps.descripcion);
					s.parent = head;
					servicios.push(s);
					head.total += s.total
				})

			}
		});
		return servicios;
	}

	public tableFormat(data: ServicioResumen) {
		return data.leaf ? "leaf" : "parent";
	}

	public updateComisiones() {

		this.comisiones = [];
		let current: Comision;
		this.contrato.comisiones.forEach(c => {
			if (!this.comisiones.some(cc => cc.comisionista.codigo === c.comisionista.codigo)) {
				this.comisiones.push(new Comision(null, c.comisionista, null, 0, 0));
			}
			current = this.comisiones.filter(cc => cc.comisionista.codigo === c.comisionista.codigo)[0];
			current.totalAComisionar += c.totalAComisionar;
			current.totalLiquidado += c.totalLiquidado;
			current.saldoComision += c.saldoComision;
		});
	}

	public updateCobranzas() {
		if (this.contrato && this.contrato.id) {
			let $this = this;
			this.addLoadingCount();
			this.cobranzasService.getCabecerasByContrato(this.contrato).then(res => {
				$this.cobranzas = res;
				$this.importeCobranzas = $this.cobranzas.reduce((a, b) => { return a + (b.importeNeto); }, 0);
				$this.ivaCobranzas = $this.cobranzas.reduce((a, b) => { return a + b.ivaCobrado; }, 0);
				$this.totalCobranzas = $this.ivaCobranzas + $this.importeCobranzas;
				$this.updateImpuestos();
				$this.susLoadingCount();
			}).catch($this.errorHandler)

		}else{
			this.updateImpuestos();
		}


	}

	get total():number{
		return this.contrato.importeNeto + this.contrato.sadaic.total + this.contrato.garantia.total + this.contrato.devolucion.cobrado + this.impuestosTotal;
	}
	public updateImpuestos() {
		/*if(this.ivaCobranzas > this.contrato.ipTotal){
			this.impuestosTotal = this.ivaCobranzas;
		}else{
			this.impuestosTotal = this.contrato.ipTotal;
		}*/
		this.impuestos = this.contrato.servicios.filter(s => s.producto.esImpuesto() || s.producto.esOperativoIP()).map(s =>
			new ServicioResumen(null, s.fechaContrato, s.producto, 0, 0, s.total, null, true, 0, null, null, s.producto.descripcion,
				s.planPago.reduce((a, b) => a = a + b.saldo, 0)
			));
			let totalImpuestos : number = 0;
			totalImpuestos = this.impuestos.reduce((a,b)=> a = a+b.total,0);
		if (totalImpuestos > this.contrato.ipTotal) {
			this.impuestosTotal = totalImpuestos;
			this.verCalculadoSobre = false;
		} else {
			this.impuestosTotal = this.contrato.ipTotal;
			this.verCalculadoSobre = true;
		}

	}
	public updateTotalMenu(){
		this.totalMenu = this.contrato.servicios.filter(s => s.producto.categoria.codigo === Categoria.MENU).reduce((a,b)=> a += b.cantidad,0);
	}
	get largoVisualizacion() {
		let n: number = 5;
		if (!this.contrato.esCongelado) n++;
		if (this.contrato.id) n++;
		return n;
	}

	get totalInvitadosNoValido(){
		return this.totalMenu == 0 || (this.totalMenu - this.contrato.bebes) !== this.contrato.totalInvitados;
	}
}
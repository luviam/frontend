import { TreeNode } from 'primeng/primeng';
import { CuotaServicio } from '../../model/CuotaServicio';
import { Producto } from '../../../model/Producto';
import * as moment from 'moment';
export class ServicioResumen {
    constructor(
        public id?: number,
        public fechaContrato?: Date,
        public producto?: Producto,
        public costoUnitario?: number,
        public cantidad?: number,
        public total : number = null,
        public grupo?: string,
        public leaf? : boolean,
        public pIncremento? :number,
        public fechaEstimadaPago? : Date,
        public fechaVencimiento? : Date,
        public _descripcion? :string,
        public saldo : number =0,
        public parent? : ServicioResumen
        
    ) {
        if(total === null || total === undefined){
            this.total = (this.pIncremento + 1) * costoUnitario * cantidad;
        }
    }


    get fechaContratoStr(): string {
        return moment(this.fechaContrato).format('DD/MM/YYYY');
    }

    

    get descripcion(): string {
        return this._descripcion? this._descripcion : (this.producto ? this.producto.nombre : "ERROR - SIN PRODUCTO");
    }
    set descripcion(val :string ){
        this._descripcion = val;
    }
    get cantidadStr(): string {
        let str : string = this.cantidad+"";
		
		if(this.producto && this.producto.codigoUnidad === "PORC"){
			return this.cantidad * 100 + "%";
		}else{
			return this.cantidad+"";
		}

    }
 
    get costoUnitarioGral() {
        return Math.round (this.costoUnitario * (1 + this.pIncremento) * 100) /100;
    }



    get label(): string {
        return this.descripcion;
    };
    get data(): any {
        return this;
    };

    get cobros():number{
        return this.total - this.saldo;
    }

    get peso():number{
        return this.producto.peso;
    }
}
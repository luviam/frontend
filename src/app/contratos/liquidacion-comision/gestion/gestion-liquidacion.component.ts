import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CuentasService } from '../../../common-services/cuentasService.service';
import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { ContratosService } from '../../../contratos/service/contratos.service';
import { Cheque } from '../../../model/Cheque';
import { CuentaAplicacion } from '../../../model/CuentaAplicacion';
import { Cobro } from '../../../model/documentos/Cobro';
import { EscalaComisionesService } from '../../../parametricos/escalas-comision/service/escala-comisiones.service';
import { SessionComponent } from '../../../session-component.component';
import { LiquidacionComisionesService } from '../service/LiquidacionComisionesService.service';
import { Descriptivo } from './../../../model/Descriptivo';
import { EscalaComisiones } from './../../../model/EscalaComisiones';
import { EstadoOperacionEnum } from './../../../model/EstadoOperacionEnum';
import { Comisionista } from './../../model/Comisionista';
import { FiltroComisiones } from './../../model/filtros/FiltroComisiones';
import { ItemLiquidacionComisiones } from './../../model/ItemLiquidacionComisiones';
import { LiquidacionComisiones } from './../../model/LiquidacionComisiones';
import { PagoComision } from './../../model/PagoComision';
import { PagoLiquidacion } from './../../model/PagoLiquidacion';
import { ComisionistaService } from './../../service/comisionista.service';





@Component({
	selector: 'gestion-liquidacion',
	styleUrls: ["./gestion-liquidacion.component.less"],
	templateUrl: 'gestion-liquidacion.component.html'
})
export class GestionLiquidacionComisionesComponent extends SessionComponent {
	public eventos: Descriptivo[] = [];
	public _premio: number = 0;
	private _prevFilter: FiltroComisiones;
	public comisionistas: Descriptivo[] = [];
	public pagosComision: PagoComision[] = [];
	public _cliente: Descriptivo;
	public _liqComision: LiquidacionComisiones = new LiquidacionComisiones();
	public soloSeleccionados: boolean = false;
	public escalas: EscalaComisiones[] = [];
	public escalaAplicada: EscalaComisiones;

	public filtroComisiones: FiltroComisiones = new FiltroComisiones();

	public editando: boolean = false;
	public centros: Descriptivo[] = [];
	public cuentasImputacion: CuentaAplicacion[] = [];
	public finalizado: boolean = false;
	public tiposComprobante: Descriptivo[] = [];
	public cuentas: any[] = [];

	public cheques: Cheque[] = [];
	public chequesSugeridos: Cheque[] = [];

	private _items: ItemLiquidacionComisiones[] = [];
	private tiposPago: Descriptivo[] = [
		new Descriptivo("E", "Efectivo"),
		new Descriptivo("C", "Cheque"),
		new Descriptivo("T", "Transferencia"),
	]
	private _editable: boolean = true;
	@Input()
	set editable(val: boolean) {
		this._editable = val;
		//if(!val) this.soloSeleccionados = true;
	};
	get editable(): boolean {
		return this._editable;
	}

	@Input()
	set comisionista(val: Comisionista) {
		if (!val || !val.codigo || val.codigo == '-1') {
			this._liqComision.comisionista = null;
			this.filtroComisiones.comisionista = Descriptivo.TODOS();
		} else {
			let $this = this;
			if (!this._liqComision.comisionista || this._liqComision.comisionista.id !== Number(val.codigo))
				this.comisionistasService.getById(Number(val.codigo)).then(r => {
					$this._liqComision.comisionista = r;
					$this.filtroComisiones.comisionista = Descriptivo.fromData(r);
					$this.updateEventos();
					$this.updatePagos();
				}).catch($this.errorHandler);

		}



	}
	get comisionista(): Comisionista {
		return this._liqComision.comisionista;
	}
	get premio(): number {
		return this._premio;
	}
	set premio(val: number) {
		this._premio = val;
		this._liqComision.premio = val;
	}

	@Input()
	set eventoSeleccionado(val: Descriptivo) {
		if (!val || !val.codigo || Descriptivo.TODOS().codigo === val.codigo) {
			this.filtroComisiones.contrato = Descriptivo.TODOS();
		} else {
			this.filtroComisiones.contrato = val;
			this.updatePagos();
		}


	}
	get eventoSeleccionado(): Descriptivo {
		return this.filtroComisiones.contrato;
	}


	@Input()
	set centroCosto(val: Descriptivo) {
		if (!val || !val.codigo || Descriptivo.TODOS().codigo === val.codigo) {
			this._liqComision.centroCosto = null;
			this.filtroComisiones.centro = Descriptivo.TODOS();
		} else {

			this._liqComision.centroCosto = val;
			this.filtroComisiones.centro = Descriptivo.fromData(val);
			this.updatePagos();
			this.actualizarCuentas();
			this.updateEscalas();
		}


	}

	get centroCosto(): Descriptivo {
		return this._liqComision.centroCosto;
	}


	@Input()
	set liquidacion(val: LiquidacionComisiones) {
		if (!val) {
			this._liqComision = new LiquidacionComisiones();
			this.filtroComisiones.centro = Descriptivo.TODOS();
			this.filtroComisiones.comisionista = Descriptivo.TODOS();
			this.soloSeleccionados = false;
			this.premio = 0;

		} else {
			this._liqComision = LiquidacionComisiones.fromData(val);
			this.filtroComisiones.centro = val.centroCosto;
			this.filtroComisiones.comisionista = val.comisionista;
			if (this._liqComision.estado
				&& (this._liqComision.estado.codigo === EstadoOperacionEnum.PENDIENTE_PAGO
					|| this._liqComision.estado.codigo == EstadoOperacionEnum.APROBADO)) {
				this.soloSeleccionados = true;
			}
			this.premio = this.liquidacion.premio;
			//this.soloSeleccionados = false;

		}

	}

	@Input()
	set contrato(val: Descriptivo) {
		if (!val || !val.codigo || Descriptivo.TODOS().codigo === val.codigo) {
			this.filtroComisiones.contrato = Descriptivo.TODOS();
		} else {
			this.filtroComisiones.contrato = val;

		}
	}
	get contrato(): Descriptivo {
		return this.filtroComisiones.contrato;
	}

	get liquidacion(): LiquidacionComisiones {
		return this._liqComision;
	}

	@Output()
	public onSave = new EventEmitter();

	@Output()
	public onCancelar = new EventEmitter();

	constructor(
		private confirmationService: ConfirmationService,
		private descriptivosService: DescriptivosService,
		private liquidacionService: LiquidacionComisionesService,
		private escalaComisionesService: EscalaComisionesService,
		private cuentasService: CuentasService,
		private comisionistasService: ComisionistaService,
		private contratoService: ContratosService) {
		super();
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		$this.actualizarCuentas();


		$this.addLoadingCount();
		$this.descriptivosService.getAllComisionistas().then((res) => {
			$this.comisionistas = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	get items() {
		if (this.soloSeleccionados) {
			return this._items.filter(i => this.liquidacion.items.some(oi => oi.pagoComision.id == i.pagoComision.id));
		} else {
			return this._items;
		}
	}



	public actualizarCuentas() {
		let $this = this;
		this.addLoadingCount();
		$this.cuentasService.getCallCajas().then((res) => {
			$this.cuentas["E"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		$this.cuentasService.getAllBancarias().then((res) => {
			$this.cuentas["T"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		$this.cuentasService.getAllChequesClientes().then((res) => {
			$this.cuentas["C"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}

	public guardar() {
		let $this = this;
		if (this.validar()) {
			this.addLoadingCount();

			//this.liquidacion.cobros.filter(p => !p.centroCosto).forEach(p => p.centroCosto = this.liquidacion.centroCosto);
			this.liquidacionService.guardar(this.liquidacion).then((res) => {
				$this.susLoadingCount();
				$this.liquidacion = new LiquidacionComisiones();
				$this.onSave.emit();
			}).catch(this.errorHandler);
		}

	}
	public volver() {
		this.onCancelar.emit();
	}
	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: "genConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				$this.volver();
			}
		});
	}


	public updatePagos() {
		let $this = this;
		if (!this.filtroComisiones.comisionistaDefinido()) {
			$this.pagosComision = [];
			return;
		}
		if (!this._prevFilter || this._prevFilter.cambio(this.filtroComisiones)) {

			this.addLoadingCount();
			this.liquidacionService.getPagosPendientes(this.filtroComisiones).then((res) => {
				$this._prevFilter = FiltroComisiones.fromData(this.filtroComisiones);
				$this.pagosComision = res;
				$this.updateItems();
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			$this.pagosComision = [];
		}

	}


	private updateItems() {
		let $this = this;
		$this._items = $this.pagosComision.map(c => {
			var l = $this.liquidacion.items.filter(i => i.pagoComision.id === c.id)[0];
			return l || new ItemLiquidacionComisiones(null, c, c.saldo);

		});
		$this.liquidacion.items.forEach(i => {
			if (!$this._items.some(ii => ii.id === i.id)) {
				$this._items.push(i);
			}
		})

		//$this.liquidacion.items = $this.liquidacion.items.filter(i => $this.pagosComision.some(p => p.id === i.pagoComision.id));
	}
	public agregarNuevoPago() {
		this.liquidacion.addPago(new PagoLiquidacion(null, this.tiposPago[0],
			this.liquidacion.centroCosto,
			null,
			this.liquidacion.total - this.liquidacion.totalPagos,
			null));

	}

	public getCuentas(pago: PagoLiquidacion) {
		if (pago && pago.tipoOperacion && pago.centroCosto)
			return this.cuentas[pago.tipoOperacion.codigo].filter(c => pago.centroCosto.codigo === c.centroCosto.codigo);
		return [];
	}


	public filtrarCheques(event: any) {

		this.chequesSugeridos = this.cheques.filter(c => c.descripcion.toUpperCase().includes(event.query.toUpperCase()));


	}
	public updateTipoCobro(oc: Cobro) {
		if (this.getCuentas(oc).length === 1) {
			oc.cuenta = this.getCuentas(oc)[0];
		}
		if (oc.tipoOperacion.codigo == "C") {
			if (!oc.comprobante) {
				oc.comprobante = new Cheque(
					null,
					"", "", Cheque.CLIENTE(), null, "", "", "", null, null,
					oc.monto, new Descriptivo("D", "Disponible"), "", false, null, null,
					this.liquidacion.fecha
				);
			}
		} else {
			oc.comprobante = null;
		}

	}

	public updateCheque(oc: Cobro) {
		if (oc.tipoOperacion.codigo !== "E" && oc.comprobante) {
			if (oc.comprobante.importe) {
				oc.monto = oc.comprobante.importe;
			}
			if (oc.tipoOperacion.codigo === 'P') {

				oc.cuenta = oc.comprobante.cuentaBancaria;
			}


		}
	}


	public autoselectCheque(oc: Cobro) {
		if (!oc.comprobante || !oc.comprobante.id) {


			if (this.chequesSugeridos.length == 1) {
				oc.comprobante = this.chequesSugeridos[0];

			} else {
				oc.comprobante = null;

			}

		}
	}
	public cobrosValidos() {

		return this.liquidacion.pagosLiquidacion.length === 0 || this.liquidacion.totalPagos === this.liquidacion.total;

	}
	public validar(): boolean {
		let estado: boolean = true;


		if (this.liquidacion.pagosLiquidacion.some(p => p.tipoOperacion.codigo == 'C' && !p.comprobante)) {
			estado = false;
			this.error("Debe completar todos los cheques");
		}
		if (this.liquidacion.pagosLiquidacion.some(p => !p.monto)) {
			estado = false;
			this.error("Debe completar todos los importes de cobros");
		}
		if (this.liquidacion.pagosLiquidacion.some(p => p.monto <= 0)) {
			estado = false;
			this.error("Los importes deben ser mayores a 0");
		}
		return estado;
	}

	public getChequeColumnStyle(): any {
		return { 'width': this.liquidacion.tieneCheques ? '40%' : '10%' };
	}

	public updateEventos() {
		if (this.filtroComisiones.comisionistaDefinido()) {
			let $this = this;
			this.contratoService.getByComisionista(this.filtroComisiones.comisionista.codigo).then((res) => {
				$this.eventos = [Descriptivo.TODOS()].concat(res);

				$this.susLoadingCount();
			}).catch(this.errorHandler)
		} else {
			this.eventos = [];
		}
	}

	public esVendedor(): boolean {
		return this.liquidacion && this.liquidacion.comisionista && this.liquidacion.comisionista.esVendedor();
	}

	public updatePremio(): number {

		if (!this.comisionista) {
			return;
		}
		let total = this._liqComision.totalCubiertos;
		this.escalaAplicada = this.escalas.filter(e => e.desde <= total && total <= e.hasta)[0];
		if (this.comisionista.centro && this.comisionista.centro.codigo === this.liquidacion.centroCosto.codigo && this.escalaAplicada) {
			this.premio = this.escalaAplicada.premio;
		} else {
			this.premio = 0;
		}

		return this.premio;

	}
	public mostrarFiltrosSeleccionados() {

		return this.editable && (!this.liquidacion.estado || this.liquidacion.estado.codigo != EstadoOperacionEnum.PENDIENTE_PAGO);

	}

	public updateEscalas() {
		let $this = this;

		if (this.centroCosto && this.centroCosto.codigo !== Descriptivo.TODOS().codigo) {

			this.addLoadingCount();
			this.escalaComisionesService.getEscalasByCentro(this.centroCosto.codigo).then(r => {
				$this.susLoadingCount();
				$this.escalas = r;



			}).catch(this.errorHandler);
		}

	}



}
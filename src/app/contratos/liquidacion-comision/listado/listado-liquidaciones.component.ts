import { EstadoOperacionEnum } from './../../../model/EstadoOperacionEnum';
import { LiquidacionComisionesService } from './../service/LiquidacionComisionesService.service';
import { LiquidacionComisiones } from './../../model/LiquidacionComisiones';

import { FiltroComisiones } from './../../model/filtros/FiltroComisiones';
import { SalonService } from '../../../salon/service/salon.service';
import { Descriptivo } from '../../../model/Descriptivo';

import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';


import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { Router, Route } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute } from '@angular/router';
import { SessionComponent } from '../../../session-component.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

import { ContratosService } from '../../../contratos/service/contratos.service';
import { ItemLiquidacionComisiones } from '../../model/ItemLiquidacionComisiones';
import { MenuItem } from 'primeng/primeng';

@Component({
	selector: 'listado-liquidaciones',
	styleUrls: ["./listado-liquidaciones.component.less"],
	templateUrl: 'listado-liquidaciones.component.html'
})

export class ListaLiquidacionComisionesComponent extends SessionComponent {


	get comisionista() { return this.filtro.comisionista };

	get titulo() : string{
		if(this.liquidacionEditada && this.liquidacionEditada.numero){
			return "Liquidación N° " + this.liquidacionEditada.numero;
		}
		if(this.comisionista && this.comisionista.codigo != Descriptivo.TODOS().codigo){
			return "Liquidaciones de " + this.comisionista.descripcion;
		}else{
			return "Liquidaciones de Comisiones";
		}
		
	}
	public estadosSeleccionados: string[] = [EstadoOperacionEnum.APROBADO, EstadoOperacionEnum.PENDIENTE, "R", EstadoOperacionEnum.PENDIENTE_PAGO];
	

	@Input()
	public filtro: FiltroComisiones = new FiltroComisiones();

	@Input()
	public mostrarFiltrosComisionista : boolean = true;


	@Output()
	public docsActualizados :EventEmitter<boolean> = new EventEmitter<boolean>();
	public editable : boolean = true;
	public comisionistas : Descriptivo[] = [];
	public salones : Descriptivo[] = [];
	public acciones: MenuItem[] = [];
	public _liquidaciones: LiquidacionComisiones[] = [];
	public _filtrados :  LiquidacionComisiones[] = [];
	public liquidacionEditada: LiquidacionComisiones = new LiquidacionComisiones();
	public editandoLiquidacionComisiones: boolean = false;
	public _liquidacionSeleccionada : LiquidacionComisiones = new LiquidacionComisiones();
	get liquidacionSeleccionada(): LiquidacionComisiones{
		return this._liquidacionSeleccionada;
	}
	set liquidacionSeleccionada(val :LiquidacionComisiones) {
		this.itemsLiquidacion = [];
		this._liquidacionSeleccionada = val;
		if(val && val.id){
			let $this = this;
			this.addLoadingCount();
			this.liquidacionesService.getItems(val.id).then((res)=>{
				$this.itemsLiquidacion = res;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
	}
	public itemsLiquidacion : ItemLiquidacionComisiones[] = [];
		public horarios: Descriptivo[] =[
		
		new Descriptivo("D","Día"),
		new Descriptivo("N", "Noche"),
		Descriptivo.TODOS()
	];
	
	
	public centros: Descriptivo[] = [];

	
	constructor(cookieService :CookieServiceHelper,

		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		public route: ActivatedRoute,
		private liquidacionesService : LiquidacionComisionesService,
		private contratoService : ContratosService,
		private salonesService : SalonService) {
		super( "lista-liquidaciones");
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		//this.filtro.fechaDesde = moment().subtract(3, 'months').startOf('day').toDate();
		this.filtro.fechaDesde = moment().startOf('year').toDate();
		this.filtro.fechaHasta = moment().add(1,'days').startOf('day').toDate();
		if(!this.filtro.centro){
			this.filtro.centro = Descriptivo.TODOS();
		}
		if(!this.filtro.salon){
			this.filtro.salon = Descriptivo.TODOS();
		}
		
		if(!this.filtro.comisionista){
			this.filtro.comisionista = Descriptivo.TODOS();
		}
		$this.addLoadingCount();
		$this.descriptivosService.getAllComisionistas().then((res) =>{
			$this.comisionistas = [Descriptivo.TODOS()].concat(res);
			$this.susLoadingCount();
		}).catch($this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = [Descriptivo.TODOS()].concat(res);
			$this.susLoadingCount();
			
		}).catch($this.errorHandler);
		$this.addLoadingCount();
		$this.descriptivosService.getAllSalones().then((res)=>{
			$this.salones = [Descriptivo.TODOS()].concat(res);
			$this.susLoadingCount();
		})

		let url = this.route.snapshot.url.join('');
		if (url.includes("nueva-liquidacion")) {
			this.nuevaLiquidacionComisiones();
		}
		this.acciones = [{
			label: "", icon: "fa fa-bars", items: [
				{
					label: "Nueva Liquidación",
					icon: 'fa-plus',
					command: (event) => {
						this.nuevaLiquidacionComisiones();
					}
				},
				{
					label: "Liquidación Vendedores",
					icon: 'fa-money',
					command: (event) => {
						this.generarMensuales();
					},
					disabled: !this.esAdministrador
				}
				]
		}];

	
		

	}
	public updateLiquidaciones() {
		let $this = this;
		this.filtro.fechaDesde.setHours(0,0,0,0);
		this.filtro.fechaHasta.setHours(23,59,59,0);
		if (this.filtro.comisionista && this.filtro.centro) {
			$this.addLoadingCount();
			$this.liquidacionesService.getCabeceras(this.filtro).then((res) => {
				$this._liquidaciones = res;
				$this.filtrar();
				if($this.liquidacionSeleccionada && !$this.liquidaciones.some(s=> s.id === $this.liquidacionSeleccionada.id)){
					$this.liquidacionSeleccionada = null;
				}
				$this.susLoadingCount();
			}).catch($this.errorHandler);
		}
	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			key:"opConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['clientes/lista-clientes']);
			}
		});
	}

	get saldo() {
		return this._liquidaciones.map(o => o.total).reduce((a,b)=> a+ b,0);
		
	}

	public pagable(liq : LiquidacionComisiones): boolean{
		return liq.estado.codigo=== EstadoOperacionEnum.PENDIENTE_PAGO && this.esAuditor;
	}
	public aprobable(liq :LiquidacionComisiones) : boolean{
		return liq.estado.codigo=== EstadoOperacionEnum.PENDIENTE && this.esAuditor;
	}

	public rechazable(liq :LiquidacionComisiones) : boolean{
		return liq.estado.codigo=== EstadoOperacionEnum.PENDIENTE && this.esAuditor;
	}

	public eliminable(liq : LiquidacionComisiones) : boolean{
		
		return  liq.estado.codigo!=EstadoOperacionEnum.APROBADO || this.esAdministrador;
	}

	public esEditable(liq : LiquidacionComisiones) : boolean{
		return liq.estado.codigo !== EstadoOperacionEnum.APROBADO  && this.esAuditor ;
	}

	public esVisualizable(liq: LiquidacionComisiones):boolean{
		return !this.esEditable(liq);
	}

	get liquidaciones() {
		return this._filtrados;
	}

	public updateTipoProveedor(event: any) {

	}

	public updateTipoCuenta(event: any) {

	}
	public editar(liquidacion: LiquidacionComisiones) {
		this.liquidacionEditada = liquidacion;
		this.editandoLiquidacionComisiones = true;
		this.editable = true;
		this.goTo("gestion-liquidacion");
	}

	public visualizar(liquidacion: LiquidacionComisiones) {
		this.liquidacionEditada = liquidacion;
		this.editandoLiquidacionComisiones = true;
		this.editable = false;
		this.goTo("gestion-liquidacion");
	}

	public pagar(liquidacion: LiquidacionComisiones) {
		this.liquidacionEditada = liquidacion;
		this.editandoLiquidacionComisiones = true;
		this.goTo("gestion-liquidacion");
	}

	public nuevaLiquidacionComisiones() {
		this.liquidacionEditada = new LiquidacionComisiones();
		this.editandoLiquidacionComisiones = true;
		this.editable = true;
		this.goTo("gestion-liquidacion");
	}
	public onLiquidacionComisionesGuardada() {
		this.editandoLiquidacionComisiones = false;
		this.success("Liquidación Guardada");
		this.updateLiquidaciones();
		super.volver();
		this.docsActualizados.emit(true);
	}

	public onEdicionCancelada(){
		super.volver();
		this.editandoLiquidacionComisiones = false;
	}
	public aprobar(liquidacion : LiquidacionComisiones){
		let $this  = this;
		this.addLoadingCount();
		this.liquidacionesService.aprobarLiquidacionComisiones(liquidacion).then((res)=>{
			$this.success("Liquidación aprobada");
			$this.updateLiquidaciones();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}

	public rechazar(liquidacion : LiquidacionComisiones){
		let $this  = this;
		this.addLoadingCount();
		this.liquidacionesService.rechazarLiquidacionComisiones(liquidacion).then((res)=>{
			$this.success("Liquidación Rechazada");
			$this.updateLiquidaciones();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}

	public eliminar(liquidacion : LiquidacionComisiones){
		this.confirmationService.confirm({
			key:"liqConf",
			header: "Va a eliminar la Liquidación " + liquidacion.descripcion,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.liquidacionesService.eliminarLiquidacionComisiones(liquidacion).then((res)=>{
					$this.susLoadingCount();
					$this.liquidacionSeleccionada = new LiquidacionComisiones();
					$this.success("Se borró la Liquidación correctamente");
					$this.liquidacionEditada = null;
					$this.liquidacionSeleccionada = null;
					$this.updateLiquidaciones();
					$this.docsActualizados.emit(true);
				}).catch($this.errorHandler);
			}
		});
	
		
	}

	
	public generarMensuales(){

		if(this.esAdministrador){
			let $this = this;
			this.addLoadingCount();
			this.liquidacionesService.generarMensuales().then(r=>{
				$this.success("Se generaron las liquidaciones del mes");

				$this.susLoadingCount();
				$this.updateLiquidaciones();
			}).catch(this.errorHandler);
		}
	}

	
	
	public filtrar(){
		this.addLoadingCount();
		this._filtrados = this._liquidaciones.filter(f => this.estadosSeleccionados.indexOf(f.estado.codigo) > -1);
		this.susLoadingCount();
	}
}
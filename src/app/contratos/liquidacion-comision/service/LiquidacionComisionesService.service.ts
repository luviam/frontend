import { PagoComision } from './../../model/PagoComision';
import { FiltroComisiones } from './../../model/filtros/FiltroComisiones';
import { ItemLiquidacionComisiones } from './../../model/ItemLiquidacionComisiones';
import { LiquidacionComisiones } from './../../model/LiquidacionComisiones';
import { Contrato } from '../../../contratos/model/Contrato';



import { Injectable } from '@angular/core';


import { ServicioAbstract } from '../../../common-services/service.service';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable()
export class LiquidacionComisionesService extends ServicioAbstract {
    
   

    public getByContrato(contrato : Contrato){
        return this.http.get(this.getApiURL() + 'liquidacionComisiones/contrato/' + contrato.id).toPromise().then(this.parseCabecerasLiquidacionComisiones, this.handleError);
    }

    public generarMensuales(){
        return this.http.get(this.getApiURL() + 'liquidacionComisiones/generarMensual').toPromise().then((r)=>{console.log(r)}, this.handleError);
    }

    public getPagosPendientes(filtro : FiltroComisiones){

        let url = "pagoComision/impagas/";
        return this.http.post(this.getApiURL() + url,filtro).toPromise().then(this.parsePagosComisiones, this.handleError);
    }
    public aprobarLiquidacionComisiones(liquidacionComisiones: LiquidacionComisiones): Promise<any> {
        
        let o : LiquidacionComisiones = LiquidacionComisiones.fromData(liquidacionComisiones);
        o.estado.codigo = "A";
        return this.cambioEstadoLiquidacionComisiones(o);
    }
    public rechazarLiquidacionComisiones(liquidacionComisiones: LiquidacionComisiones): Promise<any> {
        let o : LiquidacionComisiones = LiquidacionComisiones.fromData(liquidacionComisiones);
        o.estado.codigo = "R";
        return this.cambioEstadoLiquidacionComisiones(o);
    }

    public eliminarLiquidacionComisiones(liquidacionComisiones : LiquidacionComisiones) : Promise<any>{
        let params = new HttpParams();
       params = params.append('id',liquidacionComisiones.id+"")
        return this.http.delete(this.getApiURL() + 'liquidacionComisiones', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    private cambioEstadoLiquidacionComisiones(liquidacionComisiones: LiquidacionComisiones): Promise<any> {
        return this.http.post(this.getApiURL() + "liquidacionComisiones/cambioEstado", liquidacionComisiones).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(idliquidacionComisiones: number): Promise<any> {
        return this.http.get(this.getApiURL() + 'liquidacionComisiones/' + idliquidacionComisiones).toPromise().then(this.parseLiquidacionComisiones, this.handleError);
    }

    public guardar(liquidacionComisiones: LiquidacionComisiones): Promise<any> {
        if (liquidacionComisiones.id) {
           return this.http.post(this.getApiURL() + 'liquidacionComisiones', liquidacionComisiones).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'liquidacionComisiones', liquidacionComisiones).toPromise().then(this.handleOk, this.handleError);
        }
        
    }

    public getCabeceras(filtro : FiltroComisiones) : Promise<LiquidacionComisiones[]>{

        let url = "liquidacionComisiones/cabeceras";
        return this.http.post(this.getApiURL() + url,filtro).toPromise().then(this.parseCabecerasLiquidacionComisiones, this.handleError);
    }

    public getItems(idOrden : number) : Promise<ItemLiquidacionComisiones[]>{
        return this.http.get(this.getApiURL() + 'liquidacionComisiones/items/' + idOrden).toPromise().then(this.parseItems, this.handleError);
    }

    public parseItems(response: any) : ItemLiquidacionComisiones[]{
        let res = response.respuesta;
        if(res){
            return res.map(i => ItemLiquidacionComisiones.fromData(i));
        }
        return [];
    }

    public parsePagosComisiones(response : any) : PagoComision[]{
        let res = response.respuesta;
        return res? res.map(c => PagoComision.fromData(c)) : [];
    }
    public parseLiquidacionComisiones(response: any): LiquidacionComisiones {
        let res = response;
        let liquidacionComisiones : LiquidacionComisiones = null;
        return  LiquidacionComisiones.fromData(res.respuesta);
    }

       public parseCabecerasLiquidacionComisiones(response: any): LiquidacionComisiones[] {
        let res = response;
        let cabeceras: LiquidacionComisiones[] = [];
        if (res !== null && res.respuesta !== null) {
            cabeceras = res.respuesta.map(c => LiquidacionComisiones.fromData(c));
        }
        return cabeceras;

    }
    
}

import { CalendarEvent } from '../../common-utils/CalendarEvent';
import { MenuItem, Menu } from 'primeng/primeng';
import { ContratoCabecera } from '../../clientes/lista-clientes/shared/ContratoCabecera';
import { Cliente } from '../../model/Cliente';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { Contrato } from '../model/Contrato';
import { ContratosService } from '../service/contratos.service';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { CookieService } from 'ngx-cookie';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { noUndefined } from '@angular/compiler/src/util';

@Component({
	selector: 'lista-contrato',
	templateUrl: 'lista-contrato.component.html',
	styleUrls: ['lista-contrato.component.less']
})

export class ListaContratoComponent extends SessionComponent {

	public _contratos: ContratoCabecera[] = [];
	public _filtrados: ContratoCabecera[] = [];
	@ViewChild("menu")
	private menu: Menu;
	private _items: MenuItem[] = [];

	public mostrarComisiones: boolean = false;

	public tituloContrato: string;
	public visualizandoContrato: boolean = false;
	public contratoEditado: Contrato;

	private _contratoSeleccionado: ContratoCabecera;
	private _cliente: Cliente;

	@ViewChild("cal")
	private calendario: any;
	public eventos: CalendarEvent[] = [];
	public options: any = {};
	public headerConfig = {
		left: 'prev, today, next',
		center: 'title',
		right: 'verListado, month,agendaWeek,agendaDay'
	};


	get contratoSeleccionado(): ContratoCabecera {
		return this._contratoSeleccionado;
	}
	set contratoSeleccionado(c: ContratoCabecera) {
		let $this = this;
		this._items = [];
		this._contratoSeleccionado = c;
		if (!this.contratoSeleccionado) this._items = [];
		if (!this.contratoSeleccionado.esCerrado()) {
			this._items.push(
				{
					label: 'Editar', icon: 'fa-pencil', command: (event) => {
						$this.editarContrato($this.contratoSeleccionado);
					}
				}
			);
		}
		if (this.contratoSeleccionado.esAprobado() || this.contratoSeleccionado.esCerrado()) {
			this._items.push(
				{
					label: 'Generar Cobro', icon: 'fa-money', command: (event) => {
						$this.generarPago($this.contratoSeleccionado);
					}
				}
			);

		}
		this._items.push(
			{
				label: 'Ver Resumen', icon: 'fa-eye', command: (event) => {
					$this.visualizarContrato($this.contratoSeleccionado);
				}
			}
		);


		if (this.esAdministrador) {
			if (this.contratoSeleccionado.cobros <= 0) {
				this._items.push(
					{
						label: 'Eliminar', icon: 'fa-trash', command: (event) => {
							$this.deleteContrato($this.contratoSeleccionado);
						}
					}
				);
			}
			if (this.contratoSeleccionado.saldo <= 0 && !this.contratoSeleccionado.esCerrado() && this.contratoSeleccionado.esAprobado()) {
				this._items.push(
					{
						label: 'Cerrar Evento', icon: 'fa-flag-checkered',
						command: (event) => {
							$this.liquidarContrato($this.contratoSeleccionado);
						}
					}
				);
			}

		}

	}
	get titulo(): string {
		if (this.contratoEditado) {
			return this.contratoEditado.descripcionStr;
		}
		if (this.todosClientes) {
			return "Contratos";
		} else {
			return "Contratos de " + this.cliente.nombreCompleto;
		}
	}
	@Input()
	set cliente(val: Cliente) {
		this._cliente = val;
	}
	@Output()
	public vistaChange: EventEmitter<string> = new EventEmitter<string>();

	@Output()
	public docsActualizados: EventEmitter<boolean> = new EventEmitter<boolean>();
	get cliente(): Cliente {
		return this._cliente;
	}
	public estadosSeleccionados: string[] = ["A", "P", "R", "L"];
	public filtros = {

	};
	constructor(
		private contratosService: ContratosService,
		private router: Router,
		public route: ActivatedRoute,
		private confirmationService: ConfirmationService) {
		super( "listado");
	}
	ngOnInit() {
		let $this = this;
		this.options.customButtons = {
			verListado: {
				text: 'Ver Listado',
				icon: 'fa-hamburguer',
				click: r => { this.goTo('listado') }
			}
		}


		this.getContratos();

		let url = this.route.snapshot.url.join('');
		if (url.includes("alta-contrato")) {
			this.nuevoContrato();
		}

	}


	get items() {


		return this._items;
	}

	get esLiquidable(): boolean {
		return this.contratoSeleccionado && this.contratoSeleccionado.saldo <= 0 && this.contratoSeleccionado.estado.codigo !== "L";

	}
	public getContratos() {
		let $this = this;
		this.addLoadingCount();
		if (!this.todosClientes) {
			this.contratosService.getByCliente(this.cliente).then(r => {
				$this._contratos = r;
				$this.filtrar();
				$this.generarEventos(r);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.contratosService.getAll().then(r => {
				$this._contratos = r;
				$this.filtrar();
				$this.generarEventos(r);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
	}

	public generarEventos(contratos: ContratoCabecera[]) {
		this.eventos = [];
		let $this = this;
		contratos.forEach(c => {

			$this.eventos.push(new CalendarEvent(
				c.id, c.descripcionCorta(), false, c.fechaEvento,
				c.fechaEvento, "evento evento_" + c.estado.codigo + " horario_" + c.codigoHorario,
				c
			));
		});
		this.eventos = [... this.eventos];
	}
	public eventClick(event: any, cal?: any) {
		this.mostrarAcciones(event.jsEvent, event.calEvent.fuente);
	}

	public mostrarAcciones(event: any, contrato: ContratoCabecera) {
		this.contratoSeleccionado = contrato;
		this.menu.show(event);
	}
	public aprobable(contrato: ContratoCabecera): boolean {
		return contrato.estado.codigo === "P" && this.esAdministrador
	}

	public aprobar(contrato: ContratoCabecera) {

		if (this.esAdministrador) {
			let $this = this;
			$this.addLoadingCount();
			this.contratosService.aprobar(contrato.id).then((r) => {
				$this.getContratos();
				$this.susLoadingCount();
				$this.success("Contrato Aprobado");
			}).catch(this.errorHandler);
		} else {
			this.error("USUARIO NO AUTORIZADO");
		}


	}

	public rechazar(contrato: ContratoCabecera) {

		if (this.esAdministrador) {
			let $this = this;
			$this.addLoadingCount();
			this.contratosService.rechazar(contrato.id).then((r) => {
				$this.getContratos();
				$this.susLoadingCount();
				$this.success("Contrato Rechazado");
			}).catch(this.errorHandler);
		} else {
			this.error("USUARIO NO AUTORIZADO");
		}


	}

	public reaplicar(contrato: ContratoCabecera) {

		if (this.esAdministrador) {
			let $this = this;
			$this.addLoadingCount();
			this.contratosService.reaplicar(contrato.id).then((r) => {
				$this.getContratos();
				$this.susLoadingCount();
				$this.success("Contrato Reaplicado");
			}).catch(this.errorHandler);
		} else {
			this.error("USUARIO NO AUTORIZADO");
		}


	}

	public rechazable(contrato: ContratoCabecera): boolean {
		return contrato.estado.codigo === "P" && this.esAdministrador
	}

	public reaplicable(contrato: ContratoCabecera): boolean {
		return contrato.estado.codigo === "R";
	}

	public eliminable(contrato: ContratoCabecera): boolean {
		return contrato.estado.codigo === "A" || contrato.estado.codigo != "A";
	}

	public esEditable(contrato: ContratoCabecera): boolean {
		return true;
	}
	public contratoGuardado(contrato: Contrato) {
		this.getContratos();
		this.volverListado();

	}
	public editarContrato(contrato: ContratoCabecera) {

		let $this = this;
		this.addLoadingCount();
		this.contratosService.getById(contrato.id).then((res) => {
			$this.contratoEditado = res;
			$this.tituloContrato = this.contratoEditado.descripcionStr;
			$this.goTo("gestionar-contrato");
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public generarPago(contrato: ContratoCabecera) {

		let $this = this;
		this.addLoadingCount();
		$this.tituloContrato = this.contratoSeleccionado.descripcion;
		$this.goTo("gestionar-pago");
		$this.susLoadingCount();
	}
	public volverListado() {
		this.visualizandoContrato = false;
		this.contratoEditado = null;
		this.volver();
	}
	public visualizarContrato(contrato: ContratoCabecera) {
		this.visualizandoContrato = true;

		let $this = this;
		this.addLoadingCount();

		this.contratosService.getById(contrato.id).then((res) => {
			$this.contratoEditado = res;
			$this.tituloContrato = this.contratoEditado.descripcionStr;
			$this.goTo("visualizar-contrato");
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}

	public nuevoContrato() {
		this.contratoEditado = new Contrato();
		if (this.cliente) {
			this.contratoEditado.cliente = this.cliente;
		}
		this.tituloContrato = "NUEVO CONTRATO";
		this.goTo("gestionar-contrato");

	}
	get todosClientes(): boolean {
		return !this.cliente;
	}

	get contratos(): ContratoCabecera[] {
		return this._filtrados;
	}
	public deleteContrato(contrato: ContratoCabecera) {
		let $this = this;
		this.confirmationService.confirm({
			header: "Eliminar Evento",
			message: 'Va a eliminar el contrato ' + contrato.descripcion + '. Desea continuar?',
			accept: () => {
				$this.addLoadingCount();
				$this.contratosService.deleteById(contrato.id).then((res) => {
					$this._contratos = $this._contratos.filter(c => c.id !== contrato.id);
					$this.filtrar();
					$this.susLoadingCount();
					$this.success("Se borró el contrato correctamente");

				}).catch($this.errorHandler);
			}
		});
	}
	public liquidarContrato(contrato: ContratoCabecera) {
		let $this = this;
		if (this.esLiquidable) {


			this.confirmationService.confirm({
				header: "Cerrar Evento",
				message: 'Va a cerrar el evento ' + contrato.descripcion + '. Desea continuar?',
				accept: () => {
					$this.addLoadingCount();
					$this.contratosService.liquidar(contrato.id).then((res) => {
						$this.getContratos();
						$this.susLoadingCount();
						$this.success("Se cerró el contrato correctamente");

					}).catch($this.errorHandler);
				}
			});
		} else {
			this.error("No se puede cerrar el evento seleccionado");
		}
	}
	public filtrar() {
		this.addLoadingCount();
		this._filtrados = this._contratos.filter(f => this.estadosSeleccionados.indexOf(f.estado.codigo) > -1);
		this.susLoadingCount();
	}



}
import { Descriptivo } from '../../model/Descriptivo';


export class CabeceraComisionista extends Descriptivo{
	constructor(

		public id? : number,
		public nombre? : string,
		public contacto? :string,
		public telefono? : string,
		public celular? : string,
		public email? : string,
		public tipoComisionista? : Descriptivo,
		public activo? : boolean,
		public saldo? : number,
	){
        super(id +"", nombre);
    }

	public static fromData(data : any) : CabeceraComisionista{
		if(!data) return null; 
		let o : CabeceraComisionista  = new CabeceraComisionista(
		 data.id,
		 data.nombre,
		 data.contacto,
		 data.telefono,
		 data.celular,
		 data.email,
		data.tipoComisionista? Descriptivo.fromData(data.tipoComisionista) : null,
		 data.activo,
		 data.saldo,	);
		return o; 

	}

    get tipoComisionistaStr() : string{
        return this.tipoComisionista? this.tipoComisionista.descripcion : "SD";
    }
}
import { Contrato } from './Contrato';
import { Descriptivo } from './../../model/Descriptivo';
import { ServicioContratado } from './ServicioContratado';
import { PagoComision } from './PagoComision';

export class Comision {
	public totalLiquidado: number = 0;
	constructor(
		public id?: number,
		public comisionista?: Descriptivo,
		public producto?: Descriptivo,
		public porcentajeComision?: number,
		public totalAComisionar?: number,
		public saldoComision: number = 0,
		public tipoComision?: Descriptivo,
		public cobraAlInicio?: boolean,
		public cobraAlLiquidar?: boolean,
		public cobraPorCobro?: boolean,
		public centro?: Descriptivo,
		public porcentajeAdelanto: number = 100,
		public pagos: PagoComision[] = [],
		public baseContrato : number = 0
	) {
		this.totalLiquidado = this.pagos.reduce((a, b) => a + b.totalLiquidado, 0);
		this.saldoComision = this.totalAComisionar - this.totalLiquidado;
	}
	public static fromData(data: any): Comision {
		if (!data) return null;
		let o: Comision = new Comision(
			data.id,
			data.comisionista ? Descriptivo.fromData(data.comisionista) : null,
			data.producto ? Descriptivo.fromData(data.producto) : null,
			data.porcentajeComision,
			data.totalAComisionar ? Math.round(data.totalAComisionar * 100) / 100 : 0,
			data.saldoComision ? Math.round(data.saldoComision * 100) / 100 : 0,
			data.tipoComision ? Descriptivo.fromData(data.tipoComision) : Descriptivo.SIN_DEFINIR(),
			data.cobraAlInicio,
			data.cobraAlLiquidar,
			data.cobraPorCobro,
			data.centro ? Descriptivo.fromData(data.centro) : Descriptivo.SIN_DEFINIR(),
			data.porcentajeAdelanto ? data.porcentajeAdelanto : 100,
			
			data.pagos.map(p => PagoComision.fromData(p)),
			data.baseContrato
			);
		return o;

	}
	get comisionistaStr(): string {
		return this.comisionista.descripcion;
	}
	get productoStr(): string {
		if (!this.producto) return "SIN DEFINIR";
		return this.producto.descripcion;
	}

	public updateTotalComision(servicioComisionado: ServicioContratado, c: Contrato): number {
		if (!servicioComisionado) {
			this.totalAComisionar = 0;
			return 0;
		}
		if (this.tipoComision) {
			if (this.tipoComision.codigo === "T") {
				if (this.cobraAlInicio && !this.cobraAlLiquidar && !this.cobraPorCobro) {
					this.totalAComisionar = Math.round(servicioComisionado.getTotalByFecha(c.fechaContratacion, false)  * this.porcentajeComision) / 100;
				} else {
					this.totalAComisionar = Math.round(servicioComisionado.total * this.porcentajeComision) / 100;
				}

				return this.totalAComisionar
			} else if (this.tipoComision.codigo === "S") {
				if (this.cobraAlInicio && !this.cobraAlLiquidar && !this.cobraPorCobro) {
					this.totalAComisionar = Math.round(servicioComisionado.getTotalByFecha(c.fechaContratacion, true)   * this.porcentajeComision) / 100;
				} else {
					this.totalAComisionar = Math.round(servicioComisionado.sinIncremento * this.porcentajeComision) / 100;
				}
				return this.totalAComisionar;
			} else {
				this.totalAComisionar = 0;
				return 0;
			}
		} else {
			this.totalAComisionar = 0;
			return 0;
		}

	}



}
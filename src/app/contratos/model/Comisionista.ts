
import { TipoComisionista } from './../../model/TipoComisionista';

import { Descriptivo } from '../../model/Descriptivo';



export class Comisionista extends Descriptivo{
	constructor(

		public id? : number,
        public nombre? : string,
        public contacto? : string,
		public domicilio? : string,
		public localidad? : string,
		public telefono? : string,
		public celular? : string,
		public email? : string,
		public razonSocial? : string,
		public cuit? : string,
		public iva? : Descriptivo,
		public domicilioFacturacion? : string,
		public localidadFacturacion? : string,
		public tipoComisionista? : TipoComisionista,
		public nombreBanco? : string,
		public cbu? : string,
		public numeroCuenta? : string,
		public aliasBancario? : string,
		public activo? : boolean,
		public salon? :Descriptivo,
		public vendedor? :Descriptivo,
		public centro? : Descriptivo,
	){
        super(id+"",nombre);
    }

	public static fromData(data : any) : Comisionista{
		if(!data) return null; 
		let o : Comisionista  = new Comisionista(
		 data.id,
         data.nombre,
         data.contacto,
		 data.domicilio,
		 data.localidad,
		 data.telefono,
		 data.celular,
		 data.email,
		 data.razonSocial,
		 data.cuit,
		 data.iva? Descriptivo.fromData(data.iva) : null,
		 data.domicilioFacturacion,
		 data.localidadFacturacion,
		 data.tipoComisionista? TipoComisionista.fromData(data.tipoComisionista) : null,
		 data.nombreBanco,
		 data.cbu,
		 data.numeroCuenta,
		 data.aliasBancario,
		 data.activo,	
		 data.salon? Descriptivo.fromData(data.salon) : null,
		 data.vendedor? Descriptivo.fromData(data.vendedor) : null,
		 data.centro? Descriptivo.fromData(data.centro) : null,
		);
		return o; 

	}

	public  tieneCategoria(cat : Descriptivo) :boolean{
		return this.tipoComisionista.tieneCategoria(cat);
	}	

	public esVendedor():boolean{
		return this.vendedor && this.tipoComisionista && this.tipoComisionista.codigo === TipoComisionista.VENDEDOR_CODIGO;
	}

}
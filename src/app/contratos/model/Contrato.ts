import { CuotaServicio } from './CuotaServicio';
import { Comision } from './Comision';

import { Producto } from '../../model/Producto';
import { Cliente } from '../../model/Cliente';

import { Salon } from '../../model/Salon';
import { ServicioContratado } from './ServicioContratado';
import { Descriptivo } from "../../model/Descriptivo";


export class Contrato extends Descriptivo{
	public static PENDIENTE :string  = 'P';
	public static RECHAZADO :string  = 'R';

	constructor(

		public id? : number,
		public salon? : Salon,
		public vendedor? : Descriptivo,
		public tipoEvento? : Descriptivo,
		public cliente? : Cliente,
		public horario? : Descriptivo,
		public fechaContratacion? : Date,
		public fechaEvento? : Date,
		public baseInvitados? : number,
		public adultos : number = 0,
		public adolescentes : number = 0,
		public menores : number = 0,
		public bebes : number = 0,
		public numeroPresupuesto? : string,
		public servicios : ServicioContratado[] = [],
		public descripcion? : string,
		public esCongelado :boolean = false,
		public estado? :Descriptivo,
		public ipMaximo :number = 0,
		public cobroGarantia : number = 0,
		public comisiones: Comision[] = [],
		public totalCalculado : number = 0,
		public ipMonto : number = 0,
		public agasajados ? : string ,
		public observaciones ? : string,
		public ultimaModificacion? :Date
	){
		super(id+"", descripcion);
	}

	
	public static fromData(data : any) : Contrato{
		let o : Contrato  = new Contrato(
		data.id,
		Salon.fromData(data.salon),
		Descriptivo.fromData(data.vendedor),
		Descriptivo.fromData(data.tipoEvento),
		Cliente.fromData(data.cliente),
		Descriptivo.fromData(data.horario),
		data.fechaContratacion? new Date(data.fechaContratacion) : null,
		data.fechaEvento? new Date(data.fechaEvento) : null,
		data.baseInvitados,
		data.adultos? data.adultos : 0,
		data.adolescentes ? data.adolescentes : 0,
		data.menores ? data.menores : 0,
		data.bebes ? data.bebes : 0,
		data.numeroPresupuesto,
		data.servicios?  data.servicios.map(o => ServicioContratado.fromData(o)) : [],
		data.descripcion,
		data.esCongelado,
		Descriptivo.fromData(data.estado),
		data.ipMaximo,
		data.cobroGarantia,
		data.comisiones? data.comisiones.map(c => {
			let a = Comision.fromData(c); a.baseContrato = data.baseInvitados; return a;} ) : [],
		data.totalCalculado,
		data.ipMonto,
		data.agasajados,
		data.observaciones,
		data.ultimaModificacion? data.ultimaModificacion : null
		);
		return o;

	}

	get totalInvitados(): number{
		return this.adolescentes + this.adultos + this.menores;
	}


	get importeNetoIP(){
		return this.importeNeto * this.ipMaximo / 100
	}
	get ipTotal():number{
		return this.importeNetoIP *0.21;
	}

	public getServiciosPack(): ServicioContratado[]{
		return this.servicios.filter(s => s.grupo === Producto.PACK.codigo).sort((a,b)=>a.producto.peso > b.producto.peso?1:-1 );
	}

	public getCuotasPack() : CuotaServicio[]{
		let lista : CuotaServicio[] = [];
		this.getServiciosPack().forEach(s => lista = lista.concat(s.planPago.filter(p => p.grupo === Producto.PACK.codigo)));
		return lista;
	}
	
	public addServicio(serv : ServicioContratado){
		if(!this.servicios)	 this.servicios = [];
		this.servicios.push(serv);
		this.servicios = this.servicios.sort((a,b)=>a.producto.peso > b.producto.peso?1:-1 );
	}

	public addComision(com : Comision){
		if(!this.comisiones) this.comisiones = [];
		if(!this.getComision(com.producto.codigo,com.comisionista.codigo)){
			this.comisiones.push(com);
			this.comisiones = [...this.comisiones];
		}
		
	}

	get garantia(): ServicioContratado{
		return this.getServicio(Producto.GARANTIA) || new ServicioContratado();
	}

	get devolucion(): ServicioContratado{
		return this.getServicio(Producto.DEVOLUCION) || new ServicioContratado();
	}
	
	get sadaic(): ServicioContratado{
		return this.getServicio(Producto.SADAIC) || new ServicioContratado();
	}
	
	get importeNeto(){
		return this.servicios.filter(s => s.producto && s.producto.imputaIVA).reduce((a,b) => a = a + b.total,0);
	}

	get total(){
		return this.servicios.reduce((a,b) => a = a + b.total,0);
	}
	get esPaquete(){
		return this.servicios.some(s => s.grupo == Producto.PACK.codigo);
	}

	public quitarServicios(lista : ServicioContratado[]){
		this.servicios = this.servicios.filter(s => lista.indexOf(s) === -1);
	
		
	}
	public quitarServicio(servicio : ServicioContratado){
		this.quitarServicios([servicio]);
	}

	public eliminarCuotas(){
		this.servicios.filter(s=> s.grupo != "INTERNOS").forEach(s => s.planPago = []);
	}
	get pagosCalculados() :boolean{
		return this.servicios && this.servicios.some(s => s.planPago && s.planPago.length > 0);
	}

	get descripcionStr(){
		return this.descripcion? this.descripcion : this.tipoEvento.descripcion + " de " + this.cliente.nombreCliente;
	}

	public getServicio(codigo : string): ServicioContratado{
		return this.servicios.filter(s => s.producto && s.producto.codigo === codigo)[0] || null;
	}

	get serviciosContratados():ServicioContratado[]{
		return this.servicios.filter(s => s.grupo !== "INTERNOS").sort((a,b)=>a.producto.peso > b.producto.peso?1:-1 );
	}

	get descripcionCorta(){
        return this.descripcion;
	}
	
	get locacion(): string{
		return this.salon.descripcion;
	}

	get esEditable():boolean{
		return this.estado && (this.estado.codigo === Contrato.RECHAZADO || this.estado.codigo === Contrato.PENDIENTE);
	}
	get esAprobado():boolean{
		return this.id && !this.esEditable;
	}

	public quitarComisiones(lista :ServicioContratado[]){
		let $this = this;
		lista.forEach(s =>{
			$this.comisiones = $this.comisiones.filter(c => c.producto.codigo !== s.producto.codigo);
		});
	}
	public getComision(codigoProducto : string, codigoComisionista : string){
		return this.comisiones
			.filter(c => c.producto.codigo === codigoProducto 
					&& c.comisionista.codigo === codigoComisionista)[0] || null;
	}

	public packDivisible():boolean{
		return this.getServiciosPack().some(p => p.producto.divisible);
	}

	public getPackDivisible(): ServicioContratado{
		return this.getServiciosPack().filter(p => p.producto.divisible)[0];
	}
}

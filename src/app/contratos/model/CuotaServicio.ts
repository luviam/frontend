import { TreeNode } from 'primeng/primeng';
import { Producto } from '../../model/Producto';

import { Descriptivo } from "../../model/Descriptivo";

import * as moment from 'moment';


export class CuotaServicio implements TreeNode{
	private _incremento100 : number = 0;
	
	type?: string;
	partialSelected?: boolean;
	styleClass?: string;
	draggable?: boolean;
	droppable?: boolean;
	selectable?: boolean;
	public parent?: TreeNode;
	constructor(

		public id? : number,
		public producto? : Producto,
		public fechaContrato? : Date,
		public fechaVencimiento? : Date,
		public fechaEstimadaPago? : Date,
		public cantidad? : number,
		public costoUnitario? : number,
		public saldo? : number,
		public pIncremento : number = 0,
		public expanded?: boolean,
		public descripcion? :string,
		public grupo? : string
		
	){
		if(!saldo && saldo !== 0){
			this.saldo = this.total;
		}
		if(!descripcion){
			this.descripcion = this.producto? this.producto.nombre : "ERROR - SIN PRODUCTO";
		}
	}

	public static fromData(data : any) : CuotaServicio{
		let o : CuotaServicio  = new CuotaServicio(
			data.id,
			Producto.fromData(data.producto),
			data.fechaContrato? new Date(data.fechaContrato) : null,
			data.fechaVencimiento? new Date(data.fechaVencimiento) : null,
			data.fechaEstimadaPago? new Date(data.fechaEstimadaPago) : null,
			data.cantidad,
			data.costoUnitario,
			data.saldo,
			data.pIncremento);
			o.incremento100 = Math.round(o.pIncremento * 10000) / 100;
			o.descripcion = data.descripcion;
			o.grupo = data.grupo;
        return o; 

	}

	get fechaContratoStr() :string{
		return moment(this.fechaContrato).format('DD/MM/YYYY');
	}

	get fechaEstimadaPagoStr() :string{
		return moment(this.fechaEstimadaPago).format('DD/MM/YYYY');
	}
	get fechaVencimientoStr() :string{
		return moment(this.fechaVencimiento).format('DD/MM/YYYY');
	}
	get total(): number{
		return this.getCostoUnitario * this.cantidad;
	}

	get cantidadStr() :number{
		let str : string = this.cantidad+"";
		
		if(this.producto && this.producto.codigoUnidad === "PORC"){
			return this.cantidad * 100;
		}else{
			return this.cantidad;
		}
		
	}
	set cantidadStr(val : number){
		if(this.producto && this.producto.codigoUnidad === "PORC"){
			this.cantidad = Number(val) /100;
		}else{
			this.cantidad =Number(val);
		}
		
	}
	
	get cantidadFormateada(): string{
		return (this.producto && this.producto.codigoUnidad === "PORC") ? this.cantidadStr+"%" : this.cantidadStr+"";
	}

	/*get porcentajeMes() : number{
		return this.pIncremento * Number((moment(this.fechaContrato).diff(moment(this.fechaVencimiento), 'months', true)).toFixed(0));
	}
	get porcentajeMesStr():string{
		return this.porcentajeMes * 100 + "";
	}*/

	get sinIncremento(): number{
		return this.costoUnitario * this.cantidad;
	}

	get getCostoUnitario() : number{
		return this.costoUnitario * (1+this.pIncremento);
	}
	get label(): string {
		return this.descripcion;
	};
    get data():any{
		return this;
	};
    get children(): TreeNode[]{
		return [];
	}
    get leaf(): boolean{
		return true;	
	}
	get incremento100() : number{
		return 		this._incremento100;
		
	}
	get costoUnitarioGral(): number{
		return Math.round(this.getCostoUnitario * 100 ) / 100;
	}
	set costoUnitarioGral(val : number){
		this.costoUnitario =val;
	}


	set incremento100(val :number) {
		this.pIncremento = val / 100;
		this._incremento100 = val;
	}
	
}
import { PagoComision } from './PagoComision';
import { Descriptivo } from './../../model/Descriptivo';


export class ItemLiquidacionComisiones{
	constructor(

		public id? : number,
		public pagoComision? : PagoComision,
		public importe? : number,
	){}

	public static fromData(data : any) : ItemLiquidacionComisiones{
		if(!data) return null; 
		let o : ItemLiquidacionComisiones  = new ItemLiquidacionComisiones(
		 data.id,
		 data.pagoComision? PagoComision.fromData(data.pagoComision) : null,
		 data.importe,	);
		return o; 

	}

}
import { CentroCosto } from './../../model/CentroCosto';
import { Comisionista } from './Comisionista';
import { ItemLiquidacionComisiones } from './ItemLiquidacionComisiones';
import { PagoLiquidacion } from './PagoLiquidacion';
import { Descriptivo } from './../../model/Descriptivo';
import * as moment from 'moment';


export class LiquidacionComisiones{

    
	constructor(
        public id? : number,
        public numero?:string,
        public descripcion? : string,
        public responsable? : string,
        public fecha : Date = new Date(),   
        public fechaPago ?: Date, 
		public pagosLiquidacion : PagoLiquidacion[] = [],
		public items : ItemLiquidacionComisiones[] = [],
		public comisionista? : Comisionista,
        public observaciones? : string,
        public centroCosto? : Descriptivo,
        public estado? : Descriptivo,
        public premio : number = 0,
        public numeroComprobante? :string
	){}

	public static fromData(data : any) : LiquidacionComisiones{
		if(!data) return null; 
		let o : LiquidacionComisiones  = new LiquidacionComisiones(
        data.id,
        data.numero,
        data.descripcion,
        data.responsable,
        data.fecha? new Date(data.fecha) : null,
        data.fechaPago? new Date(data.fechaPago) : null,
		 data.pagosLiquidacion?  data.pagosLiquidacion.map(a => PagoLiquidacion.fromData(a)) : [],
		 data.items?  data.items.map(a => ItemLiquidacionComisiones.fromData(a)) : [],
		 Comisionista.fromData(data.comisionista),
         data.observaciones,	
        data.centroCosto? Descriptivo.fromData(data.centroCosto) : null,
        data.estado? Descriptivo.fromData(data.estado): Descriptivo.SIN_DEFINIR(),
        data.premio,
        data.numeroComprobante
        );
		return o; 

    }
    
    public addPago(pago : PagoLiquidacion){
        
        this.pagosLiquidacion.push(pago);
    }

    public quitarPago(pago : PagoLiquidacion){
        let i = this.pagosLiquidacion.indexOf(pago);
        if(i>-1){
            this.pagosLiquidacion.splice(i,1);
        }
    }
    public quitarItem(item :PagoLiquidacion){
        let i = this.items.indexOf(item);
        if(i>-1){
            this.items.splice(i,1);
        }
    }
    get total() : number{
        if(!this.items || this.items.length === 0){
            return this.premio;
        }
        return this.items.reduce((a,b) => a + b.importe, 0) + this.premio;
    }

    public getTotalItems():number{
      return this.total - this.premio;
    }

    get totalPagos():number{
        return this.pagosLiquidacion.reduce((a,b) => a + b.monto, 0);
    }

    get tieneCheques() :boolean{
        return this.pagosLiquidacion && this.pagosLiquidacion.some(p => p.tipoOperacion.codigo === 'C');
    }

    get fechaFormateada(){
        return moment(this.fecha).format('DD/MM/YYYY');
    }

    get totalCubiertos():number{
        if(!this.comisionista || !this.comisionista.codigo) return 0;
        return this.items.filter(i =>( i.pagoComision.tipoPago.codigo === "A" || i.pagoComision.tipoPago.codigo === "L")
                                && i.pagoComision.categoria.codigo === 'MENU' 
                                && (( this.comisionista.vendedor && i.pagoComision.vendedor && i.pagoComision.vendedor.codigo === this.comisionista.vendedor.codigo) 
                                    || !this.comisionista.vendedor))
                                .reduce((a,b) => {
                                    if(b.pagoComision.tipoEvento.codigo === "CORP" && b.pagoComision.tipoPago.codigo === "A"){
                                        return a + b.pagoComision.baseContrato;
                                    }else{
                                        return a + b.pagoComision.cantidadBase;
                                    }
                                },0);
        
    }


}


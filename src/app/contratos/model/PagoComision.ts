import { ServicioContratado } from './ServicioContratado';
import { Descriptivo } from './../../model/Descriptivo';



export class PagoComision {
	constructor(

		public id?: number,
		public salon?: Descriptivo,
		public cliente?: Descriptivo,
		public evento?: Descriptivo,
		public nombreProducto? : string,
		public categoria? : Descriptivo,
		public idServicio? :number,
		public cantidadBase?:number,
		public precioUnitario? : number,
		public tipoPago?: Descriptivo,
		public porcentajeComision?: number,
		public totalComision?: number,
		public totalLiquidado?: number,
		public fechaComision? : Date,
		public tipoEvento? : Descriptivo,
		public vendedor? : Descriptivo,
		public baseContrato? : number
	) { }

	public static fromData(data: any): PagoComision {
		if (!data) return null;
		let o: PagoComision = new PagoComision(
			data.id,
			Descriptivo.fromData(data.salon),
			Descriptivo.fromData(data.cliente),
			Descriptivo.fromData(data.evento),
			data.nombreProducto,
			Descriptivo.fromData(data.categoria),
			data.idServicio,
			data.cantidadBase? data.cantidadBase : 0,
			data.precioUnitario? data.precioUnitario : 0,
			Descriptivo.fromData(data.tipoPago),
			data.porcentajeComision,
			data.totalComision? Math.round(data.totalComision*100)/100 : 0,
			data.totalLiquidado? Math.round(data.totalLiquidado*100)/100 : 0,
			data.fechaComision? new Date(data.fechaComision):null ,
			Descriptivo.fromData(data.tipoEvento),
			Descriptivo.fromData(data.vendedor),
			data.baseContrato);
		return o;

	}

	get saldo(): number {
		return Math.round((this.totalComision - this.totalLiquidado)*100)/100;
	}

}
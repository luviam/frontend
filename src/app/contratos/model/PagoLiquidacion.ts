import { Cheque } from './../../model/Cheque';
import { Descriptivo } from './../../model/Descriptivo';

export class PagoLiquidacion{
	constructor(public id? :number,
        public tipoOperacion? : Descriptivo,
        public centroCosto? : Descriptivo,
        public cuenta? : Descriptivo,
        public monto?: number,
        public comprobante?: Cheque){
            
        }

	public static fromData(data : any) : PagoLiquidacion{
		if(!data) return null; 
		let o : PagoLiquidacion  = new PagoLiquidacion(
		 data.id,
		 Descriptivo.fromData(data.tipoOperacion),
		 Descriptivo.fromData(data.centroCosto),
		 Descriptivo.fromData(data.cuenta),
		 data.monto,
		 Cheque.fromData(data.comprobante));
		return o; 

	}

}


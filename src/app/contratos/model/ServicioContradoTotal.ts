import { CuotaServicio } from './CuotaServicio';
import { Producto } from '../../model/Producto';
import { TreeNode } from 'primeng/primeng';
import { ServicioContratado } from "./ServicioContratado";
import * as moment from 'moment';

export class ServicioContratadoTotal extends ServicioContratado{
    
    constructor(
		public id? : number,
		public fechaContrato? : Date,
        public producto? : Producto,
        public planPago : CuotaServicio[] = [],
		public costoUnitario? : number,
		public cantidad? : number,
		public grupo? :string,
		public expanded?: boolean,
		type?: string,
		partialSelected?: boolean,
		styleClass?: string,
		draggable?: boolean,
		droppable?: boolean,
		selectable?: boolean,
        parent?: TreeNode,
        _fechaEvento? : Date

	){
        super(id,fechaContrato,producto,planPago,costoUnitario,cantidad,grupo,null,expanded,
                type,partialSelected,styleClass,draggable,droppable,selectable,parent);
        
                if(this.planPago.length === 0){
                    this.agregarCuota(
                        new CuotaServicio(
                            null,
                            producto,
                            fechaContrato,
                            _fechaEvento,
                            _fechaEvento,
                            1,
                            costoUnitario,
                            costoUnitario,
                            0));
                            
                }
                
    }
    
    public setFechaEvento(val :Date){
       
        this.planPago.forEach(c => {c.fechaEstimadaPago = val; c.fechaVencimiento = val });
    }
	

}
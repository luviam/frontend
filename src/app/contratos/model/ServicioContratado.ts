import { TreeNode } from 'primeng/primeng';

import { Descriptivo } from "../../model/Descriptivo";
import { CuotaServicio } from "./CuotaServicio";

import * as moment from 'moment';
import { Producto } from "../../model/Producto";


export class ServicioContratado implements TreeNode{

	private _descripcion : string ="";
	constructor(

		public id? : number,
		public fechaContrato? : Date,
		public producto? : Producto,
		public planPago : CuotaServicio[] = [],
		public costoUnitario? : number,
		public cantidad? : number,
		public grupo? :string,
		public centroCosto? :Descriptivo,
		public expanded?: boolean,
		public type?: string,
		public partialSelected?: boolean,
		public styleClass?: string,
		public draggable?: boolean,
		public droppable?: boolean,
		public selectable?: boolean,
		public parent?: TreeNode,
		

	){
	
		if(this.planPago){
			this.planPago.forEach(p => p.parent = this);
		}else{
			this.planPago = [];
		}
	}
	public setCongelado(val : number){
		
		this.planPago.forEach(p => p.pIncremento = val)
	}
	
	public static fromData(data : any) : ServicioContratado{
		let o : ServicioContratado  = new ServicioContratado(
			data.id,
			data.fechaContrato? new Date(data.fechaContrato) : null,
			Producto.fromData(data.producto),
			data.planPago?  data.planPago.map(b => {b.producto = data.producto; return CuotaServicio.fromData(b)}) : [],
			data.costoUnitario,
			data.cantidad,
			data.grupo,
			data.centroCosto? Descriptivo.fromData(data.centroCosto) : null

		);
		o.planPago.forEach(p => p.parent = o);
		return o;

	}
	
get esPack():boolean{
	return this.grupo !==undefined;
}
	get esCantidadOk(): boolean{
		return this.cantidad === this.planPago.reduce((a,b) => a = a +b.cantidad,0);
	}
	get fechaContratoStr() :string{
		return moment(this.fechaContrato).format('DD/MM/YYYY');
	}
	
	get total(): number{
		return this.planPago.reduce((a,b) => {return a + b.total},0);
	}

	get cobrado():number{
		return this.planPago.reduce((a,b)=> a = a + (b.total - b.saldo),0);
	}
	get saldo():number{
		return this.planPago.reduce((a,b)=> a = a + b.saldo,0);
	}

	get sinIncremento(): number{
		return this.planPago.reduce((a,b) => {return a + b.sinIncremento},0);
	}

	get descripcion() : string{
		return this._descripcion? this._descripcion : (this.producto? this.producto.nombre : "ERROR - SIN PRODUCTO");
	}
	set descripcion(val : string){
		this._descripcion = val;
	}
	get cantidadStr() :number{
		let total : number = Math.round(this.planPago.reduce((a,b)=>{return a + b.cantidad},0) * 100) / 100;
		let str : string = this.cantidad+"";
		
		if(this.producto && this.producto.codigoUnidad === "PORC"){
			return Math.floor(total * 100);
		}else{
			return total;
		}
		
	}
	set cantidadStr(val : number){
		
		if(this.producto && this.producto.codigoUnidad === "PORC"){
			this.cantidad = Math.floor(Number(val) /100);
		}else{
			this.cantidad = Number(val);
		}
		
	}
	get costoUnitarioGral(){
		return this.costoUnitario;
	}
	set costoUnitarioGral( val : number){
		this.costoUnitario = val;
		this.planPago.forEach(p => p.costoUnitario = val);
	}

	public agregarCuota(cuota : CuotaServicio){
		if(!this.planPago) this.planPago = [];
		cuota.parent = this;
		this.planPago.push(cuota);
		
	}
	public quitarCuota(cuota : CuotaServicio){
		this.planPago = this.planPago.filter(c=> c != cuota);
	}



	get label(): string {
		return this.descripcion;
	};
    get data():any{
		return this;
	};
    get children(): TreeNode[]{
		return this.planPago;
	}
    get leaf(): boolean{
		return false;	
	}
	
	public getTotalByFecha(fecha : Date, sinIncremento : boolean){
		fecha = moment(fecha).startOf("day").toDate();
		return this.planPago.filter(p =>moment(p.fechaContrato).diff(fecha) == 0).reduce((a,b)=> a = a +  (sinIncremento? b.sinIncremento : b.total),0);
	}

}


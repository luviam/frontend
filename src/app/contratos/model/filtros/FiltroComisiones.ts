import { Descriptivo } from './../../../model/Descriptivo';
export class FiltroComisiones {

    constructor(public comisionista?: Descriptivo,
        public centro?: Descriptivo,
        public contrato? : Descriptivo,
        public salon? : Descriptivo,
        public fechaDesde?: Date,
        public fechaHasta?: Date,
    ) {
        if(!centro){
            this.centro = Descriptivo.TODOS();
        }

        if(!comisionista){
            this.comisionista = Descriptivo.TODOS();
        }
        if(!salon){
            this.salon = Descriptivo.TODOS();
        }
        if(!contrato){
            this.contrato = Descriptivo.TODOS();
        }
    }

    public static fromData(data : FiltroComisiones) : FiltroComisiones{
        if(!data) return null;
        let o : FiltroComisiones = new FiltroComisiones(
            Descriptivo.fromData(data.comisionista),
            Descriptivo.fromData(data.centro),
            Descriptivo.fromData(data.contrato),
            Descriptivo.fromData(data.salon),
            data.fechaDesde? new Date(data.fechaDesde) : null,
            data.fechaHasta? new Date(data.fechaHasta) : null
        );
        return o;
    }

    public cambio(val: FiltroComisiones):boolean{
        return JSON.stringify(this) !==  JSON.stringify(val);
    }

    public comisionistaDefinido():boolean{
        return this.comisionista && this.comisionista.codigo && this.comisionista.codigo != Descriptivo.TODOS().codigo;
    }
}
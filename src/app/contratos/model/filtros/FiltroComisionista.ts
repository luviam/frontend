import { Descriptivo } from '../../../model/Descriptivo';

export class FiltroComisionista {
    constructor(public proveedor?: Descriptivo,
        public centro?: Descriptivo,
        public fechaDesde?: Date,
        public fechaHasta?: Date,
        public cuenta?: Descriptivo,
        public descripcion?: string,
        public estado?: Descriptivo) {

    }

    

}
import { Comisionista } from '../model/Comisionista';
import { CabeceraComisionista } from '../model/CabeceraComisionista';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicioAbstract } from '../../common-services/service.service';

@Injectable()
export class ComisionistaService extends ServicioAbstract {

   

    
    public getAllList(filtro: any): Promise<any> {
        return this.http.get(this.getApiURL() + "comisionista/cabeceras")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cabeceras: CabeceraComisionista[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    cabeceras = res.respuesta.map( v => CabeceraComisionista.fromData(v));
                }
                return cabeceras;

            }, this.handleError);

    }

    public getAll(): Promise<Comisionista[]> {
        return this.http.get(this.getApiURL() + "comisionista/all")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let data: Comisionista[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    data = res.respuesta.map( v => Comisionista.fromData(v));
                }
                return data;

            }, this.handleError);

    }

    public getSaldo(id:number) : Promise<any>{
        return this.http.get(this.getApiURL() + "comisionista/saldo/" + id )
        .toPromise()
        .then((response: any) => {
            let res = response;
            return res.respuesta;

        }, this.handleError);
    }

    public getById(id: number): Promise<Comisionista> {
        return this.http.get(this.getApiURL() + "comisionista/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let comisionista: Comisionista;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    comisionista = this.parseComisionista(res.respuesta);
                }
                return comisionista;

            }, this.handleError);

    }
    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'comisionista', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar(comisionista: Comisionista): Promise<Comisionista> {
        if (comisionista.id) {
            return this.http.post(this.getApiURL() + 'comisionista', comisionista).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'comisionista', comisionista).toPromise().then(this.handleOk, this.handleError);
        }
    }

    private parseComisionista(data: any) : Comisionista{

        return Comisionista.fromData(data);
    }
}
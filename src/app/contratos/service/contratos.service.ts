import { EstadoIVA } from './../../model/EstadoIVA';
import { CabeceraCliente } from '../../clientes/lista-clientes/shared/CabeceraCliente';
import { ContratoCabecera } from '../../clientes/lista-clientes/shared/ContratoCabecera';
import { CuotaServicio } from '../model/CuotaServicio';

import { Cliente } from '../../model/Cliente';

import {Descriptivo} from '../../model/Descriptivo';
import { RequestOptions } from '@angular/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Contrato } from '../model/Contrato';

@Injectable()
export class ContratosService extends ServicioAbstract {

   

    public delete(contrato: Contrato): Promise<boolean> {
        return this.deleteById(contrato.id);
    }


    public deleteById(idContrato: number): Promise<boolean> {
        let params = new HttpParams();
        params = params.append("id",idContrato+"")
        return this.http.delete(this.getApiURL() + 'contrato', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(id: number): Promise<Contrato> {
        return this.http.get(this.getApiURL() + "contrato/" + id)
            .toPromise()
            .then(this.parseContrato, this.handleError);

    
        }
    public getByClienteId(codigo : string): Promise<ContratoCabecera[]> {
        return  this.http.get(this.getApiURL() + "contrato/byCliente/" +codigo)
        .toPromise()
        .then(this.parseCabeceras, this.handleError);
    }

    public getEstadoIVA(idContrato : number) : Promise<EstadoIVA>{
        return  this.http.get(this.getApiURL() + "contrato/estadoIva/" +idContrato)
        .toPromise()
        .then((r:any)=>{return EstadoIVA.fromData(r.respuesta)}, this.handleError);
    }

    public getByComisionista(codigo : string): Promise<ContratoCabecera[]> {
        return  this.http.get(this.getApiURL() + "contrato/byComisionista/" +codigo)
        .toPromise()
        .then(this.parseCabeceras, this.handleError);
    }



    public getByCliente(cliente: Cliente): Promise<ContratoCabecera[]> {
        return this.getByClienteId(cliente.id+"");
        

    }

    public getCuotas(codigoContrato : string) : Promise<CuotaServicio[]>{
        return  this.http.get(this.getApiURL() + "contrato/cuotas/impagas/" + codigoContrato)
        .toPromise()
        .then(this.parseCuotasServicio, this.handleError);
    }

    public getAll():Promise<ContratoCabecera[]>{
        return this.http.get(this.getApiURL() + "contrato/cabecera/all/").toPromise().then(this.parseCabeceras,this.handleError);
    }

    private parseContratos(response : any) : Contrato[]{
        let res = response;
        let contratos: Contrato[]  = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            contratos = res.respuesta.map(c => Contrato.fromData(c));
        }
      return contratos;
    }

    
    private parseCabeceras(response : any) : ContratoCabecera[]{
        let res = response;
        let contratos: ContratoCabecera[]  = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            contratos = res.respuesta.map(c => ContratoCabecera.fromData(c));
        }
      return contratos;
    }

    private parseCuotasServicio(response : any) : CuotaServicio[]{
        let res = response;
        let cuotas: CuotaServicio[] = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            cuotas = res.respuesta.map(c => CuotaServicio.fromData(c));
        }
        return cuotas;
    }
    private parseContrato(response : any) : Contrato{
        
            let res = response;
            let contrato: Contrato;
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                contrato = Contrato.fromData(res.respuesta);
            }
            return contrato;
    }
    public aprobar(id  : number){
        return this.http.post(this.getApiURL() + 'contrato/aprobar', id).toPromise().then(this.parseContrato, this.handleError);
    }
    public rechazar(id  : number){
        return this.http.post(this.getApiURL() + 'contrato/rechazar', id).toPromise().then(this.parseContrato, this.handleError);
    }

    public reaplicar(id  : number){
        return this.http.post(this.getApiURL() + 'contrato/reaplicar', id).toPromise().then(this.parseContrato, this.handleError);
    }

    public liquidar(id  : number){
        return this.http.post(this.getApiURL() + 'contrato/liquidar', id).toPromise().then(this.parseContrato, this.handleError);
    }
    public guardar(contrato: Contrato): Promise<any> {
        if (contrato.id) {
            return this.http.post(this.getApiURL() + 'contrato', contrato).toPromise().then(this.parseContrato, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'contrato', contrato).toPromise().then(this.parseContrato, this.handleError);
        }
    }

    

}

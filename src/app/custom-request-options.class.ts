import { ReflectiveInjector, Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie'; 
import { AppUser } from './model/AppUser';
/**
 * Extending BaseRequestOptions to inject common headers to all requests.
 */
import {RequestOptions, RequestOptionsArgs,  BaseRequestOptions} from '@angular/http';

@Injectable()
export class CustomRequestOptions extends BaseRequestOptions {
    
    private cookieService : CookieService;
    constructor(cookieService:CookieService) {
        super();
        this.cookieService = cookieService;
    }

    merge(options?: RequestOptionsArgs): RequestOptions {
        var newOptions = super.merge(options);
        if (this.cookieService.getObject("cateringUser") !== undefined && this.cookieService.getObject("cateringUser") != null){
            //newOptions.headers.append('Authorization', (<AppUser>this.cookieService.getObject("cateringUser")).authdata);
        }
        return newOptions;
    }

}
import { AbstractControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {
    /**
     * Chequea que el valor ingresado no sea vacio o nulo, 
     * y en caso de ser un string, que no este vacio, lo que incluye que sólo haya espacios
     * @param control El campo a validar
     */
    static required(control: AbstractControl): ValidationErrors {
         return !control.value || control.value === undefined || (typeof control.value == "string" && (control.value == "" || control.value.trim() == "")) ?
               {"required": true} :
               null;
    }

    /**
     * Chequea que el valor ingresado sea un email valido, segun la especificacion mundial de formato de email
     * Además chequea que lo ingresado no sean solo espacios, con validacion de trim
     * 
     * @param control El campo a validar
     */
    static email(control: AbstractControl) {
        if(!control.value ) return null;
        return !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(control.value) ?
               {email: {valid: false}} :
               null;
    }

}
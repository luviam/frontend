import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { CentroCosto } from '../../model/CentroCosto';
import { ImputacionCuentaConfig } from '../../model/ImputacionCuentaConfig';
import { MenuItem } from 'primeng/primeng';
import { EmpleadosService } from '../service/EmpleadosService';
import { ConvenioSalon } from '../../model/ConvenioSalon';

import { Descriptivo } from '../../model/Descriptivo';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CentroCostoService } from '../../common-services/centroCostoService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit } from '@angular/core';
import { Empleado } from '../../model/Empleado';
import { AltaEmpleado } from '../../model/AltaEmpleado';

@Component({
	selector: 'gestion-empleado',
	styleUrls: ["./gestion-empleados.component.less"],
	templateUrl: 'gestion-empleados.component.html'
})

export class GestionEmpleadosComponent extends SessionComponent {
	public empleado: Empleado = new Empleado();
	public tiposJornada: Descriptivo[] = [];
	public tiposEmpleado: Descriptivo[] = [];
	public sectores: Descriptivo[] = [];
	public centros: Descriptivo[] = [];
	public editable: boolean = false;
	public editando: boolean = false;
	public finalizado: boolean = false;
	public titulo: string = "";
	public mostrarAlta : boolean = false;
	public altaEditada : AltaEmpleado;
	public tituloDialogo : string;
	public altaOriginal: AltaEmpleado;
	
	constructor(cookieService :CookieServiceHelper,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private empleadoService: EmpleadosService) {
		super();
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;

		this.addLoadingCount();
		this.descriptivosService.getAllTiposJornada().then((res) => {
			if (res) {
				$this.tiposJornada = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getCentrosCosto().then((res) => {
			if (res) {
				$this.centros = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllTiposEmpleado().then((res) => {
			if (res) {
				$this.tiposEmpleado = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllSectores().then((res) => {
			if (res) {
				$this.sectores = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		
		this.empleado = new Empleado();
		

		$this.susLoadingCount();
		this.route.params.subscribe(params => {

			let idempleado: string = <string>params['id'];
			if (idempleado) {
				this.addLoadingCount();
				this.empleadoService.getById(parseInt(idempleado)).then((pro) => {
					$this.empleado = pro;
					$this.titulo = "Editando - " + $this.empleado.nombre;
					$this.susLoadingCount();
					$this.editable = true;
					$this.editando = true;					
				}).catch(this.errorHandler);
			} else {
				this.titulo = "Nuevo empleado";			
				$this.editable = true;

			}

		});

	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['empleados/lista-empleados']);
			}
		});
	}

	public guardarEmpleado() {
		let $this = this;
		if (this.empleadoValido(this.empleado)) {
			this.addLoadingCount();
			
			this.empleadoService.guardar(this.empleado).then((c) => {
				$this.success("El empleado fue guardado correctamente");
				$this.finalizado = true;
				setTimeout(() => {
					$this.router.navigate(["empleados/lista-empleados"]);
				}, 3000)
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El empleado no es valido");
		}
	}
	
	public nuevaAlta(){
		if(this.empleado.altas && this.empleado.altas.some(a => !a.fechaBaja)){
			this.error("Hay un alta activa. No se puede crear una nueva.");
		}else{
			this.mostrarAlta = true;
			this.altaEditada = new AltaEmpleado();
			this.tituloDialogo = "Nueva Alta";
		}
	}
	public editarAlta(alta : AltaEmpleado){
		this.altaOriginal = alta;
		this.mostrarAlta = true;
		this.altaEditada = AltaEmpleado.fromData(alta);
		this.tituloDialogo = "Editando Alta";

	}
	public empleadoValido(empleado: Empleado): boolean {
		return true;
	}

	public guardarAlta(){
		if(this.altaOriginal){
			
			this.altaOriginal.fechaAlta = this.altaEditada.fechaAlta;
			this.altaOriginal.fechaBaja = this.altaEditada.fechaBaja;
			this.altaOriginal.observacion = this.altaEditada.observacion;
		}else{
			
			this.empleado.addAlta(AltaEmpleado.fromData(this.altaEditada));
			
		}
		this.mostrarAlta = false;
		this.altaEditada = null;
		this.altaOriginal = null;
		this.success("Se gurdó el alta");
	}
	public updateTipoJornada(event : any){
		
		
	}

	
}
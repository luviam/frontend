import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { ListaEmpleados } from './shared/lista-empleados.model';
import { EmpleadosService } from '../service/EmpleadosService';

import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit } from '@angular/core';



@Component({
	selector: 'lista-empleados',
	templateUrl: 'lista-empleados.component.html',
	providers: [EmpleadosService]
})
export class ListaEmpleadosComponent extends SessionComponent implements OnInit {
	listaEmpleados: ListaEmpleados[] = [];

	titulo: string = "Listado de Empleados"
	private filtro: any = {}
	constructor(cookieService :CookieServiceHelper,
		private listaEmpleadosService: EmpleadosService,
		private router: Router,
		private confirmationService: ConfirmationService, ) { super() }

	ngOnInit() { 
		this.getListado();
	}
	public confirmarEliminar(empleado: ListaEmpleados) {
		this.confirmationService.confirm({
			header: "Va a eliminar al Empleado " + empleado.nombre,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.listaEmpleadosService.delete(empleado.id).then((res) => {
					$this.getListado();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
			}
		});
	}

	public getListado() {
		this.addLoadingCount();
		this.listaEmpleadosService.getAllList(this.filtro).then((res) => {
			this.listaEmpleados = res;
			this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	public editarEmpleado(empleado: ListaEmpleados) {
		this.router.navigate(["/empleados/editar/" + empleado.id + ""]);
	}
	public nuevoEmpleado() {
		this.router.navigate(["/empleados/nuevo"]);
	}
}
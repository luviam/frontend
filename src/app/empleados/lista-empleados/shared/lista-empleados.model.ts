import { AltaEmpleado } from '../../../model/AltaEmpleado';
import { Descriptivo } from '../../../model/Descriptivo';
export class ListaEmpleados extends Descriptivo{
	constructor(

		public id? : number,
		public nombre? : string,
		public telefono? : string,
		public email? : string,
		public tipo? : string,
		public sector? : string,
		public altas : AltaEmpleado[] = [],
	){
		super(id+"", nombre);
	}

	public static fromData(data : any) : ListaEmpleados{
		if(!data) return null; 
		let o : ListaEmpleados  = new ListaEmpleados(
		 data.id,
		 data.nombre,
		 data.telefono,
		 data.email,
		 data.tipo,
		 data.sector,
		 data.altas?  data.altas.map(a => AltaEmpleado.fromData(a)) : [],	);
		return o; 

	}

	get cantidadAltas() : number{
		return this.altas.length || 0;
	}
}
import {Descriptivo} from '../../model/Descriptivo';
import { ListaEmpleados } from '../lista-empleados/shared/lista-empleados.model';
import { RequestOptions } from '@angular/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Empleado } from '../../model/Empleado';

@Injectable()
export class EmpleadosService extends ServicioAbstract {

   

    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'empleado', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public getAllList(filtro: any): Promise<any> {
        let codigoTipoEmpleado = filtro.tipoEmpleado && filtro.tipoEmpleado.codigo ? filtro.tipoEmpleado.codigo : "ALL";
        
        return this.http.get(this.getApiURL() + "empleado/cabecera/" + codigoTipoEmpleado + "/")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cabeceras: ListaEmpleados[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    cabeceras = res.respuesta.map(j => ListaEmpleados.fromData(j));
                }
                return cabeceras;

            }, this.handleError);

    }

    public getById(id: number): Promise<any> {
        return this.http.get(this.getApiURL() + "empleado/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let empleado: Empleado;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    empleado = Empleado.fromData(res.respuesta);
                }
                return empleado;

            }, this.handleError);

    }

    public guardar(empleado: Empleado): Promise<any> {
        if (empleado.id) {
            return this.http.post(this.getApiURL() + 'empleado', empleado).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'empleado', empleado).toPromise().then(this.handleOk, this.handleError);
        }
    }

}

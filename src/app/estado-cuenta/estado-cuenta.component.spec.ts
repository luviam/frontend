import { TestBed, inject } from '@angular/core/testing';

import { EstadoCuentaComponent } from './estado-cuenta.component';

describe('a estado-cuenta component', () => {
	let component: EstadoCuentaComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				EstadoCuentaComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([EstadoCuentaComponent], (EstadoCuentaComponent) => {
		component = EstadoCuentaComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});
import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { PlanCuentaService } from '../common-services/planCuentaService.service';
import { Cuenta } from '../model/Cuenta';

import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CuentaConfig } from '../model/ConfigCuenta';
import { SessionComponent } from '../session-component.component';


import { Component, OnInit } from '@angular/core';


@Component({
	selector: 'gestion-planDeCuentas',
	templateUrl: 'gestion-planDeCuentas.component.html',
	styleUrls: ['./gestion-planDeCuentas.component.less']
	
})

export class GestionPlanDeCuentasComponent extends SessionComponent {
	
	public planCuenta : Cuenta[] = [];
	public cuentaSeleccionada : Cuenta;
	public titulo : string ="Plan de Cuentas";
	public menuItems = [
		{ label: 'Agregar Cuenta', 
		  icon: 'fa-plus', 
		  command: (event) => this.agregarCuenta(this.cuentaSeleccionada) },
	]
	constructor(cookieService :CookieServiceHelper,
		private planCuentaService: PlanCuentaService,
		private confirmationService: ConfirmationService,
		private router: Router
		) { 
		super();
	}

	ngOnInit() {

		this.getPlan();

	}

	private getPlan(){
		this.addLoadingCount();
		let $this = this;
		this.planCuentaService.getPlanCuentas().then((res)=>{
			$this.planCuenta = res;
			$this.editable = true;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public agregarCuenta(cuenta: Cuenta) {
		if(this.cuentaSeleccionada.tipoCuenta === "SG"){
			this.error("No se puede agregar cuentas de aplicación. Gestione el Centro de Costo específico.")
			return ;
		}
		let cuentaNueva = new Cuenta(cuenta.codigo + "." + (cuenta.cuentasHijas.length + 1), "Nueva Cuenta", cuenta.getTipoCuentaHija(),null);
		cuenta.agregarCuenta(cuentaNueva);
		cuentaNueva.expanded = true;
		cuenta.expanded = true;
		this.cuentaSeleccionada = cuentaNueva;

	}

	public planValido(){
		return true;
	}
	public guardarPlan() {
		this.addLoadingCount();
		let $this = this;
		if (this.planValido()) {
			this.planCuentaService.guardarPlan(this.planCuenta).then((res) => {
				$this.success('Se guardó el plan exitosamente.' );
				this.susLoadingCount();

				$this.getPlan();
				
			}).catch(this.errorHandler);
		} else {
			$this.error('Complete todas las cuentas' );
			this.susLoadingCount();
		}

	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {

				this.router.navigate(['admin/lista-centros']);
			}
		});
	}

	public confirmDelete(cuenta){

		this.confirmationService.confirm({
			header: "Eliminar Cuenta",
			message: '¿Está seguro/a de que desea eliminar la cuenta?',
			accept: () => {
				this.deleteCuenta(cuenta);
			}
		});

	}
	private deleteCuenta(cuenta:Cuenta){
		this.addLoadingCount();
		let $this = this;
		this.planCuentaService.delete(cuenta.tipoCuenta, cuenta.id).then(()=>{
			this.susLoadingCount();
			$this.info("Se ha eliminado la cuenta");
			$this.getPlan();
		}, $this.errorHandler);
	}
}
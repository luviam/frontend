import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { PlanCuentaService } from '../common-services/planCuentaService.service';
import { Cuenta } from '../model/Cuenta';
import { CuentasService } from '../common-services/cuentasService.service';
import { DescriptivosService } from '../common-services/descriptivos.service';
import { CentroCostoConfig } from '../model/CentroCostoConfig';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CuentaConfig } from '../model/ConfigCuenta';
import { CentroCostoService } from '../common-services/centroCostoService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Descriptivo } from '../model/Descriptivo';
import { ConfigComprobanteFiscal } from '../model/ConfigComprobanteFiscal';
import { SessionComponent } from '../session-component.component';
import { Component, OnInit } from '@angular/core';
import { TreeNode, Message } from 'primeng/primeng';

@Component({
	selector: 'catering-gestionar-centro-costo',
	templateUrl: './gestionar-centro-costo.component.html',
	styleUrls: ['./gestionar-centro-costo.component.less']
})
export class GestionarCentroCostoComponent extends SessionComponent {


	public titulo: string = "Alta de Centro de Costo";
	public provincias: Descriptivo[] = [];
	public planCuentas: Cuenta[] = [];
	public planCuentasDefault: Cuenta[] = [];
	public defaultView: number = 0;
	public tiposIIBB: any[] = [
		{ value: "C", label: "CM" },
		{ value: "I", label: "ISIB" }
	]

	public centroCosto: CentroCostoConfig = new CentroCostoConfig();
	constructor(
		private route: ActivatedRoute,
		private centroCostoService: CentroCostoService,
		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private planCuentaService: PlanCuentaService,
		private cuentasService: CuentasService) { super(); }

	public cuentaSeleccionada: CuentaConfig;
	public menuItems = [
		{ label: 'Agregar Cuenta', icon: 'fa-search', command: (event) => this.agregarCuenta(this.cuentaSeleccionada) }]

	ngOnInit() {
		let $this = this;



		this.addLoadingCount();
		this.descriptivosService.getProvincias().then((res) => {
			$this.provincias = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.route.params.subscribe(params => {

			let codigoCentroCosto: string = <string>params['codigoCentroCosto'];

			if (codigoCentroCosto) {
				this.addLoadingCount();
				this.titulo = "Editar Centro - ";
				this.defaultView = 2;
				this.centroCostoService.findByCodigo(codigoCentroCosto).then((centro) => {
					$this.centroCosto = centro;

					$this.addLoadingCount();
					$this.planCuentaService.getPlanCuentas().then((res) => {
						$this.planCuentas = res;
						$this.titulo += centro.descripcion;
						$this.editable = true;
						$this.completarPlanCuenta(centro.cuentas);
						$this.susLoadingCount();
					}).catch($this.errorHandler);


					$this.susLoadingCount();


				}).catch(this.errorHandler)

			} else {
				this.editable = true;
				this.centroCosto = new CentroCostoConfig();
				this.addLoadingCount();
				this.getPlanCuentasDefault();
				this.descriptivosService.getTipoComprobantes().then((comprobantes) => {
					if (comprobantes) {
						comprobantes.forEach(tc => {
							$this.centroCosto.agregarConfComprobante(new ConfigComprobanteFiscal(null, tc.descripcion, true, "0000-00000001"));
							this.susLoadingCount();
						});
					}

				}).catch($this.errorHandler);
			}

		});

	}

	public completarPlanCuenta(cuentas: CuentaConfig[]) {
		let $this = this;

		this.planCuentas.forEach(r => r.cuentasHijas.forEach(g => g.cuentasHijas.forEach(sg => {
			cuentas.filter(c => c.idParent === sg.id).forEach(cc => {
				sg.agregarCuenta(
					new Cuenta(cc.codigo, cc.descripcion, Cuenta.APLICACION, cc.activo ? cc.activo : true, cc.id, [], sg, cc.eliminable)
				)
				if (cc.activo) {
					sg.activo = true;
					g.activo = true;
					r.activo = true;
				}
			});
		})));
		this.planCuentas = [...this.planCuentas];
	}
	public agregarCuenta(cuenta: CuentaConfig) {
		if (this.cuentaSeleccionada.tipoCuenta != Cuenta.SUB_GRUPO) {
			this.error("No se puede agregar cuentas que no sean de Aplicación. Gestione Plan de Cuentas.")
			return;
		}
		let tipoCuenta = "";
		let cuentaNueva = new CuentaConfig(cuenta.codigo + "." + (cuenta.cuentasHijas.length + 1), "Nueva Cuenta", cuenta.id, null, true);
		cuenta.agregarCuenta(cuentaNueva);
		cuentaNueva.expanded = true;
		cuenta.expanded = true;
		this.cuentaSeleccionada = cuentaNueva;

	}
	public cuentasValidas(cuentas: Cuenta[]) {
		let estado = true;
		cuentas.forEach(c => {
			if (c.descripcion.length <= 0 || c.codigo.length <= 0) {
				estado = false;
				if (c.parent) {
					c.parent.expanded = false;
				}

				c.expanded = true;
			}
			if (!c.leaf) {
				//estado = estado && this.cuentasValidas(c.cuentasHijas);
			}
		})
		return estado;
	}
	public guardarCentro() {
		this.addLoadingCount();
		let $this = this;
		if (this.cuentasValidas(this.planCuentas)) {

			this.centroCosto.cuentas = [];
			this.planCuentas.forEach(r => r.cuentasHijas.forEach(
				g => g.cuentasHijas.forEach(
					sg => sg.cuentasHijas.forEach(
						c => $this.centroCosto.cuentas.push(
							new CuentaConfig(c.codigo, c.descripcion, sg.id, c.id, c.activo)
						)))));
			this.centroCostoService.guardar(this.centroCosto).then((res) => {
				$this.success("Se guardó el centro exitosamente.");
				setTimeout(() => {
					$this.router.navigate(["admin/lista-centros"]);
				}, 3000)
				this.susLoadingCount();
				this.finalizado = true;
			}).catch(this.errorHandler);
		} else {
			
			$this.error('Complete todas las cuentas');
			this.susLoadingCount();
		}

	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['admin/lista-centros']);
			}
		});
	}


	public getRubro(cuenta: Cuenta) {
		if (cuenta.parent) {
			return this.getRubro(cuenta.parent);
		} else {
			return cuenta;
		}
	}
	public handleActivacionCuentasHijas = (cuenta: Cuenta) => {
		cuenta.cuentasHijas.forEach(ch => {
			ch.activo = cuenta.activo;
			if (ch.cuentasHijas) {
				this.handleActivacionCuentasHijas(ch);
			}
		});
	}
	public handleActivacionCuenta = (cuenta: Cuenta) => {
		this.handleActivacionCuentasHijas(cuenta);
		if (cuenta.parent) {
			let rubro = this.getRubro(cuenta);
			let rubroActivo = false;
			rubro.cuentasHijas.forEach(g => {
				let grupoActivo = false;
				g.cuentasHijas.forEach(sg => {
					let activo = false;
					sg.cuentasHijas.forEach(cc => {
						if (cc.activo) {
							activo = true;
							grupoActivo = true;
							rubroActivo = true;
							return;
						}
					});
					sg.activo = activo;
				});
				g.activo = grupoActivo;
			});
			rubro.activo = rubroActivo;
		}
	}

	public confirmDelete(cuenta) {

		this.confirmationService.confirm({
			header: "Eliminar Cuenta",
			message: '¿Está seguro/a de que desea eliminar la cuenta?',
			accept: () => {
				this.deleteCuenta(cuenta);
			}
		});

	}
	private deleteCuenta(cuenta: Cuenta) {

		let $this = this;
		if (cuenta.id) {
			this.addLoadingCount();
			this.cuentasService.delete(cuenta.id).then(() => {

				
				$this.info('Se ha eliminado la cuenta');
				let centroIndex = $this.centroCosto.cuentas.findIndex(c => c.id === cuenta.id);
				cuenta.parent.children = cuenta.parent.children.filter(c => c.data !== cuenta.data);
				$this.susLoadingCount();
			}, $this.errorHandler);
		} else {
			cuenta.parent.children = cuenta.parent.children.filter(c => c.data !== cuenta.data);
		}
	}


	private getPlanCuentasDefault() {
		let $this = this;
		this.addLoadingCount();
		this.planCuentaService.getPlanCuentas().then((res) => {
			$this.planCuentasDefault = res;
			$this.planCuentas = Array.from($this.planCuentasDefault);
			$this.addLoadingCount();
			$this.cuentasService.getCuentasDefault().then((cuentas)=>{
				$this.completarPlanCuenta(cuentas);
				$this.susLoadingCount();	
			})
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
}
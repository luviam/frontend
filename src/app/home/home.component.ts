import { CookieServiceHelper } from './../common-services/cookieServiceHelper.service';
import { SessionComponent } from './../session-component.component';
import { HomeService } from './service/home.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'eventos-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.less']
})
export class HomeComponent extends SessionComponent implements OnInit {

	
public titulo : string ="";
	constructor(private homeService:HomeService, ) {
		super();
	 }

	ngOnInit() {	
		this.titulo = "Bienvenido " + this.getUserFullName();

	}

	get esDefault(){
		return !this.esGerencia;
	}

}
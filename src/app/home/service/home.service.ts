import { HttpClient } from '@angular/common/http';

import { environment } from 'environments/environment';
import { Injectable, Inject, ReflectiveInjector } from '@angular/core';
import 'rxjs/add/operator/toPromise';

const API_URL = environment.apiUrl;

@Injectable()
export class HomeService {

    constructor(private http:HttpClient) {}

    getUserData(): Promise<any> {
        let $this: HomeService = this;
        return this.http.get(API_URL + "user")
            .toPromise()
            .then(
            function (response:any) {
                return response.nombre;
            },this.handleError);

    }

    private handleError(error: any): Promise<any> {
        console.error('Hubo un error', error);
        return Promise.reject(error.mensaje || error);
    }

}
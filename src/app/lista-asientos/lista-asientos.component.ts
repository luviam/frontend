import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { CuentaConfig } from '../model/ConfigCuenta';
import { Descriptivo } from '../model/Descriptivo';
import { OperacionContable } from '../model/OperacionContable';
import { AsientoService } from '../asiento-manual/service/asientoService.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { AsientoContable } from '../model/AsientoContable';
import { SessionComponent } from '../session-component.component';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'lista-asientos',
	styleUrls: ['./lista-asientos.component.less'],
	templateUrl: 'lista-asientos.component.html'
})

export class ListaAsientosComponent extends SessionComponent {

	public titulo: string = "Lista de Asientos";
	public estadosSeleccionados : string[] = ["P"];
	private _asientos: AsientoContable[] = [];
	private _filtrados: AsientoContable[] = [];
	public filtro: any = { "fechaDesde": new Date(), "fechaHasta": new Date() };
	get fechaMaxima() {
		var d = new Date();
		d.setHours(23, 59, 59, 0);
		return d;
	}
	constructor(cookieService :CookieServiceHelper,
		private router: Router,
		private confirmationService: ConfirmationService,
		private asientoService: AsientoService) { super() }

	ngOnInit() {

		this.filtro.fechaDesde = moment().startOf('year').toDate();
		this.filtro.fechaHasta = moment().add(1,'days').startOf('day').toDate();

		this.addLoadingCount();
		this.asientoService.getAllCabeceras(this.filtro).then((res) => {
			this._asientos = res;
			this.susLoadingCount();
		}).catch(this.errorHandler);

	}

	get asientos(): AsientoContable[]{
		
		return this._filtrados;
	}

	set asientos( asientos : AsientoContable[]){
		this._asientos = asientos;
	}
	public editarAsiento(numero: number) {
		this.router.navigate(["/admin/editar-asiento/" + numero + ""]);
	}
	public nuevoAsiento() {
		this.router.navigate(["admin/asiento-manual"]);
	}
	public confirmarAsiento(asiento: AsientoContable) {
		let $this = this;
		asiento.estado = new Descriptivo("A", "Aprobado");
		asiento.mensaje = "por " + this.getCurrentUser().user.nombre;
		$this.addLoadingCount();
		
		this.asientoService.guardar(asiento).then((res) => {
			$this.susLoadingCount();
			this.success("Asiento aprobado");
		})
	}

	public eliminarAsiento(asiento: AsientoContable){
		let $this = this;
		this.confirmationService.confirm({
			header: "Eliminar Asiento",
			message: 'Está seguro que desea eliminar el asiento? Esta acción no puede deshacerse',
			accept: () => {
				$this.addLoadingCount();
				this.asientoService.delete(asiento).then((res) => {
					$this.buscarAsientos();
					$this.susLoadingCount();
					this.success("Asiento eliminado");
				}).catch(this.errorHandler);
			}
		});
	}
	public rechazarAsiento(asiento: AsientoContable) {
	let $this = this;
		this.confirmationService.confirm({
			header: "Rechazar Asiento",
			message: 'Está seguro que desea rechazar el asiento?',
			accept: () => {
				$this.addLoadingCount();
				
				asiento.estado = new Descriptivo("R", "Rechazado");
				asiento.mensaje = "por " + this.getCurrentUser().user.nombre;
				this.asientoService.guardar(asiento).then((res) => {
					$this.buscarAsientos();
					$this.susLoadingCount();
					this.success("Asiento rechazado");
				}).catch(this.errorHandler);
			}
		});


	}

	public buscarAsientos() {
		this.addLoadingCount();
		let $this = this;
		this.filtro.fechaHasta = moment(this.filtro.fechaHasta).add(1,'days').startOf('day').toDate();
		this.asientoService.getAllCabeceras(this.filtro).then((res) => {
			$this.asientos = res;
			$this.filtrar();
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public filtrar(){
		this.addLoadingCount();
		this._filtrados = this._asientos.filter(f => this.estadosSeleccionados.indexOf(f.estado.codigo) > -1);
		this.susLoadingCount();
	}

}
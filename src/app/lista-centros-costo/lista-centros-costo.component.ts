import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { MenuItem } from 'primeng/primeng';
import { CentroCostoService } from '../common-services/centroCostoService.service';
import { Router } from '@angular/router';
import { CabceraCentro } from '../model/CabeceraCentro';
import { SessionComponent } from '../session-component.component';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';

@Component({
  selector: 'eventos-lista-centros-costo',
  templateUrl: './lista-centros-costo.component.html',
  styleUrls: ['./lista-centros-costo.component.less']
})
export class ListaCentrosCostoComponent extends SessionComponent {

  public titulo: string = "Centros de Costo";
  public centros: CabceraCentro[] = [];
  public acciones : MenuItem[] = [];
  public buscando : boolean = false;
  constructor(cookieService :CookieServiceHelper,
    private router: Router,
    private confirmationService: ConfirmationService,
    private centroService: CentroCostoService) { super() }

  ngOnInit() {
    this.acciones = [{ label: "", icon: "fa fa-bars", items:[
      {
        label: "Nuevo Centro",
        icon: 'fa-plus',
        command : (event)=> {
          this.nuevoCentro();
        }
      }, 
      {
			label: "Traspaso de Bienes",
			command : (event)=> {
				alert("Va el popup");
			}
    
    
    }]}];
    this.addLoadingCount();
    this.centroService.getAllCentrosCabeceras().then((res) => {
      this.centros = res;
      this.susLoadingCount();

    }).catch(this.errorHandler);
  }

  public editCentroCosto(codigo: string) {
    this.router.navigate(["/admin/editar-centro/" + codigo]);
  }
  public nuevoCentro() {
    this.router.navigate(["admin/alta-centro"]);
  }

  public confirmDelete(centro: CabceraCentro) {
    let $this : ListaCentrosCostoComponent = this;
    this.confirmationService.confirm({
      header: "Eliminar Centro",
      message: 'Va a eliminar el centro de costo ' + centro.nombre + '. Desea continuar?',
      accept: () => {
        this.addLoadingCount();
        this.centroService.eliminar(centro.id).then((res) => {
          $this.success('Se borro el centro exitosamente.');
          this.susLoadingCount();
          $this.ngOnInit();                                        
        }).catch(this.errorHandler);        
      }
    });
  }

}
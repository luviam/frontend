import { LoadingService } from '../common-utils/loading-data-service.service';
import { Component, OnInit, Input } from '@angular/core';


@Component({
	selector: 'loading-panel',
	templateUrl: './loading-panel.component.html',
	styleUrls: ['./loading-panel.component.less']
})

export class LoadingPanelComponent implements OnInit {
	
	private loadingService: LoadingService;
	constructor() { 
		this.loadingService = LoadingService.injector.get(LoadingService);
	}

	get loading():boolean{
		return this.loadingService.loading;
	}
	
	ngOnInit() {

	}
}
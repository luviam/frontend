import { CookieServiceHelper } from '../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../session-component.component';
import { AppUser } from '../model/AppUser';
import { Router } from '@angular/router';
import { Usuario } from '../model/Usuario';
import { LoginService } from './service/login.service';
import { Component, OnInit } from '@angular/core';
import { Message } from "primeng/primeng";

@Component({
	selector: 'eventos-login',
	templateUrl: './login.component.html',
	styleUrls: ['login.component.less']
})
export class LoginComponent extends SessionComponent {

	username: string;
	password: string;
	msgs: Message[] = [];

	constructor(cookieService :CookieServiceHelper,
		private loginServ: LoginService, private router: Router) {
		super();
		if (this.isUserLogged()) {
			this.loggedUserRedirection(this.getCurrentUser().user);
		}
	}

	login(): void {
		this.loading = true;
		let $this: LoginComponent = this;
		
		this.loginServ.login(this.username, this.password).then(
			function (response) {
				$this.loading = false;
				if (response.success) {
					$this.loggedUserRedirection(response.user);
					$this.msgs = [];
				} else {
					$this.loading = false;
					$this.error('Error de Login');
				}
			}
		).catch(function (error) {
			$this.loading = false;
			$this.error('Error de Login');

		});
	}

	loggedUserRedirection = (user: any) => {
		this.router.navigate(["home"]);
		/*if (user.tipoUsuario.codigo == "USRADM") {
			
		  } else {
			this.router.navigate(["admin/parametros/area"]);
		  }*/
	}

}
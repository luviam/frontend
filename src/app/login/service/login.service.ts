import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';

import { ServicioAbstract } from '../../common-services/service.service';
import { environment } from 'environments/environment';
import { Usuario } from '../../model/Usuario';
import { CookieService } from 'ngx-cookie'; 
import { AppUser } from '../../model/AppUser';
import { Injectable, Inject, ReflectiveInjector } from '@angular/core';

import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';

const API_URL = environment.apiUrl;

@Injectable()
export class LoginService   {

    private currentUser: AppUser;
    private cookie:CookieService;
    
    constructor(private http:HttpClient, cookie: CookieService ) {
        this.cookie = cookie;
        
    }

    login(user: string, password: string): Promise<any> {
        let $this: LoginService = this;
        return this.http.post(API_URL + "login", { username: user, password: password })
            .toPromise()
            .then(
            function (response:any) {
                let data: any = response;
                let resp: any;
                if (data !== undefined && data.respuesta !== undefined && data.respuesta.usuario !== undefined) {
                    let usuario: Usuario = new Usuario({ username: <string>data.respuesta.usuario.email, nombre: <string>data.respuesta.usuario.nombre, apellido: <string>data.respuesta.usuario.apellido, telefono: <string>data.respuesta.usuario.telefono, email: <string>data.respuesta.usuario.email, roles: data.respuesta.usuario.authorities, activo: data.respuesta.usuario.activo });

                    $this.setCredentials(data.respuesta.token, usuario);
                    resp = { success: true, user: usuario };
                } else {
                    resp = { success: false, message: data.mensaje };
                }
                return resp;
            },this.handleError);

    }

     protected handleError(error: any): Promise<any> {
        console.error('Hubo un error', error);
        return Promise.reject(error.mensaje || error);
    }

    recuperarPassword(email:string): Promise<any> {
        
        return this.http.post(API_URL + "recuperar-password", { email: email}).toPromise().then((response:any)=>{ return response; }, this.handleError);

    }

    validarToken(email:string, token:string): Promise<any> {
        
        return this.http.post(API_URL + "validar-token/"+token, {email: email}).toPromise().then((response:any)=>{ return response; }, this.handleError);

    }

    cambiarPassword(token:string, email:string, nuevaPassword:string): Promise<any> {
        
        return this.http.post(API_URL + "cambiar-password", { token:token, email: email, nuevaPassword: nuevaPassword}).toPromise().then((response:any)=>{ return response; }, this.handleError);

    }

    isAllowed(page): boolean {
        /*if (this.currentUser) {
            if (page === "") { return true; }
            else if (page === 'admin') {
                return this.currentUser.user.tipoUsuario.codigo === 'USRADM';
            } else {
                return true;
            }
        } else {
            return false;
        }*/
        return false;
    }
    setCredentials(token, user): void {

        this.currentUser = { 
            user: user,
            token: token
        };

        // set default auth header for http requests
        // store user details in globals cookie that keeps user logged in for 1 week (or until they logout)
        let cookieExp = new Date();
        cookieExp.setDate(cookieExp.getDate() + 7);
        this.cookie.putObject('luviamUser', this.currentUser, { expires: cookieExp });
    }

}
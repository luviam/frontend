
import { Parametrico } from './Parametrico';
import { GrupoProducto } from './GrupoProducto';
export class Agrupador extends Parametrico {
    public peso: number = 99;
    public grupo: GrupoProducto;
    public itemsDetalle : string[] = [];
   
    public static get ID(): string {
        return "agrupadores";
    }

    public static fromData(data: any) {
        if (!data) return null;

        let o: Agrupador = new Agrupador(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,
           

        );
        o.peso = data.peso;
        o.grupo = GrupoProducto.fromData(data.grupo);
        o.itemsDetalle = data.itemsDetalle?  data.itemsDetalle : [];
        return o;


    }


}
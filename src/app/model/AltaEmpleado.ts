import { Descriptivo } from './Descriptivo';
import * as moment from 'moment';
export class AltaEmpleado extends Descriptivo{
	constructor(

		public id? : number,
		public fechaAlta? : Date,
		public fechaBaja? : Date,
		public observacion? : string
	){
        super(id+"","");
        this.descripcion = "Alta " + this.fechaDesdeStr + " Baja " + this.fechaHastaStr 
        this.descripcion += this.observacion? "; Observaciones: " + this.observacion : "";
        
    }

	public static fromData(data : any) : AltaEmpleado{
		if(!data) return null; 
		let o : AltaEmpleado  = new AltaEmpleado(
		 data.id,
		 data.fechaAlta? new Date(data.fechaAlta) : null,
		 data.fechaBaja? new Date(data.fechaBaja) : null,
		 data.observacion);
		return o; 

    }

    get fechaHastaStr(){
        return this.fechaBaja? moment(this.fechaAlta).format("DD/MM/YYYY") : "SIN INFORMAR";
    }
    get fechaDesdeStr(){
        return this.fechaAlta? moment(this.fechaAlta).format("DD/MM/YYYY") : "SIN INFORMAR";
    }
  

}
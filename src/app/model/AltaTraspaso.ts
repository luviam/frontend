export class AltaTraspaso {
    constructor(public idCajaDestino? : number, 
                public idCajaOrigen? :number, 
                public monto? :number, 
                public descripcion? : string,
                public fecha? : Date){

    }
}
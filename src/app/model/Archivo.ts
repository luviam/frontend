
import { Parametrico } from './Parametrico';
export const SIN_IMAGEN = "assets/images/sin_imagen.png";

export class Archivo {
    public url: string = SIN_IMAGEN;
    public id?: number;



    public static fromData(data: any) {
        if (!data) return null;

        let o: Archivo = new Archivo();
        o.id = data.id;
        o.url = data.url ? data.url : o.url;
        return o;


    }


}
import { OperacionContable } from './OperacionContable';
import { Descriptivo } from './Descriptivo';
import * as moment from 'moment';
export class AsientoContable extends Descriptivo{
    public fechaFormateada : string;
    constructor(public id? : number,
                public numeroAsiento? : number,
                public descripcion? : string,
                public responsable? : string,
                public fecha? : Date,
                public estado? : Descriptivo,
                public operaciones : OperacionContable[] = [],
                public mensaje?: string){
                    super(numeroAsiento+"",descripcion);
                    if(fecha){
                        this.fechaFormateada = moment(this.fecha).format('DD/MM/YYYY');
                    }
                   
                    
                }

    public quitarItem(item : OperacionContable){
        this.operaciones.splice(this.operaciones.indexOf(item),1);
    }

    get debe() : number {

        let total : number = this.operaciones.reduce((a, b) => a + b.debe, 0);
        
        return total? Number(total.toFixed(2)) : 0.00;
    }
    get haber(): number{
        let total : number = this.operaciones.reduce((a, b) => a + b.haber, 0);
        
        return total? Number(total.toFixed(2)) : 0.00;
    }
    
    get diferencia() : number{
        let diferencia =  this.operaciones.reduce((a, b) => a + b.debe - b.haber, 0);
        return Number(diferencia.toFixed(2));
    }
    isCentrosBalanceados() : boolean{
        let centros : number[] = [];
        if(this.operaciones.some(o => !o.centroCosto)) return false;
        this.operaciones.forEach(o => {
            
           if(!centros[o.centroCosto.codigo]){
               centros[o.centroCosto.codigo]= 0;
           }
           centros[o.centroCosto.codigo] += (o.debe - o.haber);
        });
        return !centros.some(a => a !== 0 );
    }
}


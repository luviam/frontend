import { Descriptivo } from './Descriptivo';
export class BasicEntity extends Descriptivo{
    constructor(public id? : number, public codigo? :string , public descripcion? : string ){
        super(codigo,descripcion);

    }

}
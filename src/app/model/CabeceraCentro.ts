import { Descriptivo } from './Descriptivo';
export class CabceraCentro extends Descriptivo{

    constructor(public codigo? : string,public nombre?:string, public cuit?: string, public iibb? :string, public domicilioFiscal?:string,  public id?:number){
        super(codigo,nombre);
    }
}

import { Parametrico } from './Parametrico';
import { GrupoProducto } from './GrupoProducto';
export class Categoria extends Parametrico {
    public esComisionable: boolean = false;
    public peso: number = 99;
    public grupo: GrupoProducto;
    public static IMPUESTO: string = "IMP";
    public static INTERNO: string = "INTER";
    public static OPERATIVO_IP: string = "OPERIP";
    public static BEBIDA : string = "BEBIDA";
    public static ADICIONALES : string = "ADICIONALES";
    public static MENU : string = "MENU";
    public static PACK(): Categoria {
        let o: Categoria = new Categoria(null, "PACK", "PACK", true, true);
        o.peso = 0;
        o.esComisionable = true;
        return o;
    }
    public static get ID(): string {
        return "categorias";
    }

    public static fromData(data: any) {
        if (!data) return null;

        let o: Categoria = new Categoria(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,

        );
        o.esComisionable = data.esComisionable;
        o.peso = data.peso;
        o.grupo = GrupoProducto.fromData(data.grupo);
        return o;


    }


}
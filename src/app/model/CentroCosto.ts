import { BasicEntity } from './BasicEntity';
import { Descriptivo } from './Descriptivo';
export class CentroCosto extends BasicEntity{
    constructor(public id? : number,public codigo? : string, public descripcion? : string, public cuentas ? : Descriptivo[]){
        super(id,codigo,descripcion);
    }


    public contieneTexto(texto : string){
        return this.codigo.toUpperCase().includes(texto.toUpperCase()) ||
                this.descripcion.toUpperCase().includes(texto.toUpperCase())
    }
}
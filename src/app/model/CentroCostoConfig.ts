import { CuentaConfig } from './ConfigCuenta';
import { ConfigComprobanteFiscal } from './ConfigComprobanteFiscal';
import { Descriptivo } from './Descriptivo';
export class CentroCostoConfig  extends Descriptivo{

    

    constructor(public id?:number, public codigoCentro?: string, public descripcionCentro?: string, 
                public razonSocial?:string, 
                public tipoIIBB? : string,
                public iibb?:string, 
                public cuit?:string, public domicilioFiscal?:string,
                public codigoPostal? : string, 
                public provincia?:Descriptivo,
                public comprobantes? :ConfigComprobanteFiscal[], 
                public cuentas? : CuentaConfig[], public activo? : boolean ) {
        super(codigoCentro,descripcionCentro);
        if(!tipoIIBB){
            this.tipoIIBB = "C";
        }
     
    }
    get codigo(){
        return this.codigoCentro;
    }
    set codigo(str : string){
        this.codigoCentro = str? str.toUpperCase() : "";
    }
    get descripcion(){
        return this.descripcionCentro;
    }

    set descripcion(str : string){
        this.descripcionCentro = str? str.toUpperCase() : "";
    }

    public agregarConfComprobante(conf : ConfigComprobanteFiscal){
        if(!this.comprobantes){
            this.comprobantes = [];
        }
        this.comprobantes.push(conf);
    }

    public agregarCuenta(cuenta : CuentaConfig){
        if(!this.cuentas){
            this.cuentas = [];
        }
        this.cuentas.push(cuenta);
        
    }
}
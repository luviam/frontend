
import { CuentaAplicacion } from './CuentaAplicacion';
import { Descriptivo } from './Descriptivo';

export class Cheque{
    public static CLIENTE() : Descriptivo{
        return new Descriptivo("C", "Terceros X Cliente");
    }
    constructor(
        public id?: number,
        public banco?: string,
        public numero?: string,
        public tipoCheque?: Descriptivo,
        public cuentaBancaria?: CuentaAplicacion,
        public numeroCuenta?: string,
        public titularCuenta?: string,
        public cuit?: string,
        public fechaEmision?: Date,
        public fechaPago?: Date,
        public importe: number = 0,
        public estado?: Descriptivo,
        public destino?: string,
        public esDiferido: Boolean = false,
        public pago?: Descriptivo,
        public cobro?: Descriptivo,
        public fechaIngreso? : Date,
        public numeroRecibo? : string,
        public origen?: string
    ){

    }
    public static fromData(data : any) : Cheque{
		if(!data) return null; 
		let o : Cheque  = new Cheque(
		 data.id,
		 data.banco,
		 data.numero,
		 Descriptivo.fromData(data.tipoCheque),
		 CuentaAplicacion.fromData(data.cuentaBancaria),
		 data.numeroCuenta,
		 data.titularCuenta,
		 data.cuit,
		 data.fechaEmision? new Date(data.fechaEmision) : null,
		 data.fechaPago? new Date(data.fechaPago) : null,
         data.importe,
         Descriptivo.fromData(data.estado),
         data.destino,
		 data.esDiferido,
		 Descriptivo.fromData(data.pago),
		 Descriptivo.fromData(data.cobro),
		 data.fechaIngreso? new Date(data.fechaIngreso) : null,
         data.numeroRecibo,
         data.origen);
		return o; 

	}
    public esBorrable(): boolean{
        return !this.pago && !this.destino && this.estado.codigo === 'D';
    }

    get importeStr(): string{
        var formatter = new Intl.NumberFormat('es-AR', {
            style: 'currency',
            currency: 'ARS',
            minimumFractionDigits: 2,
          });
          
        return formatter.format(this.importe);
    }
    get descripcion(){
        return this.banco + " - " + this.numero;
    }

    get destinoStr(){
        
            return this.destino? this.destino : this.pago? this.pago.descripcion : "";
        
    }

    public descripcionCorta(){
        return this.importeStr + " - " + this.destinoStr;
    }

}
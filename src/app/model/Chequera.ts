import { CuentaAplicacion } from './CuentaAplicacion';
import { Descriptivo } from './Descriptivo';
import { SecuenciaCheques } from './SecuenciaCheques';
export class Chequera {

    constructor(public id?:number, 
                public cuentaBancaria?:CuentaAplicacion,
                public secuencias?:SecuenciaCheques[], 
                public esDiferido?:boolean){

    }

    get descripcion():string{
        return this.cuentaBancaria.centroCosto.descripcion + " - " + this.cuentaBancaria.nombre;
    }

    get secuenciasStr():string{
        return this.secuencias.map(sec=>sec.minNumeracion + "-" + sec.maxNumeracion).join(", ");
    }

    get secuenciasValidas():boolean {
        return this.secuencias && this.secuencias.length > 0 && this.secuencias.every(sec=>sec.valid);
    }

}
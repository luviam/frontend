import { StringUtils } from '../common-utils/string-utils.class';
import { Contacto } from './Contacto';
import { ConvenioSalon } from './ConvenioSalon';

import { Descriptivo } from './Descriptivo';
export class Cliente  extends Descriptivo{
    public static SOCIAL : string   = "SO";
    constructor(
        public id? :number,
        public nombreCliente? : string,
        public nombreContacto?:string,
        public domicilio?:string,
        public localidad?:string,
        public telefono?:string,
        public celular?:string,
        public email?:string,
        public agasajados?: string,
        public razonSocial?: string,
        public cuit?:string,
        public iva?:Descriptivo,
        public domicilioFacturacion? : string,
        public localidadFacturacion? : string,
        public tipoCliente:Descriptivo = new Descriptivo(),
        public convenio?:ConvenioSalon,
        public nombreFirmante?: string,
        public dniFirmante?:string,
        public activo?: boolean,
        public facebook?: string,
        public instagram?: string,
        public contactos: Contacto[] = [],

    ){
        super(id+"",nombreCliente);
    }
    public static fromData(data : any) : Cliente{
        if(!data) return null;
        let o : Cliente  = new Cliente(data.id, 
            data.nombreCliente, 
            data.nombreContacto, 
            data.domicilio,
            data.localidad, 
            data.telefono, 
            data.celular, 
            data.email, 
            data.agasajados,
            data.razonSocial, 
            data.cuit, 
            Descriptivo.fromData(data.iva),
            data.domicilioFacturacion, 
            data.localidadFacturacion,
            Descriptivo.fromData(data.tipoCliente),
            ConvenioSalon.fromData(data.convenio),
            data.nombreFirmante, 
            data.dniFirmante, 
            data.activo,
            data.facebook,
            data.instagram,
            data.contactos? data.contactos.map(c => Contacto.fromData(c)) : [],);
		return o; 

	}
 
    
    get nombreCompleto(){
        return this.nombreCliente;
    }

    get contactosStr(){
        let str : string ="";
        this.contactos.forEach(c =>str += " " +  StringUtils.getSinTildes(c.nombre) +
                                        " " +   StringUtils.getSinTildes(c.email) + 
                                        " " +   StringUtils.getSinTildes(c.telefono) + 
                                        " " +   StringUtils.getSinTildes(c.numeroDocumento));
        return str;
    }
    public getContacto(tipo : string) : Contacto{
		let contacto = this.contactos.filter(c => c.relacionContacto === tipo)[0];
		return contacto ? contacto : null;
	}

	public setContacto(contacto: Contacto){
		let cont = this.getContacto(contacto.relacionContacto);
		if(cont){
			cont.nombre = contacto.nombre;
			cont.numeroDocumento = contacto.numeroDocumento;
			cont.celular = contacto.celular;
			cont.telefono = contacto.telefono;
			cont.instagram = contacto.instagram;
			cont.facebook = contacto.facebook;
			cont.email = contacto.email;
		}else{
			cont = Contacto.fromData(contacto);
			this.contactos.push(cont);
			
        }
        this.contactos = [...this.contactos];
        
		
    }
    public borrarContacto(contacto: Contacto){
        this.contactos = this.contactos.filter(c => c!== contacto);
    }

    get esSocial(): boolean{
        return this.tipoCliente && this.tipoCliente.codigo === Cliente.SOCIAL;
    }

}
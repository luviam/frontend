import { Descriptivo } from './Descriptivo';

export class ConfigComprobanteFiscal  extends Descriptivo{
     constructor(public codigo?: string, public descripcion?: string, 
                 public activo? :boolean, public numeracion? : string) {
         super(codigo,descripcion);
     
    }
}
import { Cuenta } from './Cuenta';

import { TreeNode } from 'primeng/primeng';
import { Descriptivo } from './Descriptivo';

export class CuentaConfig extends Cuenta implements TreeNode{
    public expanded : boolean;
    constructor(public codigo:string, public descripcion:string, public idParent: number , public id? : number, 
                 public activo?:boolean, public eliminable:boolean = true){
        super(codigo,descripcion, Cuenta.APLICACION);
    }
    
    set esActiva(state :boolean){
        this.activo = state;
    }

    get esActiva(){
        return this.activo;
    }
    get esHija(){
        return this.parent != null;
    }
    get nombre(){
        return this.codigo + " - " + this.descripcion;
    }
    get data(){
        return this;
    }
    get children(){
        return this.cuentasHijas;
    }
    public agregarCuenta(cuenta : CuentaConfig){
        if(!this.cuentasHijas){
            this.cuentasHijas = [];
        }
        this.cuentasHijas.push(cuenta);
        cuenta.parent = this;
    }
    public getTipoCuentaHija(){

        if(this.tipoCuenta === "R"){
            return "G";
        } else if(this.tipoCuenta === "G"){
            return "SG";
        } else {
            return "A";
        }
    }
}
import { Precio } from 'app/precios/model/Precio';
import * as moment from 'moment';
import { Descriptivo } from './Descriptivo';
import { ListaPrecio } from './ListaPrecio';
import { Producto } from './Producto';
export const mesesAlta: string[] = ["1", "8", "9", "10", "11", "12"];
export class ConfiguracionListadoPrecio extends Descriptivo {

    public listaPrecio: ListaPrecio
    public fechaDesde: Date;
    public fechaHasta: Date;
    public precios: Precio[] = [];
    public activo: boolean = true;
    public estado: Descriptivo;



    public calcular: (fecha: Date, precioBase: number) => number;

    public get rango(): string {
        return moment(this.fechaDesde).format("DD/MM/YYYY") + (this.fechaHasta ? " - " + moment(this.fechaHasta).format("DD/MM/YYYY") : " en adelante");
    }
    public aplica(fecha: Date): boolean {
        return moment(this.fechaDesde).startOf("month").startOf("day") <= moment(fecha).startOf("day") && (!this.fechaHasta || moment(this.fechaHasta).endOf("month").startOf("day") >= moment(fecha).startOf("day"));
    }
    public getPrecioByProducto(prod: Producto): Precio {
        return this.precios.filter(p => p.producto.codigo == prod.codigo)[0];
    }

    public getValorPrecioByProducto(prod: Producto): number {
        let p: Precio = this.getPrecioByProducto(prod);
        return p ? p.valor : 0;
    }

    public calcularPrecio(fecha: Date, prod: Producto): number {
        let precio: Precio = this.precios.filter(p => p.incluyeFecha(fecha) && p.producto.codigo == prod.codigo)[0];
        return precio ? precio.valor : 0;
    }


}
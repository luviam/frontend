import { ConfiguracionListadoPrecio } from 'app/model/ConfiguracionListadoPrecio';
import * as moment from 'moment';
import { PrecioAlquiler } from './../precios/model/PrecioAlquiler';
import { Descriptivo } from './Descriptivo';
import { ListaPrecio } from './ListaPrecio';
import { Producto } from './Producto';
export const mesesAlta: string[] = ["1", "8", "9", "10", "11", "12"];
export class ConfiguracionListadoPrecioAlquiler extends ConfiguracionListadoPrecio {


    public precios: PrecioAlquiler[] = [];
    public salones: Descriptivo[] = [];

    public static fromData(data: any) {
        if (!data) return null;

        let o: ConfiguracionListadoPrecioAlquiler = new ConfiguracionListadoPrecioAlquiler(
            data.codigo,
            data.descripcion,
            data.id
        );
        o.codigo = o.id + "";
        o.listaPrecio = ListaPrecio.fromData(data.listaPrecio);
        o.fechaDesde = data.fechaDesde ? new Date(data.fechaDesde) : null;
        o.fechaHasta = data.fechaHasta ? new Date(data.fechaHasta) : null;
        o.precios = data.precios.map(PrecioAlquiler.fromData);
        o.descripcion = o.listaPrecio ? o.listaPrecio.descripcion : "Sin Listado";
        o.activo = data.activo;
        o.salones = data.salones ? data.salones.map(s => Descriptivo.fromData(s)) : [];

        o.estado = Descriptivo.fromData(data.estado);

        return o;


    }
    public agregarPrecios(precios: PrecioAlquiler[]) {
        this.precios.push(...precios);
        this.precios = [...this.precios];
    }
    public quitarPrecios(precios: PrecioAlquiler[]) {
        this.precios = this.precios.filter(p => precios.indexOf(p) < 0);
        this.precios = [...this.precios];
    }
    public quitarPorProducto(p: Producto) {
        this.quitarPrecios(this.precios.filter(pp => pp.producto.codigo == p.codigo));
    }

    public get rango(): string {
        return moment(this.fechaDesde).format("DD/MM/YYYY") + (this.fechaHasta ? " - " + moment(this.fechaHasta).format("DD/MM/YYYY") : " en adelante");
    }
    public aplica(fecha: Date): boolean {
        return moment(this.fechaDesde).startOf("month").startOf("day") <= moment(fecha).startOf("day") && (!this.fechaHasta || moment(this.fechaHasta).endOf("month").startOf("day") >= moment(fecha).startOf("day"));
    }
    public getPrecioByProducto(prod: Producto): PrecioAlquiler {
        return this.precios.filter(p => p.producto.codigo == prod.codigo)[0];
    }

    public getValorPrecioByProducto(prod: Producto): number {
        let p: PrecioAlquiler = this.getPrecioByProducto(prod);
        return p ? p.valor : 0;
    }



    public get salon(): Descriptivo {
        return this.salones[0];
    }
    public set salon(val: Descriptivo) {
        if (val) {
            this.salones = [val];
        } else {
            this.salones = [];
        }

    }
    public getProductos(tipo: string): Producto[] {
        let precios = this.precios.filter(p => p.tipoPrecio === tipo);
        let resultado: PrecioAlquiler[] = [];
        precios.forEach(p => {
            if (!resultado.some(rr => rr.producto.codigo == p.producto.codigo)) {
                resultado.push(p);
            }
        })
        return resultado.map(p => p.producto);

    }
    public get preciosJornada() {
        return this.precios.filter(p => p.tipoPrecio == PrecioAlquiler.JORNADA);
    }



}
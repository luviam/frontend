import * as moment from 'moment';
import { Calculardora } from './../precios/model/Calculadora';
import { PrecioMenu } from './../precios/model/PrecioMenu';
import { ConfiguracionListadoPrecio } from './ConfiguracionListadoPrecio';
import { Descriptivo } from './Descriptivo';
import { ListaPrecio } from './ListaPrecio';
import { Producto } from './Producto';

export const mesesAlta: string[] = [];
export class ConfiguracionListadoPrecioMenu extends ConfiguracionListadoPrecio {


    public tipoCalculo: Descriptivo;
    public parteVariable: number = .7;
    public precios: PrecioMenu[] = [];
    public incremento: number = .035;
    public adicional: number = 0;
    public mesesBaja: Descriptivo[] = [];
    public listadoPadre: ListaPrecio;
    private calculadora: Calculardora;


    public static fromData(data: any) {
        if (!data) return null;

        let o: ConfiguracionListadoPrecioMenu = new ConfiguracionListadoPrecioMenu(
            data.codigo,
            data.descripcion,
            data.id
        );
        o.codigo = o.id + "";
        o.listadoPadre = data.listadoPadre ? ListaPrecio.fromData(data.listadoPadre) : null;
        o.listaPrecio = ListaPrecio.fromData(data.listaPrecio);
        o.fechaDesde = data.fechaDesde ? new Date(data.fechaDesde) : null;
        o.fechaHasta = data.fechaHasta ? new Date(data.fechaHasta) : null;
        o.precios = data.precios.map(PrecioMenu.fromData);
        o.tipoCalculo = Descriptivo.fromData(data.tipoCalculo);
        o.descripcion = o.listaPrecio ? o.listaPrecio.descripcion + (o.tipoCalculo ? " " + o.tipoCalculo.descripcion : "") : "Sin Listado";
        o.mesesBaja = data.mesesBaja ? data.mesesBaja.map(m => Descriptivo.fromData(m)) : [new Descriptivo("06", "Junio")];

        o.parteVariable = data.parteVariable || .7;
        o.incremento = data.incremento || 0.035;
        o.adicional = data.adicional || 0;
        if (o.tipoCalculo) {
            switch (o.tipoCalculo.codigo) {
                case "I":
                    o.calcular = o.incrementoFijo;
                    break;
                case "A":
                    o.calcular = o.incrementoAcumulado;
                    break;
                default:
                    o.calcular = o.calculoCongelado;
                    break;
            }


        } else {
            o.calcular = o.calculoCongelado;
        }
        o.activo = data.activo;

        if (data.listadosPadre && data.listadosPadre.length > 0) {
            o.calculadora = new Calculardora(data.listadosPadre.map(l => ConfiguracionListadoPrecioMenu.fromData(l)));

        }
        o.estado = Descriptivo.fromData(data.estado);
        return o;


    }
    public calcular: (fecha: Date, precioBase: number) => number;

    public get rango(): string {
        return moment(this.fechaDesde).format("DD/MM/YYYY") + (this.fechaHasta ? " - " + moment(this.fechaHasta).format("DD/MM/YYYY") : " en adelante");
    }
    public aplica(fecha: Date): boolean {
        return moment(this.fechaDesde).startOf("month").startOf("day") <= moment(fecha).startOf("day") && (!this.fechaHasta || moment(this.fechaHasta).endOf("month").startOf("day") >= moment(fecha).startOf("day"));
    }
    public getPrecioByProducto(prod: Producto): PrecioMenu {
        return this.precios.filter(p => p.producto.codigo == prod.codigo)[0];
    }

    public getValorPrecioByProducto(prod: Producto): number {
        let p: PrecioMenu = this.getPrecioByProducto(prod);
        return p ? p.valor : 0;
    }
    public calcularPrecio(fecha: Date, prod: Producto): number {
        let precioBase: number = 0;
        if (this.calculadora) {
            if (!this.aplica(fecha)) {
                return 0;
            }
            precioBase = this.calculadora.getPrecio(fecha, prod);
            let productoBase: PrecioMenu = this.getPrecioByProducto(prod);
            if (productoBase) {
                if (productoBase.tipoOperacion === PrecioMenu.PORCENTAJE) {
                    return Math.ceil(precioBase * (1 + productoBase.valor / 100));
                } else if (productoBase.tipoOperacion === PrecioMenu.ADICIONAL) {
                    return precioBase + productoBase.valor;
                } else {
                    return this.calcular(fecha, this.getValorPrecioByProducto(prod));
                }
            } else {
                return 0;
            }

        } else {
            precioBase = this.getValorPrecioByProducto(prod);
            return (precioBase && this.aplica(fecha)) ? this.calcular(fecha, this.getValorPrecioByProducto(prod)) : 0;
        }
    }

    public get tieneParametros() {
        return this.tipoCalculo && this.tipoCalculo.codigo != 'A';
    }
    public esBaja(mes: string) {
        moment.locale("es");
        return !this.esAlta(mes) && this.mesesBaja.some(m => moment().locale("es").month(mes).format("M") == m.codigo);
    }

    public esAlta(mes: string) {
        moment.locale("es");
        return mesesAlta.some(m => moment().locale("es").month(mes).format("M") == m);
    }


    public calculoCongelado(fecha: Date, precioBase: number): number {
        moment.locale("es");
        if (precioBase) {
            if (this.esBaja(moment(fecha).format("MMMM"))) {
                return this.calculoCongelado(this.getAnteriorBaja(fecha), precioBase);

            } else {
                let diferencia = moment(fecha).startOf("month").diff(moment(this.fechaDesde).startOf("month"), "months");
                return Math.ceil(precioBase * (1 + (this.parteVariable) * diferencia * (this.incremento)));
            }


        }
        return 0;
    }

    public getAnteriorBaja(fecha: Date): Date {
        moment.locale("es");
        let dateStart = moment(this.fechaDesde).startOf("month");
        var dateEnd = moment(fecha).endOf("month");
        while ((dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) && this.esBaja(dateEnd.format("MMMM"))) {
            dateEnd.subtract(1, "month");
        }
        return dateEnd.toDate();
    }

    public incrementoFijo(fecha: Date, precioBase: number): number {
        moment.locale("es");
        if (precioBase) {
            /*   if (this.esBaja(moment(fecha).format("MMMM"))) {
                   return this.incrementoFijo(this.getAnteriorBaja(fecha), precioBase);
   
               } else {*/
            let diferencia = moment(fecha).startOf("month").diff(moment(this.fechaDesde).startOf("month"), "months");
            return Math.ceil(precioBase * (1 + (this.parteVariable) * diferencia * (this.incremento)));
            //}


        }
        return 0;
    }

    public incrementoAcumulado(fecha: Date, precioBase: number): number {
        moment.locale("es");
        if (precioBase) {
            /*  if (this.esBaja(moment(fecha).format("MMMM"))) {
                  return this.incrementoAcumulado(this.getAnteriorBaja(fecha), precioBase);
  
              } else {*/
            let diferencia = moment(fecha).startOf("month").diff(moment(this.fechaDesde).startOf("month"), "months");
            return Math.ceil(precioBase * Math.pow(1 + this.incremento * (this.parteVariable), diferencia));
            //            }


        }
        return 0;
    }

    public get esHeredado(): boolean {
        return this.listadoPadre != undefined;
    }
}
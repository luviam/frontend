import { Descriptivo } from './Descriptivo';
export class Contacto extends Descriptivo{
constructor(
	public id?: number,
	public relacionContacto?: string,
	public nombre?: string,
	public email?: string,
	public telefono?: string,
	public celular?: string,
	public facebook?: string,
	public instagram?: string,
	public numeroDocumento?: string
){
	super(id? id.toString(): "", nombre)
}
	get label(){
		return this.descripcion;
	}

	public static fromData(contacto: any) : Contacto{
		let cont : Contacto = new Contacto(
			contacto.id,
			contacto.relacionContacto,
			contacto.nombre,
			contacto.email,
			contacto.telefono,
			contacto.celular,
			contacto.facebook,
			contacto.instagram,
			contacto.numeroDocumento);
		return cont;
		}
			
}

import { Descriptivo } from './Descriptivo';
export class ConvenioSalon extends Descriptivo{
    constructor(
        public porcComision? : number,
        public valorComision? : number,
        public porcDescuentos? : number,
        public valorAdicional?: number
    ){
        super()
    }

    public static fromData(data : any) : ConvenioSalon{
        let o : ConvenioSalon;
        if(!data){
            return null;
        }
        o = new ConvenioSalon(data.porcComision, data.valorComision, data.porcDescuentos, data.valorAdicional);
        return o;
    }

}
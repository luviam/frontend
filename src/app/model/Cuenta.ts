
import { TreeNode } from 'primeng/primeng';
import { Descriptivo } from './Descriptivo';
export class Cuenta extends Descriptivo implements TreeNode{
    public static RUBRO = "R";
    public static GRUPO = "G";
    public static SUB_GRUPO ="SG";
    public static APLICACION ="A";
    
    icon?: any;
    expandedIcon?: any;
    collapsedIcon?: any;
    expanded?: boolean;
    type?: string;
    partialSelected?: boolean;
    styleClass?: string;
    draggable?: boolean;
    droppable?: boolean;
    selectable?: boolean;
    
    constructor( public codigo, public descripcion, public tipoCuenta : string, public activo? : boolean, 
                public id? : number, public cuentasHijas : Cuenta[] = [], public parent? : Cuenta, public eliminable? : boolean){
        super(codigo,descripcion);
    }

    get label() : string{
        return this.descripcion;
    }

    get children() : Cuenta[]{
        return this.cuentasHijas || [];
    }

    set children(children : Cuenta[]){
        this.cuentasHijas = children;
    }

    get data() : any{
        return  this;
    }

    public getTipoCuentaHija(){
        if(this.tipoCuenta === "R"){
            return "G";
        } else if(this.tipoCuenta === "G"){
            return "SG";
        } else {
            return "A";
        }
    }

    public agregarCuenta(cuenta : Cuenta){
        if(!this.cuentasHijas){
            this.cuentasHijas = [];
        }
        this.cuentasHijas.push(cuenta);
        cuenta.parent = this;
    }

    get esEliminable():boolean {
        return this.eliminable && (!this.cuentasHijas || this.cuentasHijas.length == 0);
    }

    get esEliminableCentroCosto():boolean {
        let $this = this;
        return  this.esEliminable;
    }

    get leaf():boolean {
        return !this.cuentasHijas || this.cuentasHijas.length == 0;
    }
}
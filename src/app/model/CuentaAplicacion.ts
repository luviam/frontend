import { Descriptivo } from './Descriptivo';

export class CuentaAplicacion extends Descriptivo {

    constructor(public id?:number, 
                public codigo?:string, 
                public nombre?:string,
                public centroCosto?:Descriptivo){
        super(id+"", nombre);
    }

    public static fromData(data :any) : CuentaAplicacion{
        if(!data) return null;
        let c : CuentaAplicacion = new CuentaAplicacion(
            data.id,
            data.codigo,
            data.nombre,
            Descriptivo.fromData(data.centroCosto)
        );
        return c;
    }
    get nombreConCentro(){
        return this.centroCosto.codigo + " - " + this.nombre;
    }

}
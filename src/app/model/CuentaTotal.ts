
import { TreeNode } from 'primeng/primeng';
import * as moment from 'moment';
export class CuentaTotal implements TreeNode{

    parent?: CuentaTotal;
    icon?: any;
    expandedIcon?: any;
    collapsedIcon?: any;
    expanded?: boolean;
    type?: string;
    partialSelected?: boolean;
    styleClass?: string;
    draggable?: boolean;
    droppable?: boolean;
    selectable?: boolean;
    constructor(public id: number,
                public codigo : string,
                public cuenta : string,
                public esCuentaAplicacion : boolean,
                public debe : number = 0,
                public haber : number = 0,
                public total : number = 0,
                public fecha? : Date,
                public esOperacion :boolean = false,
                public cuentasHijas? : CuentaTotal[]){
                    if(cuentasHijas){
                        cuentasHijas.forEach(c => c.parent = this);
                    }
                   
                   
                    

    }
        

        public agregarCuentaHija(c : CuentaTotal){
            if(!this.cuentasHijas) this.cuentasHijas = [];
            this.debe += c.debe;
            this.haber += c.haber;
            this.total = this.debe - this.haber;
            
            c.parent = this;
            if(this.parent){
                this.parent.actualizarTotales(c);
            }
            this.cuentasHijas.push(c);
        }
        private actualizarTotales(hija : CuentaTotal){
            this.debe += hija.debe;
            this.haber += hija.haber;
            this.total = this.debe - this.haber;
            if(this.parent){
                this.parent.actualizarTotales(hija);
            }
        }
        get label() : string{
            return this.cuenta;
        }

        get children() : TreeNode[]{
            return this.cuentasHijas;
        }


        get leaf() :boolean{
            return this.esOperacion;
        }
        get data() : any{
            return this;
        }
               
        get fechaFormateada(){
        if(this.fecha)
            return moment(this.fecha).format("DD/MM/YYYY");
        return "";
        }
               
}
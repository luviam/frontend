import { AltaEmpleado } from './AltaEmpleado';
import { Descriptivo } from './Descriptivo';


export class Empleado{
	public static VENDEDOR_CODIGO:string = "VEND";
	constructor(

		public id? : number,
		public nombre? : string,
		public fechaNacimiento? : Date,
		public cuil? : string,
		public domicilio? : string,
		public activo? : boolean,
		public cvOk? : boolean,
		public preocupacionalOk? : boolean,
		public fechaRealizacionPreocupacional? : Date,
		public dniOK? : boolean,
		public cuilOK? : boolean,
		public constanciaDomicilioOK? : boolean,
		public seguroOK? : boolean,
		public artOK? : boolean,
		public tipoJornada? : Descriptivo,
		public sector? : Descriptivo,
		public funcion? : string,
		public responsabilidades? : string,
		public remuneracion? : number,
		public fechaInicio? : Date,
		public fechaFin? : Date,
		public tipoEmpleado? : Descriptivo,
		public altas? : AltaEmpleado[],
		public email? : string,
		public telefono? : string,
		public centro? : Descriptivo,
	){}

	public static fromData(data : any) : Empleado{
		if(!data) return null; 
		let o : Empleado  = new Empleado(
		 data.id,
		 data.nombre,
		 data.fechaNacimiento? new Date(data.fechaNacimiento) : null,
		 data.cuil,
		 data.domicilio,
		 data.activo,
		 data.cvOk,
		 data.preocupacionalOk,
		 data.fechaRealizacionPreocupacional? new Date(data.fechaRealizacionPreocupacional) : null,
		 data.dniOK,
		 data.cuilOK,
		 data.constanciaDomicilioOK,
		 data.seguroOK,
		 data.artOK,
		 Descriptivo.fromData(data.tipoJornada),
		 Descriptivo.fromData(data.sector),
		 data.funcion,
		 data.responsabilidades,
		 data.remuneracion,
		 data.fechaInicio? new Date(data.fechaInicio) : null,
		 data.fechaFin? new Date(data.fechaFin) : null,
		 Descriptivo.fromData(data.tipoEmpleado),
		 data.altas?  data.altas.map(a => AltaEmpleado.fromData(a)) : [],
		 data.email,
		 data.telefono,
		data.centro? Descriptivo.fromData(data.centro) : Descriptivo.SIN_DEFINIR()	);
		return o; 

	}

	public addAlta(alta : AltaEmpleado){
		if(!this.altas) this.altas = [];
		this.altas.push(alta);
		this.altas = [...this.altas];
	}

}
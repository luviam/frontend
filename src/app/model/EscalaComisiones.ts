import { Descriptivo } from './Descriptivo';
import { Parametrico } from './Parametrico';
export class EscalaComisiones{

    constructor(
        public id?: number,
        public desde : number = 0,
        public hasta : number = 0,
        public premio :number = 0,
        public centro? : Descriptivo
    ){

    }

    public static fromData(data : any):EscalaComisiones{
        if(!data) return null;
        let o : EscalaComisiones = new EscalaComisiones(
            data.id,
            data.desde,
            data.hasta,
            data.premio,
            Descriptivo.fromData(data.centro)
        )
        return o;
    }
}
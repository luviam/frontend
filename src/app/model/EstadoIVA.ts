export class EstadoIVA {

    constructor(
        public idContrato?: number,
        public ivaMaximo: number = 0,
        public ivaCobrado: number = 0
    ) {
        if(!ivaMaximo) this.ivaMaximo = 0;
        if(!ivaCobrado) this.ivaCobrado = 0;
    }

    public static fromData(data: any) {
        if (!data) return null;

        let o = new EstadoIVA(
            data.idContrato,
            data.ivaMaximo,
            data.ivaCobrado
        );
        return o;
    }
}
export class EstadoOperacionEnum{
    public static PENDIENTE : string ="P";
    public static APROBADO : string ="A";
    public static PENDIENTE_PAGO : string ="PP";
    public static RECHAZADO : string ="R";
    public static LIQUIDADO : string ="C";
}
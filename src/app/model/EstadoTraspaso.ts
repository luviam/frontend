import { Descriptivo } from './Descriptivo';
import * as moment from 'moment';
export class EstadoTraspaso{
    constructor(public id : number,
                public descripcion : string, 
                public fecha : Date,
                public idCajaOrigen : number,
                public origen : string, 
                public idCajaDestino : number,
                public destino : string, 
                public responsable : string,
                public estado : Descriptivo, 
                public monto : number){

    }

    get fechaFormateada(){
        return  moment(this.fecha).format('DD/MM/YYYY');
    }
}
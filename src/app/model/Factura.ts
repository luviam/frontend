import { ItemFactura } from './ItemFactura';
import { Descriptivo } from './Descriptivo';

import { BasicEntity } from './BasicEntity';
export class Factura extends BasicEntity{
    public static PAGO_A_CUENTA : string ="PA";
    constructor(public id?: number,
                public numero?:string,
                public descripcion? : string,
                public fecha? : Date,
                public fechaIVA? : Date,
                public tipoComprobante? : Descriptivo,
                public proveedor? : Descriptivo,
                public centroCosto? :Descriptivo,
                public items?: ItemFactura[],
                public iibbCABA:number = 0,
                public iibbBSAS:number = 0,
                public iibbOtros:number = 0,
                public impuestoSellos:number = 0,
                public estado? : Descriptivo){
                    super(id,numero,descripcion);
                    if(!items){
                        this.items = [];
                    }
                }

    public static fromData(c :any): Factura{
        if(!c) return null;
        let o : Factura = new Factura(c.id,
            c.numero,
            c.descripcion,
            c.fecha? new Date(c.fecha): null,
            c.fechaIVA? new Date(c.fechaIVA): null,
           Descriptivo.fromData(c.tipoComprobante),
           Descriptivo.fromData(c.proveedor),
           Descriptivo.fromData(c.centroCosto),
            c.items.map(i =>ItemFactura.fromData(i)),
            c.iibbCABA,
            c.iibbBSAS,
            c.iibbOtros,
            c.impuestoSellos,
            Descriptivo.fromData(c.estado));
            return o;


    }

    public addItem(item : ItemFactura){
        this.items.push(item);
    }

    public quitarItem(item :ItemFactura){
        let i = this.items.indexOf(item);
        if(i>-1){
            this.items.splice(i,1);
        }
    }

     get iva0():number{
        return 0;
    }

    get iva105():number{
        return parseFloat(this.items.filter(i => i.tipoIVA.codigo === "105").map(a => a.importe * 0.105).reduce((a,b)=> a + b, 0).toFixed(2));
    }
    get iva21():number{
        return parseFloat(this.items.filter(i => i.tipoIVA.codigo === "21").map(a => a.importe * 0.21).reduce((a,b)=> a + b, 0).toFixed(2));
    }
    get iva27():number{
        return parseFloat(this.items.filter(i => i.tipoIVA.codigo === "27").map(a => a.importe * 0.27).reduce((a,b)=> a + b, 0).toFixed(2));
    }

    get total() : number{
        let totalItems = this.items.map(a => a.importe).reduce((a,b) => a + b, 0);
        return parseFloat((this.iva0 + this.iva105 + this.iva21 + 
                this.iva27 + this.iibbBSAS + this.iibbCABA  +
                this.impuestoSellos + this.iibbOtros + totalItems).toFixed(2));
    }

    get esComprobanteNumerado():boolean {

    return this.tipoComprobante && (this.tipoComprobante.descripcion.includes("Factura") || this.tipoComprobante.descripcion.includes("Nota"));
    }
}
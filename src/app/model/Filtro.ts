import { BehaviorSubject } from 'rxjs';

export abstract class Filtro implements IPersistent{
    replacer(key,value)
    {
        if (key=="stateObs") return undefined;
        else return value;
    }
    public stateObs : BehaviorSubject<IPersistent> = new BehaviorSubject<IPersistent>(null);
    constructor(){
       this.stateObs = new BehaviorSubject<IPersistent>(null); 
   }
    abstract getResumen(): string;
    public abstract isEmpty():boolean;
    public onChange(){
        this.stateObs.next(this);
    }
    abstract get key():string;
    

    ;
    public abstract parse(o): Filtro;
  
}
export interface IPersistent{
    key: string;
    replacer(key,value);
}
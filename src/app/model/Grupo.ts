import { Descriptivo } from './Descriptivo';
export class Grupo extends Descriptivo{

    constructor(
        public id?:number,
        codigo? : string,
        descripcion? : string,
        public subGrupos : Descriptivo[] = []

    ){
        super(codigo,descripcion)
    }

    public static fromData(data:any):Grupo{
        if(!data) return null;
        let a: Grupo = new Grupo(
            data.id,
            data.codigo,
            data.descripcion,
            data.subGrupos? data.subGrupos.map(s => Descriptivo.fromData(s)) : []
        )
        return a;
    }
}
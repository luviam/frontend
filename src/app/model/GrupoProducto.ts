
import { Descriptivo } from './Descriptivo';
import { Parametrico } from './Parametrico';
export class GrupoProducto extends Parametrico {
    public esAdicional: boolean = false;
    public peso: number = 99;
    public static BEBIDA: Descriptivo = new Descriptivo("003", "Bebida");
    public static MENU: Descriptivo = new Descriptivo("001", "Menú");
    public static ALQUILER: Descriptivo = new Descriptivo("002", "Alquiler");
    public static ADICIONALES_N_G: string = "004";
    public static ADICIONALES_E: string = "005";
    public static ADICIONALES_G_P: string = "006";
    public static ADICIONALES_G_C: string = "007";
    public static get ID(): string {
        return "grupoproducto";
    }

    public static fromData(data: any) {
        if (!data) return null;

        let o: GrupoProducto = new GrupoProducto(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,

        );
        o.esAdicional = data.esAdicional;
        o.peso = data.peso;
        return o;


    }


}
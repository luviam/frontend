import { CuentaAplicacion } from './CuentaAplicacion';
import { CentroCosto } from './CentroCosto';
import { Descriptivo } from './Descriptivo';
import { BasicEntity } from './BasicEntity';
export class ImputacionCuentaConfig {

    constructor(public id?: number,
        public centroCosto?: Descriptivo,
        public cuentaAplicacion?: Descriptivo,
        public tipoImputacion?: Descriptivo,
        public habilitado : boolean = true) {

    }
public static fromData(data : any) : ImputacionCuentaConfig{
    if(!data) return null;
    let o : ImputacionCuentaConfig = new ImputacionCuentaConfig(
        data.id,
        Descriptivo.fromData(data.centroCosto),
        Descriptivo.fromData(data.cuentaAplicacion),
        Descriptivo.fromData(data.tipoImputacion),
        data.habilitado

    );
    return o;
}
    get codigo(){
        return this.centroCosto.codigo + this.tipoImputacion.codigo
    }
}
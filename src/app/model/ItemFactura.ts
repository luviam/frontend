import { Descriptivo } from './Descriptivo';
import { BasicEntity } from './BasicEntity';
export class ItemFactura {
    constructor(public id?: number,
        public centroCosto?: Descriptivo,
        public cuenta?: Descriptivo,
        public descripcion?: string,
        public tipoIVA?: Descriptivo,
        public importe?: number) {

        if (!importe) {
            this.importe = 0;
        }
    }

    public static fromData(i: any): ItemFactura {
        if (!i) return null;
        let o: ItemFactura = new ItemFactura(i.id,
            Descriptivo.fromData(i.centroCosto),
            Descriptivo.fromData(i.cuenta),
            i.descripcion,
            Descriptivo.fromData(i.tipoIVA),
            i.importe);
        return o;
    }
}

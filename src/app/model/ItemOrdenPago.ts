import { CabeceraFactura } from '../proveedores/facturas/listado/shared/CabeceraFactura';
import { Descriptivo } from './Descriptivo';
import { BasicEntity } from './BasicEntity';
export class ItemOrdenPago {
    private _valorValidado : number;
    constructor(public id?: number,
        public cabeceraFactura?: CabeceraFactura,
        public importe?: number) {

        if (!importe) {
            this.importe = 0;
            this._valorValidado = cabeceraFactura? cabeceraFactura.saldo : 0;
        }else{
            this._valorValidado = importe;
        }

        
    }
    public static fromData(data :any): ItemOrdenPago{
        if(!data) return null;
        let o : ItemOrdenPago = new ItemOrdenPago(
            data.id,
            CabeceraFactura.fromData(data.cabeceraFactura),
            data.importe
        );
        return o;
    }
    get esNotaCredito(): boolean{
        return this.cabeceraFactura && this.cabeceraFactura.esNotaCredito;
    }
  
}

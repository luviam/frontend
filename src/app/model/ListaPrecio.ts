import { GrupoProducto } from 'app/model/GrupoProducto';
import { Parametrico } from './Parametrico';

export class ListaPrecio extends Parametrico {
    public grupo: GrupoProducto;
    public padre: ListaPrecio;
    public static get ID(): string {
        return "listaprecio";
    }

    public static fromData(data: any) {
        if (!data) return null;

        let o: ListaPrecio = new ListaPrecio(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,


        );
        o.grupo = GrupoProducto.fromData(data.grupo);
        o.padre = ListaPrecio.fromData(data.padre);


        return o;


    }


}
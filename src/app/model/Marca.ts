import { Archivo, SIN_IMAGEN } from './Archivo';

import { environment } from "environments/environment";
import { Parametrico } from './Parametrico';
export class Marca extends Parametrico {
    public logo: Archivo ;

    public hayCambio: boolean = false;
    private _file : File;

    public get file():File{
        return this._file;
    }

    public set file(val:File){
        this._file = val;
        this.hayCambio = val != undefined;
    }
    public static get ID(): string {
        return "marcas";
    }

    public static fromData(data: any) {
        if (!data) return null;

        let o: Marca = new Marca(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,
            data.peso
        );
        o.logo = Archivo.fromData(data.logo);
        return o;


    }

    public get imagenUrl():string{
        return this.logo? environment.baseServerUrl + this.logo.url  : SIN_IMAGEN;
    }


}
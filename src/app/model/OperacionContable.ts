import { CuentaConfig } from './ConfigCuenta';
import { Descriptivo } from './Descriptivo';
import * as moment from 'moment';
export class OperacionContable{

    constructor(public id? : number,
                public centroCosto?: Descriptivo, 
                public cuenta?:Descriptivo,
                public descripcion? : string, 
                public debe?:number, 
                public haber?:number,
                public comprobante? : string,
                public fecha? : Date,
                public responsable? : string,
                public estado? :Descriptivo,
                public numeroAsiento? : number,
                public saldo? : number,
                public tipoGenerador?: string){
                    if(!centroCosto){
                        this.centroCosto = null;
                    }
                    if(!debe){
                        this.debe = 0.00;
                    }else{
                        debe = Number(debe.toFixed(2));
                    }
                    if(!haber){
                        this.haber = 0.00;
                    }else{
                        haber = Number(haber.toFixed(2));
                    }
                    if(!estado){
                        this.estado = new Descriptivo("ND", "No Definido");
                    }
    }
    
    get fechaFormateada() : string{
        if(this.fecha)
            return moment(this.fecha).format("DD/MM/YYYY");
        return "";
    }

    
}
import { BasicEntity } from './BasicEntity';
import { Descriptivo } from './Descriptivo';
import { ItemOrdenPago } from './ItemOrdenPago';
import { Pago } from './Pago';


export class OrdenPago extends BasicEntity {
    constructor(public id?: number,
        public numero?: string,
        public descripcion?: string,
        public responsable?: string,
        public fecha?: Date,
        public proveedor?: Descriptivo,
        public centroCosto?: Descriptivo,
        public items: ItemOrdenPago[] = [],
        public pagoACuenta: number = 0,
        public pagos: Pago[] = [],
        public estado?: Descriptivo,
        public importe?: number) {
        super(id, numero, descripcion);
    }

    public static fromData(data: any): OrdenPago {
        if (!data) return null;
        let o: OrdenPago = new OrdenPago(
            data.id,
            data.numero,
            data.descripcion,
            data.responsable,
            data.fecha ? new Date(data.fecha) : null,
            Descriptivo.fromData(data.proveedor),
            Descriptivo.fromData(data.centroCosto),
            data.items.map(i => ItemOrdenPago.fromData(i)),
            data.pagoACuenta,
            data.pagos.map(p => Pago.fromData(p)),
            Descriptivo.fromData(data.estado),
            data.importe


        );
        return o;

    }
    public addItem(item: ItemOrdenPago) {
        this.items.push(item);
    }

    public addPago(pago: Pago) {

        this.pagos.push(pago);
    }

    public quitarPago(pago: Pago) {
        let i = this.pagos.indexOf(pago);
        if (i > -1) {
            this.pagos.splice(i, 1);
        }
    }
    public quitarItem(item: ItemOrdenPago) {
        let i = this.items.indexOf(item);
        if (i > -1) {
            this.items.splice(i, 1);
        }
    }


    get total(): number {
        if (!this.items || this.items.length === 0) {
            return this.pagoACuenta;
        }
        return Math.round((this.items.reduce((a, b) => a + b.importe, 0) + this.pagoACuenta) * 100) / 100;
    }

    public getTotalItems(): number {
        return this.total - this.pagoACuenta;
    }

    get totalPagos(): number {
        return this.pagos.reduce((a, b) => a + b.monto, 0);
    }

    get tieneCheques(): boolean {
        return this.pagos && this.pagos.some(p => p.tipoOperacion.codigo === 'C');
    }

}
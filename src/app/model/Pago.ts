import { Descriptivo } from './Descriptivo';
import { Cheque } from './Cheque';

export class Pago{
    constructor(public id? :number,
                public tipoOperacion? : Descriptivo,
                public centroCosto? : Descriptivo,
                public cuenta? : Descriptivo,
                public monto?: number,
                public comprobante?: Cheque){
                    
                }

    public static fromData(data : any): Pago{
        if(!data) return null;
        let o : Pago = new Pago(
            data.id,
            Descriptivo.fromData(data.tipoOperacion),
            Descriptivo.fromData(data.centroCosto),
            Descriptivo.fromData(data.cuenta),
            data.monto,
            Cheque.fromData(data.comprobante)
        );
        return o;
    }
}
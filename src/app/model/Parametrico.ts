import { Descriptivo } from './Descriptivo';
export class  Parametrico extends Descriptivo{
    
    public static UNIDAD_PRODUCTO_ID = "unidad_producto";
    
    constructor(public id? : number, codigo?:string, descripcion?: string, 
        public habilitado : boolean = true,
         public esSistema : boolean = false,
         public peso : number = 99){
        super(codigo,descripcion);
    }

    public static fromData(data : any){
        if(!data) return null;

        let o : Parametrico = new Parametrico(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,
            data.peso
        );
        return o;


    }
    
}
import { Agrupador } from './Agrupador';
import { Subproducto } from './Subproducto';
import { Marca } from './Marca';
import { TipoMenu } from './TipoMenu';

import { ImputacionCuentaConfig } from './ImputacionCuentaConfig';
import { StringUtils } from '../common-utils/string-utils.class';
import { Descriptivo } from './Descriptivo';
import { Categoria } from './Categoria';
export class Producto extends Descriptivo {
    public static BEBIDA_BASE : string = "BALC";
    public static GARANTIA : string = "GARAN";
    public static IP_MAX : string = "IPMAX";
	public static SADAIC : string = "SADA";
	public static DEVOLUCION : string = "DEVGAR";
	public static ALQUILER : string = "ALQUI";
	public static MENU : string = "MENU";
	public static BARRA_NACIONAL: string ="BEBIDA52";
	public static BARRA_PREMIUM: string ="BEBIDA53";
	public static BARRA_SIN_ALCOHOL: string ="BARRAADOL";
	
	public static PACK : Producto = new Producto(null,"PACK", "Paquete");


    constructor(

		public id? : number,
		public codigo? : string,
		public nombre? : string,
		public unidad? : Descriptivo,
		public categoria? : Categoria,
		public activo? : boolean,
		public esBorrable? : boolean,
		public divisible? : boolean,
		public adolescenteProp? : number,
		public menorProp? : number,
		public bebeProp? : number,
		public cuentas: ImputacionCuentaConfig[] = [],
		public imputaIVA : boolean = true,
		public tipoMenu?: TipoMenu,
		public marca ? :Marca,
		public agrupador? :Agrupador,
		public detalleItem? :string,
		public subproductos : Subproducto[] = [],
		public notas ?: string,
	){
        super(codigo, nombre)
    }


    get peso(): number{
		return this.categoria? this.categoria.peso : 99;
	}
    
    get codigoUnidad() : string{
        return this.unidad? this.unidad.codigo : "SD";
    }
    public static fromData(data : any) : Producto{
		if(!data) return null; 
		let o : Producto  = new Producto(
		 data.id,
		 data.codigo,
		 data.nombre,
		 Descriptivo.fromData(data.unidad),
		 Categoria.fromData(data.categoria),
		 data.activo,
		 data.esBorrable,
		 data.divisible,
		 data.adolescenteProp,
		 data.menorProp,
		 data.bebeProp,
		 data.cuentas? data.cuentas.map( c => ImputacionCuentaConfig.fromData(c))	: [],
		 data.imputaIVA,
		 TipoMenu.fromData(data.tipoMenu),
		 Marca.fromData(data.marca),
		 Agrupador.fromData(data.agrupador),
		 data.detalleItem,
		data.subproductos? data.subproductos.map(s => Subproducto.fromData(s)):[],
		data.notas);
		return o; 

	}
	 get cuentasImputadas(): ImputacionCuentaConfig[]{
        return this.cuentas.filter(c => c.habilitado && c.cuentaAplicacion && c.cuentaAplicacion.codigo);
	}
	
	get centros(): Descriptivo[]{
		return this.cuentasImputadas.map(c =>c.centroCosto);
	}

    public esCategoria(codigo: string) {
        return this.categoria && this.categoria.codigo === codigo;
    }



    get estado(): string {
        return this.activo ? "Habilitado" : "Baja"
	}
	
	public esMenu():boolean{
		return this.esCategoria(Producto.MENU);
	}
	public esImpuesto(): boolean{
		return this.categoria && this.categoria.codigo === Categoria.IMPUESTO;
	}

	public esOperativoIP(): boolean{
		return this.categoria && this.categoria.codigo === Categoria.OPERATIVO_IP;
	}
	public esInterno(): boolean{
		return this.categoria && this.categoria.codigo === Categoria.INTERNO;
	}

	public nombreFull():string{
		return (this.tipoMenu?this.tipoMenu.descripcion +  ' - ' : "") + this.nombre; 
	}
	public datosGenerales():string{
		return this.categoria.descripcion + " " + this.categoria.codigo + " " + this.nombreFull();
	}
	public quitarSubproducto(s: Subproducto){
		this.subproductos = this.subproductos.filter(sub => sub !== s);
	}
	public agregarSubproducto(s:Subproducto){
		this.subproductos.push(s);
		this.subproductos = [...this.subproductos];
	}

   
}

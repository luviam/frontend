import { Descriptivo } from './Descriptivo';
import { ImputacionCuentaConfig } from './ImputacionCuentaConfig';
import { Cuenta } from './Cuenta';
import { ConvenioSalon } from './ConvenioSalon';
import { CentroCosto } from './CentroCosto';
export class Proveedor extends Descriptivo {
    constructor(
        public id?: number,
        public nombreProveedor?: string,
        public nombreContacto?: string,
        public domicilio?: string,
        public localidad?: string,
        public telefono?: string,
        public celular?: string,
        public email?: string,
        public razonSocial?: string,
        public cuit?: string,
        public iva?: Descriptivo,
        public domicilioFacturacion?: string,
        public localidadFacturacion?: string,
        public tipoProveedor?: Descriptivo,
        public tipoCuenta?: Descriptivo,        
        public activo?: boolean,
        public saldo? :number,
        public imputacionCuentaConfigs : ImputacionCuentaConfig[] = []

    ) {
        super(id + "", nombreProveedor);
        
    }

    public static fromData(data :any) :Proveedor{
        if(!data) return null;
        let o : Proveedor = new Proveedor(data.id,
            data.nombreProveedor,
            data.nombreContacto,
            data.domicilio,
            data.localidad,
            data.telefono,
            data.celular,
            data.email,
            data.razonSocial,
            data.cuit,
            Descriptivo.fromData(data.iva),
            data.domicilioFacturacion,
            data.localidadFacturacion,
            Descriptivo.fromData(data.tipoProveedor),
            Descriptivo.fromData(data.tipoCuenta),
            data.activo,0,
            data.imputacionCuentaConfigs.map(i => ImputacionCuentaConfig.fromData(i)
            )
        );
        return o;
    }
    public getCuentaImputacion(tipo : string, centro : string): ImputacionCuentaConfig{
        if(!this.imputacionCuentaConfigs)
            return null;
        let res : ImputacionCuentaConfig[] = this.imputacionCuentaConfigs
            .filter(c => c.centroCosto.codigo === centro && c.tipoImputacion.codigo === tipo);
        
        return res[0] || null;
    }

    public addCuentaImputacion(cuenta : ImputacionCuentaConfig){
        if(!this.imputacionCuentaConfigs){
            this.imputacionCuentaConfigs = [];
        }
        this.imputacionCuentaConfigs.push(cuenta);
    }

    public getCentrosConImputacion() : Descriptivo[]{
        return this.imputacionCuentaConfigs.filter(i => i.cuentaAplicacion).map( c => c.centroCosto);
    }

}
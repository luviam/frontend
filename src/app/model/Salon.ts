import { Descriptivo } from './Descriptivo';

import { StringUtils } from '../common-utils/string-utils.class';
import { Producto } from './Producto';


export class Salon extends Descriptivo{
	public static OTRO : string = 'OTRO';
	constructor(

		public id? : number,
		public nombre? : string,
		public direccion? : string,
		public codigoPostal? : string,
		public provincia? : Descriptivo,
		public nombreContacto? : string,
		public telefono? : string,
		public email? : string,
		public latitud? : number,
		public longitud? : number,
		public activo? : boolean,
		public productoAlquiler? : Producto,
		public esComisionista? : boolean,
		public observaciones?: string,
	){
        super(id+"", nombre);
    }

	public static fromData(data : any) : Salon{
		if(!data){
			return null;
		}
		let o : Salon  = new Salon(
		 data.id? data.id : null,
		 data.nombre,
		 data.direccion,
		 data.codigoPostal,
		 Descriptivo.fromData(data.provincia),
		 data.nombreContacto,
		 data.telefono,
		 data.email,
		 data.latitud,
		 data.longitud,
		 data.activo,	
		 Producto.fromData(data.productoAlquiler),
		 data.esComisionista,
		 data.observaciones);
		 
		 if(!o.id){
			 o.codigo = Salon.OTRO;
		 }
		return o; 

	}


	get esOtros(): boolean{
		return this.codigo === Salon.OTRO;
	}
    get contactoCompleto(): string {
        return this.nombreContacto + this.telefono + this.email;
    }

    get direccionCompleta(): string {
        let direccionCompleta = this.direccion ? (this.direccion + "##") : "##";

        return direccionCompleta;

    }

    get emails(): string[] {
        return this.email.toLowerCase().split("/");
    }

    get estado(): string {
        return this.activo ? "Habilitado" : "Baja"
    }

}

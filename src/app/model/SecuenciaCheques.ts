export class SecuenciaCheques {

    constructor(public id?:number, public minNumeracion?:number, public maxNumeracion?:number){

    }

    get valid():boolean{
        return this.minNumeracion!=undefined && this.maxNumeracion!=undefined ? 
                    this.minNumeracion >= 0 && this.maxNumeracion >= 0 && 
                    this.minNumeracion <= this.maxNumeracion : 
                false;
    }

}
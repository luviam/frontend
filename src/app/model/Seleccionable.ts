import { Descriptivo } from './Descriptivo';
export class Seleccionable extends Descriptivo{

    constructor(public descriptivo : Descriptivo, public seleccionado?: boolean){
        super(descriptivo.codigo,descriptivo.descripcion)
        if(!seleccionado){
            this.seleccionado = false;
        }
    }
}
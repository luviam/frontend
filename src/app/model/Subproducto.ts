

export class Subproducto{
	constructor(

		public id? : number,
		public descripcion? : string,
		public peso? : number,
	){}

	public static fromData(data : any) : Subproducto{
		if(!data) return null; 
		let o : Subproducto  = new Subproducto(
		 data.id,
		 data.descripcion,
		 data.peso,	);
		return o; 

	}

}


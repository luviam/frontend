import { Descriptivo } from './Descriptivo';
import { Categoria } from './Categoria';

import { Parametrico } from './Parametrico';
export class TipoComisionista extends Parametrico{
    public categorias : Categoria[] = [];
    public static SALON_CODIGO : string = "SALON";
    public static VENDEDOR_CODIGO : string = "VEND";
    public static get ID(): string{
        return "tipo-comisionistas";
    }

    public static fromData(data : any){
        if(!data) return null;

        let o : TipoComisionista = new TipoComisionista(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,
        );
        o.categorias = data.categorias? data.categorias.map(c => Categoria.fromData(c)) : [];
        return o;


    }

    get categoriasComisionadas(): string{
        if(this.categorias && this.categorias.length > 0){
            return this.categorias.map(c => c.codigo).join(", ");
        }else{
            return "SIN DEFINIR";
        }
    }

    public tieneCategoria(cat : Descriptivo) : boolean{
        return this.categorias && this.categorias.some(c => c.codigo === cat.codigo);
    }
   

}
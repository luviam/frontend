import { Parametrico } from './Parametrico';
import { Descriptivo } from './Descriptivo';
export class TipoEvento extends Parametrico{

    public static get ID(): string{
        return "tipoEvento";
    }
}
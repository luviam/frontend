import { Parametrico } from "./Parametrico";

export class TipoMenu extends Parametrico{

    constructor(id? : number, codigo?:string, descripcion?: string, 
    habilitado : boolean = true,
    esSistema : boolean = false){
        super(id, codigo,descripcion, habilitado,esSistema);
    }

public static get ID() : string{
    return "tipomenu";
}

    public static fromData(data : any): TipoMenu{
        if(!data) return null;

        let o : TipoMenu = new TipoMenu(
            data.id,
            data.codigo,
            data.descripcion,
            data.habilitado,
            data.esSistema,
        );
        return o;


    }

}
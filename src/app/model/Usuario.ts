import { Rol } from './Rol';
import { Descriptivo } from './Descriptivo';
export class Usuario extends Descriptivo{
    
    public id?:number;
    public username:string;
    public password?:string;
    public nombre?:string;
    public apellido?:string;
    public telefono?:string;
    public roles?:Rol[];
    public activo?:boolean;
    
    constructor(data? : any){
        super(data.id+"",data.username);
        this.id= data.id;
        this.username= data.username;
        this.password= data.password;
        this.nombre= data.nombre;
        this.apellido= data.apellido;
        this.telefono= data.telefono;
        this.roles= data.roles;
        this.activo = (data.activo != undefined) ? data.activo  : true;
    }

    tieneRol(roles:string[]){
        for(let rol = 0; rol < roles.length; rol++)
            if (this.roles.filter(r=>r.authority === roles[rol]).length > 0) 
                return true;

        return false;
    }

}
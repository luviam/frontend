import { Descriptivo } from '../Descriptivo';


import { Cheque } from '../Cheque';

export class Cobro{
    constructor(public id? :number,
        public tipoOperacion? : Descriptivo,
        public centroCosto? : Descriptivo,
        public cuenta? : Descriptivo,
        public monto?: number,
        public comprobante?: Cheque){
            
        }

	public static fromData(data : any) : Cobro{
		if(!data) return null; 
		let o : Cobro  = new Cobro(
		 data.id,
		 Descriptivo.fromData(data.tipoOperacion),
		 Descriptivo.fromData(data.centroCosto),
		 Descriptivo.fromData(data.cuenta),
		 data.monto,
		 Cheque.fromData(data.comprobante));
		return o; 

	}
}
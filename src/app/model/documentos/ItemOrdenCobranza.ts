import { CuotaServicio } from '../../contratos/model/CuotaServicio';

import { Descriptivo } from '../Descriptivo';

export class ItemOrdenCobranza {
    private _valorValidado : number;
    constructor(public id?: number,
        public cuotaServicio?: CuotaServicio,
        public importe?: number) {

        if (!importe) {
            this.importe = 0;
            this._valorValidado = cuotaServicio? cuotaServicio.saldo : 0;
        }else{
            this._valorValidado = importe;
        }

        
    }
    public static fromData(data : any): ItemOrdenCobranza{
        if(!data) return null;
        let o : ItemOrdenCobranza = new ItemOrdenCobranza(
            data.id,
            CuotaServicio.fromData(data.cuotaServicio),
            data.importe
        );
        return o;
    }
  
    get importeValidado(){
        return this._valorValidado;
    }

    set importeValidado(importe : number){
        if(importe > this.cuotaServicio.saldo){
            this.importe = this.cuotaServicio.saldo;
        }else{
            this.importe = importe;
        }
        this._valorValidado = importe;
    }
}

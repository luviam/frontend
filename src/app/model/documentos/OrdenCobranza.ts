import { Cobro } from './Cobro';
import { BasicEntity } from '../BasicEntity';
import { Descriptivo } from '../Descriptivo';
import { ItemOrdenCobranza } from './ItemOrdenCobranza';
import { ContratoCabecera } from '../../clientes/lista-clientes/shared/ContratoCabecera';
import * as moment from 'moment';


export class OrdenCobranza extends BasicEntity{
    
    constructor(public id?: number,
                public numero?:string,
                public descripcion? : string,
                public responsable? : string,
                public fecha : Date = new Date(),   
                public contrato? : ContratoCabecera,
                public centroCosto? :Descriptivo,
                public items: ItemOrdenCobranza[] = [],
                public cobros: Cobro[] = [],
                public estado? : Descriptivo,
                public importeNeto?: number,
                public numeroComprobante? : string,
                public cobrador? :Descriptivo,
                public observaciones?:string){
                    super(id,numero,descripcion);
                }

    public static fromData(data : any): OrdenCobranza{
        if(!data) return null;
        let o : OrdenCobranza = new OrdenCobranza(
            data.id,
            data.numero,
            data.descripcion,
            data.responsable,
            data.fecha? new Date(data.fecha): null,
            ContratoCabecera.fromData(data.contrato),
            Descriptivo.fromData(data.centroCosto),
            data.items.map(i => ItemOrdenCobranza.fromData(i)),
            data.cobros.map(c => Cobro.fromData(c)),
            Descriptivo.fromData(data.estado),
            data.importeNeto,
            data.numeroComprobante,
            Descriptivo.fromData(data.cobrador),
            data.observaciones

        );
        return o;
    }
  
    public addItem(item : ItemOrdenCobranza){
        this.items.push(item);
    }

    public addCobro(cobro : Cobro){
        
        this.cobros.push(cobro);
    }

    public quitarCobro(cobro : Cobro){
        let i = this.cobros.indexOf(cobro);
        if(i>-1){
            this.cobros.splice(i,1);
        }
    }
    public quitarItem(item :ItemOrdenCobranza){
        let i = this.items.indexOf(item);
        if(i>-1){
            this.items.splice(i,1);
        }
    }

    get itemsNeto() : ItemOrdenCobranza[]{
        return this.items.filter(i=> i.cuotaServicio.producto.categoria.codigo !=="IMP");
    }
    
    get impuestos() : ItemOrdenCobranza[]{
        return this.items.filter(i=> i.cuotaServicio.producto.categoria.codigo ==="IMP");
    }
    get total() : number{
        return this.items.reduce((a,b) => a + b.importe, 0);
    }

    get totalItems():number{
      return this.items.filter(i=> i.cuotaServicio.producto.categoria.codigo !=="IMP").reduce((a,b) => a + b.importe, 0);
    }

    get totalImpuestos():number{
        return this.items.filter(i=> i.cuotaServicio.producto.categoria.codigo ==="IMP").reduce((a,b) => a + b.importe, 0);
      }

    get totalCobros():number{
        return this.cobros.reduce((a,b) => a + b.monto, 0);
    }

    get tieneCheques() :boolean{
        return this.cobros && this.cobros.some(p => p.tipoOperacion.codigo === 'C');
    }
    get fechaFormateada(){
        return moment(this.fecha).format('DD/MM/YYYY');
    }
   

}
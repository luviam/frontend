import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import { NgModule } from '@angular/core';
import {  } from 'primeng/primeng';

@NgModule({
  imports: [
    BsDropdownModule.forRoot()
  ],
  exports: [
    BsDropdownModule
  ]
})
export class NgBootstrapModule { }
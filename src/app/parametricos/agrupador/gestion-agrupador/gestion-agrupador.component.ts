import { Descriptivo } from 'app/model/Descriptivo';
import { Agrupador } from '../../../model/Agrupador';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';
import { GrupoProducto } from 'app/model/GrupoProducto';


@Component({
	selector: 'gestion-agrupador',
	templateUrl: 'gestion-agrupador.component.html',
	styleUrls: ['gestion-agrupador.component.less']
})

export class GestionAgrupadorComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	public grupos : GrupoProducto[] = [];
	public items: string[] = [];

	private _item: Agrupador = new Agrupador();
	public finalizado: boolean = false;
	
	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : Agrupador){
		this._item = val;
	}

	get item(): Agrupador{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<Agrupador> = new EventEmitter<Agrupador>();

	@Output()
	public onCancelar : EventEmitter<Agrupador> = new EventEmitter<Agrupador>();

	@Output()
	public onGuardar : EventEmitter<Agrupador> = new EventEmitter<Agrupador>();

	constructor(
		private service: ParametricoService, private grupoService: GrupoProductoService
		) {
		super();
		
  }

  ngOnInit() {
    let $this = this;
		this.finalizado = false;
		this.addLoadingCount();
		this.grupoService.getAll().then(r=>{
			$this.grupos = r;
			$this.susLoadingCount();
		})
		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(Agrupador.ID,this.item).then((c) => {
				$this.success("El agrupador fue guardado correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El agrupador no es valido");
		}
	}
	

	public itemValido(item: Agrupador): boolean {
		return true;
  }

}

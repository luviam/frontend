import { AgrupadorService } from '../service/agrupador.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Agrupador } from '../../../model/Agrupador';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lista-agrupador',
	templateUrl: 'lista-agrupador.component.html'
})

export class ListaAgrupadorComponent extends SessionComponent implements OnInit {

	public items: Agrupador[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : Agrupador;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Agrupadores";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: AgrupadorService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : Agrupador) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : Agrupador) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = Agrupador.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : Agrupador){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: Agrupador){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: Agrupador) {
		this.itemEditado = Agrupador.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new Agrupador();
		this.editandoItem = true;
		
	}
	
}
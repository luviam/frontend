import { Descriptivo } from './../../../model/Descriptivo';
import { Agrupador } from '../../../model/Agrupador';

import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';

@Injectable()
export class AgrupadorService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<Agrupador[]>{
    
        return this.http.get(this.getApiURL() +"agrupadores/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getByCategoria(cat: Descriptivo): Promise<Agrupador[]>{
    
        return this.http.get(this.getApiURL() +"agrupadores/byCategoria/" + cat.codigo).toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getComisionables(): Promise<Agrupador[]>{
    
        return this.http.get(this.getApiURL() +"agrupadores/comisionables").toPromise().then(this.parseResults).catch(this.handleError);
    }
    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"agrupadores/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar( parametrico: Agrupador): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + "agrupadores/", parametrico).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "agrupadores/",  parametrico).toPromise().then(this.handleOk, this.handleError);
        }
    }

    public parseResults(data : any): Agrupador[]{
        let res = data.respuesta;
        let parametricos: Agrupador[] = [];
        if (res) {
            parametricos = res.map(p => Agrupador.fromData(p) );
        }
        return parametricos;
    }

}
import { Descriptivo } from 'app/model/Descriptivo';
import { Categoria } from '../../../model/Categoria';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';
import { GrupoProducto } from 'app/model/GrupoProducto';


@Component({
	selector: 'gestion-categoria',
	templateUrl: 'gestion-categoria.component.html',
	styleUrls: ['gestion-categoria.component.less']
})

export class GestionCategoriaComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	public grupos : GrupoProducto[] = [];
	

	private _item: Categoria = new Categoria();
	public finalizado: boolean = false;
	
	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : Categoria){
		this._item = val;
	}

	get item(): Categoria{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<Categoria> = new EventEmitter<Categoria>();

	@Output()
	public onCancelar : EventEmitter<Categoria> = new EventEmitter<Categoria>();

	@Output()
	public onGuardar : EventEmitter<Categoria> = new EventEmitter<Categoria>();

	constructor(
		private service: ParametricoService, private grupoService: GrupoProductoService
		) {
		super();
		
  }

  ngOnInit() {
    let $this = this;
		this.finalizado = false;
		this.addLoadingCount();
		this.grupoService.getAll().then(r=>{
			$this.grupos = r;
			$this.susLoadingCount();
		})
		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(Categoria.ID,this.item).then((c) => {
				$this.success("La Categoría fue guardada correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("La Categoría no es valida");
		}
	}
	

	public itemValido(item: Categoria): boolean {
		return true;
  }

}

import { CategoriaService } from '../service/categoria.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Categoria } from '../../../model/Categoria';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lista-categoria',
	templateUrl: 'lista-categoria.component.html'
})

export class ListaCategoriaComponent extends SessionComponent implements OnInit {

	public items: Categoria[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : Categoria;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Categorías";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: CategoriaService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : Categoria) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : Categoria) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = Categoria.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : Categoria){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: Categoria){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: Categoria) {
		this.itemEditado = Categoria.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new Categoria();
		this.editandoItem = true;
		
	}
	
}
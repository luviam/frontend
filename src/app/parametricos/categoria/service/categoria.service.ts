import { Categoria } from '../../../model/Categoria';

import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';

@Injectable()
export class CategoriaService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<Categoria[]>{
    
        return this.http.get(this.getApiURL() +"categorias/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getComisionables(): Promise<Categoria[]>{
    
        return this.http.get(this.getApiURL() +"categorias/comisionables").toPromise().then(this.parseResults).catch(this.handleError);
    }
    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"categorias/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar( parametrico: Categoria): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + "categorias/", parametrico).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "categorias/",  parametrico).toPromise().then(this.handleOk, this.handleError);
        }
    }

    public parseResults(data : any): Categoria[]{
        let res = data.respuesta;
        let parametricos: Categoria[] = [];
        if (res) {
            parametricos = res.map(p => Categoria.fromData(p) );
        }
        return parametricos;
    }

}
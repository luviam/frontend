import { Descriptivo } from './../../../model/Descriptivo';
import { DescriptivosService } from './../../../common-services/descriptivos.service';
import { EscalaComisiones } from '../../../model/EscalaComisiones';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EscalaComisionesService } from '../service/escala-comisiones.service';


@Component({
	selector: 'gestion-escala-comisiones',
	templateUrl: 'gestion-escala-comisiones.component.html',
	styleUrls: ['gestion-escala-comisiones.component.less']
})

export class GestionEscalaComisionesComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	

	private _item: EscalaComisiones = new EscalaComisiones();
	public finalizado: boolean = false;
	public centros : Descriptivo[] = [];
	
	@Input()
  	public editable: boolean = true;

	@Input()
	public escalas : EscalaComisiones[] = [];

	@Input()
	set item(val : EscalaComisiones){
		this._item = val;
	}

	get item(): EscalaComisiones{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<EscalaComisiones> = new EventEmitter<EscalaComisiones>();

	@Output()
	public onCancelar : EventEmitter<EscalaComisiones> = new EventEmitter<EscalaComisiones>();

	@Output()
	public onGuardar : EventEmitter<EscalaComisiones> = new EventEmitter<EscalaComisiones>();

	constructor(cookieService :CookieServiceHelper,
		private service: EscalaComisionesService,
		private descriptivos : DescriptivosService,
		private route: ActivatedRoute		
		) {
		super();
		
  }

  ngOnInit() {
    let $this = this;
	this.finalizado = false;
	this.addLoadingCount();
	this.descriptivos.getCentrosCosto().then(r =>{
		$this.centros = r;
		$this.susLoadingCount();
	}).catch(this.errorHandler);
		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(this.item).then((c) => {
				$this.success("La Escala fue guardada correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("La Escala no es valida");
		}
	}
	

	public itemValido(item: EscalaComisiones): boolean {
		let estado : boolean = true;
		if(this.escalas.filter(c=> c.id !== item.id && c.centro.codigo === item.centro.codigo ).some(e => (item.desde<= e.hasta && item.desde >= e.desde )
		|| (item.hasta <= e.hasta && item.desde >=e.desde))){
			this.error("Existe una escala que está en conflicto con la nueva");
			estado = false;
		}
		if(item.desde <=0 || item.hasta <=0){
			this.error("Ingrese valores mayores a 0 para el intervalo de escala")
			estado = false;
		}
		if(item.premio == 0){
			estado = false;
			this.error("El premio debe ser mayor a 0");
		}
		if(item.desde >= item.hasta){
			estado = false;
			this.error("Desde tiene que ser menor a Hasta");
		}
		if(!item.centro || !item.centro.codigo){
			estado = false;
			this.error("Elija un Centro")
		}
		return estado;
	}
	

}

import { EscalaComisionesService } from '../service/escala-comisiones.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { EscalaComisiones } from '../../../model/EscalaComisiones';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lista-escala-comisiones',
	templateUrl: 'lista-escala-comisiones.component.html'
})

export class ListaEscalaComisionesComponent extends SessionComponent implements OnInit {

	public items: EscalaComisiones[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : EscalaComisiones;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Categorías";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: EscalaComisionesService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : EscalaComisiones) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : EscalaComisiones){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: EscalaComisiones){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: EscalaComisiones) {
		this.itemEditado = EscalaComisiones.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new EscalaComisiones();
		this.editandoItem = true;
		
	}
	
}
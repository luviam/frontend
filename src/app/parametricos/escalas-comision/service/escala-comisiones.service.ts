import { EscalaComisiones } from './../../../model/EscalaComisiones';


import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';

@Injectable()
export class EscalaComisionesService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<EscalaComisiones[]>{
    
        return this.http.get(this.getApiURL() +"escala-comisiones/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getEscalasByCentro(centro : string): Promise<EscalaComisiones[]>{
        return this.http.get(this.getApiURL() +"escala-comisiones/byCentro/" + centro).toPromise().then(this.parseResults).catch(this.handleError);
    }
    public getComisionables(): Promise<EscalaComisiones[]>{
    
        return this.http.get(this.getApiURL() +"escala-comisiones/comisionables").toPromise().then(this.parseResults).catch(this.handleError);
    }
    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"escala-comisiones/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar( parametrico: EscalaComisiones): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + "escala-comisiones/", parametrico).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "escala-comisiones/",  parametrico).toPromise().then(this.handleOk, this.handleError);
        }
    }

    public parseResults(data : any): EscalaComisiones[]{
        let res = data.respuesta;
        let parametricos: EscalaComisiones[] = [];
        if (res) {
            parametricos = res.map(p => EscalaComisiones.fromData(p) );
        }
        return parametricos;
    }

}
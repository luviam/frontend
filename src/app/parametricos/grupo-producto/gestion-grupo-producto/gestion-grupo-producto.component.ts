import { GrupoProducto } from '../../../model/GrupoProducto';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
	selector: 'gestion-grupo-producto',
	templateUrl: 'gestion-grupo-producto.component.html',
	styleUrls: ['gestion-grupo-producto.component.less']
})

export class GestionGrupoProductoComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	

	private _item: GrupoProducto = new GrupoProducto();
	public finalizado: boolean = false;
	
	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : GrupoProducto){
		this._item = val;
	}

	get item(): GrupoProducto{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<GrupoProducto> = new EventEmitter<GrupoProducto>();

	@Output()
	public onCancelar : EventEmitter<GrupoProducto> = new EventEmitter<GrupoProducto>();

	@Output()
	public onGuardar : EventEmitter<GrupoProducto> = new EventEmitter<GrupoProducto>();

	constructor(private service: ParametricoService,
		) {
		super();
		
  }

  ngOnInit() {
		this.finalizado = false;
		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(GrupoProducto.ID,this.item).then((c) => {
				$this.success("El Grupo de Producto fue guardado correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El Grupo de Producto no es valido");
		}
	}
	

	public itemValido(item: GrupoProducto): boolean {
		return true;
  }

}

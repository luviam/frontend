import { GrupoProductoService } from '../service/grupo-producto.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { GrupoProducto } from '../../../model/GrupoProducto';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lista-grupo-producto',
	templateUrl: 'lista-grupo-producto.component.html'
})

export class ListaGrupoProductoComponent extends SessionComponent implements OnInit {

	public items: GrupoProducto[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : GrupoProducto;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Grupos de Producto";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: GrupoProductoService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : GrupoProducto) {
		this.confirmationService.confirm({
			key: 'grupoConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado? Esto deshabilitará todas las listas y precios asociados',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : GrupoProducto) {
		this.confirmationService.confirm({
			key: 'grupoConf',
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = GrupoProducto.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : GrupoProducto){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: GrupoProducto){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: GrupoProducto) {
		this.itemEditado = GrupoProducto.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new GrupoProducto();
		this.editandoItem = true;
		
	}
	
}
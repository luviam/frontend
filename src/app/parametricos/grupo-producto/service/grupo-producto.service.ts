import { GrupoProducto } from '../../../model/GrupoProducto';

import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';

@Injectable()
export class GrupoProductoService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<GrupoProducto[]>{
    
        return this.http.get(this.getApiURL() +"grupoproducto/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

   /* public getAdicionales(): Promise<GrupoProducto[]>{
    
        return this.http.get(this.getApiURL() +"grupoproducto/adicionales").toPromise().then(this.parseResults).catch(this.handleError);
    }*/
    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"grupoproducto/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar( parametrico: GrupoProducto): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + "grupoproducto/", parametrico).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "grupoproducto/",  parametrico).toPromise().then(this.handleOk, this.handleError);
        }
    }

    public parseResults(data : any): GrupoProducto[]{
        let res = data.respuesta;
        let parametricos: GrupoProducto[] = [];
        if (res) {
            parametricos = res.map(p => GrupoProducto.fromData(p) );
        }
        return parametricos;
    }

}
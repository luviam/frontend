import { StringUtils } from './../../../common-utils/string-utils.class';
import { ListaPrecioService } from 'app/parametricos/lista-precios/service/lista-precios.service';
import { GrupoProductoService } from './../../grupo-producto/service/grupo-producto.service';
import { ListaPrecio } from '../../../model/ListaPrecio';
import { ParametricoService } from '../../services/parametricos.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GrupoProducto } from 'app/model/GrupoProducto';


@Component({
	selector: 'gestion-lista-precios',
	templateUrl: 'gestion-lista-precios.component.html',
	styleUrls: ['gestion-lista-precios.component.less']
})

export class GestionListaPrecioComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	public grupos: GrupoProducto[] = [];

	public invalidInput: boolean = false;

	private _item: ListaPrecio = new ListaPrecio();
	@Input()
	public items: ListaPrecio[] = [];

	@Input()
	public set grupo(g : GrupoProducto){
		this._grupo = g;
		if(this._item){
			this._item.grupo = g;
		}
	}
	public get grupo():GrupoProducto{
		return this._grupo;
	}
	private _grupo: GrupoProducto;

	public finalizado: boolean = false;

	@Input()
	public editable: boolean = true;


	@Input()
	set item(val: ListaPrecio) {
		this._item = val;
	}

	get item(): ListaPrecio {
		return this._item;
	}

	@Output()
	public itemChange: EventEmitter<ListaPrecio> = new EventEmitter<ListaPrecio>();

	@Output()
	public onCancelar: EventEmitter<ListaPrecio> = new EventEmitter<ListaPrecio>();

	@Output()
	public onGuardar: EventEmitter<ListaPrecio> = new EventEmitter<ListaPrecio>();


	constructor(
		private service: ParametricoService, private grupoService: GrupoProductoService, private listaPrecioService: ListaPrecioService
	) {
		super();

	}

	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		this.addLoadingCount();
		this.grupoService.getAll().then(r => {
			$this.grupos = r;
			$this.susLoadingCount();
		})
		if (this.items.length === 0) {
			$this.addLoadingCount();
			this.listaPrecioService.getAll().then(r => {
				$this.items = r;
				$this.susLoadingCount();
			}).catch(this.error);
		}

	}

	public confirmarCancelar() {
		this.onCancelar.emit(this.item);
	}

	public onInputChange(event: any) {
		this.invalidInput = this.items.filter(
					 l => l.id !== this.item.id 
					 && l.grupo && this.item.grupo && l.grupo.codigo ===  this.item.grupo.codigo
					 && StringUtils.getSinTildes(l.descripcion) === StringUtils.getSinTildes(this.item.descripcion)).length > 0;
	}

	public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();

			this.service.guardar(ListaPrecio.ID, this.item).then((c) => {
				$this.success("El Tipo Lista de Precios fue guardado correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(ListaPrecio.fromData(c));
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El Tipo Lista de Precios no es valido");
		}
	}


	public itemValido(item: ListaPrecio): boolean {
		return true;
	}

}

import { ListaPrecioService } from '../service/lista-precios.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { ListaPrecio } from '../../../model/ListaPrecio';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lista-lista-precios',
	templateUrl: 'lista-lista-precios.component.html'
})

export class ListaListaPrecioComponent extends SessionComponent implements OnInit {

	public items: ListaPrecio[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : ListaPrecio;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Tipos Lista de Precio";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: ListaPrecioService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : ListaPrecio) {
		this.confirmationService.confirm({
			key: 'lpConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado? Esto deshabilitará todos los precios asociados',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : ListaPrecio) {
		this.confirmationService.confirm({
			key: 'lpConf',
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = ListaPrecio.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : ListaPrecio){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: ListaPrecio){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: ListaPrecio) {
		this.itemEditado = ListaPrecio.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new ListaPrecio();
		this.editandoItem = true;
		
	}
	
}
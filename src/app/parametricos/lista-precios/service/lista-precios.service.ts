import { ListaPrecio } from '../../../model/ListaPrecio';

import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';
import * as moment from 'moment';

@Injectable()
export class ListaPrecioService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<ListaPrecio[]>{
    
        return this.http.get(this.getApiURL() +"listaprecio/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getAllByGrupo(grupo:string): Promise<ListaPrecio[]>{
    
        return this.http.get(this.getApiURL() +"listaprecio/byGrupo/" + grupo).toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getById(id:number): Promise<ListaPrecio[]>{
    
        return this.http.get(this.getApiURL() +"listaprecio/byId/" + id).toPromise().then(this.parseView).catch(this.handleError);
    }


    public getVigente(grupo:string, fecha:Date){
        fecha = fecha? fecha :new Date();
        return this.http.get(this.getApiURL() +"listaprecio/vigente/"+grupo + "/" + moment(fecha).format("YYYYMMDD") + "/").toPromise().then(this.parseResults).catch(this.handleError); 
    }

   /* public getAdicionales(): Promise<ListaPrecio[]>{
    
        return this.http.get(this.getApiURL() +"ListaPrecio/adicionales").toPromise().then(this.parseResults).catch(this.handleError);
    }*/
    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"listaprecio/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar( parametrico: ListaPrecio): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + "listaprecio/", parametrico).toPromise().then(this.parseView, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "listaprecio/",  parametrico).toPromise().then(this.parseView, this.handleError);
        }
    }
    public parseView(r): ListaPrecio{
        let res = r.respuesta;
        let parametricos: ListaPrecio;
        if (res) {
            parametricos = ListaPrecio.fromData(res);
        }
        return parametricos;
    }
    public parseResults(data : any): ListaPrecio[]{
        let res = data.respuesta;
        let parametricos: ListaPrecio[] = [];
        if (res) {
            parametricos = res.map(p => ListaPrecio.fromData(p) );
        }
        return parametricos;
    }

}
import { Marca } from '../../../model/Marca';
import { ParametricoService } from '../../services/parametricos.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MarcaService } from '../service/marca.service';


@Component({
	selector: 'gestion-marca',
	templateUrl: 'gestion-marca.component.html',
	styleUrls: ['gestion-marca.component.less']
})

export class GestionMarcaComponent extends SessionComponent implements OnInit {

	public titulo: string = "";

	private _item: Marca = new Marca();
	public finalizado: boolean = false;
	
	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : Marca){
		this._item = val;
		this.imagenVisible = val.imagenUrl;
	}

	get item(): Marca{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<Marca> = new EventEmitter<Marca>();

	@Output()
	public onCancelar : EventEmitter<Marca> = new EventEmitter<Marca>();

	@Output()
	public onGuardar : EventEmitter<Marca> = new EventEmitter<Marca>();

	public imagenVisible : string;

	constructor(
		private service: MarcaService,
		) {
		super();
		
  }

  ngOnInit() {
    let $this = this;
		this.finalizado = false;
		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
	
			this.service.guardar(this.item).then((c) => {
				$this.success("La Marca fue guardada correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("La Marca no es valida");
		}
	}
	

	public itemValido(item: Marca): boolean {
		return true;
	}
	
	public readURL(event : any){
		if (event.target.files && event.target.files[0]) {
			const file = event.target.files[0];
			const reader = new FileReader();
			this.item.file = file;
			reader.onload = e => {this.imagenVisible = reader.result.toString();};

			reader.readAsDataURL(file);
	}
	}


}

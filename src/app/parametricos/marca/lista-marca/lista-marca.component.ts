import { MarcaService } from '../service/marca.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Marca } from '../../../model/Marca';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'lista-marca',
	templateUrl: 'lista-marca.component.html',
	styleUrls:["lista-marca.component.less"]
})

export class ListaMarcaComponent extends SessionComponent implements OnInit {

	public items: Marca[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : Marca;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Marca";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: MarcaService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : Marca) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar la marca seleccionada?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : Marca) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = Marca.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : Marca){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: Marca){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: Marca) {
		this.itemEditado = Marca.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new Marca();
		this.editandoItem = true;
		
	}
	
}
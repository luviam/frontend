import { Marca } from '../../../model/Marca';

import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';

@Injectable()
export class MarcaService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<Marca[]>{
    
        return this.http.get(this.getApiURL() +"marcas/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public getComisionables(): Promise<Marca[]>{
    
        return this.http.get(this.getApiURL() +"marcas/comisionables").toPromise().then(this.parseResults).catch(this.handleError);
    }
    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"marcas/", {params}).toPromise().then(this.handleOk, this.handleError);
    }


    public guardar( marca: Marca): Promise<any> {
        const frmData = new FormData();
    
        if(marca.file){
            frmData.append("logo",marca.file);
        }
        frmData.append("marca",JSON.stringify(this.omitKeys(marca,['_file','file','value','label','sinCaracteresEspeciales','descriptivo','imagenUrl','hayCambio'])));
       
        if (marca.id) {
            return this.http.post(this.getApiURL() + "marcas/", frmData).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "marcas/",  frmData).toPromise().then(this.handleOk, this.handleError);
        }
    }

    public parseResults(data : any): Marca[]{
        let res = data.respuesta;
        let parametricos: Marca[] = [];
        if (res) {
            parametricos = res.map(p => Marca.fromData(p) );
        }
        return parametricos;
    }

}
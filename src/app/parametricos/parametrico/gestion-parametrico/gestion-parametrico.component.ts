import { Parametrico } from '../../../model/Parametrico';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
	selector: 'gestion-parametrico',
	templateUrl: 'gestion-parametrico.component.html',
	styleUrls: ['gestion-parametrico.component.less']
})

export class GestionParametricoComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	

	private _item: Parametrico = new Parametrico();
	public finalizado: boolean = false;
	
	@Input()
	public tipoParametro :string;

	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : Parametrico){
		this._item = val;
	}

	get item(): Parametrico{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<Parametrico> = new EventEmitter<Parametrico>();

	@Output()
	public onCancelar : EventEmitter<Parametrico> = new EventEmitter<Parametrico>();

	@Output()
	public onGuardar : EventEmitter<Parametrico> = new EventEmitter<Parametrico>();

	constructor(cookieService :CookieServiceHelper,
		private service: ParametricoService,
    private router: Router,
    private confirmationService: ConfirmationService,
		private route: ActivatedRoute		
		) {
		super();
		
  }

  ngOnInit() {
    let $this = this;
	this.finalizado = false;
		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(this.tipoParametro,this.item).then((c) => {
				$this.success("El parametrico fue guardada correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El Item no es valida");
		}
	}
	

	public itemValido(item: Parametrico): boolean {
		return true;
  }

}

import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Parametrico } from '../../../model/Parametrico';
import { Component, OnInit, Input } from '@angular/core';
import { ParametricoService } from '../../services/parametricos.service';


@Component({
	selector: 'lista-parametrico',
	templateUrl: 'lista-parametrico.component.html'
})

export class ListaParametricoComponent extends SessionComponent implements OnInit {

	public items: Parametrico[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	private _idParametro : string;
	
	@Input()
	public nombre : string ="Parametricos";

	@Input()
	set idParametro(val : string){
		this._idParametro = val
		if(this._idParametro){
			this.getItems();
		}
		
	};
	get idParametro(): string{
		return this._idParametro;
	}

	public editandoItem : boolean = false;
	public itemEditado : Parametrico;
	get titulo() : string{
	
		if(!this.itemEditado){
			return this.nombre;
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: ParametricoService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll(this.idParametro).then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : Parametrico) {
		this.confirmationService.confirm({
			key: 'cParam' + this.idParametro,
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(this.idParametro,item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : Parametrico) {
		this.confirmationService.confirm({
			key: 'cParam' + this.idParametro,
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = Parametrico.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(this.idParametro,itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : Parametrico){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: Parametrico){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: Parametrico) {
		this.itemEditado = Parametrico.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new Parametrico();
		this.editandoItem = true;
		
	}
	
}
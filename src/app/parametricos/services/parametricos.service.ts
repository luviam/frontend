import { Injectable } from '@angular/core';
import { Parametrico } from '../../model/Parametrico';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';

@Injectable()
export class ParametricoService extends ServicioAbstract{
    
  

    
    public getAll(tipoParam :string): Promise<Parametrico[]>{
    
        return this.http.get(this.getApiURL() +tipoParam+"/parametrico/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public delete(tipoParam :string, id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +tipoParam+"/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar(tipoParam : string, parametrico: Parametrico): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + tipoParam+"/", parametrico).toPromise().then(this.getResult, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + tipoParam+'/',  parametrico).toPromise().then(this.getResult, this.handleError);
        }
    }

    public parseResults(data : any): Parametrico[]{
        let res = data.respuesta;
        let parametricos: Parametrico[] = [];
        if (res) {
            parametricos = res.map(p => Parametrico.fromData(p) );
        }
        return parametricos;
    }

    public getResult(data: any){
        let res = data.respuesta;
        if (res) {
            return res;
        }
        return null;
    }

}
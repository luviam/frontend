import { TipoComisionistaService } from '../service/tipo-comisionista.service';
import { CategoriaService } from '../../categoria/service/categoria.service';
import { Categoria } from '../../../model/Categoria';
import { TipoComisionista } from '../../../model/TipoComisionista';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
	selector: 'gestion-tipo-comisionista',
	templateUrl: 'gestion-tipo-comisionista.component.html',
	styleUrls: ['gestion-tipo-comisionista.component.less']
})

export class GestionTipoComisionistaComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	

	private _item: TipoComisionista = new TipoComisionista();
	public finalizado: boolean = false;
	public categorias : Categoria[] = [];
	
	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : TipoComisionista){
		this._item = val;
		if(val){
			this.categorias = this.categorias.filter(c => !val.tieneCategoria(c));
		}
		
	}

	get item(): TipoComisionista{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<TipoComisionista> = new EventEmitter<TipoComisionista>();

	@Output()
	public onCancelar : EventEmitter<TipoComisionista> = new EventEmitter<TipoComisionista>();

	@Output()
	public onGuardar : EventEmitter<TipoComisionista> = new EventEmitter<TipoComisionista>();

	constructor(cookieService :CookieServiceHelper,
		private service: TipoComisionistaService,
		private categoriasService : CategoriaService
		) {
		super();
		
  }

  ngOnInit() {
	this.finalizado = false;
	let $this = this;

	this.addLoadingCount();
	this.categoriasService.getComisionables().then(r=>{
		$this.categorias = r;
		if($this.item){
			$this.categorias = $this.categorias.filter(c => !$this.item.tieneCategoria(c));
		}
		$this.susLoadingCount();
	}).catch($this.errorHandler);

		
  }

  public confirmarCancelar() {
		this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(this.item).then((c) => {
				$this.success("El tipo de Comisionista fue guardada correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("La Categoría no es valida");
		}
	}
	

	public itemValido(item: TipoComisionista): boolean {
		return true;
  }

}

import { TipoComisionistaService } from '../service/tipo-comisionista.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { TipoComisionista } from '../../../model/TipoComisionista';
import { Component, OnInit } from '@angular/core';
import { ParametricoService } from '../../services/parametricos.service';
import { Parametrico } from '../../../model/Parametrico';

@Component({
	selector: 'lista-tipo-comisionista',
	templateUrl: 'lista-tipo-comisionista.component.html'
})

export class ListaTipoComisionistaComponent extends SessionComponent implements OnInit {

	public items: TipoComisionista[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : TipoComisionista;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Categorías";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: TipoComisionistaService, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : TipoComisionista) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : TipoComisionista) {
		this.confirmationService.confirm({
			key: 'cConf',
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = TipoComisionista.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : TipoComisionista){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: TipoComisionista){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: TipoComisionista) {
		this.itemEditado = TipoComisionista.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new TipoComisionista();
		this.editandoItem = true;
		
	}
	
}
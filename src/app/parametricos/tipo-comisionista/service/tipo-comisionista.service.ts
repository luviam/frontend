import { TipoComisionista } from '../../../model/TipoComisionista';

import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';

@Injectable()
export class TipoComisionistaService extends ServicioAbstract{
    
  

    
    public getAll(): Promise<TipoComisionista[]>{
    
        return this.http.get(this.getApiURL() +"tipo-comisionistas/parametrico/all").toPromise().then(this.parseResults).catch(this.handleError);
    }

    public delete(id :number){
        let params = new HttpParams();
        params = params.append('id', id+"")
         return this.http.delete(this.getApiURL() +"tipo-comisionistas/", {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar( parametrico: TipoComisionista): Promise<any> {
        if (parametrico.id) {
            return this.http.post(this.getApiURL() + "tipo-comisionistas/", parametrico).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + "tipo-comisionistas/",  parametrico).toPromise().then(this.handleOk, this.handleError);
        }
    }

    public parseResults(data : any): TipoComisionista[]{
        let res = data.respuesta;
        let parametricos: TipoComisionista[] = [];
        if (res) {
            parametricos = res.map(p => TipoComisionista.fromData(p) );
        }
        return parametricos;
    }

}
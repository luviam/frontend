import { TipoEvento } from '../../../model/TipoEvento';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router, ActivatedRoute } from '@angular/router';
import { ParametricoService } from '../../services/parametricos.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
	selector: 'gestion-tipo-evento',
	templateUrl: 'gestion-tipo-evento.component.html',
	styleUrls: ['gestion-tipo-evento.component.less']
})

export class GestionTipoEventoComponent extends SessionComponent implements OnInit {

	public titulo: string = "";
	

	private _item: TipoEvento = new TipoEvento();
	public finalizado: boolean = false;
	
	@Input()
  	public editable: boolean = true;


	@Input()
	set item(val : TipoEvento){
		this._item = val;
	}

	get item(): TipoEvento{
		return this._item;
	}

	@Output()
	public itemChange : EventEmitter<TipoEvento> = new EventEmitter<TipoEvento>();

	@Output()
	public onCancelar : EventEmitter<TipoEvento> = new EventEmitter<TipoEvento>();

	@Output()
	public onGuardar : EventEmitter<TipoEvento> = new EventEmitter<TipoEvento>();

	constructor(cookieService :CookieServiceHelper,
		private service: ParametricoService,
    private router: Router,
    private confirmationService: ConfirmationService,
		private route: ActivatedRoute		
		) {
		super();
		
  }

  ngOnInit() {
    let $this = this;
	this.finalizado = false;
		
  }

  public confirmarCancelar() {
	this.onCancelar.emit(this.item);
  }
  
  public guardarItem() {
		let $this = this;
		if (this.itemValido(this.item)) {
			this.addLoadingCount();
			
			this.service.guardar(TipoEvento.ID,this.item).then((c) => {
				$this.success("El Tipo de Evento fue guardado correctamente");
				$this.finalizado = true;
				this.onGuardar.emit(this.item);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El Tipo de Evento no es valido");
		}
	}
	

	public itemValido(item: TipoEvento): boolean {
		return true;
  }

}

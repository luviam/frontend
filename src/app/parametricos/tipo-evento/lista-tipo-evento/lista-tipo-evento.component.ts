import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { Router } from '@angular/router';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { TipoEvento } from '../../../model/TipoEvento';
import { Component, OnInit } from '@angular/core';
import { ParametricoService } from '../../services/parametricos.service';

@Component({
	selector: 'lista-tipo-evento',
	templateUrl: 'lista-tipo-evento.component.html'
})

export class ListaTipoEventoComponent extends SessionComponent implements OnInit {

	public items: TipoEvento[];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	
	public editandoItem : boolean = false;
	public itemEditado : TipoEvento;
	get titulo() : string{
	
		if(!this.itemEditado){
			return "Tipos de Evento";
		}else{
			let tt : string = "";
			if(this.itemEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo ";
			}
			tt += this.itemEditado.codigo?this.itemEditado.codigo :"";
			tt += this.itemEditado.descripcion?  " - " + this.itemEditado.descripcion : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: ParametricoService, 
		private router: Router, 
		private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getItems();
		
	}

	public getItems(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll(TipoEvento.ID).then(items => {
			$this.items = [...items];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(item : TipoEvento) {
		this.confirmationService.confirm({
			key: "teConf",
			header: "Deshabilitar",
			message: 'Desea deshabilitar el item seleccionado?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(TipoEvento.ID,item.id).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(item : TipoEvento) {
		this.confirmationService.confirm({
			key: "teConf",
			header: "Habilitación de item",
			message: 'Desea habilitar el item?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				let itemCambio = TipoEvento.fromData(item);
				itemCambio.habilitado = true;
				this.service.guardar(TipoEvento.ID,itemCambio).then((res) => {
					$this.getItems();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionItem(item : TipoEvento){
		this.editandoItem = false;
		this.itemEditado = null;
	}
	public itemGuardado(item: TipoEvento){
		this.editandoItem = false;
		this.itemEditado = null;
		this.getItems();
		
	}
	public editarItem(item: TipoEvento) {
		this.itemEditado = TipoEvento.fromData(item);
		this.editandoItem = true;
		
	}

	public nuevoItem() {
		this.itemEditado = new TipoEvento();
		this.editandoItem = true;
		
	}
	
}
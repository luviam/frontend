import { Pipe, PipeTransform } from '@angular/core';
import { FiltroPrecio } from './../precios/model/FiltroPercio';
import { PrecioAlquiler } from './../precios/model/PrecioAlquiler';
@Pipe({ name: 'preciosFiltrados' })
export class PrecioFiltradoPipe implements PipeTransform {
    transform(precios: PrecioAlquiler[], filtro: FiltroPrecio) {
        return precios ? precios.filter(p => filtro.aplica(p)) : [];
    }
}
import { Producto } from '../model/Producto';
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'productoCategoria' })
export class ProductoCategoriaPipe implements PipeTransform {
  transform(productos: Producto[], codigoCategoria:string) {
    return productos.filter(p => p.esCategoria(codigoCategoria));
  }
}
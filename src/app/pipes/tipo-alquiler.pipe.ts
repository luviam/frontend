import { Pipe, PipeTransform } from '@angular/core';
import { PrecioAlquiler } from './../precios/model/PrecioAlquiler';


@Pipe({ name: 'tipoAlquiler' })
export class TipoAlquilerPipe implements PipeTransform {
  transform(productos: PrecioAlquiler[], tipo: string) {
    return productos.filter(p => p.tipoPrecio === tipo);
  }
}
import { ListaPrecioService } from './../../../parametricos/lista-precios/service/lista-precios.service';
import { PrecioAdicionales } from './../../model/PrecioAdicionales';
import { ConflictoComponent } from "app/precios/conflicto/conflicto.component";
import { OnInit, Component } from '@angular/core';
import { PreciosAdicionalesService } from 'app/precios/precios-adicionales.service';

@Component({
	selector: 'conflicto-adicionales',
	templateUrl: '../../conflicto/conflicto.component.html',
	styleUrls: ['../../conflicto/conflicto.component.less']
})
export class ConflictosAdicionalesComponent extends ConflictoComponent<PrecioAdicionales> implements OnInit{
    constructor(  listaPrecioService: ListaPrecioService,
		protected pService : PreciosAdicionalesService) {
		super(listaPrecioService,
			pService);
	}
    ngOnInit(){
        super.ngOnInit();
    }
}
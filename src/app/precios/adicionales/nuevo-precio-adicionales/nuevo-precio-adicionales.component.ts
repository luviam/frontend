import { PreciosService } from './../../precios.service';
import { ListaPrecio } from './../../../model/ListaPrecio';
import { GrupoProducto } from 'app/model/GrupoProducto';
import { ListaPrecioService } from '../../../parametricos/lista-precios/service/lista-precios.service';
import { FiltroPrecio } from '../../model/FiltroPercio';
import { ConflictoPrecioAdicionales } from '../../model/ConflictoPrecioAdicionales';
import { PrecioAdicionales } from '../../model/PrecioAdicionales';
import { Categoria } from '../../../model/Categoria';
import { Producto } from '../../../model/Producto';
import { ProductoService } from '../../../producto/service/producto.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Descriptivo } from 'app/model/Descriptivo';
import * as moment from 'moment';
import { PreciosAdicionalesService } from 'app/precios/precios-adicionales.service';
import {  NuevoPrecioComponent } from 'app/precios/nuevo-precio.component';
@Component({
	selector: 'nuevo-precio-adicionales',
	templateUrl: 'nuevo-precio-adicionales.component.html',
	styleUrls: ["nuevo-precio-adicionales.component.less", "./../../precios.less"]
})

export class NuevoPrecioAdicionalesComponent extends NuevoPrecioComponent<PrecioAdicionales> implements OnInit{
	
	constructor(productoService: ProductoService,
		protected preciosService : PreciosAdicionalesService,
		 listaPreciosService : ListaPrecioService) {
		super(productoService,listaPreciosService);
	}
	ngOnInit() {
		super.ngOnInit();
	}


	public getGrupo(): Descriptivo {
		return new Descriptivo(GrupoProducto.ADICIONALES_E,"");
	}
	public generarPrecios() {
		let $this = this;
		if(this.esValido()){
			let preciosNuevos : PrecioAdicionales[] = [];
			this.addLoadingCount();
			let diasSemana = [];
			this.diasSeleccionados.forEach(d =>{
				diasSemana.push(new Number(d));
			})
			this.productosSeleccionados.forEach(p => {
				this.tListadoSeleccionados.forEach(s =>{
					let nuevoPrecio : PrecioAdicionales = new PrecioAdicionales(null,
						p.producto,p.base,this.fechaDesde,this.fechaHasta,new Date(),
						true,this.getCurrentUser().user.descriptivo,
						diasSemana,this.getTipoListado(s)
					);
					preciosNuevos.push(nuevoPrecio);
				})
			});
			this.preciosService.guardarPrecios(preciosNuevos).then((r:ConflictoPrecioAdicionales[])=>{
				if(r.some(c => c.conflictos.length>0)){
					$this.conflictos = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
				}else{
					$this.success("Precios Guardados");
					$this.onGuardado.emit(preciosNuevos);
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		

		}
	}
/*
	@Input()
	public  set productos(p : Producto[]){
		this.productosBase = p.map(a=> new ProductoBase(a));
	};

	public productosBase: ProductoBase[] = [];
	@Input()
	public tListados: ListaPrecio[] = [];

	public mostrarProductos = false;

	@Input()
	public grupo : GrupoProducto;
	private precios : PrecioAdicionales[] = []
	public verConflictos: boolean = false;
	public conflictos : ConflictoPrecioAdicionales[] = [];

	@Output()
	public onCancelar: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	public onGuardado: EventEmitter<PrecioAdicionales[]> = new EventEmitter<PrecioAdicionales[]>();

	public diasSeleccionados: string[] = [];
	@Input()
	public tListadoSeleccionados: String[] = [];
	public precio: PrecioAdicionales = new PrecioAdicionales();
	
	public productosSeleccionados: ProductoBase[] = [];
	public productosMarcados: ProductoBase[] = [];
	public filtro: FiltroPrecio= new FiltroPrecio();
	constructor(private productoService: ProductoService,
		private preciosService : PreciosAdicionalesService,
		private listaPreciosService : ListaPrecioService) {
		super();
	}
	ngOnInit() {
		let $this = this;
		if (this.productosBase.length === 0 && this.grupo) {
			this.addLoadingCount();
			this.productoService.getByGrupo(this.grupo.codigo).then((r) => {
				$this.productosBase = r.map(p => new ProductoBase(p));
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
		if (this.tListados.length === 0 && this.grupo) {
			this.addLoadingCount();
			this.listaPreciosService.getAllByGrupo(this.grupo.codigo).then((r) => {
				$this.tListados = r;
				$this.susLoadingCount();
			}).catch(this.errorHandler)
		}
		this.todosDias = true;
	}
	set todosDias(val: boolean){
		if(val){
			this.diasSeleccionados = this.locale["es"].dayNames.map((e,i) => {
				return (i+1) + "";
			});;	
		}else{
			this.diasSeleccionados = [];
		}
		
	};

	get todosDias():boolean{
		return this.diasSeleccionados.length === 7;
	}
	set todosListados(val: boolean) {
		if (val) {
			this.tListadoSeleccionados = this.tListados.map(l => l.codigo);
		} else {
			this.tListadoSeleccionados = [];
		}

	};
	get todosListados() : boolean{
		return this.tListadoSeleccionados.length == this.tListados.length;
	}
	public getCategoria(): string {
		return this.grupo.codigo;
	}
	public updateSeleccionados(){
		this.productosSeleccionados = [...this.productosSeleccionados];
		this.productosMarcados = [];
	}
	public generarPrecios() {
		let $this = this;
		if(this.esValido()){
			let preciosNuevos : PrecioAdicionales[] = [];
			this.addLoadingCount();
			let diasSemana = [];
			this.diasSeleccionados.forEach(d =>{
				diasSemana.push(new Number(d));
			})
			this.productosSeleccionados.forEach(p => {
				this.tListadoSeleccionados.forEach(s =>{
					let nuevoPrecio : PrecioAdicionales = new PrecioAdicionales(null,
						p.producto,p.base,this.precio.fechaDesde,this.precio.fechaHasta,new Date(),
						true,this.getCurrentUser().user.descriptivo,
						diasSemana,this.tListados.filter(l => l.codigo === s)[0]
					);
					preciosNuevos.push(nuevoPrecio);
				})
			});
			this.preciosService.guardarPrecios(preciosNuevos).then((r:ConflictoPrecioAdicionales[])=>{
				if(r.some(c => c.conflictos.length>0)){
					$this.conflictos = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
				}else{
					$this.success("Precios Guardados");
					$this.onGuardado.emit(preciosNuevos);
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		

		}
	}
	public eliminarItem(item:ProductoBase){
		this.eliminarItems([item]);
	}
	public eliminarItems(items :ProductoBase[]){
		this.productosSeleccionados = this.productosSeleccionados.filter(i => items.indexOf(i) < 0 );
		
	}
	public cancelarConflictos(){
		this.verConflictos = false;
	}
	public finConflictos(precios : PrecioAdicionales[]){
		this.verConflictos = false;
		if(precios.length >0){
			this.success("Precios Guardados");
			this.onGuardado.emit(precios);
		}
	}

	public esValido(): boolean{

		let estado : boolean = true;
		if(this.productosSeleccionados.length == 0){
			this.error("Seleccione al menos 1 producto");
			estado = false;
		}
		if(this.tListadoSeleccionados.length == 0){
			this.error("Seleccione al menos 1 Tipo de Listado");
			estado = false;
		}
		if(this.diasSeleccionados.length == 0){
			this.error("Seleccione al menos 1 día");
			estado = false;
		}
		if(this.precio.valor == undefined || this.precio.valor < 0){
			this.error("El precio debe ser mayor a 0");
			estado = false;
		}
		if(this.precio.fechaDesde == undefined ){
			this.error("Indique una fecha Desde");
			estado = false;
		}else if(this.precio.fechaHasta != undefined && moment(this.precio.fechaHasta).isBefore(this.precio.fechaDesde)){
			this.error("La fecha Hasta debe ser mayor a Desde");
			estado = false;
		}
		return estado;
	}
	public cancelar() {
		this.reset();
		this.onCancelar.emit(true);
	}
	public reset(){
		this.precio = new PrecioAdicionales();
		this.productosSeleccionados = [];
		this.diasSeleccionados = [];
		this.tListadoSeleccionados = [];
	}

	public agregarProductos(){
		this.mostrarProductos = true;
	}*/
}

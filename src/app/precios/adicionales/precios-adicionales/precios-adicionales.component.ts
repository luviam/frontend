import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { PreciosService } from './../../precios.service';
import { ActivatedRoute } from '@angular/router';
import { GrupoProductoService } from './../../../parametricos/grupo-producto/service/grupo-producto.service';
import { GrupoProducto } from '../../../model/GrupoProducto';
import { ListaPrecioService } from '../../../parametricos/lista-precios/service/lista-precios.service';
import { ListaPrecio } from '../../../model/ListaPrecio';
import { ParametricoService } from '../../../parametricos/services/parametricos.service';

import { MarcaService } from '../../../parametricos/marca/service/marca.service';
import { Marca } from '../../../model/Marca';
import { FiltroPrecio } from '../../model/FiltroPercio';
import { DataTable } from 'primeng/primeng';
import { Categoria } from '../../../model/Categoria';
import { Descriptivo } from '../../../model/Descriptivo';
import { Producto } from 'app/model/Producto';
import { ProductoService } from '../../../producto/service/producto.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PrecioAdicionales } from '../../model/PrecioAdicionales';
import * as moment from "moment";
import { PreciosAdicionalesService } from 'app/precios/precios-adicionales.service';
import { ListadoPreciosComponent } from 'app/precios/listado-precios.component';

@Component({
	selector: 'precios-adicionales',
	templateUrl: 'precios-adicionales.component.html',
	styleUrls: ['precios-adicionales.component.less']
})

export class PreciosAdicionalesComponent extends ListadoPreciosComponent<PrecioAdicionales> implements OnInit {

	ngOnInit() {
		super.ngOnInit();
	 }
	 public nombre : string ="Precios - Adicionales";
	constructor(
		preciosService: PreciosAdicionalesService,
		 productoService: ProductoService,
		 marcasService: MarcaService,
		 listaPreciosService: ListaPrecioService,
		 route : ActivatedRoute,
		 confirmationService: ConfirmationService,
		 grupoService: GrupoProductoService,){
		super(	 preciosService,
			 productoService,
			 marcasService,
			 listaPreciosService,
			 route,
			 confirmationService,
			 grupoService,);
	}



}
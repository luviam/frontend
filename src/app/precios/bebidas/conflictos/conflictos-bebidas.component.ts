import { PreciosBebidasService } from './../../precios-bebidas.service';
import { PreciosService } from 'app/precios/precios.service';
import { ListaPrecioService } from '../../../parametricos/lista-precios/service/lista-precios.service';
import { PrecioAdicionales } from '../../model/PrecioAdicionales';
import { ConflictoComponent } from "app/precios/conflicto/conflicto.component";
import { OnInit, Component } from '@angular/core';
import { PrecioBebida } from 'app/precios/model/PrecioBebida';

@Component({
	selector: 'conflicto-bebidas',
	templateUrl: '../../conflicto/conflicto.component.html',
	styleUrls: ['../../conflicto/conflicto.component.less']
})
export class ConflictosBebidasComponent extends ConflictoComponent<PrecioBebida> implements OnInit{
	constructor(  listaPrecioService: ListaPrecioService,
		protected pService : PreciosBebidasService) {
		super(listaPrecioService,
			pService);
	}
    ngOnInit(){
        super.ngOnInit();
    }
}
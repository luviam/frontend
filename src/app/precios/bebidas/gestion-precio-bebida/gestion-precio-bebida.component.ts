import { GrupoProducto } from 'app/model/GrupoProducto';
import { ListaPrecioService } from '../../../parametricos/lista-precios/service/lista-precios.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ConflictoPrecioBebida } from '../../model/ConflictoPrecioBebida';
import { PreciosBebidasService } from '../../precios-bebidas.service';
import { PrecioBebida } from '../../model/PrecioBebida';
import { SessionComponent } from '../../../session-component.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Descriptivo } from 'app/model/Descriptivo';
import { SelectItem } from 'primeng/primeng';
import * as moment from 'moment';

@Component({
	selector: 'gestion-precio-bebida',
	templateUrl: 'gestion-precio-bebida.component.html',
	styleUrls: ['gestion-precio-bebida.component.less','../../precios.less']
})

export class GestionPrecioBebidaComponent extends SessionComponent {

	public static ADD_O = "add";
	public static PRC_O = "prc";
	public static RPL_O = "rpl";
	public fechaHasta: Date;
	public fechaDesde: Date;
	public verConflictos: boolean = false;
	public _precios: PrecioBebida[] = [];
	public _preciosOriginales: PrecioBebida[] = [];
	public precioEditado: PrecioBebida;
	public filteredProductos: Descriptivo[] = [];
	public diasSeleccionados: string[] = [];
	public conflictos: ConflictoPrecioBebida[] = [];
	public precioGlobal: string = "";
	public operaciones: SelectItem[] = [{ value: GestionPrecioBebidaComponent.ADD_O, icon: "fa fa-plus" },
	{ value: GestionPrecioBebidaComponent.RPL_O, icon: "fa fa-pencil" },
	{ value: GestionPrecioBebidaComponent.PRC_O, icon: "fa fa-percent" }];
	public operacionSeleccionada: string = GestionPrecioBebidaComponent.ADD_O;


	@Output()
	public onCancelar: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	public onGuardado: EventEmitter<PrecioBebida[]> = new EventEmitter<PrecioBebida[]>();




	@Input()
	public editable: boolean = true;

	public esNuevoListado: boolean = false;

	@Input()
	public tiposListado: Descriptivo[] = [];

	@Input()
	set precios(val: PrecioBebida[]) {
		if (val) {
			this._preciosOriginales = val;
			this._precios = val.map(p => PrecioBebida.fromData(p));
			this.esNuevoListado = val.every(p => !p.id);
		}

		if (val && val.length > 0) {
			this.precioEditado = this.precioEnComun(this._precios);
		} else {
			this.precioEditado = new PrecioBebida();
		}

	}

	get precios(): PrecioBebida[] {
		return this._precios;
	}


	constructor(
		private preciosService: PreciosBebidasService,
		private confirmationService: ConfirmationService,
		private listaPreciosService : ListaPrecioService) {
		super('gestion-precio');
	}
	ngOnInit() {

		let $this = this;
		this.precioEditado = new PrecioBebida();
		if (this.tiposListado.length == 0) {
			this.addLoadingCount();
			this.listaPreciosService.getAllByGrupo(GrupoProducto.BEBIDA.codigo).then((r) => {
				$this.tiposListado = r;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}




	}

	public precioEnComun(val: PrecioBebida[]): PrecioBebida {
		let prec: PrecioBebida = new PrecioBebida();
		prec.id = val.some((p, i) => i > 0 && val[0].id !== p.id) ? null : val[0].id;
		prec.valor = val.some((p, i) => i > 0 && val[0].valor !== p.valor) ? null : val[0].valor;
		prec.fechaDesde = val.some((p, i) => i > 0 && val[0].fechaDesde !== p.fechaDesde) ? null : val[0].fechaDesde;
		prec.fechaHasta = val.some((p, i) => i > 0 && val[0].fechaHasta !== p.fechaHasta) ? null : val[0].fechaHasta;
		prec.diasSemana = val.some((p, i) => i > 0 && val[0].diasSemana !== p.diasSemana) ? null : val[0].diasSemana;
		prec.salon = val.some((p, i) => i > 0 && val[0].salon !== p.salon) ? null : val[0].salon;
		this.diasSeleccionados = prec.diasSemana ? prec.diasSemana.map(d => d + "") : [];
		return prec;
	}

	public cancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: "genConf",
			header: "Deshacer Cambios",
			message: 'Todos los cambios realizados hasta el momento se perderán. ¿Desea continuar?',
			accept: () => {
				$this.onCancelar.emit(true);
				
			}
		});
		
	}
	public guardarPrecios() {
		let $this = this;
		$this.addLoadingCount();
		if (this.precios.length === 0) {

		} else {
			this._precios.forEach((p, i) => {
				this._preciosOriginales[i].diasSemana = this.locale.es.dayNamesMin;
				this._preciosOriginales[i].listaPrecio = p.listaPrecio;
				this._preciosOriginales[i].valor = p.valor;
				this._preciosOriginales[i].fechaDesde = p.fechaDesde;
				this._preciosOriginales[i].fechaHasta = p.fechaHasta;
				this._preciosOriginales[i].fechaAlta = new Date();
			})
			this.preciosService.guardarPrecios($this.precios).then((r: ConflictoPrecioBebida[]) => {
				if (r.some(c => c.conflictos.length > 0)) {
					$this.conflictos = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
				} else {
					$this.reset();
					$this.success("Precios actualizados");
					$this.onGuardado.emit($this.precios);
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
	}

	public cancelarConflictos() {
		this.verConflictos = false;
	}
	public finConflictos(precios: PrecioBebida[]) {
		this.verConflictos = false;
		if (precios.length > 0) {
			this.success("Precios Guardados");
			this.onGuardado.emit(precios);
		}
	}
	public reset() {
		this.precios = [];
		this.diasSeleccionados = [];
		this.fechaDesde = null;
		this.fechaHasta = null;

	}
	public tieneDia(i: number): boolean {
		return this.diasSeleccionados.indexOf(i + "") >= 0;
	}
	public toggleDia(i: number) {
		let index = this.diasSeleccionados.indexOf(i + "");
		if (index >= 0) {
			this.diasSeleccionados.splice(index, 1);
			this.precios.forEach(p => p.quitarDia(i));
		} else {
			this.diasSeleccionados.push(i + "");
			this.precios.forEach(p => p.seleccionarDia(i));
		}
	}

	public actualizarPrecioGlobal(val: number) {
		this.precios.forEach(p => {
			switch (this.operacionSeleccionada) {
				case GestionPrecioBebidaComponent.PRC_O:
					p.valor = p.valor * (1 + (val / 100));
					break;
				case GestionPrecioBebidaComponent.ADD_O:
					p.valor = p.valor + val;
					break;
				case GestionPrecioBebidaComponent.RPL_O:
					p.valor = val;
					break;
				default:
					break;
			}
			p.valor = Math.round(p.valor * 100) / 100;
		})
		this.precioGlobal = "";
		this.success("Precios actualizados");
	}

	public actualizarFechas() {
		let $this = this;

		if (!this.fechaDesde ){
			this.error("No puede usar fecha desde vacía")
		}else{
			this.confirmationService.confirm({
				key: "genConf",
				header: "Reemplazar Fechas",
				message: 'Todos los precios tendrán las fechas que indicó. ¿Desea continuar?',
				accept: () => {
					$this._precios.forEach(p =>{
						p.fechaDesde = $this.fechaDesde;
						p.fechaHasta = $this.fechaHasta;
					});
					
				}
			});
		}
	}

	public limpiar(){
		let $this = this;
		this.confirmationService.confirm({
			key: "genConf",
			header: "Deshacer Cambios",
			message: 'Todos los cambios realizados hasta el momento se perderán. ¿Desea continuar?',
			accept: () => {
				$this.addLoadingCount();
				$this._precios = $this._preciosOriginales.map(p => PrecioBebida.fromData(p));
				$this.susLoadingCount();
				
			}
		});
	}

	

}
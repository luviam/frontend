import { AgrupadorMarca } from '../../model/AgrupadoresDef';

import { Agrupador } from '../../../model/Agrupador';
import { Producto } from 'app/model/Producto';
import { Marca } from '../../../model/Marca';
import { PrecioBebida } from '../../model/PrecioBebida';
import { Component, OnInit, Input } from '@angular/core';
import { SessionComponent } from 'app/session-component.component';
import { Precio } from '../../model/Precio';
import { Subproducto } from 'app/model/Subproducto';

@Component({
	selector: 'impresion-lista-bebidas',
	templateUrl: 'impresion-lista-bebidas.component.html',
	styleUrls: ['impresion-lista-bebidas.component.less']
})

export class ImpresionListaBebidasComponent extends SessionComponent implements OnInit {

	public tipoBarras: string[] = [Producto.BARRA_NACIONAL, Producto.BARRA_PREMIUM, Producto.BARRA_SIN_ALCOHOL];
	public titulo : string ="";
	@Input()
	public set precios(val: PrecioBebida[]) {
		if (val && val.length > 0) {
			this._precios = val;
			this.agruparPrecios(val);
			let base = this._precios.filter(p => p.listaPrecio)[0];
			this.titulo = base? base.listaPrecio.descripcion : "Lista de Precios";
		} else {
			this._precios = [];
		}
	}

	public get precios(): PrecioBebida[] {
		return this._precios;
	}
	public barras: Barra[] = [];
	
	public marcas : AgrupadorMarca[] = [];
	private _precios: PrecioBebida[] = [];
	constructor() {
		super('vista-impresion')
	}
	ngOnInit() { }

	private agruparPrecios(val: PrecioBebida[]) {
		let marcas :Map<string, AgrupadorMarca> = new Map();
		marcas  = new Map();
		val.filter(p =>!this.tipoBarras.some(s => s == p.producto.codigo)).forEach(p => {
			
			if(!marcas.has(p.getCodigoMarca())){
				marcas.set(p.getCodigoMarca(),new AgrupadorMarca(p.getMarca()))
			}
			let agrupadoMarca = marcas.get(p.getCodigoMarca());
			agrupadoMarca.add(p);

			
		});
		let barraNac = new Barra(val.filter(p => p.producto.codigo === Producto.BARRA_NACIONAL)[0]);
		if (barraNac && barraNac.precio) {
			this.barras.push(barraNac);
		}
		let premium = new Barra(val.filter(p => p.producto.codigo === Producto.BARRA_PREMIUM)[0]);
		if (premium && premium.precio) {			
			this.barras.push(premium);
		}

		let sinAlcohol = new Barra(val.filter(p => p.producto.codigo === Producto.BARRA_SIN_ALCOHOL)[0]);
		if (sinAlcohol && sinAlcohol.precio) {
			
			this.barras.push(sinAlcohol);
		}
		
		
		this.marcas = Array.from(marcas.values());
		
		this.marcas = this.marcas.sort((a,b)=> {
			if(a.marca && b.marca){
				return a.marca.peso - b.marca.peso
			}else{
				return a.marca? -1 : 1;
			}
		});
	}

	getKeys(map){
		return Array.from(map.keys());
	}

}

export class Barra {
	public precio: PrecioBebida;
	public incluidos: Subproducto[] = [];
	public notas: string;
	constructor(precio: PrecioBebida) {
		this.precio = precio;
		if(precio && precio.producto){
			this.incluidos = precio.producto.subproductos? precio.producto.subproductos : []
			this.notas = precio.producto.notas;
		}
		
	}
	get valor() {
		return this.precio ? this.precio.valor : 0;
	}
	get descripcion() {
		return (this.precio && this.precio.producto) ? this.precio.producto.descripcion : "SIN PRODUCTO";
	}
}


import { NuevoPrecioComponent } from 'app/precios/nuevo-precio.component';
import { GrupoProducto } from 'app/model/GrupoProducto';
import { ListaPrecioService } from '../../../parametricos/lista-precios/service/lista-precios.service';
import { FiltroPrecio } from '../../model/FiltroPercio';
import { ConflictoPrecioBebida } from '../../model/ConflictoPrecioBebida';
import { PreciosBebidasService } from '../../precios-bebidas.service';
import { PrecioBebida } from '../../model/PrecioBebida';
import { Categoria } from '../../../model/Categoria';
import { Producto } from '../../../model/Producto';
import { ProductoService } from '../../../producto/service/producto.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Descriptivo } from 'app/model/Descriptivo';
import * as moment from 'moment';
import { PrecioAdicionales } from '../../model/PrecioAdicionales';
@Component({
	selector: 'nuevo-precio-bebida',
	templateUrl: 'nuevo-precio-bebida.component.html',
	styleUrls: ["nuevo-precio-bebida.component.less", "../../precios.less"]
})

export class NuevoPrecioBebidaComponent extends NuevoPrecioComponent<PrecioAdicionales> implements OnInit {

	constructor(productoService: ProductoService,
		protected preciosService : PreciosBebidasService,
		 listaPreciosService : ListaPrecioService) {
		super(productoService,listaPreciosService);
	}

	ngOnInit() {
		super.ngOnInit();
	}
	public generarPrecios() {
		let $this = this;
		if(this.esValido()){
			let preciosNuevos : PrecioBebida[] = [];
			this.addLoadingCount();
			let diasSemana = [];
			this.diasSeleccionados.forEach(d =>{
				diasSemana.push(new Number(d));
			})
			this.productosSeleccionados.forEach(p => {
				this.tListadoSeleccionados.forEach(s =>{
					let nuevoPrecio : PrecioBebida = new PrecioBebida(null,
						p.producto,p.base,this.fechaDesde,this.fechaHasta,new Date(),
						true,this.getCurrentUser().user.descriptivo,
						diasSemana, this.getTipoListado(s)
					);
					preciosNuevos.push(nuevoPrecio);
				})
			});
			this.preciosService.guardarPrecios(preciosNuevos).then((r:ConflictoPrecioBebida[])=>{
				if(r.some(c => c.conflictos.length>0)){
					$this.conflictos = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
				}else{
					$this.success("Precios Guardados");
					$this.onGuardado.emit(preciosNuevos);
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		

		}
	}

/*
	@Input()
	public productos: Producto[] = [];
	@Input()
	public tListados: Descriptivo[] = [];
	private precios : PrecioBebida[] = []
	public verConflictos: boolean = false;

	public conflictos : ConflictoPrecioBebida[] = [];

	@Output()
	public onCancelar: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	public onGuardado: EventEmitter<PrecioBebida[]> = new EventEmitter<PrecioBebida[]>();

	public diasSeleccionados: string[] = [];
	public tListadoSeleccionados: Descriptivo[] = [];
	public precio: PrecioBebida = new PrecioBebida();
	public productosSeleccionados: Producto[] = [];
	public filtro: FiltroPrecio= new FiltroPrecio();
	constructor(private productoService: ProductoService,
		private preciosService : PreciosBebidasService,
		private listaPreciosBebidasService : ListaPrecioService) {
		super();
	}
	ngOnInit() {
		let $this = this;
		if (this.productos.length === 0) {
			this.addLoadingCount();
			this.productoService.getByCategoria(this.getCategoria()).then((r) => {
				$this.productos = r;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
		if (this.tListados.length === 0) {
			this.addLoadingCount();
			this.listaPreciosBebidasService.getAllByGrupo(GrupoProducto.BEBIDA.codigo).then((r) => {
				$this.tListados = r;
				$this.susLoadingCount();
			}).catch(this.errorHandler)
		}
		this.todosDias = true;
	}
	set todosDias(val: boolean){
		if(val){
			this.diasSeleccionados = this.locale["es"].dayNames.map((e,i) => {
				return (i+1) + "";
			});;	
		}else{
			this.diasSeleccionados = [];
		}
		
	};

	get todosDias():boolean{
		return this.diasSeleccionados.length === 7;
	}
	set todosListados(val: boolean) {
		if (val) {
			this.tListadoSeleccionados = this.tListados;
		} else {
			this.tListadoSeleccionados = [];
		}

	};
	get todosListados() : boolean{
		return this.tListadoSeleccionados.length == this.tListados.length;
	}
	public getCategoria(): string {
		return Categoria.BEBIDA;
	}

	public generarPrecios() {
		let $this = this;
		if(this.esValido()){
			let preciosNuevos : PrecioBebida[] = [];
			this.addLoadingCount();
			let diasSemana = [];
			this.diasSeleccionados.forEach(d =>{
				diasSemana.push(new Number(d));
			})
			this.productosSeleccionados.forEach(p => {
				this.tListadoSeleccionados.forEach(s =>{
					let nuevoPrecio : PrecioBebida = new PrecioBebida(null,
						p,this.precio.valor,this.precio.fechaDesde,this.precio.fechaHasta,new Date(),
						true,this.getCurrentUser().user.descriptivo,
						diasSemana,s
					);
					preciosNuevos.push(nuevoPrecio);
				})
			});
			this.preciosService.guardarPrecios(preciosNuevos).then((r:ConflictoPrecioBebida[])=>{
				if(r.some(c => c.conflictos.length>0)){
					$this.conflictos = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
				}else{
					$this.success("Precios Guardados");
					$this.onGuardado.emit(preciosNuevos);
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		

		}
	}
	public cancelarConflictos(){
		this.verConflictos = false;
	}
	public finConflictos(precios : PrecioBebida[]){
		this.verConflictos = false;
		if(precios.length >0){
			this.success("Precios Guardados");
			this.onGuardado.emit(precios);
		}
	}

	public esValido(): boolean{

		let estado : boolean = true;
		if(this.productosSeleccionados.length == 0){
			this.error("Seleccione al menos 1 producto");
			estado = false;
		}
		if(this.tListadoSeleccionados.length == 0){
			this.error("Seleccione al menos 1 Tipo de Listado");
			estado = false;
		}
		if(this.diasSeleccionados.length == 0){
			this.error("Seleccione al menos 1 día");
			estado = false;
		}
		if(this.precio.valor == undefined || this.precio.valor < 0){
			this.error("El precio debe ser mayor a 0");
			estado = false;
		}
		if(this.precio.fechaDesde == undefined ){
			this.error("Indique una fecha Desde");
			estado = false;
		}else if(this.precio.fechaHasta != undefined && moment(this.precio.fechaHasta).isBefore(this.precio.fechaDesde)){
			this.error("La fecha Hasta debe ser mayor a Desde");
			estado = false;
		}
		return estado;
	}
	public cancelar() {
		this.reset();
		this.onCancelar.emit(true);
	}
	public reset(){
		this.precio = new PrecioBebida();
		this.productosSeleccionados = [];
		this.diasSeleccionados = [];
		this.tListadoSeleccionados = [];
	}
	*/
}
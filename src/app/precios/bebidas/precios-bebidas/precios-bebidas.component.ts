import { Precio } from '../../model/Precio';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute } from '@angular/router';
import { GrupoProducto } from '../../../model/GrupoProducto';
import { ListaPrecioService } from '../../../parametricos/lista-precios/service/lista-precios.service';
import { ListaPrecio } from '../../../model/ListaPrecio';
import { ParametricoService } from '../../../parametricos/services/parametricos.service';

import { MarcaService } from '../../../parametricos/marca/service/marca.service';
import { Marca } from '../../../model/Marca';
import { FiltroPrecio } from '../../model/FiltroPercio';
import { PreciosBebidasService } from '../../precios-bebidas.service';
import { DataTable } from 'primeng/primeng';
import { Categoria } from '../../../model/Categoria';
import { Descriptivo } from '../../../model/Descriptivo';
import { Producto } from 'app/model/Producto';
import { ProductoService } from '../../../producto/service/producto.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../../session-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PrecioBebida } from '../../model/PrecioBebida';
import * as moment from "moment";
import { ListadoPreciosComponent } from '../../listado-precios.component';
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';

@Component({
	selector: 'precios-bebidas',
	templateUrl: 'precios-bebidas.component.html',
	styleUrls: ['precios-bebidas.component.less']
})

export class PreciosBebidasComponent extends ListadoPreciosComponent<PrecioBebida> implements OnInit {

	ngOnInit() {
		super.ngOnInit();
	 }
	 public nombre : string ="Precios - Bebida";
	constructor(
		preciosService: PreciosBebidasService,
		 productoService: ProductoService,
		 marcasService: MarcaService,
		 listaPreciosService: ListaPrecioService,
		 route : ActivatedRoute,
		 confirmationService: ConfirmationService,
		 grupoService: GrupoProductoService,){
		super(	 preciosService,
			 productoService,
			 marcasService,
			 listaPreciosService,
			 route,
			 confirmationService,
			 grupoService,);
			 this.grupo = GrupoProducto.BEBIDA;
	}
}
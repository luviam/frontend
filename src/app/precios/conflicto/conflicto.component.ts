import { ListaPrecioService } from './../../parametricos/lista-precios/service/lista-precios.service';
import { PreciosBebidasService } from '../precios-bebidas.service';
import { TreeNode, SelectItem } from 'primeng/primeng';

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SessionComponent } from 'app/session-component.component';
import { Precio } from '../model/Precio';
import { Descriptivo } from 'app/model/Descriptivo';
import * as moment from 'moment';
import { ValidacionPrecios } from '../model/ValidacionPrecios';
import { PreciosService } from '../precios.service';


export abstract class ConflictoComponent<P extends Precio> extends SessionComponent implements OnInit {

	private _conflictos: ValidacionPrecios<P>[] = [];
	public static ACOTAR = "A";
	public static CANCELAR = "C";
	public static MODIFICAR = "M";
	public static REEMPLAZAR = "R";
	public procesadorAcciones: Array<(en: ConflictoView<P>) => P[]> = [];
	@Output()
	public finConflictos: EventEmitter<P[]> = new EventEmitter<P[]>();
	@Output()
	public onCancelarConflictos: EventEmitter<boolean> = new EventEmitter<boolean>();

	@Input()
	public autoResolverMerge :boolean = false;
	private preciosAMostrar: ConflictoView<P>[] = [];


	@Input()
	public set conflictos(val: ValidacionPrecios<P>[]) {
		this._conflictos = val;

		this.preciosAMostrar = [];
		if (val) {
			val.forEach(c => {


				let precio = new ConflictoView<P>(c.precio, ConflictoView.PRECIO_OK, c.conflictos);
				if (c.conflictos.length > 1) {
					precio.tipo = ConflictoView.PRECIO_ERROR;
					precio.acciones = [
						{ value: ConflictoComponent.MODIFICAR, title: 'Se podrá modificar antes de guardar el nuevo precio', icon: 'fa fa-pencil' },
						{ value: ConflictoComponent.CANCELAR, title: 'No se guardará el nuevo precio', icon: 'fa fa-ban' }
					];
					precio.accionATomar = ConflictoComponent.MODIFICAR;
				} else if (c.conflictos.length == 1) {
					precio.tipo = ConflictoView.PRECIO_WARNING;
					precio.acciones = [{ value: ConflictoComponent.MODIFICAR, title: 'Se podrá modificar antes de guardar el nuevo precio', icon: 'fa fa-pencil' },
					{ value: ConflictoComponent.CANCELAR, title: 'No se guardará el nuevo precio', icon: 'fa fa-ban' }];
					precio.accionATomar = ConflictoComponent.MODIFICAR;
					if (this.mismosDias(c.conflictos[0] ,c.precio)&&
						this.mismoSalon(c.precio, c.conflictos[0])) {
						precio.acciones.push(
							{ value: ConflictoComponent.REEMPLAZAR, title: 'Se deshabilitará el precio conflictivo dejando solo el nuevo', icon: 'fa fa-exchange' });
						precio.accionATomar = ConflictoComponent.REEMPLAZAR;
						if (!this.incluye(c.precio, c.conflictos[0])) {
							precio.acciones.push({ value: ConflictoComponent.ACOTAR, title: 'Acotar el precio conflictivo limitandolo según el nuevo. El precio mas antiguo quedará con fecha hasta igual a la fecha desde del más', icon: 'fa fa-scissors' });
							precio.accionATomar = ConflictoComponent.ACOTAR;
						}
					}
				} else {

				}

				this.preciosAMostrar.push(precio);
				c.conflictos.forEach(conf => this.preciosAMostrar.push(new ConflictoView(conf, ConflictoView.CONFLICTO, [])));

			});
		}


	}
	public mismosDias(p:P, c:P):boolean{
        return  c.diasResumen === p.diasResumen;
	}
	public mismoSalon(p:P,c:P): boolean{
        return (p.salon != null && c.salon !=null && p.salon.codigo === c.salon.codigo) || (c.salon == null && p.salon == null);
	}
	
	public incluye(c: Precio, p:P){
        return c.fechaDesde <= p.fechaDesde && (!c.fechaHasta || (p.fechaHasta && c.fechaHasta >= p.fechaHasta));
    }
	public getRowStyle(event: ConflictoView<P>) {

		return event.tipo;
	}
	public get conflictos(): ValidacionPrecios<P>[] {
		return this._conflictos;
	}

	public listaPrecios: Descriptivo[] = [];
	constructor( protected listaPrecioService: ListaPrecioService,
				protected preciosService : PreciosService<P>) {
		super();
	}
	ngOnInit() {

		this.procesadorAcciones[ConflictoComponent.REEMPLAZAR] = this.procesarReemplazar;
		this.procesadorAcciones[ConflictoComponent.ACOTAR] = this.procesarAjustar;
		this.procesadorAcciones[ConflictoComponent.CANCELAR] = this.procesarCancelar;
		this.procesadorAcciones[ConflictoComponent.MODIFICAR] = this.procesarModificacion;

		let $this = this;
		this.addLoadingCount();
		this.listaPrecioService.getAll().then((r) => {
			$this.listaPrecios = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler)


	}
	public procesarModificacion(c: ConflictoView<P>): P[] {
		c.precio = c.copia;
		return [c.precio];
	}
	public procesarReemplazar(c: ConflictoView<P>) {
		let res: P[] = [];
		res.push(c.precio);
		c.conflictos.forEach(conf => { conf.activo = false; res.push(conf) });
		return res;

	}
	public procesarCancelar(c: ConflictoView<P>) {
		return []
	}
	public procesarAjustar(c: ConflictoView<P>) {
		let res: P[]= [];
		res.push(c.precio);
		res = res.concat(c.conflictos);
		res = res.sort((a, b) => !a.activo && b.activo? 1: ((a.activo && !b.activo)? -1 : (a.fechaDesde < b.fechaDesde ? -1 : 1)));

		res.forEach((v, i) => {
			if (i != (res.length - 1)) {
				v.fechaHasta = v=== c.precio? v.fechaHasta : moment(res[i + 1].fechaDesde).startOf("day").add('days', -1).toDate();
			}
			if (i != 0) {
				v.fechaDesde =  v=== c.precio? v.fechaDesde :res[i - 1].fechaHasta ? moment(res[i - 1].fechaHasta).startOf("day").add('days', 1).toDate() : v.fechaHasta;
			}

		})

		return res;
	}
	public procesarCambios() {
		let reproceso: P[] = [];
		this.addLoadingCount();
		this.preciosAMostrar.forEach(p => {
			if (p.accionATomar) {
				reproceso = reproceso.concat(this.procesadorAcciones[p.accionATomar](p));
			} else {

			}
		});

		if (reproceso.length == 0) {
			this.warning("No se realizaron cambios");
			this.susLoadingCount();
			this.finConflictos.emit([]);
		} else {
			let $this = this;
			this.preciosService.guardarPrecios(reproceso).then((r) => {
				if (r.some(c => c.conflictos.length > 0)) {
					$this.conflictos = [...r];
					if($this.autoResolverMerge && $this.preciosAMostrar.every(c => c.accionATomar === ConflictoComponent.ACOTAR)){
						$this.procesarCambios();
					}
				} else {
					$this.finConflictos.emit(reproceso);
				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}


	}
	public cancelarConflictos() {
		this.onCancelarConflictos.emit();
		this.warning("No se realizaron modificaciones");
	}

	public tipoListadoResumen(p:P) : string{
		return p.listaPrecio? p.listaPrecio.descripcion : "TODOS"; 
	}
	public diasResumen(p:P) : string{
		return p.diasSemana.length === 7?"TODOS": p.diasSemana.sort((a,b)=>a < b?-1:1).map(d => SessionComponent.LOCALE_ES.dayNamesShort[d-1]).join(",");
	}
	
}


export class ConflictoView<P>{
	public static PRECIO_OK: string = "precio ok";
	public static PRECIO_ERROR: string = "precio error";
	public static PRECIO_WARNING: string = "precio warning";
	public static CONFLICTO: string = "conflicto";

	public acciones: SelectItem[] = [];
	public accionATomar: string;
	public copia;
	constructor(public precio: P, public tipo: string, public conflictos: P[]) {
		this.copia = Object.assign({} ,precio)


	}
}
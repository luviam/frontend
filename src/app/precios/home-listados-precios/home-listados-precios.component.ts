import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GrupoProducto } from 'app/model/GrupoProducto';
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';
import { SessionComponent } from 'app/session-component.component';

@Component({
	selector: 'home-listados-precios',
	templateUrl: 'home-listados-precios.component.html',
	styleUrls: ['home-listados-precios.component.less']
})

export class HomeListadosPreciosComponent extends SessionComponent implements OnInit {

	public grupos: GrupoProducto[] = [];
	constructor(
		private router: Router,
		private grupoService: GrupoProductoService) {
		super('home-precios')
	}
	ngOnInit() {
		let $this = this;
		this.addLoadingCount();
		this.grupoService.getAll().then(g => {
			$this.grupos = g;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public navegar(grupo: GrupoProducto) {
		if (grupo.codigo === GrupoProducto.MENU.codigo || grupo.codigo === GrupoProducto.ALQUILER.codigo) {
			this.router.navigate(['admin/precios/listado/' + grupo.codigo], {
				queryParams: {
					grupo: grupo.codigo
				}
			});
		} else {
			super.goTo("listado-" + grupo.codigo);
			this.router.navigate(['admin/precios/vistas/' + grupo.codigo]);
		}



	}
}
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { PagoLiquidacion } from './../contratos/model/PagoLiquidacion';
import { GrupoProducto } from './../model/GrupoProducto';
import { ActivatedRoute } from '@angular/router';
import { ListaPrecioService } from 'app/parametricos/lista-precios/service/lista-precios.service';
import { MarcaService } from './../parametricos/marca/service/marca.service';
import { PreciosService } from './precios.service';
import { Producto } from 'app/model/Producto';
import { FiltroPrecio } from './model/FiltroPercio';
import { Descriptivo } from 'app/model/Descriptivo';
import { DataTable } from 'primeng/primeng';
import { SessionComponent } from 'app/session-component.component';
import { Precio } from "./model/Precio";
import { OnInit, ViewChild } from '@angular/core';
import { Marca } from 'app/model/Marca';
import { ProductoService } from 'app/producto/service/producto.service';
import * as moment from 'moment';


export abstract class ListadoPreciosComponent<P extends Precio> extends SessionComponent implements OnInit {

	public precios: P[] = [];
	@ViewChild("tabla")
	public table: DataTable;

	public abstract get nombre(): string;
	public preciosFiltrados: P[] = [];
	public tituloDialogo: string = "";
	public preciosEditados: P[] = [];
	public tiposLista: Descriptivo[] = [];
	public filtro: FiltroPrecio = new FiltroPrecio();
	public marcas: Marca[] = [];
	public marcasFiltradas: Marca[] = [];
	protected tipoPrecio: string;
	private grupos: GrupoProducto[] = [];
	public productos: Producto[] = [];
	public productosFiltrados: Producto[] = [];
	public grupo: Descriptivo;

	constructor(
		protected preciosService: PreciosService<P>,
		protected productoService: ProductoService,
		protected marcasService: MarcaService,
		protected listaPreciosService: ListaPrecioService,
		protected route: ActivatedRoute,
		protected confirmationService: ConfirmationService,
		protected grupoService: GrupoProductoService,

		currentView: string = "listado"
	) {
		super(currentView);
	}

	ngOnInit() {
		this.filtro.fechaVigencia = new Date();
		let $this = this;
		
		this.grupoService.getAll().then(g => {
			$this.grupos = g;
			this.route.queryParams
				.subscribe(params => {

					if (params.fecha) {
						$this.filtro.fechaVigencia = moment(params.fecha, "YYYYMMDD").toDate();
					}
					if (params.grupo && !$this.grupo) {
						$this.grupo = $this.grupos.filter(g => g.codigo === params.grupo)[0];
						if(!$this.grupo){
							this.error("No hay grupo seleccionado");
							return;
						}
					}else if(!$this.grupo){
						this.error("No hay grupo seleccionado");
						return;
					}
					let inits: Promise<any>[] = [];
					inits.push(this.getTipoListaPrecio());
					inits.push(this.getBebidas());
					inits.push(this.getMarcas());
					Promise.all(inits).then(r => {
						if (params.listado) {
							$this.filtro.listaPrecios = $this.tiposLista.filter(l => l.codigo === params.listado);
							$this.getPrecios().then(r => {
								$this.preciosEditados = $this.preciosFiltrados? $this.preciosFiltrados : [];
								switch (params.action) {
									case "editar":
										if ($this.filtro.listaPrecios.length == 1) {
											$this.editarPrecios($this.preciosEditados);
										}
										break;
									case "imprimir":
										if ($this.filtro.listaPrecios.length == 1) {
											$this.imprimir($this.preciosEditados);
										}


										break;
									case "copiar":
										if ($this.filtro.listaPrecios.length == 1) {
											$this.nuevoListado();
										}

										break;
									default:
										break;
								}
							});
						} else {
							$this.getPrecios();
						}
						this.filtro.stateObs.subscribe(() => {
							this.getPrecios();
						});
					}).catch($this.errorHandler)


				});
		}).catch(this.errorHandler);
	}

	public getMarcas(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.marcasService.getAll().then((r) => {
			$this.marcas = r;
			$this.marcasFiltradas = [...r];
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public getPrecios(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.preciosService.getAll(this.filtro).then((r) => {
			$this.precios = [...r];
			$this.preciosFiltrados = $this.precios.filter(p => p.activo);
			$this.susLoadingCount();
		}).catch(this.errorHandler)
	}
	public getTipoListaPrecio(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.listaPreciosService.getAllByGrupo(this.grupo.codigo).then((r) => {
			$this.tiposLista = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler)
	}
	public getBebidas(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.productoService.getByGrupo(this.grupo.codigo).then((p) => {

			$this.productos = [...p];
			$this.productosFiltrados = [...p];
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public validoImpresion(): boolean {
		return this.preciosEditados.length > 0 && this.filtro.listaPrecios.length == 1 && this.filtro.fechaVigencia != undefined;
	}

	public eliminarPrecios(precios: P[]) {
		let $this = this;
		this.confirmationService.confirm({
			key: 'genConf',
			header: "Eliminar",
			message: '¿Desea eliminar los precios seleciconados?',
			accept: () => {
				this.addLoadingCount();
				let pr: Promise<any>[] = [];
				precios.forEach(p => pr.push(this.preciosService.delete(p.id)));
				Promise.all(pr).then(r => {
					$this.success("Precios Eliminados");
					$this.susLoadingCount();
					$this.getPrecios();
				}).catch($this.errorHandler);

			}
		});
	}

	public cancelarEdicion(event: boolean) {
		this.volver();
		this.preciosEditados = [];
		this.getPrecios();
	}

	public finGuardado(event: P[]) {
		this.getPrecios();
		this.volver();
		this.preciosEditados = [];
	}
	public nuevoPrecio() {
		this.tituloDialogo = "Nuevo Precio";
		this.preciosEditados = [];
		this.goTo("nuevos");
	}
	public editarPrecio(val: P) {

		this.editarPrecios([val]);
	}
	public eliminar(p: P) {
		let $this = this;
		if (p) {
			this.addLoadingCount();
			this.preciosService.delete(p.id).then(r => {
				$this.success("Precio Eliminado");
				$this.susLoadingCount();
				$this.getPrecios();
			}).catch(this.errorHandler);
		}
	}
	public parsePrecio(p) {
		return Object.assign({}, p);
	}
	public nuevoListado() {
		if (this.preciosEditados.length > 0) {
			let nuevos: P[] = [];
			this.preciosEditados.forEach(p => {
				let pp = this.parsePrecio(p)
				pp.id = null;
				pp.fechaDesde = p.fechaHasta ? moment(p.fechaHasta).add('days', 1).startOf('day').toDate() : new Date();
				if (pp.fechaHasta && pp.fechaHasta < pp.fechaDesde) {
					pp.fechaHasta = null;
				}
				nuevos.push(pp);
			});
			this.editarPrecios(nuevos);
		} else {
			this.error("Seleccione al menos 1 precio para crear el nuevo listado");
		}
	}
	public editarPrecios(val: P[]) {
		this.tituloDialogo = "Editando Precios";
		this.preciosEditados = val;
		this.goTo("editarItems");
	}
	public filtrarProductos(val: any) {
		let p: Producto = val.new;
		if (p && p.codigo) {
			this.filtro.marcas = [];
			this.marcasFiltradas = this.marcas.filter(m => p.marca && p.marca.codigo === m.codigo);

			this.getPrecios();
		}
	}

	public filtrarMarcas(val: any) {
		let m: Marca = val.new;
		if (m && m.codigo) {
			this.filtro.productos = [];
			this.productosFiltrados = this.productos.filter(p => !p.marca || p.marca.codigo === m.codigo);

			this.getPrecios();
		}


	}
	public imprimir(val: P[]) {
		this.goTo("impresion");
	}
	public vigenciaHoy() {
		this.filtro.fechaVigencia = new Date();
		this.getPrecios();
	}
	public vigenciaTodos() {
		this.filtro.fechaVigencia = null;
		this.getPrecios();
	}

	protected getFiltro() {
		return this.filtro;
	}
	protected parseFiltro(o: any) {
		if (!o) {
		} else {
			this.filtro.parse(o);

		}
	}




}
import { Precio } from './Precio';
import { Agrupador } from './../../model/Agrupador';
import { Marca } from 'app/model/Marca';
export class AgrupadorMarca{
	constructor(public marca:  Marca,
	public agrupadores : AgrupadorAgrupador[] = []){

	}
	public tieneAgrupador(a: Agrupador) :boolean	{
		if(a == null){
			return this.agrupadores.some(aa => !aa.agrupador);
		}
		return a && this.agrupadores.some(aa => aa.agrupador && aa.agrupador.codigo === a.codigo);
	}

	public add(p:Precio){
		if(!this.tieneAgrupador(p.getAgrupador())){
			this.addAgrupador(p.getAgrupador());
		}
		this.get(p.getAgrupador()).add(p)
		
	}
	public get(a: Agrupador): AgrupadorAgrupador{
		return this.agrupadores.filter(aa => (!a && !aa.agrupador) || (a && aa.agrupador && aa.agrupador.codigo === a.codigo ))[0];
	}
	public addAgrupador(a : Agrupador){
        this.agrupadores.push(new AgrupadorAgrupador(a));
        this.agrupadores = this.agrupadores.sort((a,b)=> {
            if(a.agrupador && b.agrupador){
                return a.agrupador.peso - b.agrupador.peso;
            }else{
                return a.agrupador? -1 : 1;
            }
        })
	}
}
export class AgrupadorAgrupador{
	constructor(public agrupador: Agrupador,
	public precios : Precio[] = []){

	}
	public add(p:Precio){
        this.precios.push(p);
        this.precios.sort((a,b) =>a.valor -b.valor)
    }
    
    public getSubproductos(){
      /*  let precioBase = this.precios.filter(p => p.producto && p.producto.subproductos.length > 0)[0];
		return precioBase? precioBase.producto.subproductos : [];
		*/
		return this.agrupador.itemsDetalle? this.agrupador.itemsDetalle : [];
    }

}
import { ConfiguracionListadoPrecioMenu } from 'app/model/ConfiguracionListadoPrecioMenu';
import { Producto } from 'app/model/Producto';
import * as moment from 'moment';

export class Calculardora {


	constructor(
		public listados: ConfiguracionListadoPrecioMenu[] = []
	) { }

	public getPrecio(fecha: Date, producto: Producto): number {
		let listado = this.getAplica(fecha);
		if (!listado) return 0;
		return listado.calcularPrecio(fecha, producto);
	}

	public esBaja(mes: string, year: number): boolean {
		moment.locale("es");
		let listado = this.getAplica(moment().month(mes).year(year).startOf("month").toDate());
		return listado ? listado.esBaja(mes) : false;
	}
	public esAlta(mes: string, year: number): boolean {
		moment.locale("es");
		let listado = this.getAplica(moment().month(mes).year(year).startOf("month").toDate());
		return listado ? listado.esAlta(mes) : false;
	}
	public getAplica(fecha: Date) {
		return this.listados.filter(a => a ? a.aplica(fecha) : false)[0];

	}

	public getProductos(fecha: Date): Producto[] {
		return this.getAplica(fecha).precios.map(p => p.producto);
	}


}
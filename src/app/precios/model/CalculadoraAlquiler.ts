import { Producto } from 'app/model/Producto';
import { ConfiguracionListadoPrecioAlquiler } from './../../model/ConfiguracionListadoPrecioAlquiler';

export class CalculardoraAlquiler {


	constructor(
		public listados: ConfiguracionListadoPrecioAlquiler[] = []
	) { }

	public getPrecio(fecha: Date, producto: Producto): number {
		let listado = this.getAplica(fecha);
		if (!listado) return 0;
		return listado.calcularPrecio(fecha, producto);
	}
	public getAplica(fecha: Date) {
		return this.listados.filter(a => a.aplica(fecha))[0];

	}

	public getProductos(fecha: Date): Producto[] {
		return this.getAplica(fecha).precios.map(p => p.producto);
	}


}
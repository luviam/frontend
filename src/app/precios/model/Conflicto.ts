import { Precio } from './Precio';
import { SelectItem } from 'primeng/primeng';

export class Conflicto<E>{
    constructor(
        public aComparar: E,
        public conflictos: E[] = [],
        public tipo: string = null,
    ) {
        this.copia = Object.assign({}, aComparar)

    }
    public static PRECIO_OK: string = "ok";
    public static PRECIO_ERROR: string = "error";
    public static PRECIO_WARNING: string = "warning";
    public static CONFLICTO: string = "conflicto";

    public acciones: SelectItem[] = [];
    public accionATomar: string;
    public copia;

}
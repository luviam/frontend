import { PrecioAdicionales } from './PrecioAdicionales';
import { PrecioBebida } from './PrecioBebida';
import { ValidacionPrecios } from './ValidacionPrecios';
export class ConflictoPrecioAdicionales extends ValidacionPrecios<PrecioAdicionales>{


    public static fromData(data: any) {
        if (!data) return null;
        let o: ConflictoPrecioAdicionales = new ConflictoPrecioAdicionales(
            PrecioAdicionales.fromData(data.precio),
            data.conflictos ? data.conflictos.map(c => PrecioAdicionales.fromData(c)) : []
        );
        return o;
    }
}
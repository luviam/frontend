import { PrecioBebida } from './PrecioBebida';
import { ValidacionPrecios } from './ValidacionPrecios';
export class ConflictoPrecioBebida extends ValidacionPrecios<PrecioBebida>{


    public static fromData(data: any) {
        if (!data) return null;
        let o: ConflictoPrecioBebida = new ConflictoPrecioBebida(
            PrecioBebida.fromData(data.precio),
            data.conflictos ? data.conflictos.map(c => PrecioBebida.fromData(c)) : []
        );
        return o;
    }
}
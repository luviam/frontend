import { Descriptivo } from 'app/model/Descriptivo';
import * as moment from 'moment';
import { Filtro } from './../../model/Filtro';
import { Precio } from './Precio';
export class FiltroPrecio extends Filtro {

    key: string = "filtroPrecios";
    private _fechaVigencia: Date = moment().startOf("day").toDate();
    private _listaPrecios: Descriptivo[] = [];
    private _productos: Descriptivo[] = [];
    private _marcas: Descriptivo[] = [];
    private _grupoPrecio: string;
    private _buscador: string;
    private _salones: Descriptivo[] = [];

    public get buscador(): string {
        return this._buscador;
    }
    public set buscador(val: string) {
        if (val != this._buscador) {
            this._buscador = val;
            this.onChange();
        }
    }
    public get fechaVigencia(): Date {
        return this._fechaVigencia;
    }
    public set fechaVigencia(val: Date) {
        if (val != this._fechaVigencia) {
            this._fechaVigencia = val;
            this.onChange();
        }
    }
    public get grupoPrecio(): string {
        return this._grupoPrecio;
    }
    public set grupoPrecio(val: string) {
        if (val != this._grupoPrecio) {
            this._grupoPrecio = val;
            this.onChange();
        }
    }

    public get listaPrecios(): Descriptivo[] {
        return this._listaPrecios;
    }
    public set listaPrecios(val: Descriptivo[]) {
        if (val != this._listaPrecios) {
            this._listaPrecios = val;
            this.onChange();
        }
    }

    public get salones(): Descriptivo[] {
        return this._salones;
    }
    public set salones(val: Descriptivo[]) {
        if (val != this._salones) {
            this._salones = val;
            this.onChange();
        }
    }
    public get marcas(): Descriptivo[] {
        return this._marcas;
    }
    public set marcas(val: Descriptivo[]) {
        if (val != this._marcas) {
            this._marcas = val;
            this.onChange();
        }
    }

    public get productos(): Descriptivo[] {
        return this._productos;
    }
    public set productos(val: Descriptivo[]) {
        if (val != this._productos) {
            this._productos = val;
            this.onChange();
        }
    }



    public getResumen(): string {
        let str: string = "<strong style='font-variant: small-caps'>Vigencia:</strong> ";
        if (this.fechaVigencia) {
            if (moment(this.fechaVigencia).startOf('days') == moment(new Date()).startOf('days')) {
                str += "Hoy";
            } else {
                str += moment(this.fechaVigencia).format("DD/MM/YYYY");
            }
        } else {
            str += "Todos";
        }

        if (this.listaPrecios.length <= 3 && this.listaPrecios.length > 0) {
            str += " - <strong style='margin-left:1.5em;font-variant: small-caps'>Tipo Lista:</strong> " + this.listaPrecios.map(m => m.descripcion).join(", ");
        } else if (this.listaPrecios.length > 3) {
            str += " - " + this.listaPrecios.length + " Tipos Precios seleccionados";
        }

        if (this.productos.length <= 2 && this.productos.length > 0) {
            str += " - <strong style='margin-left:1.5em;font-variant: small-caps'>Productos:</strong> " + this.productos.map(m => m.descripcion).join(", ");
        } else if (this.productos.length > 2) {
            str += " - " + this.productos.length + " productos seleccionados";
        }

        if (this.marcas.length <= 2 && this.marcas.length > 0) {
            str += " - <strong style='margin-left:1.5em;font-variant: small-caps'>Marcas:</strong> " + this.marcas.map(m => m.descripcion).join(", ");
        } else if (this.marcas.length > 2) {
            str += " - " + this.marcas.length + " marcas seleccionadas";
        }

        return str;
    }

    public static fromData(data: any): FiltroPrecio {
        if (!data) return null
        let f: FiltroPrecio = new FiltroPrecio();
        f.fechaVigencia = data.fechaVigencia ? data.fechaVigencia : moment().startOf("day").toDate();
        f.listaPrecios = data.listaPrecios ? data.listaPrecios.map(l => Descriptivo.fromData(l)) : [];
        f.productos = data.productos ? data.productos.map(l => Descriptivo.fromData(l)) : [];
        f.marcas = data.marcas ? data.marcas.map(l => Descriptivo.fromData(l)) : [];
        f.grupoPrecio = data.grupoPrecio;
        return f;
    }
    public parse(data: any): Filtro {
        this.fechaVigencia = data.fechaVigencia ? data.fechaVigencia : moment().startOf("day").toDate();
        this.listaPrecios = data.listaPrecios ? data.listaPrecios.map(l => Descriptivo.fromData(l)) : [];
        this.productos = data.productos ? data.productos.map(l => Descriptivo.fromData(l)) : [];
        this.marcas = data.marcas ? data.marcas.map(l => Descriptivo.fromData(l)) : [];
        this.grupoPrecio = data.grupoPrecio;
        return this;
    }
    public isEmpty(): boolean {
        return !this.fechaVigencia && this.listaPrecios.length == 0 && this.productos.length == 0 && this.marcas.length == 0 && !this.grupoPrecio;
    }

    public getJSONclean() {
        let filtro = this;
        let f = {
            fechaVigencia: moment(filtro.fechaVigencia).startOf("day").toDate() ? filtro.fechaVigencia : null,
            listaPrecios: filtro.listaPrecios ? filtro.listaPrecios.map(l => Descriptivo.fromData(l)) : [],
            productos: filtro.productos ? filtro.productos.map(l => Descriptivo.fromData(l)) : [],
            marcas: filtro.marcas ? filtro.marcas.map(l => Descriptivo.fromData(l)) : [],
            grupoPrecio: filtro.grupoPrecio,

        };
        return f;
    }

    public aplica(precio: Precio) {
        return (!this.productos || this.productos.length == 0 || precio.producto && this.productos.some(p => p.codigo == precio.producto.codigo)) &&
            (!this.marcas || this.marcas.length == 0 || precio.producto && precio.producto.marca && this.marcas.some(p => p.codigo == precio.producto.marca.codigo)) &&
            (!this.listaPrecios || this.listaPrecios.length == 0 || precio.listaPrecio && this.listaPrecios.some(p => p.codigo == precio.listaPrecio.codigo)) &&
            (!this.buscador || this.buscador.includes(precio.producto.descripcion));
    }
}
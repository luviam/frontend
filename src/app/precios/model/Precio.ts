import { Producto } from "app/model/Producto";
import { Agrupador } from './../../model/Agrupador';
import { Descriptivo } from './../../model/Descriptivo';
import { Marca } from './../../model/Marca';
import { SessionComponent } from './../../session-component.component';

export class Precio {
    constructor(
        public id?: number,
        public producto?: Producto,
        public valor: number = 0,
        public fechaDesde?: Date,
        public fechaHasta?: Date,
        public fechaAlta?: Date,
        public activo?: Boolean,
        public responsable?: Descriptivo,
        public diasSemana: number[] = [],
        public salon?: Descriptivo,
        public listaPrecio?: Descriptivo
    ) {
        this.valor = Math.round(valor * 100) / 100;
    }

    get codProd(): string {
        return this.producto.codigo;
    }

    get busqueda(): string {
        return (this.producto ? this.producto.codigo + " " + this.producto.descripcion + " " : "") + (this.responsable ? this.responsable.descripcion : "");
    }

    get salonesResumen(): string {
        return this.salon.descripcion;
    }
    get tipoListadoResumen(): string {
        return this.listaPrecio ? this.listaPrecio.descripcion : "TODOS";
    }
    get diasResumen(): string {
        return this.diasSemana.length === 7 ? "TODOS" : this.diasSemana.sort((a, b) => a < b ? -1 : 1).map(d => SessionComponent.LOCALE_ES.dayNamesShort[d - 1]).join(",");
    }
    public tieneDia(i: number): boolean {
        return this.diasSemana.indexOf(i) >= 0;
    }
    public toggleDia(i: number) {
        let index = this.diasSemana.indexOf(i);
        if (index >= 0) {
            this.diasSemana.splice(index, 1);
        } else {
            this.diasSemana.push(i);
        }
    }
    public quitarDia(i: number) {
        let index = this.diasSemana.indexOf(i);
        if (index >= 0) {
            this.diasSemana.splice(index, 1);
        }
    }
    public seleccionarDia(i: number) {
        let index = this.diasSemana.indexOf(i);
        if (index < 0) {
            this.diasSemana.push(i);
        }
    }

    public incluye(p: Precio) {
        if (!p.fechaHasta) {
            if (this.fechaHasta) {
                return this.fechaDesde <= p.fechaDesde && this.fechaHasta >= p.fechaDesde;
            } else {
                return this.fechaDesde <= p.fechaHasta;
            }
        } else {
            return this.fechaDesde < p.fechaHasta && p.fechaDesde < this.fechaHasta;
        }
    }
    public incluyeFecha(date: Date) {
        return this.fechaDesde < date && (!this.fechaHasta || date < this.fechaHasta);
    }
    public mismoSalon(p: Precio): boolean {
        return (this.salon != null && p.salon != null && p.salon.codigo === this.salon.codigo) || (this.salon == null && p.salon == null);
    }
    public mismosDias(p: Precio): boolean {
        return p.diasResumen === this.diasResumen;
    }

    public copy(): Precio {
        return Precio.fromData(this);
    };

    public getMarca(): Marca {
        return this.producto ? this.producto.marca : null;
    }

    public getAgrupadorStr(): string {
        return this.producto ? (this.producto.agrupador ? this.producto.agrupador.descripcion : "") : "";
    }
    public getAgrupador(): Agrupador {
        return this.producto ? (this.producto.agrupador ? this.producto.agrupador : null) : null;
    }

    public static fromData(data: Precio): Precio {
        if (!data) return null;
        let p: Precio = new Precio(
            data.id,
            Producto.fromData(data.producto),
            data.valor ? Math.ceil(data.valor) : 0,
            data.fechaDesde ? new Date(data.fechaDesde) : null,
            data.fechaHasta ? new Date(data.fechaHasta) : null,
            data.fechaAlta ? new Date(data.fechaAlta) : null,
            data.activo,
            Descriptivo.fromData(data.responsable),
            data.diasSemana ? data.diasSemana : [],
            Descriptivo.fromData(data.salon),
            Descriptivo.fromData(data.listaPrecio));
        return p;

    }

}
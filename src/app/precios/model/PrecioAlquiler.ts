import { Producto } from "app/model/Producto";
import * as moment from 'moment';
import { Descriptivo } from './../../model/Descriptivo';
import { Precio } from "./Precio";

export class PrecioAlquiler extends Precio {
    public static JORNADA = "J";
    public static SOCIAL = "S";
    public static DIAS = ["lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"];
    constructor(
        id?: number,
        producto?: Producto,
        fechaDesde?: Date,
        fechaHasta?: Date,
        fechaAlta?: Date,
        activo?: Boolean,
        responsable?: Descriptivo,
        diasSemana: number[] = [],
        listaPrecio?: Descriptivo,
        public tipoPrecio: string = PrecioAlquiler.JORNADA,
        public lunes: number = 0,
        public martes: number = 0,
        public miercoles: number = 0,
        public jueves: number = 0,
        public viernes: number = 0,
        public sabado: number = 0,
        public domingo: number = 0,
        public minPersonas: number = 0,
        public maxPersonas: number = 10000,

    ) {
        super(id, producto, 0, fechaDesde, fechaHasta, fechaAlta, activo, responsable, diasSemana, null, listaPrecio);
        moment.locale("es");


    }
    public static fromData(data: any): PrecioAlquiler {
        if (!data) return null;
        let p: PrecioAlquiler = new PrecioAlquiler(
            data.id,
            Producto.fromData(data.producto),
            data.fechaDesde ? new Date(data.fechaDesde) : null,
            data.fechaHasta ? new Date(data.fechaHasta) : null,
            data.ultimaModificacion ? new Date(data.ultimaModificacion) : null,
            data.activo,
            Descriptivo.fromData(data.responsable),
            data.diasSemana ? data.diasSemana : [],
            data.listaPrecio ? Descriptivo.fromData(data.listaPrecio) : null,

        );
        p.lunes = data.lunes;
        p.martes = data.martes;
        p.miercoles = data.miercoles;
        p.jueves = data.jueves;
        p.viernes = data.viernes;
        p.sabado = data.sabado;
        p.domingo = data.domingo;
        p.minPersonas = data.minPersonas;
        p.maxPersonas = data.maxPersonas;
        p.tipoPrecio = data.tipoPrecio;

        return p;
    }
    public copy(): PrecioAlquiler {
        return PrecioAlquiler.fromData(this);
    }

    public getCodigoMarca(): string {
        return this.producto ? (this.producto.marca ? this.producto.marca.codigo : "") : "";
    }
    public get descripcion(): string {
        return this.producto ? (this.producto.detalleItem ? this.producto.detalleItem : this.producto.descripcion) : "";
    }

    public get periodoStr(): string {
        if (!this.fechaDesde || !this.fechaHasta) {
            return "Sin definir";
        }
        let mDesde = moment(this.fechaDesde);
        let mHasta = moment(this.fechaHasta);
        let dif = mHasta.diff(mDesde, "days");
        if ((mDesde.get("month") == 1 && dif >= 26 && dif <= 27) || dif >= 29 && dif <= 31) {
            return mDesde.format("MMMM YY");
        } else {
            if (mDesde.format('D') == '1' && mHasta.format('D') == '15') {
                return mDesde.format("MMMM YY") + " 1Q";;
            } else if (mDesde.format('D') == '16' && moment(this.fechaHasta).endOf("month").format('D') == mHasta.format("D")) {
                return mDesde.format("MMMM YY") + " 2Q";
            } else if (mDesde.format('DDMM') == '0101' && mHasta.format("DDMM") == '3006') {
                return "Primer Semestre " + mDesde.format('YY');
            } else if (mDesde.format('DDMM') == '0107' && mHasta.format("DDMM") == '3112') {
                return "Segundo Semestre " + mDesde.format('YY');
            } else {
                return mDesde.format("DD-MMM-YY") + " al " + mHasta.format("DD-MMM-YY");
            }
        }

    }
    public get personas(): string {
        return "de " + this.minPersonas + " hasta " + this.maxPersonas + " Personas";
    }
    public set lunesAJueves(val: number) {
        this.lunes = val;
        this.martes = val;
        this.miercoles = val;
        this.jueves = val;

    }
    public get lunesAJueves(): number {
        return this.lunes;
    }

    public incluye(p: PrecioAlquiler) {
        let dias = p.getDias();
        return super.incluye(p) &&
            this.getDias().some(r => dias.indexOf(r) >= 0);
    }

    public getDias(): string[] {
        let dias: string[] = [];
        PrecioAlquiler.DIAS.forEach(d => {
            if (this[d] > 0) {
                dias.push(d);
            }
        });
        return dias;
    }

}
import { Agrupador } from './../../model/Agrupador';
import { Descriptivo } from './../../model/Descriptivo';
import { Precio } from "./Precio";
import { Producto } from "app/model/Producto";

export class PrecioMenu extends Precio{
    public static VALOR = 'V';
    public static PORCENTAJE = 'P';
    public static ADICIONAL = "A";
    constructor(
         id?:number,
         producto?: Producto,
         valor: number = 0,
         fechaDesde?: Date,
         fechaHasta?: Date,
         fechaAlta?: Date,
         activo?: Boolean,
         responsable?: Descriptivo,
         diasSemana : number[] = [],
         listaPrecio? :Descriptivo,
        public tipoOperacion : string = PrecioMenu.VALOR

    ){
        super(id,producto,valor,fechaDesde,fechaHasta,fechaAlta,activo,responsable,diasSemana,null,listaPrecio);
        
	
    }
    public static fromData(data : any) : PrecioMenu{
        if(!data) return null;
        let p : PrecioMenu = new PrecioMenu(
            data.id,
            Producto.fromData(data.producto),
            data.valor?Math.ceil(data.valor): 0,
            data.fechaDesde? new Date(data.fechaDesde):null,
            data.fechaHasta? new Date(data.fechaHasta):null,
            data.ultimaModificacion? new Date(data.ultimaModificacion):null,
            data.activo,
            Descriptivo.fromData(data.responsable),
            data.diasSemana? data.diasSemana : [],
            data.listaPrecio?Descriptivo.fromData(data.listaPrecio): null,
            data.tipoOperacion
            
        );
        return p;
    }
    public copy():PrecioMenu{
        return PrecioMenu.fromData(this);
    }
 
    public getCodigoMarca():string{
        return this.producto? (this.producto.marca?  this.producto.marca.codigo : "") :"";
    }
    public getDescripcion():string{
        return this.producto? (this.producto.detalleItem? this.producto.detalleItem : this.producto.descripcion) : "";
    }


}
import { Producto } from './../../model/Producto';
import { PrecioMenu } from './PrecioMenu';


export class ProductoBase {
	public producto: Producto;
	public base: number;
	public tipoOperacion: string;
	public valor: number;
	public tipoProducto: string;

	constructor(p: Producto, b: number = 0, tipoOperacio: string = PrecioMenu.VALOR, valor: number = 0, tipo: string = "") {
		this.producto = p;
		this.producto = p;
		this.base = b;
		this.valor = valor;
		this.tipoOperacion = tipoOperacio;
		this.tipoProducto = this.tipoProducto
	}

	public get descripcion(): string {
		return this.producto.descripcion;
	}

	public get key(): string {
		return this.producto.codigo;
	}
	public get precio(): number {
		if (this.tipoOperacion === PrecioMenu.PORCENTAJE) {
			return Math.ceil(this.base * (1 + this.valor / 100));
		} else if (this.tipoOperacion === PrecioMenu.ADICIONAL) {
			return this.base + this.valor;
		} else {
			return this.valor;
		}
	}
}
import { Precio } from "./Precio";

export abstract class ValidacionPrecios<P extends Precio>{
    constructor(
        public precio: P,
        public conflictos : P[] = []
    ){

    }

    
}
import { ListaPrecio } from './../../model/ListaPrecio';
import { Descriptivo } from "app/model/Descriptivo";

export class VistaListado extends Descriptivo{
    constructor(codigo ? : string, descripcion ? : string, 
        private tipoLista? : ListaPrecio,
        private esPadre : boolean = false){
        super(codigo,descripcion);
    }

    public static fromData(data : VistaListado) : VistaListado{
        if(!data) return null;
        let o : VistaListado = new VistaListado(
            data.codigo,
            data.descripcion,
            ListaPrecio.fromData(data.tipoLista),
            data.esPadre
        );
        return o;
    }
}
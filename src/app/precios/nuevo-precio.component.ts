import { ProductoBase } from './model/ProductoBase';
import { SelectItem } from 'primeng/primeng';
import { PrecioMenu } from './model/PrecioMenu';
import { GrupoProducto } from 'app/model/GrupoProducto';
import { ListaPrecio } from './../model/ListaPrecio';
import { Input, EventEmitter, Output } from '@angular/core';
import { PreciosService } from './precios.service';
import { ListaPrecioService } from './../parametricos/lista-precios/service/lista-precios.service';
import { FiltroPrecio } from './model/FiltroPercio';
import { Producto } from './../model/Producto';
import { ProductoService } from './../producto/service/producto.service';
import { SessionComponent } from './../session-component.component';
import { Descriptivo } from 'app/model/Descriptivo';
import { Precio } from './model/Precio';
import { ValidacionPrecios } from './model/ValidacionPrecios';
import * as moment from 'moment';

export abstract class NuevoPrecioComponent<P extends Precio> extends SessionComponent {


	public abstract generarPrecios();

	@Input()
	public set productos(p: Producto[]) {
		this.productosBase = p.map(a => new ProductoBase(a));
	};

	public productosBase: ProductoBase[] = [];
	@Input()
	public tListados: ListaPrecio[] = [];

	public mostrarProductos = false;
	protected _fechaDesde: Date = new Date();
	protected _fechaHasta: Date = null;

	public get fechaDesde() : Date{
		return this._fechaDesde;
	}
	public get fechaHasta():Date{
		return this._fechaHasta;
	}

	public set fechaDesde(val: Date){
		this._fechaDesde = val;
	}
	public set fechaHasta(val: Date){
		this._fechaHasta = val;
	}
	@Input()
	public grupo: GrupoProducto;
	public verConflictos: boolean = false;
	public conflictos: ValidacionPrecios<P>[] = [];

	@Output()
	public onCancelar: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	public onGuardado: EventEmitter<P[]> = new EventEmitter<P[]>();

	public diasSeleccionados: string[] = [];
	@Input()
	public tListadoSeleccionados: string[] = [];


	public productosSeleccionados: ProductoBase[] = [];
	public productosMarcados: ProductoBase[] = [];
	public filtro: FiltroPrecio = new FiltroPrecio();
	constructor(protected productoService: ProductoService,
		protected listaPreciosService: ListaPrecioService) {
		super();
	}

	ngOnInit() {
		let $this = this;
		let promises: Promise<any>[] = [];

		if (this.productosBase.length === 0 && this.grupo) {

			this.addLoadingCount();
			promises.push(this.productoService.getByGrupo(this.grupo.codigo).then((r) => {
				$this.productosBase = r.map(p => new ProductoBase(p));
				$this.susLoadingCount();
			}))
				;
		}
		if (this.tListados.length === 0 && this.grupo) {
			this.addLoadingCount();
			promises.push(this.listaPreciosService.getAllByGrupo(this.grupo.codigo).then((r) => {
				$this.tListados = r;
				$this.susLoadingCount();
			}))

		}
		Promise.all(promises).then(r => this.afterInit()).catch(this.errorHandler)
		this.todosDias = true;
	}
	set todosDias(val: boolean) {
		if (val) {
			this.diasSeleccionados = this.locale["es"].dayNames.map((e, i) => {
				return (i + 1) + "";
			});;
		} else {
			this.diasSeleccionados = [];
		}

	};

	get todosDias(): boolean {
		return this.diasSeleccionados.length === 7;
	}
	set todosListados(val: boolean) {
		if (val) {
			this.tListadoSeleccionados = this.tListados.map(l => l.codigo);
		} else {
			this.tListadoSeleccionados = [];
		}

	};
	get todosListados(): boolean {
		return this.tListadoSeleccionados.length == this.tListados.length;
	}
	public getCategoria(): string {
		return this.grupo.codigo;
	}
	public updateSeleccionados() {
		this.productosSeleccionados = [...this.productosSeleccionados];
		this.productosMarcados = [];
	}
	public eliminarItem(item: ProductoBase) {
		this.eliminarItems([item]);
	}
	public eliminarItems(items: ProductoBase[]) {
		this.productosBase.push(...items);
		this.productosSeleccionados = this.productosSeleccionados.filter(i => items.indexOf(i) < 0);

	}
	public cancelarConflictos() {
		this.verConflictos = false;
	}
	public finConflictos(precios: P[]) {
		this.verConflictos = false;
		if (precios.length > 0) {
			this.success("Precios Guardados");
			this.onGuardado.emit(precios);
		}
	}

	public esValido(): boolean {

		let estado: boolean = true;
		if (this.productosSeleccionados.length == 0) {
			this.error("Seleccione al menos 1 producto");
			estado = false;
		}
		if (this.tListadoSeleccionados.length == 0) {
			this.error("Seleccione al menos 1 Tipo de Listado");
			estado = false;
		}
		if (this.fechaDesde == undefined) {
			this.error("Indique una fecha Desde");
			estado = false;
		} else if (this.fechaHasta != undefined && moment(this.fechaHasta).isBefore(this.fechaDesde)) {
			this.error("La fecha Hasta debe ser mayor a Desde");
			estado = false;
		}
		return estado;
	}
	public cancelar() {
		this.reset();
		this.onCancelar.emit(true);
	}
	public reset() {
		this.productosSeleccionados = [];
		this.diasSeleccionados = [];
		this.tListadoSeleccionados = [];
		this.productosMarcados = [];
	}

	public agregarProductos() {
		this.mostrarProductos = true;
	}

	public getTipoListado(s: string): ListaPrecio {
		return this.tListados.filter(l => l.codigo === s)[0];
	}

	public afterInit() { };
}
import { PreciosService } from 'app/precios/precios.service';
import { FiltroPrecio } from './model/FiltroPercio';
import { ConflictoPrecioAdicionales } from './model/ConflictoPrecioAdicionales';
import { HttpParams } from '@angular/common/http';
import { ServicioAbstract } from "app/common-services/service.service";
import { Precio } from "./model/Precio";
import { ValidacionPrecios } from "./model/ValidacionPrecios";
import { PrecioAdicionales } from "./model/PrecioAdicionales";

export class PreciosAdicionalesService extends PreciosService<PrecioAdicionales>{
    public baseUrl: string = "precioadicionales";
    public fromData(data: any): PrecioAdicionales {
        return PrecioAdicionales.fromData(data);
    }
    public parseConflictos(r :any): Promise<ConflictoPrecioAdicionales[]>
        {
            if(!r.respuesta) return null; return r.respuesta.map(r =>ConflictoPrecioAdicionales.fromData(r))
        }
    
}
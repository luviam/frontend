import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicioAbstract } from 'app/common-services/service.service';
import { ConfiguracionListadoPrecioAlquiler } from 'app/model/ConfiguracionListadoPrecioAlquiler';
import * as moment from 'moment';
import { Conflicto } from './model/Conflicto';
import { FiltroPrecio } from './model/FiltroPercio';

@Injectable()
export class PreciosAlquilerService extends ServicioAbstract {
    public fromData(data: any): ConfiguracionListadoPrecioAlquiler {
        return ConfiguracionListadoPrecioAlquiler.fromData(data);
    }
    public baseUrl: string = "configuracionprecioalquiler";

    public delete(id: any): Promise<any> {
        let params = new HttpParams();
        params = params.append('id', id)
        return this.http.delete(this.getApiURL() + this.baseUrl, { params }).toPromise().then(this.handleOk, this.handleError);
    }

    public parseConflictos(r: any): Promise<Conflicto<ConfiguracionListadoPrecioAlquiler>[]> {
        if (!r.respuesta) return null; return r.respuesta.map(d => {
            let c = new Conflicto<ConfiguracionListadoPrecioAlquiler>(
                ConfiguracionListadoPrecioAlquiler.fromData(d.aComparar),
                d.conflictos ? d.conflictos.map(cc => ConfiguracionListadoPrecioAlquiler.fromData(cc)) : []
            );
            return c;

        })

    }

    public getById(id: number): Promise<ConfiguracionListadoPrecioAlquiler> {

        return this.http.get(this.getApiURL() + this.baseUrl + "/" + id).toPromise().then((r: any) => {
            if (r && r.respuesta) {
                return ConfiguracionListadoPrecioAlquiler.fromData(r.respuesta);
            }
            return null;
        }).catch(this.handleError);
    }
    public getByPeriodo(id: number, fechaDesde: Date, fechaHasta: Date): Promise<ConfiguracionListadoPrecioAlquiler[]> {

        return this.http.get(this.getApiURL() + this.baseUrl + "/byPeriodo/" + id + "/" + moment(fechaDesde).format("DDMMYYYY") + "/" + moment(fechaHasta).format("DDMMYYYY")).toPromise().then((r: any) => {
            if (r && r.respuesta) {
                return r.respuesta.map(m => ConfiguracionListadoPrecioAlquiler.fromData(m));
            }
            return null;
        }).catch(this.handleError);
    }
    public aprobar(listado: ConfiguracionListadoPrecioAlquiler): Promise<ConfiguracionListadoPrecioAlquiler> {
        return this.http.post(this.getApiURL() + this.baseUrl + "/aprobar", listado).toPromise().then(this.parseVo, this.handleError);
    }

    public guardarListado(listado: ConfiguracionListadoPrecioAlquiler): Promise<ConfiguracionListadoPrecioAlquiler> {
        if (!listado.id) {
            return this.http.put(this.getApiURL() + this.baseUrl, listado).toPromise().then(this.parseVo, this.handleError);
        }
        else {
            return this.http.post(this.getApiURL() + this.baseUrl, listado).toPromise().then(this.parseVo, this.handleError);
        }

    }

    public verificarListados(listados: ConfiguracionListadoPrecioAlquiler[]): Promise<Conflicto<ConfiguracionListadoPrecioAlquiler>[]> {
        return this.http.post(this.getApiURL() + this.baseUrl + "/verificar", listados).toPromise().then(this.parseConflictos, this.handleError);
    }

    public parseVo(r: any): ConfiguracionListadoPrecioAlquiler {
        r.respuesta;
        if (r && r.respuesta) {
            return ConfiguracionListadoPrecioAlquiler.fromData(r.respuesta);
        } else {
            return null;
        }
    }

    public getAll(filtro: FiltroPrecio): Promise<ConfiguracionListadoPrecioAlquiler[]> {

        return this.http.post(this.getApiURL() + this.baseUrl + "/all", filtro.getJSONclean()).toPromise().then((r: any) => {
            r.respuesta;
            if (r && r.respuesta) {
                return r.respuesta.map(p => this.fromData(p));
            } else {
                return [];
            }
        }).catch(this.handleError);
    }

}
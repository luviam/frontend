import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfiguracionListadoPrecioAlquiler } from 'app/model/ConfiguracionListadoPrecioAlquiler';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from './../../../session-component.component';
import { Conflicto } from './../../model/Conflicto';
import { PreciosAlquilerService } from './../../precios-alquiler.service';

@Component({
	selector: 'conflictos-alquiler',
	templateUrl: 'conflictos-alquiler.component.html',
	styleUrls: ['../../listados.less', 'conflictos-alquiler.component.less']
})

export class ConflictosAlquilerComponent extends SessionComponent implements OnInit {
	public static ACOTAR = "A";
	public static CANCELAR = "C";
	public static MODIFICAR = "M";
	public static REEMPLAZAR = "R";
	constructor(private confirmationService: ConfirmationService,
		private preciosService: PreciosAlquilerService) {
		super("conflictos-alquiler");
	}
	@Input()
	public autoResolverMerge: boolean = false;

	@Output()
	public finConflictos = new EventEmitter<ConfiguracionListadoPrecioAlquiler[]>();

	@Output()
	public onCancelarConflictos = new EventEmitter<boolean>();
	@Input()
	public set conflictos(val: Conflicto<ConfiguracionListadoPrecioAlquiler>[]) {
		this._conflictos = val;
		if (!val) this._conflictos = [];
		this._conflictos.forEach(c => {
			if (c.conflictos.length == 0) {
				c.tipo = Conflicto.PRECIO_OK;
				c.acciones = [];
			} else if (c.conflictos.length == 1) {
				c.tipo = Conflicto.PRECIO_WARNING;
				c.acciones = [{ value: ConflictosAlquilerComponent.CANCELAR, title: 'No se guardará el nuevo listado', icon: 'fa fa-ban' }];
				c.accionATomar = null;

				c.acciones.push(
					{ value: ConflictosAlquilerComponent.REEMPLAZAR, title: 'Se deshabilitará el listado conflictivo dejando solo el nuevo', icon: 'fa fa-exchange' });
				c.accionATomar = ConflictosAlquilerComponent.REEMPLAZAR;
				if (!this.incluye(c.aComparar, c.conflictos[0])) {
					c.acciones.push({ value: ConflictosAlquilerComponent.ACOTAR, title: 'Acotar el listado conflictivo limitandolo según el nuevo. El listado más antiguo quedará con fecha \'hasta\' igual a la fecha \'desde\' del más', icon: 'fa fa-scissors' });
					c.accionATomar = ConflictosAlquilerComponent.ACOTAR;
				}

			} else {
				c.tipo = Conflicto.PRECIO_ERROR;
				c.acciones = [{ value: ConflictosAlquilerComponent.CANCELAR, title: 'No se guardará el nuevo listado', icon: 'fa fa-ban' }];
				c.accionATomar = ConflictosAlquilerComponent.CANCELAR;
			}
		});
		this._conflictos = [...this._conflictos];
	};

	private _conflictos: Conflicto<ConfiguracionListadoPrecioAlquiler>[] = [];
	public get conflictos(): Conflicto<ConfiguracionListadoPrecioAlquiler>[] {
		return this._conflictos;
	}


	public incluye(c: ConfiguracionListadoPrecioAlquiler, p: ConfiguracionListadoPrecioAlquiler) {
		return c.fechaDesde <= p.fechaDesde && (!c.fechaHasta || (p.fechaHasta && c.fechaHasta >= p.fechaHasta));
	}
	public procesadorAcciones: Array<(en: Conflicto<ConfiguracionListadoPrecioAlquiler>) => ConfiguracionListadoPrecioAlquiler[]> = [];

	ngOnInit() {

		this.procesadorAcciones[ConflictosAlquilerComponent.REEMPLAZAR] = this.procesarReemplazar;
		this.procesadorAcciones[ConflictosAlquilerComponent.ACOTAR] = this.procesarAjustar;
		this.procesadorAcciones[ConflictosAlquilerComponent.CANCELAR] = this.procesarCancelar;
	}

	public procesarCancelar(c: Conflicto<ConfiguracionListadoPrecioAlquiler>): ConfiguracionListadoPrecioAlquiler[] {
		return []
	}
	public procesarReemplazar(c: Conflicto<ConfiguracionListadoPrecioAlquiler>): ConfiguracionListadoPrecioAlquiler[] {

		let res: ConfiguracionListadoPrecioAlquiler[] = [];
		res.push(c.aComparar);
		c.conflictos.forEach(conf => { conf.activo = false; res.push(conf) });
		return res;
	}
	public procesarAjustar(c: Conflicto<ConfiguracionListadoPrecioAlquiler>): ConfiguracionListadoPrecioAlquiler[] {
		let res: ConfiguracionListadoPrecioAlquiler[] = [];
		res.push(c.aComparar);
		res = res.concat(c.conflictos);
		res = res.sort((a, b) => !a.activo && b.activo ? 1 : ((a.activo && !b.activo) ? -1 : (a.fechaDesde < b.fechaDesde ? -1 : 1)));

		res.forEach((v, i) => {
			if (i != (res.length - 1)) {
				v.fechaHasta = v === c.aComparar ? v.fechaHasta : moment(res[i + 1].fechaDesde).endOf("day").add('days', -1).toDate();
			}
			if (i != 0) {
				v.fechaDesde = v === c.aComparar ? v.fechaDesde : res[i - 1].fechaHasta ? moment(res[i - 1].fechaHasta).startOf("day").add('days', 1).toDate() : v.fechaHasta;
			}

		})

		return res;
	}
	public procesarCambios() {
		let reproceso: ConfiguracionListadoPrecioAlquiler[] = [];
		this.addLoadingCount();
		this.conflictos.forEach(p => {
			if (p.accionATomar) {
				reproceso = reproceso.concat(this.procesadorAcciones[p.accionATomar](p));
			} else {

			}
		});

		if (reproceso.length == 0) {
			this.warning("No se realizaron cambios");
			this.susLoadingCount();
			this.finConflictos.emit([]);
		} else {
			let $this = this;
			this.preciosService.verificarListados(reproceso).then((r) => {
				if (r.some(c => c.conflictos.length > 0)) {
					$this.conflictos = [...r];
					if ($this.autoResolverMerge && $this.conflictos.every(c => c.accionATomar === ConflictosAlquilerComponent.ACOTAR)) {
						$this.procesarCambios();
					}
				} else {
					let promises: Promise<ConfiguracionListadoPrecioAlquiler>[] = [];
					reproceso.forEach(rr => {
						promises.push($this.preciosService.guardarListado(rr));
					});
					Promise.all(promises).catch(this.errorHandler).then(rrr => {
						this.finConflictos.emit(reproceso);
					})


				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}


	}
	public procesar() {

	}

	public cancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: 'genConf',
			header: "Cancelar",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				$this.onCancelarConflictos.emit(true);

			}
		});
	}

}


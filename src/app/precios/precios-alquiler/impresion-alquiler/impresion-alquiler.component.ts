
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfiguracionListadoPrecioAlquiler } from 'app/model/ConfiguracionListadoPrecioAlquiler';
import { PreciosAlquilerService } from 'app/precios/precios-alquiler.service';
import * as moment from 'moment';
import { SessionComponent } from './../../../session-component.component';
import { CalculardoraAlquiler } from './../../model/CalculadoraAlquiler';
import { PrecioAlquiler } from './../../model/PrecioAlquiler';

@Component({
	selector: 'impresion-menu',
	templateUrl: 'impresion-alquiler.component.html',
	styleUrls: ['impresion-alquiler.component.less']
})

export class ImpresionAlquilerComponent extends SessionComponent implements OnInit {

	public years: YearPrecio[] = [];
	public fechaDesde: Date = moment().startOf("year").startOf("day").toDate();
	public fechaHasta: Date = moment().endOf("year").endOf("day").toDate();
	public now: Date = new Date();
	@Input()
	public set listado(val: ConfiguracionListadoPrecioAlquiler) {
		this._listado = val;
		this.generarCalculadora().then(c => {
			if (c) {
				this.years = [...this.calcularPrecios()];
				this.fechaDesde = moment(val.fechaDesde).startOf("year").startOf("day").toDate();
				this.fechaHasta = moment(val.fechaHasta ? val.fechaHasta : val.fechaDesde).endOf("year").endOf("day").toDate();
			} else {
				this.years = [];
			}


		})

	}
	public get listado(): ConfiguracionListadoPrecioAlquiler {
		return this._listado;
	}

	private _listado: ConfiguracionListadoPrecioAlquiler;

	private calculadora: CalculardoraAlquiler = new CalculardoraAlquiler();
	constructor(private route: ActivatedRoute,
		private preciosService: PreciosAlquilerService, ) {
		super("impresion-menu")
	}
	ngOnInit() {
		moment.locale("es");
		let $this = this;
		this.route.queryParams.subscribe(p => {
			if (p && p["listado"]) {
				$this.addLoadingCount();
				$this.preciosService.getById(p["listado"]).then(r => {
					$this.listado = r;
					$this.susLoadingCount();
				}).catch(this.errorHandler);

			}
		})

	}
	private generarCalculadora(): Promise<boolean | void> {
		let $this = this;
		$this.addLoadingCount();
		if (!this.listado) return Promise.resolve(false);
		return $this.preciosService.getByPeriodo(this.listado.id, this.fechaDesde, this.fechaHasta).then(r => {
			$this.calculadora = new CalculardoraAlquiler(r);
			$this.susLoadingCount();
			return Promise.resolve(true);
		}).catch(this.errorHandler);
	}
	public calcularPrecios(): YearPrecio[] {
		let precios: YearPrecio[] = [];
		let dateStart = moment(this.fechaDesde).startOf("year");
		var dateEnd = moment(this.fechaHasta).endOf("year");
		let year: string = "";
		if (!dateEnd) {
			this.error("Debe indicar una fecha de fin");
			return [];
		}
		let y: YearPrecio;
		while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {

			if (year !== dateStart.format('Y')) {
				y = new YearPrecio();
				y.year = dateStart.format('Y');
				precios.push(y);
				year = dateStart.format("Y");

			}
			y.preciosSocial = [new CellContent(0, "Periodo", 1, 1, 2, 1, "titulo")];
			this.diasSeman.forEach((d, col) => {
				y.preciosSocial.push(new CellContent(0, d, col + 2, 1, 2, 1, "titulo dia"));
			})
			let periodosTemp = [];
			this.listado.precios.filter(p => p.tipoPrecio == 'S').forEach((p, row) => {
				periodosTemp.push(new CellContent(0, p.periodoStr, 1, 1, row + 3, 1, "periodo"));
				let previo = null;
				PrecioAlquiler.DIAS.forEach((d, col) => {
					if (col > 0 && p[PrecioAlquiler.DIAS[col - 1]] == p[d]) {
						previo.colSpan++;
					} else {
						previo = new CellContent(p[d], null, col + 2, 1, row + 3, 1, 'valores');
						previo.precio = p;
						periodosTemp.push(previo);
					}

				});
			})
			y.preciosSocial.push(...periodosTemp.filter(p => p.class != 'valores'));
			periodosTemp = periodosTemp.filter(p => p.class == 'valores');
			periodosTemp.forEach(precio => {
				let current = y.preciosSocial.filter(p => p.class == 'valores'
					&& p.valor === precio.valor && p.startCol == precio.startCol
					&& p.colSpan === precio.colSpan
					&& p.containsRow(precio.startRow))[0];

				if (!current) {
					current = precio;
					y.preciosSocial.push(current);
				}
				if (periodosTemp.filter(p => p.class == 'valores'
					&& p.valor === precio.valor
					&& p.startCol == precio.startCol
					&& p.startRow == precio.startRow + 1
					&& p.colSpan === precio.colSpan)[0]) {

					current.rowSpan++;
				}
			});

			y.preciosJornada = [new CellContent(0, "Periodo", 1, 2, 2, 1, "titulo")];



			let count = 0;
			let periodos = [];
			let cantPersonas = [];
			this.listado.preciosJornada.forEach(p => {
				if (periodos.indexOf(p.periodoStr) < 0)
					periodos.push(p.periodoStr);
				if (cantPersonas.indexOf(p.personas) < 0)
					cantPersonas.push(p.personas);
			})
			cantPersonas.forEach((p, i) => {
				let col = i + 3 + "";
				y.preciosJornada.push(
					new CellContent(0, p, i + 3, 1, 2, 1, "dia titulo"));

			});
			y.colsTemplate = "repeat(" + (cantPersonas.length + 2) + ",1fr)";
			periodos.forEach((periodo, row) => {
				y.preciosJornada.push(
					new CellContent(0, periodo, 1, 1, row + 3, 1, "periodo"));

				if (row % 2 == 0) {
					y.preciosJornada.push(
						new CellContent(0, "Lunes a Jueves", 2, 1, row + 3, 1, "lunesjueves"));
				} else {

					y.preciosJornada.push(
						new CellContent(0, "Viernes", 2, 1, row + 3, 1, "viernes"));
				}
				cantPersonas.forEach((cc, col) => {
					let precio = this.listado.preciosJornada.filter(p => p.periodoStr === periodo && p.personas == cc)[0];
					if (col % 2 == 0) {
						y.preciosJornada.push(
							new CellContent(precio.lunesAJueves, null, col + 3, 1, row + 3, 1, "valores"));
					} else {
						y.preciosJornada.push(
							new CellContent(precio.viernes, null, col + 3, 1, row + 3, 1, "valores"));
					}
				})
				count++;
				if (count > (cantPersonas.length + 2)) count = 0;
			})
			dateStart = dateStart.add(1, "year");
		}
		console.log(precios);
		return precios;
	}
	public get rangoFechas() {
		return moment(this.fechaDesde).format("DD/MM/YYYY") + (this.fechaHasta ? " - " + moment(this.fechaHasta).format("DD/MM/YYYY") : " en adelante");
	}

	public get diasSeman(): string[] {
		return ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
	}
}

export class YearPrecio {
	public year: string;
	public preciosSocial: CellContent[] = [];
	public preciosJornada: CellContent[] = [];
	public maxCols = 0;
	public colsTemplate: string;

}

export class CellContent {
	public _startCol?: number;
	public _colSpan: number = 1;
	public _startRow?: number;
	public _rowSpan: number = 1;
	public col: string = "";
	public row: string = "";
	public gridArea = "";
	public precio: PrecioAlquiler;
	constructor(
		public valor?: number,
		public label?: string,
		startCol?: number,
		colSpan: number = 1,
		startRow?: number,
		rowSpan: number = 1,
		public styleClass?: string,
		precio?: PrecioAlquiler
	) {
		this.colSpan = colSpan;
		this.rowSpan = rowSpan;
		this.startRow = startRow;
		this.startCol = startCol;
		this.precio = precio;
	}
	public get startCol() {
		return this._startCol;
	}

	public set startCol(val: number) {
		this._startCol = val;
		this.updateCol();
	}


	public get colSpan() {
		return this._colSpan;
	}
	public set colSpan(val: number) {
		this._colSpan = val;
		this.updateCol();

	}

	public get startRow() {
		return this._startRow;
	}

	public set startRow(val: number) {
		this._startRow = val;
		this.updateRow();
	}


	public get rowSpan() {
		return this._rowSpan;
	}
	public set rowSpan(val: number) {
		this._rowSpan = val;
		this.updateRow();

	}

	private updateCol() {
		this.col = this.startCol + " / span " + this.colSpan;
		this.updateArea();
	}

	private updateRow() {
		this.row = this.startRow + " / span " + this.rowSpan;
		this.updateArea();
	}
	private updateArea() {
		this.gridArea = this.col + " / " + this.row;
	}


	get class(): string {
		return this.styleClass;
	}

	public containsRow(row: number) {
		return this._startRow <= row && (this._startRow + this._rowSpan) >= row;
	}

	/*public containsCol(cell:CellContent){
		return this._startCol <= col && (this._startCol+this._colSpan) >=col ;
	}*/



}

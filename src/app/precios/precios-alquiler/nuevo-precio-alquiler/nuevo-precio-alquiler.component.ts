import { Component, Input, OnInit } from '@angular/core';
import { Descriptivo } from 'app/model/Descriptivo';
import { ListaPrecio } from 'app/model/ListaPrecio';
import { ListaPrecioService } from 'app/parametricos/lista-precios/service/lista-precios.service';
import { ProductoBase } from 'app/precios/model/ProductoBase';
import { NuevoPrecioComponent } from 'app/precios/nuevo-precio.component';
import { PreciosAlquilerService } from 'app/precios/precios-alquiler.service';
import { ProductoService } from 'app/producto/service/producto.service';
import * as moment from 'moment';
import { ConfiguracionListadoPrecioAlquiler } from './../../../model/ConfiguracionListadoPrecioAlquiler';
import { Producto } from './../../../model/Producto';
import { SalonService } from './../../../salon/service/salon.service';
import { Conflicto } from './../../model/Conflicto';
import { PrecioAlquiler } from './../../model/PrecioAlquiler';
import { PrecioMenu } from './../../model/PrecioMenu';


@Component({
	selector: 'nuevo-precio-alquiler',
	templateUrl: 'nuevo-precio-alquiler.component.html',
	styleUrls: ['nuevo-precio-alquiler.component.less', '../../precios.less'],
})

export class NuevoPrecioAlquilerComponent extends NuevoPrecioComponent<PrecioAlquiler> implements OnInit {

	public tiposOperacion = [{ value: PrecioMenu.ADICIONAL, icon: "fa fa-plus" },
	{ value: PrecioMenu.VALOR, icon: "fa fa-pencil" },
	{ value: PrecioMenu.PORCENTAJE, icon: "fa fa-percent" }];
	public tipoOperacionSocial: string = this.tiposOperacion[0].value;
	public tipoOperacionJornada: string = this.tiposOperacion[0].value;
	constructor(productoService: ProductoService,
		private salonesService: SalonService,
		protected preciosService: PreciosAlquilerService,
		listaPreciosService: ListaPrecioService) {
		super(productoService, listaPreciosService);

	}
	public mostrarProductosSocial: boolean = false;
	public mostrarProductosJornada: boolean = false;
	public preciosMarcadosSocial: PrecioAlquiler[] = [];
	public preciosMarcadosJornada: PrecioAlquiler[] = [];
	public productosSeleccionadosSocial: ProductoBase[] = [];
	public productosSeleccionadosJornada: ProductoBase[] = [];
	public listadosPadre: ListaPrecio[] = [];
	private _tListadoSeleccionado: ListaPrecio;
	public conflictosAlquiler: Conflicto<ConfiguracionListadoPrecioAlquiler>[] = [];
	public startYear: Date = moment(new Date()).startOf("year").toDate();
	public endYear: Date = moment(new Date()).endOf("year").toDate();

	@Input()
	public operacion: string = "N";
	public _listado: ConfiguracionListadoPrecioAlquiler = new ConfiguracionListadoPrecioAlquiler();
	public mesesBaja: Descriptivo[] = [];
	public meses: Descriptivo[] = [];
	public pIncremento: number = 3.5;
	public pFijo: number = 30;
	public pVariable: number = 70;
	public pAdicional: number = 0;
	public valorOperacionSocial: number = 0;
	public valorOperacionJornada: number = 0;
	public salones: Descriptivo[] = [];
	private _salonSeleccionado: Descriptivo;

	ngOnInit() {
		super.ngOnInit();

		this.locale["es"].monthNames.forEach((m, i) => {
			this.meses.push(new Descriptivo(i + 1 + "", m));
		});
		this.addLoadingCount();
		this.salonesService.getDescriptivos("/descriptivos/salones").then(r => {
			this.salones = r;
			this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	@Input()
	public set listado(val: ConfiguracionListadoPrecioAlquiler) {
		this.preciosMarcadosSocial = [];
		this.preciosMarcadosJornada = [];
		this.productosSeleccionadosJornada = [];
		this.productosSeleccionadosSocial = [];
		if (val) {
			this._listado = ConfiguracionListadoPrecioAlquiler.fromData(val);

			this.tListadoSeleccionado = this._listado.listaPrecio;
			this.salonSeleccionado = this._listado.salon;
			this.fechaDesde = this._listado.fechaDesde;
			this.fechaHasta = this._listado.fechaHasta;
			this.productosSeleccionadosSocial = this._listado.precios.filter(p => p.tipoPrecio == PrecioAlquiler.SOCIAL)
				.map(p => new ProductoBase(p.producto, 0, "V", p.valor, p.tipoPrecio));
			this.productosSeleccionadosJornada = this._listado.precios.filter(p => p.tipoPrecio == PrecioAlquiler.JORNADA)
				.map(p => new ProductoBase(p.producto, 0, "V", p.valor, p.tipoPrecio));
			this.productosBase = this.productosBase
				.filter(p => !this.productosSeleccionadosJornada.some(pp => pp.producto.codigo == p.producto.codigo)
					&& !this.productosSeleccionadosSocial.some(pp => pp.producto.codigo == p.producto.codigo));

		} else {
			this._listado = new ConfiguracionListadoPrecioAlquiler();
			this.productosSeleccionadosSocial = [];
			this.productosSeleccionadosJornada = [];

			this.tListadoSeleccionado = null;
			this.salonSeleccionado = null;
			this.mesesBaja = [];
			this.fechaDesde = new Date();
			this.fechaHasta = null;
			this.pVariable = 30;
			this.pIncremento = 3.5;
		}
	}


	public get listado(): ConfiguracionListadoPrecioAlquiler {
		return this._listado
	}

	public nuevoPrecio(p: Producto, tipo: string) {
		let precio = new PrecioAlquiler(null, p, null, null, new Date(), true);
		precio.tipoPrecio = tipo;
		this.listado.agregarPrecios([
			precio
		])
	}
	public quitarPrecios(p: PrecioAlquiler[]) {
		this.listado.quitarPrecios(p);
	}
	public quitarPrecio(p: PrecioAlquiler) {
		this.quitarPrecios([p]);
	}
	public get salonSeleccionado() {
		return this._salonSeleccionado;
	}
	public set salonSeleccionado(d: Descriptivo) {
		this._salonSeleccionado = d;
	}
	public set fechaDesde(val: Date) {
		this._fechaDesde = val;
		if (val != this._listado.fechaDesde) {
			this._listado.fechaDesde = val;
		}

	}

	public set fechaHasta(val: Date) {
		this._fechaHasta = val;
		if (val != this._listado.fechaHasta) {
			this._listado.fechaHasta = val;
		}

	}
	public agregarProductoSocial(event: any) {
		if (event.items) {
			event.items.forEach(i => {
				this.listado.agregarPrecios(this.crearDefaultNuevoProductoSocial(i));
			})
		}
	}
	public quitarProducto(event: any) {
		if (event.items) {
			event.items.forEach(i => {
				this.listado.quitarPorProducto(i.producto);
			})
		}
	}


	public agregarProductoJornada(event: any) {
		if (event.items) {
			event.items.forEach(i => {
				this.listado.agregarPrecios(this.crearDefaultNuevoProductoJornada(i));
			})
		}
	}


	public conflictoPeriodos(rowData, rowIndex) {
		let $this: any = this;
		return $this.dataToRender.some((d, i) => i != rowIndex && d.producto.codigo == rowData.producto.codigo && d.incluye(rowData)) ? "conflicto" : "";
	}

	private crearDefaultNuevoProductoJornada(p: ProductoBase) {
		let precios: PrecioAlquiler[] = [];

		let fechaDesde: Date = moment(new Date()).set('month', 0).startOf("month").toDate();
		let fechaHasta: Date = moment(fechaDesde).set('month', 5).endOf("month").toDate();
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 50, 99));
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 100, 149));
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 150, 199));
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 200, 399));

		fechaDesde = moment(fechaDesde).set('month', 6).startOf("month").toDate();
		fechaHasta = moment(fechaDesde).set('month', 11).endOf("month").toDate();

		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 50, 99));
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 100, 149));
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 150, 199));
		precios.push(new PrecioAlquiler(null, p.producto,
			fechaDesde, fechaHasta, new Date(), true, this.getUserDescriptivo(), [], null, PrecioAlquiler.JORNADA, 0, 0, 0, 0, 0, 0, 0, 200, 399));



		return precios;
	}

	private crearDefaultNuevoProductoSocial(p: ProductoBase) {
		let precios: PrecioAlquiler[] = [];
		for (let index = 0; index < 12; index++) {
			if (index == 10) {
				precios.push(new PrecioAlquiler(null, p.producto,
					moment(new Date()).set('month', index).startOf("month").toDate(),
					moment(new Date()).set('month', index).set('day', 15).toDate(),
					new Date(),
					true,
					this.getUserDescriptivo(),
					[],
					null,
					PrecioAlquiler.SOCIAL));

				precios.push(new PrecioAlquiler(null, p.producto,
					moment(new Date()).set('month', 10).set('day', 16).toDate(),
					moment(new Date()).set('month', 10).endOf("month").toDate(),
					new Date(),
					true,
					this.getUserDescriptivo(),
					[],
					null,
					PrecioAlquiler.SOCIAL));
			} else {
				precios.push(new PrecioAlquiler(null, p.producto,
					moment(new Date()).set('month', index).startOf("month").toDate(),
					moment(new Date()).set('month', index).endOf("month").toDate(),
					new Date(),
					true,
					this.getUserDescriptivo(),
					[],
					null,
					PrecioAlquiler.SOCIAL));
			}

		}
		return precios;
	}

	public set tListadoSeleccionado(val: ListaPrecio) {
		if (val != this._tListadoSeleccionado) {
			this._tListadoSeleccionado = val;
		}


	}
	public get tListadoSeleccionado(): ListaPrecio {
		return this._tListadoSeleccionado;
	}

	public preview() {

	}
	public generarPrecios() {
		let $this = this;
		if (this.esValido()) {
			this.addLoadingCount();
			this.listado.salon = this.salonSeleccionado;
			this.listado.listaPrecio = this.tListadoSeleccionado;
			this.preciosService.verificarListados([this._listado]).then((r: Conflicto<ConfiguracionListadoPrecioAlquiler>[]) => {
				if (r.some(c => c.conflictos.length > 0)) {
					$this.conflictosAlquiler = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
					$this.susLoadingCount();
				} else {
					$this.preciosService.guardarListado(this.listado).then(r => {
						$this.success("Listado Guardado");
						$this.onGuardado.emit(this.listado.precios);
						$this.susLoadingCount();
					}).catch(this.errorHandler);

				}

			}).catch(this.errorHandler);


		}
	}


	public esValido(): boolean {

		let estado: boolean = true;
		if (this.listado.precios.length == 0) {
			this.error("Seleccione al menos 1 producto");
			estado = false;
		}
		if (!this.salonSeleccionado) {
			this.error("Seleccione un Salón");
			estado = false;
		}

		if (!this.tListadoSeleccionado) {
			this.error("Seleccione un Tipo de Listado");
			estado = false;
		}
		if (this.listado.fechaDesde == undefined) {
			this.error("Indique una fecha Desde");
			estado = false;
		} else if (this.listado.fechaHasta != undefined && moment(this.listado.fechaHasta).isBefore(this.listado.fechaDesde)) {
			this.error("La fecha Hasta debe ser mayor a Desde");
			estado = false;
		}
		return estado;
	}
	public getBase(item: PrecioAlquiler) {
		return 0;
	}
	public agregarProductosSocial() {
		this.mostrarProductosSocial = true;
	}
	public agregarProductosJornada() {
		this.mostrarProductosJornada = true;
	}
	private getValorOperacion(tipo: string, valor: number, valorOperacion) {
		if (tipo == PrecioMenu.ADICIONAL) {
			return valor + valorOperacion;
		} else if (tipo == PrecioMenu.PORCENTAJE) {
			return Math.ceil(valor * (1 + valorOperacion / 100));
		} else {
			return valorOperacion;
		}

	}

	public aplicarOperacion(tipo: string) {

		if ((this.tipoOperacionSocial && tipo == PrecioAlquiler.SOCIAL) || (this.tipoOperacionJornada && tipo == PrecioAlquiler.JORNADA)) {
			let vOperacion = tipo === PrecioAlquiler.SOCIAL ? this.valorOperacionSocial : this.valorOperacionJornada;
			let tOperacion = tipo === PrecioAlquiler.SOCIAL ? this.tipoOperacionSocial : this.tipoOperacionJornada;
			this.listado.precios.filter(p => p.tipoPrecio == tipo).forEach(p => {
				p.lunes = this.getValorOperacion(tOperacion, p.lunes, vOperacion);
				p.martes = this.getValorOperacion(tOperacion, p.martes, vOperacion);
				p.miercoles = this.getValorOperacion(tOperacion, p.miercoles, vOperacion);
				p.jueves = this.getValorOperacion(tOperacion, p.jueves, vOperacion);
				p.viernes = this.getValorOperacion(tOperacion, p.viernes, vOperacion);
				p.sabado = this.getValorOperacion(tOperacion, p.sabado, vOperacion);
				p.domingo = this.getValorOperacion(tOperacion, p.domingo, vOperacion);
			})

		}

	}


}
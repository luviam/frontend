import { PreciosService } from 'app/precios/precios.service';
import { Descriptivo } from 'app/model/Descriptivo';
import { FiltroPrecio } from './model/FiltroPercio';
import { ConflictoPrecioBebida } from './model/ConflictoPrecioBebida';
import { HttpParams } from '@angular/common/http';
import { ServicioAbstract } from "app/common-services/service.service";
import { Precio } from "./model/Precio";
import { ValidacionPrecios } from "./model/ValidacionPrecios";
import { PrecioBebida } from "./model/PrecioBebida";
import * as moment from 'moment';
import { Injectable } from '@angular/core';
@Injectable()
export class PreciosBebidasService extends PreciosService<PrecioBebida> {
    public fromData(data: any): PrecioBebida {
        return PrecioBebida.fromData(data);
    }
    public baseUrl: string = "preciobebidas";



    public parseConflictos(r: any): Promise<ConflictoPrecioBebida[]> {
        if (!r.respuesta) return null; return r.respuesta.map(r => ConflictoPrecioBebida.fromData(r))
    }

}
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ServicioAbstract } from 'app/common-services/service.service';
import { ConfiguracionListadoPrecioMenu } from 'app/model/ConfiguracionListadoPrecioMenu';
import * as moment from 'moment';
import { Conflicto } from './model/Conflicto';
import { FiltroPrecio } from './model/FiltroPercio';

@Injectable()
export class PreciosMenuService extends ServicioAbstract {
    public fromData(data: any): ConfiguracionListadoPrecioMenu {
        return ConfiguracionListadoPrecioMenu.fromData(data);
    }
    public baseUrl: string = "configuracionpreciomenu";

    public delete(id: any): Promise<any> {
        let params = new HttpParams();
        params = params.append('id', id)
        return this.http.delete(this.getApiURL() + this.baseUrl, { params }).toPromise().then(this.handleOk, this.handleError);
    }

    public parseConflictos(r: any): Promise<Conflicto<ConfiguracionListadoPrecioMenu>[]> {
        if (!r.respuesta) return null; return r.respuesta.map(d => {
            let c = new Conflicto<ConfiguracionListadoPrecioMenu>(
                ConfiguracionListadoPrecioMenu.fromData(d.aComparar),
                d.conflictos ? d.conflictos.map(cc => ConfiguracionListadoPrecioMenu.fromData(cc)) : []
            );
            return c;

        })

    }

    public getById(id: number): Promise<ConfiguracionListadoPrecioMenu> {

        return this.http.get(this.getApiURL() + this.baseUrl + "/" + id).toPromise().then((r: any) => {
            if (r && r.respuesta) {
                return ConfiguracionListadoPrecioMenu.fromData(r.respuesta);
            }
            return null;
        }).catch(this.handleError);
    }
    public getByPeriodo(id: number, fechaDesde: Date, fechaHasta: Date): Promise<ConfiguracionListadoPrecioMenu[]> {

        return this.http.get(this.getApiURL() + this.baseUrl + "/byPeriodo/" + id + "/" + moment(fechaDesde).format("DDMMYYYY") + "/" + moment(fechaHasta).format("DDMMYYYY")).toPromise().then((r: any) => {
            if (r && r.respuesta) {
                return r.respuesta.map(m => ConfiguracionListadoPrecioMenu.fromData(m));
            }
            return null;
        }).catch(this.handleError);
    }
    public getByPeriodoYCalculo(tipoListado: string, tipoCalculo: string, fechaDesde: Date, fechaHasta: Date): Promise<ConfiguracionListadoPrecioMenu[]> {

        return this.http.get(this.getApiURL() + this.baseUrl + "/byPadre/" + tipoListado + "/" + tipoCalculo + "/" + moment(fechaDesde).format("DDMMYYYY") + "/" + moment(fechaHasta).format("DDMMYYYY")).toPromise().then((r: any) => {
            if (r && r.respuesta) {
                return r.respuesta.map(m => ConfiguracionListadoPrecioMenu.fromData(m));
            }
            return null;
        }).catch(this.handleError);
    }

    public aprobar(listado: ConfiguracionListadoPrecioMenu): Promise<ConfiguracionListadoPrecioMenu> {
        return this.http.post(this.getApiURL() + this.baseUrl + "/aprobar", listado).toPromise().then(this.parseVo, this.handleError);
    }
    public guardarListado(listado: ConfiguracionListadoPrecioMenu): Promise<ConfiguracionListadoPrecioMenu> {
        if (!listado.id) {
            return this.http.put(this.getApiURL() + this.baseUrl, listado).toPromise().then(this.parseVo, this.handleError);
        }
        else {
            return this.http.post(this.getApiURL() + this.baseUrl, listado).toPromise().then(this.parseVo, this.handleError);
        }

    }

    public verificarListados(listados: ConfiguracionListadoPrecioMenu[]): Promise<Conflicto<ConfiguracionListadoPrecioMenu>[]> {
        return this.http.post(this.getApiURL() + this.baseUrl + "/verificar", listados).toPromise().then(this.parseConflictos, this.handleError);
    }

    public parseVo(r: any): ConfiguracionListadoPrecioMenu {
        r.respuesta;
        if (r && r.respuesta) {
            return ConfiguracionListadoPrecioMenu.fromData(r.respuesta);
        } else {
            return null;
        }
    }

    public getAll(filtro: FiltroPrecio): Promise<ConfiguracionListadoPrecioMenu[]> {

        return this.http.post(this.getApiURL() + this.baseUrl + "/all", filtro.getJSONclean()).toPromise().then((r: any) => {
            r.respuesta;
            if (r && r.respuesta) {
                return r.respuesta.map(p => this.fromData(p));
            } else {
                return [];
            }
        }).catch(this.handleError);
    }

}
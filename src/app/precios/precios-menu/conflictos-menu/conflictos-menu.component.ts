import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfiguracionListadoPrecioMenu } from 'app/model/ConfiguracionListadoPrecioMenu';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from './../../../session-component.component';
import { Conflicto } from './../../model/Conflicto';
import { PreciosMenuService } from './../../precios-menu.service';

@Component({
	selector: 'conflictos-menu',
	templateUrl: 'conflictos-menu.component.html',
	styleUrls: ['../../listados.less', 'conflictos-menu.component.less']
})

export class ConflictosMenuComponent extends SessionComponent implements OnInit {
	public static ACOTAR = "A";
	public static CANCELAR = "C";
	public static MODIFICAR = "M";
	public static REEMPLAZAR = "R";
	constructor(private confirmationService: ConfirmationService,
		private preciosService: PreciosMenuService) {
		super("conflictos-menu");
	}
	@Input()
	public autoResolverMerge: boolean = false;

	@Output()
	public finConflictos = new EventEmitter<ConfiguracionListadoPrecioMenu[]>();

	@Output()
	public onCancelarConflictos = new EventEmitter<boolean>();
	@Input()
	public set conflictos(val: Conflicto<ConfiguracionListadoPrecioMenu>[]) {
		this._conflictos = val;
		this._conflictos.forEach(c => {
			if (c.conflictos.length == 0) {
				c.tipo = Conflicto.PRECIO_OK;
				c.acciones = [];
			} else if (c.conflictos.length == 1) {
				c.tipo = Conflicto.PRECIO_WARNING;
				c.acciones = [{ value: ConflictosMenuComponent.CANCELAR, title: 'No se guardará el nuevo listado', icon: 'fa fa-ban' }];
				c.accionATomar = null;

				c.acciones.push(
					{ value: ConflictosMenuComponent.REEMPLAZAR, title: 'Se deshabilitará el listado conflictivo dejando solo el nuevo', icon: 'fa fa-exchange' });
				c.accionATomar = ConflictosMenuComponent.REEMPLAZAR;
				if (!this.incluye(c.aComparar, c.conflictos[0])) {
					c.acciones.push({ value: ConflictosMenuComponent.ACOTAR, title: 'Acotar el listado conflictivo limitandolo según el nuevo. El listado más antiguo quedará con fecha \'hasta\' igual a la fecha \'desde\' del más', icon: 'fa fa-scissors' });
					c.accionATomar = ConflictosMenuComponent.ACOTAR;
				}

			} else {
				c.tipo = Conflicto.PRECIO_ERROR;
				c.acciones = [{ value: ConflictosMenuComponent.CANCELAR, title: 'No se guardará el nuevo listado', icon: 'fa fa-ban' }];
				c.accionATomar = ConflictosMenuComponent.CANCELAR;
			}
		});
		this._conflictos = [...this._conflictos];
	};

	private _conflictos: Conflicto<ConfiguracionListadoPrecioMenu>[] = [];
	public get conflictos(): Conflicto<ConfiguracionListadoPrecioMenu>[] {
		return this._conflictos;
	}


	public incluye(c: ConfiguracionListadoPrecioMenu, p: ConfiguracionListadoPrecioMenu) {
		return c.fechaDesde <= p.fechaDesde && (!c.fechaHasta || (p.fechaHasta && c.fechaHasta >= p.fechaHasta));
	}
	public procesadorAcciones: Array<(en: Conflicto<ConfiguracionListadoPrecioMenu>) => ConfiguracionListadoPrecioMenu[]> = [];

	ngOnInit() {

		this.procesadorAcciones[ConflictosMenuComponent.REEMPLAZAR] = this.procesarReemplazar;
		this.procesadorAcciones[ConflictosMenuComponent.ACOTAR] = this.procesarAjustar;
		this.procesadorAcciones[ConflictosMenuComponent.CANCELAR] = this.procesarCancelar;
	}

	public procesarCancelar(c: Conflicto<ConfiguracionListadoPrecioMenu>): ConfiguracionListadoPrecioMenu[] {
		return []
	}
	public procesarReemplazar(c: Conflicto<ConfiguracionListadoPrecioMenu>): ConfiguracionListadoPrecioMenu[] {

		let res: ConfiguracionListadoPrecioMenu[] = [];
		res.push(c.aComparar);
		c.conflictos.forEach(conf => { conf.activo = false; res.push(conf) });
		return res;
	}
	public procesarAjustar(c: Conflicto<ConfiguracionListadoPrecioMenu>): ConfiguracionListadoPrecioMenu[] {
		let res: ConfiguracionListadoPrecioMenu[] = [];
		res.push(c.aComparar);
		res = res.concat(c.conflictos);
		res = res.sort((a, b) => !a.activo && b.activo ? 1 : ((a.activo && !b.activo) ? -1 : (a.fechaDesde < b.fechaDesde ? -1 : 1)));

		res.forEach((v, i) => {
			if (i != (res.length - 1)) {
				v.fechaHasta = v === c.aComparar ? v.fechaHasta : moment(res[i + 1].fechaDesde).startOf("day").add('days', -1).toDate();
			}
			if (i != 0) {
				v.fechaDesde = v === c.aComparar ? v.fechaDesde : res[i - 1].fechaHasta ? moment(res[i - 1].fechaHasta).startOf("day").add('days', 1).toDate() : v.fechaHasta;
			}

		})

		return res;
	}
	public procesarCambios() {
		let reproceso: ConfiguracionListadoPrecioMenu[] = [];
		this.addLoadingCount();
		this.conflictos.forEach(p => {
			if (p.accionATomar) {
				reproceso = reproceso.concat(this.procesadorAcciones[p.accionATomar](p));
			} else {

			}
		});

		if (reproceso.length == 0) {
			this.warning("No se realizaron cambios");
			this.susLoadingCount();
			this.finConflictos.emit([]);
		} else {
			let $this = this;
			this.preciosService.verificarListados(reproceso).then((r) => {
				if (r.some(c => c.conflictos.length > 0)) {
					$this.conflictos = [...r];
					if ($this.autoResolverMerge && $this.conflictos.every(c => c.accionATomar === ConflictosMenuComponent.ACOTAR)) {
						$this.procesarCambios();
					}
				} else {
					let promises: Promise<ConfiguracionListadoPrecioMenu>[] = [];
					reproceso.forEach(rr => {
						promises.push($this.preciosService.guardarListado(rr));
					});
					Promise.all(promises).catch(this.errorHandler).then(rrr => {
						this.finConflictos.emit(reproceso);
					})


				}
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}


	}
	public procesar() {

	}

	public cancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: 'genConf',
			header: "Cancelar",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				$this.onCancelarConflictos.emit(true);

			}
		});
	}

}


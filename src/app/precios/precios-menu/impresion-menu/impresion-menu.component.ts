import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfiguracionListadoPrecioMenu } from 'app/model/ConfiguracionListadoPrecioMenu';
import { PreciosMenuService } from 'app/precios/precios-menu.service';
import * as moment from 'moment';
import { Calculardora } from '../../model/Calculadora';
import { SessionComponent } from './../../../session-component.component';

@Component({
	selector: 'impresion-menu',
	templateUrl: 'impresion-menu.component.html',
	styleUrls: ['impresion-menu.component.less']
})

export class ImpresionMenuComponent extends SessionComponent implements OnInit {

	public precios: YearPrecio[] = [];
	public fechaDesde: Date = moment().startOf("year").startOf("day").toDate();
	public fechaHasta: Date = moment().endOf("year").endOf("day").add(2, "year").toDate();
	public now: Date = new Date();
	@Input()
	public set listado(val: ConfiguracionListadoPrecioMenu) {
		this._listado = val;
		this.generarCalculadora().then(c => {
			if (c) {
				this.precios = [...this.calcularPrecios()];
			} else {
				this.precios = [];
			}


		})

	}
	public get listado(): ConfiguracionListadoPrecioMenu {
		return this._listado;
	}

	private _listado: ConfiguracionListadoPrecioMenu;

	private calculadora: Calculardora = new Calculardora();
	constructor(private route: ActivatedRoute,
		private preciosService: PreciosMenuService, ) {
		super("impresion-menu")
	}
	ngOnInit() {
		moment.locale("es");
		let $this = this;
		this.route.queryParams.subscribe(p => {
			if (p && p["listado"]) {
				$this.addLoadingCount();
				$this.preciosService.getById(p["listado"]).then(r => {
					$this.listado = r;
					$this.susLoadingCount();
				})

			}
		})

	}
	private generarCalculadora(): Promise<boolean | void> {
		let $this = this;
		$this.addLoadingCount();
		if (!this.listado) return Promise.resolve(false);
		return $this.preciosService.getByPeriodo(this.listado.id, this.fechaDesde, this.fechaHasta).then(r => {
			$this.calculadora = new Calculardora(r);
			$this.susLoadingCount();
			return Promise.resolve(true);
		}).catch(this.errorHandler);
	}
	public calcularPrecios(): YearPrecio[] {
		let precios: YearPrecio[] = [];
		let dateStart = moment(this.fechaDesde).startOf("year");
		var dateEnd = moment(this.fechaHasta).endOf("year");
		let year: string = "";
		if (!dateEnd) {
			this.error("Debe indicar una fecha de fin");
			return [];
		}
		let y: YearPrecio;
		while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {

			if (year !== dateStart.format('Y')) {
				y = new YearPrecio();
				y.year = dateStart.format('Y');
				precios.push(y);
				year = dateStart.format("Y");

			}
			let m: MesPrecio = new MesPrecio()
			m.mes = (dateStart.locale("es").format("MMMM"));
			this.listado.precios.forEach(p => {
				p.producto.codigo

				m.precios.push({
					producto: p.producto.codigo,
					precio: this.calculadora.getPrecio(dateStart.toDate(), p.producto)
				})
			});
			y.meses.push(m);
			dateStart = dateStart.add(1, "month");
		}
		return precios;
	}
	public get rangoFechas() {
		return moment(this.fechaDesde).format("DD/MM/YYYY") + (this.fechaHasta ? " - " + moment(this.fechaHasta).format("DD/MM/YYYY") : " en adelante");
	}
}

export class MesPrecio {
	public mes: string;
	public precios: { precio: number, producto: string }[] = [];

}

export class YearPrecio {
	public year: string;
	public meses: MesPrecio[] = [];

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionListadoPrecioMenu } from 'app/model/ConfiguracionListadoPrecioMenu';
import { Descriptivo } from 'app/model/Descriptivo';
import { GrupoProducto } from 'app/model/GrupoProducto';
import { Producto } from 'app/model/Producto';
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';
import { ListaPrecioService } from 'app/parametricos/lista-precios/service/lista-precios.service';
import { ProductoService } from 'app/producto/service/producto.service';
import { SessionComponent } from 'app/session-component.component';
import * as moment from 'moment';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { AdminPreciosAG } from './../../../authguards/AdminPreciosAG';
import { Marca } from './../../../model/Marca';
import { MarcaService } from './../../../parametricos/marca/service/marca.service';
import { FiltroPrecio } from './../../model/FiltroPercio';
import { PreciosMenuService } from './../../precios-menu.service';

@Component({
	selector: 'listado-precios-menu',
	templateUrl: 'listado-precios-menu.component.html',
	styleUrls: ['listado-precios-menu.component.less', '../../listados.less']
})

export class ListadoPreciosMenuComponent extends SessionComponent implements OnInit {

	public nombre: string = "Precios - Menú";
	public tituloDialogo: string = "";
	public tiposLista: Descriptivo[] = [];
	public filtro: FiltroPrecio = new FiltroPrecio();
	public marcas: Marca[] = [];
	public marcasFiltradas: Marca[] = [];
	protected tipoPrecio: string;
	private grupos: GrupoProducto[] = [];
	public productos: Producto[] = [];
	public productosFiltrados: Producto[] = [];
	public grupo: Descriptivo;
	public listadoEditado: ConfiguracionListadoPrecioMenu;
	public listados: ConfiguracionListadoPrecioMenu[] = [];
	public listadosFiltrados: ConfiguracionListadoPrecioMenu[] = [];

	constructor(
		private preciosService: PreciosMenuService,
		private productoService: ProductoService,
		private marcasService: MarcaService,
		private listaPreciosService: ListaPrecioService,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private router: Router,
		private grupoService: GrupoProductoService,
		private adminPreciosAG: AdminPreciosAG) {
		super("listadoMenu");
	}

	ngOnInit() {
		let $this = this;
		this.grupoService.getAll().then(g => {
			$this.grupos = g;
			this.route.queryParams
				.subscribe(params => {

					if (params.fecha) {
						$this.filtro.fechaVigencia = moment(params.fecha, "YYYYMMDD").toDate();
					}
					if (params.grupo && !$this.grupo) {
						$this.grupo = $this.grupos.filter(g => g.codigo === params.grupo)[0];
						if (!$this.grupo) {
							this.error("No hay grupo seleccionado");
							return;
						}
					} else if (!$this.grupo) {
						this.error("No hay grupo seleccionado");
						return;
					}
					let inits: Promise<any>[] = [];
					inits.push(this.getTipoListaPrecio());
					inits.push(this.getBebidas());
					inits.push(this.getMarcas());
					Promise.all(inits).then(r => {
						if (params.listado) {
							$this.filtro.listaPrecios = $this.tiposLista.filter(l => l.codigo === params.listado);
							$this.getListados();
						}
						this.filtro.stateObs.subscribe(() => {
							this.getListados();
						});
					}).catch($this.errorHandler)


				});
		}).catch(this.errorHandler);
	}
	public getMarcas(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.marcasService.getAll().then((r) => {
			$this.marcas = r;
			$this.marcasFiltradas = [...r];
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public getListados(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.preciosService.getAll(this.filtro).then((r) => {
			$this.listados = r;
			$this.listadosFiltrados = [...r];
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}

	public getTipoListaPrecio(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.listaPreciosService.getAllByGrupo(this.grupo.codigo).then((r) => {
			$this.tiposLista = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler)
	}
	public getBebidas(): Promise<any> {
		let $this = this;
		this.addLoadingCount();
		return this.productoService.getByGrupo(this.grupo.codigo).then((p) => {

			$this.productos = [...p];
			$this.productosFiltrados = [...p];
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	public getProductos(mes: string) {
		return [];
	}
	public volver() {
		this.cancelarEdicion()
	}
	public nuevoListado() {
		this.tituloDialogo = "Nuevo Precio";
		this.goTo("nuevos");
	}
	public finGuardado(event: any) {
		this.listadoEditado = null;
		this.getListados();
		this.volver();
	}
	public vigenciaTodos() {
		this.filtro.fechaVigencia = null;
	}

	public vigenciaHoy() {
		this.filtro.fechaVigencia = new Date();
	}
	public ver(listado: ConfiguracionListadoPrecioMenu) {
		this.router.navigate(["admin/precios/listado/001/imprimir"], {
			queryParams: {
				listado: listado.id
			}
		});
	}

	public heredar(listado: ConfiguracionListadoPrecioMenu) {
		this.tituloDialogo = "Nuevo Listado";
		this.listadoEditado = ConfiguracionListadoPrecioMenu.fromData(listado);
		this.listadoEditado.id = null;
		this.listadoEditado.precios.forEach(p => p.id = null);
		this.listadoEditado.listaPrecio = null;
		this.listadoEditado.listadoPadre = listado.listaPrecio;
		this.listadoEditado.fechaDesde = listado.fechaDesde;
		this.listadoEditado.fechaHasta = listado.fechaHasta;
		this.goTo("nuevos");
	}
	public editar(listado) {
		this.tituloDialogo = "Editar Listado";
		this.listadoEditado = listado;
		this.goTo("nuevos");
	}
	public copiar(listado) {
		this.tituloDialogo = "Editar Listado";
		this.listadoEditado = ConfiguracionListadoPrecioMenu.fromData(listado);
		this.listadoEditado.id = null;
		this.listadoEditado.precios.forEach(p => p.id = null);
		this.listadoEditado.fechaDesde = listado.fechaHasta ? moment(listado.fechaHasta).startOf("day").add(1, "day").toDate() : moment().startOf("day").add(1, "day").toDate();
		this.listadoEditado.fechaHasta = null;
		this.goTo("nuevos");
	}

	public get esAdminPrecios() {
		return this.adminPreciosAG.esVisible();
	}
	public aprobar(listado: ConfiguracionListadoPrecioMenu) {
		this.addLoadingCount();
		this.preciosService.aprobar(listado).then(r => {
			listado.estado = r.estado;
			this.susLoadingCount();
			this.success("Listado aprobado")
		}).catch(this.errorHandler);
	}
	public borrar(listado) {
		let $this = this;
		this.confirmationService.confirm({
			key: 'genConf',
			header: "Eliminar",
			message: '¿Desea eliminar el listado seleciconado?',
			accept: () => {
				this.addLoadingCount();
				let pr: Promise<any>[] = [];
				pr.push(this.preciosService.delete(listado.id));
				Promise.all(pr).then(r => {
					$this.success("Listado Eliminado");
					$this.susLoadingCount();
					$this.getListados();
				}).catch($this.errorHandler);

			}
		});
	}

	public cancelarEdicion(event?: any) {
		this.tituloDialogo = "Precios - Menú";
		this.currentView = "listadoMenu";
	}
}

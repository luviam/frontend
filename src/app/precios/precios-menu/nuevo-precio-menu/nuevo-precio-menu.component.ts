import { Component, Input, OnInit } from '@angular/core';
import { Descriptivo } from 'app/model/Descriptivo';
import { ListaPrecio } from 'app/model/ListaPrecio';
import { ListaPrecioService } from 'app/parametricos/lista-precios/service/lista-precios.service';
import { NuevoPrecioComponent } from 'app/precios/nuevo-precio.component';
import { PreciosMenuService } from 'app/precios/precios-menu.service';
import { ProductoService } from 'app/producto/service/producto.service';
import * as moment from 'moment';
import { SelectItem } from 'primeng/primeng';
import { Calculardora } from '../../model/Calculadora';
import { ConfiguracionListadoPrecioMenu } from './../../../model/ConfiguracionListadoPrecioMenu';
import { Conflicto } from './../../model/Conflicto';
import { PrecioMenu } from './../../model/PrecioMenu';
import { ProductoBase } from './../../model/ProductoBase';


@Component({
	selector: 'nuevo-precio-menu',
	templateUrl: 'nuevo-precio-menu.component.html',
	styleUrls: ['nuevo-precio-menu.component.less', '../../precios.less'],
})

export class NuevoPrecioMenuComponent extends NuevoPrecioComponent<PrecioMenu> implements OnInit {



	constructor(productoService: ProductoService,
		protected preciosService: PreciosMenuService,
		listaPreciosService: ListaPrecioService) {
		super(productoService, listaPreciosService);
		this.tiposOperacion = [{ value: PrecioMenu.ADICIONAL, icon: "fa fa-plus" },
		{ value: PrecioMenu.VALOR, icon: "fa fa-pencil" },
		{ value: PrecioMenu.PORCENTAJE, icon: "fa fa-percent" }];

	}
	public tiposOperacion: SelectItem[] = [];
	public tipoOperacion: string = PrecioMenu.VALOR;
	public listadosPadre: ListaPrecio[] = [];
	private _tListadoSeleccionado: ListaPrecio;
	private _tListadoPadreSeleccionado: ListaPrecio;
	public conflictosMenu: Conflicto<ConfiguracionListadoPrecioMenu>[] = [];
	private calculadora: Calculardora;

	@Input()
	public operacion: string = "N";
	public _listado: ConfiguracionListadoPrecioMenu = new ConfiguracionListadoPrecioMenu();
	public mesesBaja: Descriptivo[] = [];
	public meses: Descriptivo[] = [];
	public pIncremento: number = 3.5;
	public pFijo: number = 70;
	public pVariable: number = 30;
	public pAdicional: number = 0;
	public valorOperacion: number = 0;
	private _esDependiente: boolean = false;
	ngOnInit() {
		super.ngOnInit();

		this.locale["es"].monthNames.forEach((m, i) => {
			this.meses.push(new Descriptivo(i + 1 + "", m));
		});
	}
	@Input()
	public set listado(val: ConfiguracionListadoPrecioMenu) {
		if (val) {
			this._listado = ConfiguracionListadoPrecioMenu.fromData(val);

			this.tListadoSeleccionado = this._listado.listaPrecio;
			this.tListadoPadreSeleccionado = this._listado.listadoPadre;
			this.mesesBaja = this._listado.mesesBaja;
			this.fechaDesde = this._listado.fechaDesde;
			this.fechaHasta = this._listado.fechaHasta;
			this.pVariable = Math.round(this._listado.parteVariable * 10000) / 100;
			this.pIncremento = Math.round(this._listado.incremento * 10000) / 100;
			this.esDependiente = this._listado.listadoPadre != undefined;
			this.productosSeleccionados = this._listado.precios.map(p => new ProductoBase(p.producto, 0, p.tipoOperacion, p.valor));
			this.productosBase = this.productosBase.filter(p => !this.productosSeleccionados.some(pp => pp.producto.codigo == p.producto.codigo));

		} else {
			this._listado = new ConfiguracionListadoPrecioMenu();
			this.productosSeleccionados = [];
			this.tListadoSeleccionado = null;
			this.mesesBaja = [];
			this.fechaDesde = new Date();
			this.fechaHasta = null;
			this.pVariable = 70;
			this.pIncremento = 3.5;
		}
	}


	public get listado(): ConfiguracionListadoPrecioMenu {
		return this._listado
	}


	public get esDependiente(): boolean {
		return this._esDependiente;
	}

	public set esDependiente(val: boolean) {
		this._esDependiente = val;
		if (!val) {
			this.tListadoPadreSeleccionado = null;
		} else {
			this.updateCalculadora();
		}
	}

	public aplicarOperacion() {
		if (this.tipoOperacion) {
			this.productosSeleccionados.forEach(p => {
				p.valor = this.valorOperacion;
				p.tipoOperacion = this.tipoOperacion;
			})
		}

	}

	public tiposCalculo: Descriptivo[] = [
		new Descriptivo("C", "Congelado"),
		new Descriptivo("I", "I. Fijo"),
		new Descriptivo("A", "I. Acumulado"),
	];
	public set tipoCalculoSeleccionado(val: Descriptivo) {
		this._listado.tipoCalculo = val;
		this.updateCalculadora();

	}

	public set fechaDesde(val: Date) {
		this._fechaDesde = val;
		if (val != this._listado.fechaDesde) {
			this._listado.fechaDesde = val;
			this.updateCalculadora();
		}

	}

	public set fechaHasta(val: Date) {
		this._fechaHasta = val;
		if (val != this._listado.fechaHasta) {
			this._listado.fechaHasta = val;
			this.updateCalculadora();
		}

	}
	public get tipoCalculoSeleccionado(): Descriptivo {
		return this._listado.tipoCalculo;
	}
	public get tListadoSeleccionado(): ListaPrecio {
		return this._tListadoSeleccionado;
	}

	public set tListadoSeleccionado(val: ListaPrecio) {
		if (val != this._tListadoSeleccionado) {
			this._tListadoSeleccionado = val;
			this._listado.listaPrecio = val;
			this.updateCalculadora();

		}

	}

	public get tListadoPadreSeleccionado(): ListaPrecio {
		return this._tListadoPadreSeleccionado;
	}

	public set tListadoPadreSeleccionado(val: ListaPrecio) {
		if (val != this._tListadoSeleccionado) {
			this._tListadoPadreSeleccionado = val;
			this._listado.listadoPadre = val;
			this.updateCalculadora();

		}

	}
	public updateCalculadora() {
		if (this.tienePadre) {
			this.generarCalculadora().then(c => {
				if (!this.productosSeleccionados || this.productosSeleccionados.length == 0) {
					this.productosSeleccionados = c.getProductos(this.fechaDesde).map(p => new ProductoBase(p, 0, PrecioMenu.PORCENTAJE, 0))
				}
				this.productosSeleccionados.forEach(p => {
					p.base = c.getPrecio(this._listado.fechaDesde, p.producto);

				})
			})
		}

	}
	private generarCalculadora(): Promise<Calculardora> {
		let $this = this;
		$this.addLoadingCount();

		return $this.preciosService.getByPeriodoYCalculo(this.tListadoPadreSeleccionado.codigo,
			this.tipoCalculoSeleccionado.codigo,
			this.listado.fechaDesde,
			moment(this.listado.fechaDesde).endOf("day").toDate()).then(r => {
				$this.calculadora = new Calculardora(r);
				$this.susLoadingCount();
				return Promise.resolve(this.calculadora);
			}).catch(e => {

				this.errorHandler(e);
				return Promise.reject(null);
			});
	}

	public afterInit() {
		this.updatePadres();
	}
	public updatePadres() {
		this.listadosPadre = this.tListados.filter(r => this.tListadoSeleccionado && this.tListadoSeleccionado.codigo != r.codigo);
	}

	public mesEsBaja(mes: number) {
		return this.mesesBaja.some(m => m.codigo == mes + "");
	}

	public preview() {

	}
	public generarPrecios() {
		let $this = this;
		if (this.esValido()) {
			this.addLoadingCount();
			let diasSemana = [];
			this.diasSeleccionados.forEach(d => {
				diasSemana.push(new Number(d));
			});
			this._listado.parteVariable = this.pVariable / 100;
			this._listado.incremento = this.pIncremento / 100;
			this._listado.adicional = this.pAdicional / 100;
			this._listado.mesesBaja = this.mesesBaja;
			this._listado.precios = this._listado.precios.filter(p => this.productosSeleccionados.some(pp => pp.producto.codigo == p.producto.codigo));
			this.productosSeleccionados.forEach(p => {
				let precio: PrecioMenu = this._listado.getPrecioByProducto(p.producto);
				if (precio) {
					precio.valor = p.valor;
					precio.tipoOperacion = p.tipoOperacion;
				} else {
					this._listado.precios.push(new PrecioMenu(null,
						p.producto, p.base,
						this.listado.fechaDesde, this.listado.fechaHasta,
						new Date(),
						true, this.getCurrentUser().user.descriptivo,
						diasSemana, this.listado.listaPrecio, p.tipoOperacion));
				}
			});

			this.preciosService.verificarListados([this._listado]).then((r: Conflicto<ConfiguracionListadoPrecioMenu>[]) => {
				if (r.some(c => c.conflictos.length > 0)) {
					$this.conflictosMenu = r;
					$this.verConflictos = true;
					$this.error("Se encontraron conflictos");
					$this.susLoadingCount();
				} else {
					$this.preciosService.guardarListado(this.listado).then(r => {
						$this.success("Listado Guardado");
						$this.onGuardado.emit(this.listado.precios);
						$this.susLoadingCount();
					}).catch(this.errorHandler);

				}

			}).catch(this.errorHandler);


		}
	}


	public esValido(): boolean {

		let estado: boolean = true;
		if (this.productosSeleccionados.length == 0) {
			this.error("Seleccione al menos 1 producto");
			estado = false;
		}
		if (!this.tipoCalculoSeleccionado) {
			this.error("Seleccione un tipo de cálculo");
			estado = false;
		} else if (this.pVariable > 100 || this.pVariable <= 0) {
			this.error("La parte Variable debe ser menor a 100 y mayr a 0");
			estado = false;
		}
		if (!this.listado.listaPrecio) {
			this.error("Seleccione un Tipo de Listado");
			estado = false;
		}
		if (this.listado.fechaDesde == undefined) {
			this.error("Indique una fecha Desde");
			estado = false;
		} else if (this.listado.fechaHasta != undefined && moment(this.listado.fechaHasta).isBefore(this.listado.fechaDesde)) {
			this.error("La fecha Hasta debe ser mayor a Desde");
			estado = false;
		}
		return estado;
	}
	public getBase(item: PrecioMenu) {
		return 0;
	}

	public get tienePadre() {
		return this.tListadoPadreSeleccionado && this.tListadoPadreSeleccionado.codigo &&
			this.tListadoSeleccionado && this.tListadoSeleccionado.codigo &&
			this.listado.fechaDesde &&
			this.tipoCalculoSeleccionado && this.tipoCalculoSeleccionado.codigo && this.esDependiente;
	}
}
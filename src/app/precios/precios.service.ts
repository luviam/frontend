import { Injectable } from '@angular/core';
import { FiltroPrecio } from './model/FiltroPercio';
import { HttpParams } from '@angular/common/http';
import { ServicioAbstract } from "app/common-services/service.service";
import { Precio } from "./model/Precio";
import { ValidacionPrecios } from "./model/ValidacionPrecios";
import { PrecioBebida } from "./model/PrecioBebida";
import { VistaListado } from './model/VistaListado';
import { ListaPrecio } from 'app/model/ListaPrecio';


export abstract class  PreciosService<P extends Precio> extends ServicioAbstract{

    public abstract get baseUrl() : string;;

    public abstract fromData(data:any) : P;
    public guardarPrecios(precios : P[]): Promise<ValidacionPrecios<P>[]>{
        precios.forEach(p => p.valor = Math.ceil(p.valor));
        if (precios.every(p => !p.id)) {
            return this.http.put(this.getApiURL() + this.baseUrl, precios).toPromise().then(this.parseConflictos, this.handleError);
        }
        else {
            return this.http.post(this.getApiURL() + this.baseUrl, precios).toPromise().then(this.parseConflictos, this.handleError);
        }
    }
    public getAll(filtro: FiltroPrecio): Promise<P[]> {
        
        return this.http.post(this.getApiURL()+ this.baseUrl+"/all", filtro.getJSONclean()).toPromise().then((r:any)=> {
             r.respuesta;
             if(r && r.respuesta){
                 return r.respuesta.map(p => this.fromData(p));
             }else{
                 return [];
             }
        }).catch(this.handleError);
    }

    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + this.baseUrl, {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(id: number): Promise<P> {
        return this.http.get(this.getApiURL() + this.baseUrl + "/" + id)
            .toPromise()
            .then((r:any) =>{
                if(r && r.respuesta){
                    return this.fromData(r.respuesta);
                }else{
                    return null;
                }
            }).catch(this.handleError);

    }

    public parseConflictos(r :any): Promise<ValidacionPrecios<P>[]>
        {
            if(!r.respuesta) return null; return r.respuesta;
        }
    
    public getVistas(fecha: Date, codigoGrupo: string) :Promise<VistaListado[]>{
     /*   return this.http.get(this.getApiURL() + "vistasListado/" + this.baseUrlGrupo + "/" + fecha)
        .toPromise()
        .then().catch(this.handleError); */
        return Promise.resolve([new VistaListado("0004","Lista 1", new ListaPrecio(1,"0004","shd"),false),
        new VistaListado("0004","Lista 2", new ListaPrecio(1,"0004","shd"),false),
        new VistaListado("0004","Lista 3", new ListaPrecio(2,"0004","shd"),false),
        new VistaListado("0004","Lista 4", new ListaPrecio(3,"0004","shd"),false)]);
    }
}
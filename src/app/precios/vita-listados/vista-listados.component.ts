import { ListaPrecio } from './../../model/ListaPrecio';
import { FiltroPrecio } from '../model/FiltroPercio';
import { ProductoService } from '../../producto/service/producto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GrupoProducto } from '../../model/GrupoProducto';
import { Component, OnInit } from '@angular/core';
import { SessionComponent } from 'app/session-component.component';
import * as moment from 'moment';
import { ListaPrecioService } from 'app/parametricos/lista-precios/service/lista-precios.service';
import { GrupoProductoService } from 'app/parametricos/grupo-producto/service/grupo-producto.service';
import { Producto } from 'app/model/Producto';
import { VistaListado } from '../model/VistaListado';
import { PreciosService } from '../precios.service';
import { Precio } from '../model/Precio';
import { BehaviorSubject } from 'rxjs';

@Component({
	selector: 'vista-listados',
	templateUrl: 'vista-listados.component.html',
	styleUrls: ['vista-listados.component.less']
})

export class VistaListadosComponent extends SessionComponent implements OnInit {

	public filtro : FiltroPrecio = new FiltroPrecio();
	private grupos: GrupoProducto[] = [];
	public vistas : VistaListado[] = [];
	public nuevoListado : boolean = false;
	private _grupoSeleccionado : GrupoProducto;
	public set grupoSeleccionado(val : GrupoProducto){
		this._grupoSeleccionado = val;
		if(val && val.codigo){
			this.updateGrupo();
		}

	}
	public get grupoSeleccionado(): GrupoProducto{
		return this._grupoSeleccionado;
	}
	constructor(private listaPreciosService : ListaPrecioService, 
				private grupoProductoService : GrupoProductoService,
				private route: ActivatedRoute,
				private router: Router){
		super('vistaListaPrecios')
	}
	ngOnInit() {
		this.recargarFiltros();
		this.addLoadingCount();
		
		this.filtro.stateObs.subscribe(f=>{
			if(f){
				this.saveFiltro();
				if(this.grupoSeleccionado)
					this.updateGrupo();
			}
			
		});
		let $this = this
		let init : Promise<any>[] = [];
		init.push(this.grupoProductoService.getAll().then(r => {
			$this.grupos = r;
			this.route.paramMap.subscribe(p =>{
				let codigo = p.get("codigo");
				$this.grupoSeleccionado = $this.grupos.filter(g => g.codigo === codigo)[0];
			});
		}));
		Promise.all(init).then(r => this.susLoadingCount()).catch(this.errorHandler);

	 }

	 public updateGrupo(){
		this.addLoadingCount();
		let $this = this
		let init : Promise<any>[] = [];
		if(this.grupoSeleccionado && this.grupoSeleccionado.codigo){
			init.push(this.listaPreciosService.getVigente(this.grupoSeleccionado.codigo,this.filtro.fechaVigencia).then(r=>{
				$this.vistas = r.map(v => VistaListado.fromData(v));
			}));
			Promise.all(init).then(r => this.susLoadingCount()).catch(this.errorHandler);
		}else{
			this.susLoadingCount();
			this.error("No hay grupo de precios seleccionado");
		}
		
	 }

	

	

	public hoy(){
		this.filtro.fechaVigencia = moment().startOf("day").toDate();
	}


	public editar(vista:VistaListado){
		this.navigate({
			queryParams:{
				grupo:this.grupoSeleccionado.codigo,
				action:'editar',
				listado:vista.codigo,
				fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
			}
		});
	}

	public navegarNuevo(vista:VistaListado){
		this.navigate({
			queryParams:{
				grupo:this.grupoSeleccionado.codigo,
				action:'nuevo',
				listado:vista.codigo,
				fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
			}
		});
	}
	public borrar(vista:VistaListado){
		this.navigate({
			queryParams:{
				grupo:this.grupoSeleccionado.codigo,
				action:'borrar',
				listado:vista.codigo,
				fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
			}
		});
	}
	public ver(vista:VistaListado){
		this.navigate({
			queryParams:{
				grupo:this.grupoSeleccionado.codigo,
				action:'ver',
				listado:vista.codigo,
				fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
			}
		});
	}
	public imprimir(vista:VistaListado){
		this.navigate({
			queryParams:{
				grupo:this.grupoSeleccionado.codigo,
				action:'imprimir',
				listado:vista.codigo,
				fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
			}
		});
	}
	public copiar(vista:VistaListado){
		this.navigate({
			queryParams:{
				grupo:this.grupoSeleccionado.codigo,
				action:'copiar',
				listado:vista.codigo,
				fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
			}
		});
	}
	
	public verTodos(){
		this.navigate({queryParams:{
			grupo:this.grupoSeleccionado.codigo,
			fecha:moment(this.filtro.fechaVigencia? this.filtro.fechaVigencia : new Date()).format("YYYYMMDD")
		}});
	}

	private navigate(query:any){

		if(this.grupoSeleccionado.esAdicional){
			this.router.navigate(['admin/precios/listado/adic'],query);
		}else{
			this.router.navigate(['admin/precios/listado/' + this.grupoSeleccionado.codigo],query);
		}
	
	}

	public crearNueva(){
		this.nuevoListado = true;
	}
	public nuevoGuardado(l : ListaPrecio ){
		this.nuevoListado = false;
		this.navegarNuevo(new VistaListado(l.codigo,l.descripcion,l,false));
	}

	protected getFiltro(){
		return this.filtro;
	}
	protected parseFiltro(o:any){
		if(!o){
			this.hoy();
		}else{
			this.filtro.parse(o);
			
		}
	}
}

import { NgModule } from '@angular/core';




import { ContextMenuModule } from 'primeng/components/contextmenu/contextmenu';
import { InputMaskModule } from 'primeng/components/inputmask/inputmask';
import {TableModule} from 'primeng/table';
import { DataTableModule, 
        SharedModule,
        CheckboxModule,
        ConfirmDialogModule,
        GrowlModule,
        DialogModule,
        CalendarModule,
        AccordionModule,
        PanelModule,
        RadioButtonModule,
        ChartModule,
        MultiSelectModule,
        ScheduleModule,
        AutoCompleteModule,
        TabViewModule,
        ToggleButtonModule,
        PickListModule,
        DataListModule,
        ChipsModule,
        DropdownModule,
        InputSwitchModule,
        MenubarModule,
        TreeTableModule,
        PaginatorModule,
        ConfirmationService, 
        TooltipModule,
        InputTextareaModule,
        StepsModule,
        CardModule,
        MenuModule,
        SelectButtonModule} from 'primeng/primeng';


@NgModule({
    exports: [
    DataTableModule,
    SharedModule,
    CheckboxModule,
    ConfirmDialogModule,
    GrowlModule,
    DialogModule,
    CalendarModule,
    AccordionModule,
    PanelModule,
    RadioButtonModule,
    ChartModule,
    MultiSelectModule,
    ScheduleModule,
    AutoCompleteModule,
    TabViewModule,
    ToggleButtonModule,
    PickListModule,
    DataListModule,
    ChipsModule,
    DropdownModule,
    InputSwitchModule,
    MenubarModule,
    TreeTableModule,
    ContextMenuModule,
    InputMaskModule,
    PaginatorModule,
    TooltipModule,
    InputTextareaModule,
    StepsModule,
    TableModule,
    MenuModule,
    CardModule,
    SelectButtonModule
    
  ],
   providers: [ConfirmationService]
})
export class PrimeNgModule { }
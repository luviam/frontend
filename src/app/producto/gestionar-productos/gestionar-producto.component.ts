import { AgrupadorService } from './../../parametricos/agrupador/service/agrupador.service';
import { MarcaService } from './../../parametricos/marca/service/marca.service';
import { Marca } from './../../model/Marca';
import { TipoMenu } from './../../model/TipoMenu';
import { ParametricoService } from './../../parametricos/services/parametricos.service';
import { ImputacionCuentaConfig } from '../../model/ImputacionCuentaConfig';
import { CentroCostoService } from '../../common-services/centroCostoService.service';
import { CentroCosto } from '../../model/CentroCosto';
import { Producto } from '../../model/Producto';

import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Empleado } from '../../model/Empleado';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from '../../session-component.component';
import { DescriptivosService } from '../../common-services/descriptivos.service';

import { ProductoService } from '../service/producto.service';
import { Descriptivo } from '../../model/Descriptivo';
import { Subproducto } from 'app/model/Subproducto';


@Component({
	selector: 'gestionar-producto',
	templateUrl: './gestionar-producto.component.html',
	styleUrls: ['./gestionar-producto.component.less']
})
export class GestionarProductoComponent extends SessionComponent implements OnInit {
	private _producto: Producto = new Producto();
	private _productoCopia: Producto = new Producto();
	public centros: CentroCosto[] = [];
	public categorias: Descriptivo[] = [];
	public unidades: Descriptivo[] = [];
	public finalizado: boolean = false;
	public marcas: Marca[] = [];
	public marcasFiltradas: Marca[] = [];
	public agrupadores: Descriptivo[] = [];

	@Input()
	public editable: boolean = true;

	public tiposMenu: TipoMenu[] = [];

	@Input()
	set producto(val: Producto) {
		this._producto = val;
		this._productoCopia = Producto.fromData(val);
		this.finalizado = false;
		if (val) {
			this.centros.forEach(c => {
				if (!this._productoCopia.cuentas.some(cta => cta.centroCosto.codigo === c.codigo)) {
					this._productoCopia.cuentas.push(new ImputacionCuentaConfig(null, c, null, null, false));
				}

			});
			if(this.producto && this.producto.categoria){
				this.getAgrupadores(this.producto.categoria);
			}
			
		}
	}

	get producto(): Producto {
		return this._productoCopia;
	}

	@Output()
	public productoChange: EventEmitter<Producto> = new EventEmitter<Producto>();

	@Output()
	public onCancelar: EventEmitter<Producto> = new EventEmitter<Producto>();

	@Output()
	public onGuardar: EventEmitter<Producto> = new EventEmitter<Producto>();

	constructor(
		private service: ProductoService,
		private centorService: CentroCostoService,
		private confirmationService: ConfirmationService,
		private descriptivosService: DescriptivosService,
		private parametricoService: ParametricoService,
		private marcasService: MarcaService,
		private agrupadorService: AgrupadorService,
	) {
		super();
		let $this = this;
		this.messageLife = 1000;
		this.addLoadingCount();
		this.parametricoService.getAll(TipoMenu.ID).then(t => {
			$this.tiposMenu = t;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		this.addLoadingCount();
		this.marcasService.getAll().then((r) => {
			$this.marcas = r;
			$this.marcasFiltradas = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.centorService.getAllCentros().then((r) => {
			$this.centros = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllUnidadesProducto().then((res) => {
			if (res) {
				$this.unidades = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllCategoriasProducto().then((res) => {
			if (res) {
				$this.categorias = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}
	public getAgrupadores(cat: Descriptivo) {
		let $this = this;
		this.addLoadingCount();
		this.agrupadorService.getByCategoria(cat).then((r) => {
			$this.agrupadores = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	ngOnInit() {
		let $this = this;
		this.finalizado = false;
	}

	public confirmarCancelar() {
		this.confirmationService.confirm({
			key: "genConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.onCancelar.emit(this.producto);
			}
		});
	}
	public getCuentasAplicacion(item: ImputacionCuentaConfig) {
		if (item.centroCosto) {
			let centro = this.centros.filter(c => c.codigo === item.centroCosto.codigo)[0];

			return centro ? centro.cuentas : [];
		}
		return [];
	}

	public guardarProducto() {
		let $this = this;
		if (this.productoValido(this.producto)) {
			this.addLoadingCount();

			this.service.guardar(this.producto).then((c) => {
				$this.success("El producto fue guardado correctamente");
				$this.finalizado = true;
				$this.onGuardar.emit(this.producto);
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El producto no es valido");
		}
	}
	public filtrarMarcas(event) {
		this.marcasFiltradas = [];
		this.marcasFiltradas = this.marcas.filter(m => m.descripcion.toLowerCase().indexOf(event.query.toLowerCase()) == 0);
		this.marcasFiltradas.concat(this.marcasFiltradas, this.marcas.filter(m => m.descripcion.toLowerCase().indexOf(event.query.toLowerCase()) > 0));
	}

	public productoValido(producto: Producto): boolean {
		return true;
	}

	public esMenu(): boolean {
		return this.producto.esMenu();
	}
	public tieneMarca(): boolean {
		return !this.esMenu() && !this.producto.esImpuesto() && !this.producto.esInterno() && !this.producto.esOperativoIP();
	}

	public quitarSubproducto(item: Subproducto) {

		this.producto.quitarSubproducto(item);
	}
	public agregarSubproducto(item: Subproducto) {
		this.producto.agregarSubproducto(item);
	}
	public nuevoSubproducto() {	
		this.agregarSubproducto(new Subproducto(null, "Subproducto 1", this.producto.subproductos.length + 1));
	}

	public cambioCategoria(cat: any) {
		if (!cat.new || !cat.new.codigo) {
			this.agrupadores = [];
		} else {
			this.getAgrupadores(cat.new);
		}
	}
}

import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Message } from 'primeng/primeng';

import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SessionComponent } from '../../session-component.component';
import { Descriptivo } from '../../model/Descriptivo';
import { ProductoService } from '../service/producto.service';
import { Producto } from '../../model/Producto';

@Component({
	selector: 'lista-productos',
	templateUrl: './lista-productos.component.html',
	styleUrls: ['./lista-productos.component.less']
})
export class ListaProductosComponent extends SessionComponent implements OnInit {

	public productos: Producto[];
	public msgs: Message[] = [];
	public displayDialog: boolean = false;
	public tituloDialog: string;
	public vista : string ="lista-productos";
	
	public editandoProducto : boolean = false;
	public productoEditado : Producto = new Producto();
	get titulo() : string{
	
		if(!this.productoEditado){
			return "Productos";
		}else{
			let tt : string = "";
			if(this.productoEditado.id){
				tt = "Editando ";
			}else{
				tt = "Nuevo Producto ";
			}
			tt += this.productoEditado.codigo?this.productoEditado.codigo :"";
			tt += this.productoEditado.nombre?  " - " + this.productoEditado.nombre : "" ;
			return tt;
		}

		
	}
	
	constructor(cookieService :CookieServiceHelper,
		private service: ProductoService, private router: Router, private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {

		this.getProductos();
		
	}

	public getProductos(){
		let $this = this;
		$this.addLoadingCount();
		this.service.getAll().then(prds => {
			$this.productos = [...prds];
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public eliminar(producto : Producto) {
		this.confirmationService.confirm({
			key: "genConf",
			header: "Eliminación de producto",
			message: 'Desea deshabilitar el producto?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(producto.id).then((res) => {
					$this.susLoadingCount();
					$this.ngOnInit();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(producto : Producto) {
		this.confirmationService.confirm({
			key: "genConf",
			header: "Habilitación de producto",
			message: 'Desea habilitar el producto?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				producto.activo = true;
				this.service.guardar(producto).then((res) => {
					$this.susLoadingCount();
					$this.ngOnInit();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public cancelarEdicionProducto(prod : Producto){
		this.editandoProducto = false;
		this.productoEditado = new Producto();
		this.vista = "lista-productos";
	}
	public productoGuardado(prod: Producto){
		this.editandoProducto = false;
		this.productoEditado = new Producto();
		this.getProductos();
		this.vista = "lista-productos";
		
	}
	public editarProducto(producto: Producto) {
		let $this = this;
		this.addLoadingCount();
		this.service.getById(producto.id).then((r)=>{
			$this.productoEditado = r;
			$this.editandoProducto = true;
			$this.vista = "gestion-producto";
			$this.susLoadingCount();
		}).catch(this.errorHandler)
		
		
	}

	public nuevoProducto() {
		this.productoEditado = new Producto();
		this.editandoProducto = true;
		this.vista = "gestion-producto";
		
	}
	
}
import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { ServicioAbstract } from '../../common-services/service.service';
import { Descriptivo } from '../../model/Descriptivo';
import { Producto } from '../../model/Producto';



@Injectable()
export class ProductoService extends ServicioAbstract {

    public getDescriptivosByCategoria(categoria: string): Promise<Descriptivo[]> {
        return this.getDescriptivos("producto/categoria/" + categoria);
    }
    public getByCategoria(categoria: string): Promise<Producto[]> {
        return this.http.get(this.getApiURL() + "producto/categoria/" + categoria).toPromise().then(
            (r: any) => {
                if (!r || !r.respuesta) {
                    return null
                } else {
                    return r.respuesta.map(p => Producto.fromData(p));
                }
            }, this.handleError);
    };
    public getByGrupo(grupo: string): Promise<Producto[]> {
        return this.http.get(this.getApiURL() + "producto/grupo/" + grupo).toPromise().then(
            (r: any) => {
                if (!r || !r.respuesta) {
                    return null
                } else {
                    return r.respuesta.map(p => Descriptivo.fromData(p));
                }
            }, this.handleError);
    };
    getAll = (): Promise<Producto[]> => {


        return this.http.get(this.getApiURL() + "producto/all").toPromise().then(
            function (response: any) {
                let lista: Producto[] = [];
                lista = response.respuesta.map(p => Producto.fromData(p));
                lista.sort((a: Producto, b: Producto) => { return a.nombre.toLocaleLowerCase().localeCompare(b.nombre.toLocaleLowerCase()) });
                return lista;
            }
        )
            .catch(this.handleError);
    }

    public delete(id: any): Promise<any> {
        let params = new HttpParams();
        params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'producto', { params }).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(id: number): Promise<any> {
        return this.http.get(this.getApiURL() + "producto/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let producto: Producto;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    producto = Producto.fromData(res.respuesta);
                }
                return producto;

            }, this.handleError);

    }

    public guardar(producto: Producto): Promise<any> {
        if (producto.id) {
            return this.http.post(this.getApiURL() + 'producto', producto).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'producto', producto).toPromise().then(this.handleOk, this.handleError);
        }
    }



}


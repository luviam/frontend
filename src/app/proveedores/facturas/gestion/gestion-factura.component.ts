import { Component, EventEmitter, Input, Output, QueryList, Renderer2, ViewChildren } from '@angular/core';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { AutoComplete } from 'primeng/primeng';
import { CentroCostoService } from '../../../common-services/centroCostoService.service';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { CuentasService } from '../../../common-services/cuentasService.service';
import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { CuentaAplicacion } from '../../../model/CuentaAplicacion';
import { Factura } from '../../../model/Factura';
import { ItemFactura } from '../../../model/ItemFactura';
import { SessionComponent } from '../../../session-component.component';
import { ProveedoresService } from '../../service/ProveedoresService';
import { CabeceraFactura } from '../listado/shared/CabeceraFactura';
import { FacturaService } from '../service/FacturaService.service';
import { Descriptivo } from './../../../model/Descriptivo';






@Component({
	selector: 'gestion-factura',
	styleUrls: ["./gestion-factura.component.less"],
	templateUrl: 'gestion-factura.component.html'
})

export class GestionFacturaComponent extends SessionComponent {

	private _proveedor: Descriptivo;
	private _centro: Descriptivo;

	@Input()
	set proveedorSeleccionado(val: Descriptivo) {
		if (val && val.codigo !== Descriptivo.TODOS().codigo) {
			this._proveedor = val;
			this.factura.proveedor = val;
		} else {
			this._proveedor = null;
			this.factura.proveedor = null;
		}
	}
	get proveedorSeleccionado(): Descriptivo {
		return this._proveedor;
	}


	@Input()
	set centroCosto(val: Descriptivo) {
		if (val && val.codigo !== Descriptivo.TODOS().codigo) {
			this._centro = val;
			this.factura.centroCosto = val;
		} else {
			this._centro = null;
			this.factura.centroCosto = null;
		}
	}
	get centroCosto(): Descriptivo {
		return this._centro;
	}

	public proveedores: Descriptivo[] = [];
	public factura: Factura = new Factura();
	private _cabeceraFactura: CabeceraFactura;
	private tiposIVA: Descriptivo[] = [
		new Descriptivo("0", "0%"),
		new Descriptivo("105", "10.5%"),
		new Descriptivo("21", "21%"),
		new Descriptivo("27", "27%")
	]
	public editable: boolean = false;
	public editando: boolean = false;
	public centros: Descriptivo[] = [];
	public cuentasImputacion: CuentaAplicacion[] = [];
	public cuentasFiltradas: CuentaAplicacion[] = [];
	public finalizado: boolean = false;
	public tiposComprobante: Descriptivo[] = [];

	@Input()
	set cabeceraFactura(cabecera: CabeceraFactura) {
		this._cabeceraFactura = cabecera;
		if (cabecera.id) {
			this.buscarFactura(cabecera.id);
		} else {
			this.factura = new Factura();
			this.factura.centroCosto = this.centroCosto || null;
			this.factura.tipoComprobante = null;
			this.factura.proveedor = this.proveedorSeleccionado || null;
			this.factura.fecha = this.factura.fechaIVA = new Date();
			this.editable = true;
		}
	}

	@Input()
	public visible: boolean = false;

	@Output()
	public visibleChange = new EventEmitter();

	@Output()
	public onFacturaGuardada = new EventEmitter();

	@Output()
	public onCancelar = new EventEmitter();

	@ViewChildren('autoFocus') items: QueryList<AutoComplete>;

	constructor(cookieService: CookieServiceHelper,
		private confirmationService: ConfirmationService,
		private descriptivosService: DescriptivosService,
		private proveedorService: ProveedoresService,
		private facturasService: FacturaService,
		private cuentasService: CuentasService,
		private centroService: CentroCostoService,
		private renderer: Renderer2) {
		super("facturas-gestion");
	}

	public nuevoItemFactura() {
		let i = new ItemFactura();
		i.tipoIVA = this.tiposIVA[2];
		i.centroCosto = this.factura.centroCosto || null;
		i.cuenta = null;
		this.factura.addItem(i);
		let $this = this;
		setTimeout(() => {
			$this.items.last.inputEL.nativeElement.focus();
		}, 0)
	}



	public buscarFactura(idFactura: number) {
		this.addLoadingCount();
		let $this = this;
		$this.facturasService.getById(idFactura).then((res) => {
			$this.factura = (res);
			$this.susLoadingCount();
			$this.editable = $this.factura.estado.codigo != "A";
		}).catch($this.errorHandler);
	}



	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		$this.addLoadingCount();
		$this.descriptivosService.getProveedores().then((res) => {
			$this.proveedores = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);
		this.addLoadingCount();
		this.cuentasService.getAllFacturacion().then((res) => {
			$this.cuentasImputacion = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getTipoComprobantesFacturables().then((res) => {
			$this.tiposComprobante = res;
			if (!$this.factura.tipoComprobante) {
				//$this.factura.tipoComprobante = $this.tiposComprobante[0];
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public getCuentas(centro: Descriptivo) {
		return this.cuentasImputacion.filter(c => centro.codigo === c.centroCosto.codigo);
	}
	public guardar() {
		let $this = this;
		if (this.validarFactura()) {


			this.addLoadingCount();
			this.facturasService.guardar(this.factura).then((res) => {
				$this.susLoadingCount();
				$this.factura = new Factura();
				$this.factura.tipoComprobante = null;
				$this.factura.fecha = this.factura.fechaIVA = new Date();
				$this.onFacturaGuardada.emit();
			}).catch(this.errorHandler);
		}

	}
	public validarFactura(): boolean {
		let estado: boolean = true;
		if (!this.factura.items || this.factura.items.length <= 0) {
			this.error("Ingrese al menos un item de factura");
			estado = false;
		}
		if (!this.factura.proveedor) {
			this.error("Elija un proveedor")
			estado = false;
		}
		if (!this.factura.centroCosto) {
			this.error("Elija un centro de costo");
			estado = false;
		}
		if (!this.factura.fecha) {
			this.error("Elija una fecha de facturación");
			estado = false;
		}

		if (!this.factura.fechaIVA) {
			this.error("Elija un período de IVA");
			estado = false;
		}
		if (!this.factura.numero) {
			this.error("Elija un N° de comprobante");
			estado = false;
		}

		if (this.factura.items.some(i => !i.centroCosto || !i.cuenta)) {
			this.error("Todos los items deben tener Centro y Cuenta");
			estado = false;
		}
		return estado;

	}
	public volver() {
		this.visible = false;
		this.visibleChange.emit(this.visible);
		this.onCancelar.emit();

	}

	public searchCuentas(event: any) {
		this.cuentasFiltradas = this.cuentasImputacion.filter(s => s.centroCosto.codigo === this.factura.centroCosto.codigo
			&& (s.codigo.toLowerCase().includes(event.query.toLowerCase())
				|| s.descripcion.toLowerCase().includes(event.query.toLowerCase())))
			;
	}
	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: "fConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				$this.volver();
			}
		});
	}

	public habilitarItems(): boolean {
		return this.editable && (
			this.factura.centroCosto && this.factura.centroCosto.codigo !== Descriptivo.TODOS().codigo &&
			this.factura.proveedor && this.factura.proveedor.codigo !== Descriptivo.TODOS().codigo);
	}
	public keyPressHandler(event: any) {

		if (event.charCode === 13 && this.editable) {
			this.guardar();
			event.preventDefault();
		} else if (event.charCode === 43) {
			this.nuevoItemFactura();
			event.preventDefault();


		}
	}

}
import { Factura } from './../../../model/Factura';
import { FiltroProveedor } from '../../filtros/FiltroProveedor';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { FacturaService } from '../service/FacturaService.service';
import { CabeceraFactura } from './shared/CabeceraFactura';
import { ProveedoresService } from '../../service/ProveedoresService';
import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute } from '@angular/router';
import { Proveedor } from '../../../model/Proveedor';
import { Descriptivo } from '../../../model/Descriptivo';
import { MenuItem } from 'primeng/primeng';
import { SessionComponent } from '../../../session-component.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'facturas',
	styleUrls: ["./facturas.component.less"],
	templateUrl: 'facturas.component.html'
})

export class FacturasComponent extends SessionComponent {

	get proveedor() { return this.filtro.proveedor || null };
	
	public estadosSeleccionados: string[] = ["A", "P", "R"];
	public _facturas: CabeceraFactura[] = [];
	private _filtradas : CabeceraFactura[] = [];
	public proveedores : Descriptivo[] = [];
	public facturaEditada: CabeceraFactura = new CabeceraFactura();
	public editandoFactura: boolean = false;
	public centros: Descriptivo[] = [];
	
	get titulo() : string{
		if(this.facturaEditada && this.facturaEditada.numero){
			return this.facturaEditada.tipoComprobante.descripcion + " N° "  +this.facturaEditada.numero;
		}
		if(this.proveedor && this.proveedor.codigo!== Descriptivo.TODOS().codigo){
			return "Facturas de " + this.proveedor.descripcion;
		}else{
			return "Facturas";
		}
		
	}

	@Input()
	public filtro: FiltroProveedor = new FiltroProveedor();

	@Input()
	public mostrarfiltrosProveedor : boolean = true;

	
	@Output()
	public vistaChange :EventEmitter<string> = new EventEmitter<string>();

	@Output()
	public docsActualizados :EventEmitter<boolean> = new EventEmitter<boolean>();

	constructor(cookieService :CookieServiceHelper,

		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private proveedorService: ProveedoresService,
		private facturaService : FacturaService) {
		super( "facturas-lista");
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		//this.filtro.fechaDesde = moment().subtract(3, 'months').startOf('day').toDate();
		this.filtro.fechaDesde = moment().startOf('year').toDate();
		this.filtro.fechaHasta = moment().add(1,'days').startOf('day').toDate();
		if(!this.filtro.centro)
			this.filtro.centro = Descriptivo.TODOS();
		
		if(!this.filtro.proveedor)
			this.filtro.proveedor = Descriptivo.TODOS();

		$this.addLoadingCount();
		$this.descriptivosService.getProveedores().then((res) =>{
			$this.proveedores = res.concat(Descriptivo.TODOS());
			
			if(!this.filtro.proveedor)
				$this.filtro.proveedor =  Descriptivo.TODOS();
			//$this.updateFacturas();
			$this.susLoadingCount();
		}).catch($this.errorHandler);
		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res.concat(Descriptivo.TODOS());
			if(!this.filtro.centro)
				$this.filtro.centro = Descriptivo.TODOS();
			//$this.updateFacturas();
			$this.susLoadingCount();
		}).catch($this.errorHandler);


	}
	public updateFacturas() {
		let $this = this;
		this.filtro.fechaDesde.setHours(0,0,0,0);
		this.filtro.fechaHasta.setHours(23,59,59,0);
		if (this.filtro.proveedor && this.filtro.centro) {
			$this.addLoadingCount();
			$this.proveedorService.getFacturas(this.filtro).then((res) => {
				$this._facturas = res;
				$this.filtrar({});
				$this.susLoadingCount();
			}).catch($this.errorHandler);
		}
	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['proveedores/lista-proveedores']);
			}
		});
	}


	get pagosTotal() :number {
		let total = 0;
		this._facturas.filter(f => f.estado === "A").forEach(f => total += ( f.importe - f.saldo ));
		return total;
	}
	get saldo() {
		let saldo = 0;
		this._facturas.filter(f => f.estado === "A").forEach(f => saldo += f.saldo);
		return saldo;
	}

	public aprobable(fact :CabeceraFactura) : boolean{
		return fact.tipoComprobante.codigo != Factura.PAGO_A_CUENTA && fact.estado === "P" && this.esAdministrador //es Administrador o Supervisor
	}

	public rechazable(fact :CabeceraFactura) : boolean{
		return fact.tipoComprobante.codigo != Factura.PAGO_A_CUENTA && fact.estado === "P" && this.esAdministrador
	}

	public eliminable(fact : CabeceraFactura) : boolean{
		return fact.tipoComprobante.codigo != Factura.PAGO_A_CUENTA && ((fact.estado === "A" && fact.saldo === fact.importe) || fact.estado !="A");
	}

	public esEditable(fact : CabeceraFactura) : boolean{
		return fact.tipoComprobante.codigo != Factura.PAGO_A_CUENTA && fact.estado !="A";
	}
	public esVisualizable(fact : CabeceraFactura) : boolean{
		return fact.tipoComprobante.codigo != Factura.PAGO_A_CUENTA && !this.esEditable(fact);
	}

	get facturas() {
		return this._filtradas;
	}

	public updateTipoProveedor(event: any) {

	}

	public updateTipoCuenta(event: any) {

	}
	public editar(factura: CabeceraFactura) {
		this.facturaEditada = factura;
		this.editandoFactura = true;
		this.goTo("facturas-gestion");
	}

	public nuevaFactura() {
		this.facturaEditada = new CabeceraFactura();
		this.editandoFactura = true;
		this.goTo("facturas-gestion");
	}
	public onFacturaGuardada() {
		super.volver();
		this.editandoFactura = false;
		this.success("Factura Guardada");
		this.updateFacturas();
		this.docsActualizados.emit(true);
	}

	public onEdicionCancelada(){
		super.volver();
		this.editandoFactura = false;
	}

	
	public proveedorValido(proveedor: Proveedor): boolean {
		return true;
	}
	public aprobar(factura : CabeceraFactura){
		let $this  = this;
		this.addLoadingCount();
		this.facturaService.aprobarFactura(CabeceraFactura.fromData(factura)).then((res)=>{
			$this.success("Factura aprobada");
			$this.updateFacturas();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}

	public rechazar(factura : CabeceraFactura){
		let $this  = this;
		this.addLoadingCount();
		this.facturaService.rechazarFactura(factura).then((res)=>{
			$this.success("Factura Rechazada");
			$this.updateFacturas();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}
	public eliminar(factura : CabeceraFactura){
		let $this = this;
		this.confirmationService.confirm({
			key: "fConf",
			header: "Eliminar Factura",
			message: 'Va a eliminar la factura ' + (factura.descripcion? factura.descripcion : factura.numero ) + '. Desea continuar?',
			accept: () => {
				$this.addLoadingCount();
				$this.facturaService.eliminarFactura(factura).then((res)=>{
					$this.susLoadingCount();
					$this.success("Se borró la factura correctamente");
					$this.updateFacturas();
					$this.docsActualizados.emit(true);
				}).catch($this.errorHandler);
			}
		});
		
	}
	public filtrar(event : any){
		this.addLoadingCount();
		this._filtradas = this._facturas.filter(f => this.estadosSeleccionados.indexOf(f.estado) > -1);
		this.susLoadingCount();
	}
}
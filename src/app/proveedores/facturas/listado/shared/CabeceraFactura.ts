import { Descriptivo } from '../../../../model/Descriptivo';

import { BasicEntity } from '../../../../model/BasicEntity';
import * as moment from 'moment';
export class CabeceraFactura extends BasicEntity {
    constructor(public id?: number, public fecha?: Date,
        public numero?: string,
        public descripcion?: string,
        public importe?: number,
        public saldo?: number,
        public estado?: string,
        public responsable?: string,
        public tipoComprobante?: Descriptivo,
        public centro?: Descriptivo,
        public proveedor?: Descriptivo) {
        super(id, numero, descripcion);
    }

    public static fromData(c: any): CabeceraFactura {
        if (!c) return null;
        let o: CabeceraFactura = new CabeceraFactura(c.id,
            c.fecha ? new Date(c.fecha) : null,
            c.numero,
            c.descripcion,
            c.importe,
            c.saldo,
            c.estado,
            c.responsable,
            Descriptivo.fromData(c.tipoComprobante),
            Descriptivo.fromData(c.centro),
            Descriptivo.fromData(c.proveedor));
        return o;
    }

    get fechaFormateada() {
        return moment(this.fecha).format('DD/MM/YYYY');
    }



    get esNotaCredito(): boolean {
        return this.tipoComprobante && this.tipoComprobante.codigo.substring(0, 2) === 'NC';
    }

    get keyDocumento(): string{
        return this.tipoComprobante.codigo + " - " + this.numero;
    }
}
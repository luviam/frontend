import { RequestOptions } from '@angular/http';
import { ItemFactura } from '../../../model/ItemFactura';
import { Descriptivo } from '../../../model/Descriptivo';
import { CabeceraFactura } from '../listado/shared/CabeceraFactura';
import { Injectable } from '@angular/core';
import { Factura } from '../../../model/Factura';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../../common-services/service.service';
@Injectable()
export class FacturaService extends ServicioAbstract {
   

    public aprobarFactura(factura: CabeceraFactura): Promise<any> {
        factura.estado = "A";
        return this.cambioEstadoFactura(factura);
    }
    public rechazarFactura(factura: CabeceraFactura): Promise<any> {
        factura.estado = "R";
        return this.cambioEstadoFactura(factura);
    }

    public eliminarFactura(factura : CabeceraFactura) : Promise<any>{
        let params = new HttpParams();
        params = params.append('id',factura.id+"")
        return this.http.delete(this.getApiURL() + 'facturas', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    private cambioEstadoFactura(factura: CabeceraFactura): Promise<any> {
        return this.http.post(this.getApiURL() + "facturas/cambioEstadoFactura", factura).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(idFactura: number): Promise<any> {
        return this.http.get(this.getApiURL() + 'facturas/' + idFactura).toPromise().then(this.parseFactura, this.handleError);
    }

    public parseFactura(response: any): Factura {
        let res = response;
        let factura : Factura = null;
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            let c = res.respuesta;
            return  Factura.fromData(c);
           
        }
        return factura;


    }
    public guardar(factura: Factura): Promise<any> {
        if (factura.id) {
            return this.http.post(this.getApiURL() + 'facturas', factura).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'facturas', factura).toPromise().then(this.handleOk, this.handleError);
        }
    }
}
import { Descriptivo } from '../../model/Descriptivo';
import { Cuenta } from '../../model/Cuenta';
import { CentroCosto } from '../../model/CentroCosto';
import { Proveedor } from '../../model/Proveedor';
export class FiltroProveedor {
    constructor(public proveedor?: Descriptivo,
        public centro?: Descriptivo,
        public fechaDesde?: Date,
        public fechaHasta?: Date,
        public cuenta?: Descriptivo,
        public descripcion?: string,
        public estado?: Descriptivo) {

    }

}
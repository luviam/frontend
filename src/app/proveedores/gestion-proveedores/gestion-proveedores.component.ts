import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { CentroCosto } from '../../model/CentroCosto';
import { ImputacionCuentaConfig } from '../../model/ImputacionCuentaConfig';
import { MenuItem } from 'primeng/primeng';
import { ProveedoresService } from '../service/ProveedoresService';
import { Proveedor } from '../../model/Proveedor';
import { ConvenioSalon } from '../../model/ConvenioSalon';

import { Descriptivo } from '../../model/Descriptivo';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CentroCostoService } from '../../common-services/centroCostoService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'gestion-proveedor',
	styleUrls: ["./gestion-proveedores.component.less"],
	templateUrl: 'gestion-proveedores.component.html'
})

export class GestionProveedoresComponent extends SessionComponent {
	public acciones: MenuItem[];
	private proveedor: Proveedor = new Proveedor();
	private tiposProveedor: Descriptivo[] = [];
	private tiposCuenta: Descriptivo[] = [];
	private tipoIVA: Descriptivo[] = [];
	public editable: boolean = false;
	public editando: boolean = false;
	public finalizado: boolean = false;
	public titulo: string = "";
	public saldo: number;
	public paginaVisible = "DATOS";
	public centros: CentroCosto[] = [];
	public tiposCuentaImputacion = [new Descriptivo("FC", "Imputacion Factura")];
	constructor(cookieService :CookieServiceHelper,
		private route: ActivatedRoute,
		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private centroCostoService: CentroCostoService,
		private proveedorService: ProveedoresService) {
		super();
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		this.acciones = [{
			label: "", icon: "fa fa-bars", items: [{
				label: "Datos del Proveedor",
				command: function () { $this.paginaVisible = "DATOS"; $this.titulo = "Editando - " + $this.proveedor.nombreProveedor; }
			},
			{
				label: "Facturas",
				command: function () {
					if($this.proveedor.getCentrosConImputacion().length === 0){
						$this.error("Debe configurar al menos una cuenta de imputacion");
						return;
					}
					 $this.paginaVisible = "FACTURAS"; $this.titulo = "Facturas - " + $this.proveedor.nombreProveedor }
			},
			{
				label: "Ordenes de Pago",
				command: function () { 
					if($this.proveedor.getCentrosConImputacion().length === 0){
						$this.error("Debe configurar al menos una cuenta de imputacion");
						return;
					}
					$this.paginaVisible = "OPS"; $this.titulo = "Ordenes de Pago - " + $this.proveedor.nombreProveedor }
			}]
		}
		]


		this.saldo = null;
		this.proveedor = new Proveedor();
		this.proveedor.tipoProveedor = new Descriptivo();
		this.proveedor.iva = new Descriptivo();
	
		this.addLoadingCount();
		this.descriptivosService.getAllTiposProveedor().then((res) => {
			if (res) {
				$this.tiposProveedor = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllTiposCuenta().then((res) => {
			if (res) {
				$this.tiposCuenta = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descriptivosService.getAllTipoIVA().then((res) => {
			if (res) {
				$this.tipoIVA = res;
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.centroCostoService.getAllCentros().then((res) => {
			$this.centros = res;
			$this.susLoadingCount();
			this.route.params.subscribe(params => {

				let idProveedor: string = <string>params['id'];
				if (idProveedor) {
					this.addLoadingCount();
					this.proveedorService.getById(parseInt(idProveedor)).then((pro) => {
						$this.proveedor = pro;
						$this.titulo = "Editando - " + $this.proveedor.nombreProveedor;
						$this.susLoadingCount();
						$this.editable = true;
						$this.editando = true;
						$this.addLoadingCount();
						$this.proveedorService.getSaldo($this.proveedor.id).then((res) => {
							$this.saldo = res;
							$this.susLoadingCount();

						})
						$this.tiposCuentaImputacion.forEach(t => {
							$this.centros.forEach(c =>{
								if(!$this.proveedor.getCuentaImputacion(t.codigo,c.codigo)){
									$this.proveedor.addCuentaImputacion(new ImputacionCuentaConfig(null, c, null, t));
								}
							})
						});
					}).catch(this.errorHandler);
				} else {
					this.titulo = "Nuevo Proveedor";
					$this.tiposCuentaImputacion.forEach(t => {

						$this.proveedor.imputacionCuentaConfigs = $this.centros.map(c =>
							new ImputacionCuentaConfig(null, c, null, t))
					})
					$this.editable = true;

				}

			});
		}).catch($this.errorHandler)

	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['proveedores/lista-proveedores']);
			}
		});
	}

	public updateTipoProveedor(event: any) {

	}

	public updateTipoCuenta(event: any) {

	}

	public guardarProveedor() {
		let $this = this;
		if (this.proveedorValido(this.proveedor)) {
			this.addLoadingCount();
			if (this.proveedor.tipoProveedor.codigo === 'N') {
				this.proveedor.domicilioFacturacion = '';
				this.proveedor.cuit = '';
				this.proveedor.razonSocial = '';
				this.proveedor.iva = null;
				this.proveedor.domicilioFacturacion = '';
				this.proveedor.localidadFacturacion = '';
			}

			this.proveedorService.guardar(this.proveedor).then((c) => {
				$this.success("El proveedor fue guardado correctamente");
				$this.finalizado = true;
				setTimeout(() => {
					$this.router.navigate(["proveedores/lista-proveedores"]);
				}, 3000)
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		} else {
			this.error("El proveedor no es valido");
		}
	}
	public facturaGuardada() {
		this.paginaVisible = "FACTURAS";
		this.success("Factura Guardada");
	}

	public ordenPagoGuardada() {
		this.paginaVisible = "OPS";
		this.success("Orden de Pago Guardada");
	}

	public proveedorValido(proveedor: Proveedor): boolean {
		return true;
	}

	public getCuentas(centro: Descriptivo) {
		if (!this.centros || this.centros.length === 0) {
			return [];
		}

		return this.centros.filter(c => c.codigo === centro.codigo)[0].cuentas;
	}
}
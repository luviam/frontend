import { Proveedor } from '../../model/Proveedor';
import { FiltroProveedor } from '../filtros/FiltroProveedor';
import { MenuItem, Menu } from 'primeng/primeng';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { ListaProveedores } from './shared/lista-proveedores.model';
import { ProveedoresService } from '../service/ProveedoresService';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';



@Component({
	selector: 'lista-proveedores',
	templateUrl: 'lista-proveedores.component.html',
	styleUrls:['lista-proveedores.component.less'],
	providers: [ProveedoresService]
})
export class ListaProveedoresComponent extends SessionComponent implements OnInit {
	@ViewChild("menu")
	private menu : Menu;
	public items: MenuItem[];
	public proveedorSeleccionado : ListaProveedores;
	public proveedorEditado : Proveedor;
	listaProveedores: ListaProveedores[] = [];
	titulo: string = "Listado de Proveedores"
	private filtro: FiltroProveedor = new FiltroProveedor();
	private filtroDocumentos: FiltroProveedor = new FiltroProveedor();
	public acciones : MenuItem[] = [];
	public tituloDocumentos: string ="";
	constructor(cookieService :CookieServiceHelper,
		private proveedorService: ProveedoresService,
		private router: Router,
		private confirmationService: ConfirmationService ) { super("lista") }

	ngOnInit() {
		this.items =[
			{label: 'Editar', icon: 'fa-pencil', command: (event)=>{
				this.editarProveedor(this.proveedorSeleccionado);
			}},
			{label: 'Ver OPs', icon: 'fa-money', command: (event)=>{
				this.mostrarOP(this.proveedorSeleccionado);
			}},
			{label: 'Ver Facturas', icon: 'fa-barcode',command: (event)=>{
				this.mostrarFacturas(this.proveedorSeleccionado);
			}}
		];

		if(this.esAdministrador){
			this.items.push(
				{label: 'Eliminar', icon: 'fa-trash', command: (event)=>{
					this.confirmarEliminar(this.proveedorSeleccionado);
				}}
			);
		}
		this.getListado();
		this.acciones = [{ label: "", icon: "fa fa-bars", items:[
			{
			  label: "Nuevo Proveedor",
			  icon: 'fa-plus',
			  command : (event)=> {
				this.nuevoProveedor();
			  }
			}]}];
	}

	public ocultarVolver():boolean{
		return !this.currentView.includes('-lista');
	}
	public confirmarEliminar(proveedor: ListaProveedores) {
		let $this = this;
		this.confirmationService.confirm({
			header: "Va a eliminar al Proveedor " + proveedor.contacto,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				$this.addLoadingCount();
				$this.proveedorService.delete(proveedor.id).then((res) => {
					$this.getListado();
					$this.susLoadingCount();
				}).catch($this.errorHandler);
			}
		});
	}

	public getListado() {
		let $this = this;
		this.addLoadingCount();
		this.proveedorService.getAllList(this.filtro).then((res) => {
			$this.listaProveedores = [...res];
			if($this.proveedorSeleccionado){
				$this.proveedorSeleccionado = 
					$this.listaProveedores.filter(p => p.id == this.proveedorSeleccionado.id)[0];
			}
			$this.susLoadingCount();
		}).catch(this.errorHandler);
	}
	public editarProveedor(proveedor: ListaProveedores) {
		this.router.navigate(["/proveedores/editar/" + proveedor.id + ""]);
	}
	public nuevoProveedor() {
		this.router.navigate(["/proveedores/nuevo"]);
	}

	public mostrarAcciones(event:any,proveedor: ListaProveedores){
		this.proveedorSeleccionado = proveedor;
		this.menu.show(event);
	}
	public mostrarOP(proveedor :ListaProveedores){
		
		let $this = this;
		this.addLoadingCount();
		this.proveedorService.getById(proveedor.id).then((p)=>{
			$this.goTo("ops-lista");
			$this.proveedorEditado = p;
			$this.tituloDocumentos = "Ordenes de Pago de " + p.nombreProveedor;
			$this.filtro = new FiltroProveedor(
				p
			);
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		
	}

	public mostrarFacturas(proveedor :ListaProveedores){
		
		let $this = this;
		this.addLoadingCount();
		this.proveedorService.getById(proveedor.id).then((p)=>{
			$this.goTo("facturas-lista");
			$this.tituloDocumentos = "Facturas de " + p.nombreProveedor;
			$this.proveedorEditado = p;
			$this.filtro = new FiltroProveedor(
				p
			);
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		
	}
	public onDocsActualizados(){
		this.proveedorEditado = null;
		this.getListado();
	}

	get saldoProveedor():number{
		if(this.proveedorSeleccionado){
			return this.proveedorSeleccionado.saldo;
		}else{
			return 0;
		}
	}
}
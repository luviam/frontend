import { EstadoOperacionEnum } from './../../../model/EstadoOperacionEnum';

import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { CentroCosto } from '../../../model/CentroCosto';
import { Descriptivo } from '../../../model/Descriptivo';
import { Chequera } from '../../../model/Chequera';
import { ChequeraService } from '../../../cheque/service/chequera.service';

import { Pago } from '../../../model/Pago';
import { CuentaAplicacion } from '../../../model/CuentaAplicacion';
import { CuentasService } from '../../../common-services/cuentasService.service';
import { OrdenPagoService } from '../service/OrdenPagoService.service';
import { ItemOrdenPago } from '../../../model/ItemOrdenPago';
import { OrdenPago } from '../../../model/OrdenPago';
import { Proveedor } from '../../../model/Proveedor';
import { CabeceraOrdenPago } from '../listado/shared/CabeceraOrdenPago';
import { SessionComponent } from '../../../session-component.component';
import { MenuItem } from 'primeng/primeng';
import { ProveedoresService } from '../../service/ProveedoresService';

import { ConvenioSalon } from '../../../model/ConvenioSalon';


import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { CentroCostoService } from '../../../common-services/centroCostoService.service';
import { ActivatedRoute, Router } from '@angular/router';

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cheque } from '../../../model/Cheque';
import { ChequeService } from '../../../cheque/service/cheque.service';
import { Cuenta } from '../../../model/Cuenta';

@Component({
	selector: 'gestion-op',
	styleUrls: ["./gestion-op.component.less"],
	templateUrl: 'gestion-op.component.html'
})

export class GestionOrdenPagoComponent extends SessionComponent {
	public proveedores : Descriptivo[] = [];
	private  _proveedor: Descriptivo;
	private _centro: Descriptivo;
	public ordenPago: OrdenPago = new OrdenPago();
	public soloSeleccionados: boolean = false;
	public editable: boolean = true;
	public editando: boolean = false;
	public centros: Descriptivo[] = [];
	public cuentasImputacion: CuentaAplicacion[] = [];
	public finalizado: boolean = false;
	public tiposComprobante: Descriptivo[] = [];
	public cuentas: any[] = [];

	public cheques: Cheque[] = [];
	public chequesSugeridos: Cheque[] = [];

	private _items: ItemOrdenPago[] = [];
	private tiposPago: Descriptivo[] = [
		new Descriptivo("E", "Efectivo"),
		new Descriptivo("P", "Cheque Propio"),
		new Descriptivo("C", "Cheque Clientes"),
		new Descriptivo("O", "Cheque Otros"),
	]

	@Input()
	set proveedor(val : Descriptivo){
		if(val && val.codigo !== Descriptivo.TODOS().codigo){
			
			this.ordenPago.proveedor = val;
			this._proveedor = val;
		}else{
			
			this.ordenPago.proveedor = null;
			this._proveedor = null;
		}
	}
	get proveedor(): Descriptivo{
		return this._proveedor;
	}

	@Input()
	set centroCosto(val : Descriptivo){
		if(val && val.codigo !== Descriptivo.TODOS().codigo){
			
			this.ordenPago.centroCosto = val;
			this._centro = val;
		}else{
			
			this.ordenPago.centroCosto = null;
			this._centro = null;
		}
	}
	get centroCosto() : Descriptivo{
		return this._centro;
	}

	@Input()
	set cabeceraOrdenPago(cabecera: CabeceraOrdenPago) {
		
		if (cabecera.id) {
			this._items = [];
			this.buscarOrdenPago(cabecera.id);
		} else {

			this.ordenPago = new OrdenPago();
			this.ordenPago.centroCosto = this.centroCosto || null;
			this.ordenPago.proveedor = this.proveedor || null;
			this.editable = true;
			this.ordenPago.fecha = new Date();
			//this.soloSeleccionados = false;
		}
	}

	@Input()
	public visible: boolean = false;

	@Output()
	public visibleChange = new EventEmitter();

	@Output()
	public onOrdenPagoGuardada = new EventEmitter();

	@Output()
	public onCancelar = new EventEmitter();

	constructor(cookieService :CookieServiceHelper,
		private confirmationService: ConfirmationService,
		private descriptivosService: DescriptivosService,
		private proveedorService: ProveedoresService,
		private ordenPagoService: OrdenPagoService,
		private cuentasService: CuentasService,
		private chequeService: ChequeService,
		private centroService: CentroCostoService) {
		super();
	}

	get items() {
		if (this.soloSeleccionados) {
			return this._items.filter(i => this.ordenPago.items.some(oi => oi.cabeceraFactura.id === i.cabeceraFactura.id));
		} else {
			return this._items;
		}
	}

	public esModificable(){
		
		return this.editable && (!this.ordenPago.estado || this.ordenPago.estado.codigo !== EstadoOperacionEnum.PENDIENTE_PAGO);

	}
	public nuevoItemOrdenPago() {
		let i = new ItemOrdenPago();
	}
	public actualizarCuentas() {
		let $this = this;
		this.addLoadingCount();
		$this.cuentasService.getAllBancarias().then((res) => {
			$this.cuentas["P"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler)

		this.addLoadingCount();
		$this.cuentasService.getAllChequesClientes().then((res) => {
			$this.cuentas["C"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		$this.cuentasService.getAllChequesOtros().then((res) => {
			$this.cuentas["O"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		$this.cuentasService.getCallCajas().then((res) => {
			$this.cuentas["E"] = res;
			$this.susLoadingCount();
		}).catch(this.errorHandler)
	}
	public buscarOrdenPago(idOrdenPago: number) {
		let $this = this;
		this.addLoadingCount();
		$this.ordenPagoService.getById(idOrdenPago).then((res) => {
			$this.ordenPago = (res);
			$this.susLoadingCount();
			$this.editable = $this.ordenPago.estado.codigo != "A";
			$this.soloSeleccionados = !$this.esModificable();
			$this.proveedor = $this.ordenPago.proveedor;
			$this.centroCosto = $this.ordenPago.centroCosto;
			$this.updateFacturas();

		}).catch($this.errorHandler);
	}



	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		$this.actualizarCuentas();

		$this.addLoadingCount();
		$this.descriptivosService.getProveedores().then((res) =>{
			$this.proveedores = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);
		
		$this.addLoadingCount();
		$this.chequeService.getDisponibles().then((res) => {
			$this.cheques = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res;
			$this.susLoadingCount();
		}).catch($this.errorHandler);
	}
	public updateImporte(cf : ItemOrdenPago){
		this.ordenPago.items.filter(i => i.cabeceraFactura.id === cf.cabeceraFactura.id).forEach(i =>{
			i.importe = cf.importe;
		});
	}
	
	public updateFacturas(){
		let $this = this;
		
		if(this.ordenPago.proveedor && this.ordenPago.proveedor.codigo && this.ordenPago.centroCosto && this.ordenPago.centroCosto.codigo){
			this.addLoadingCount();
			this.proveedorService.getFacturasImpagas(Number(this.ordenPago.proveedor.codigo), this.ordenPago.centroCosto.codigo).then((res) => {
				$this._items = [];
				res.forEach(f => {
					$this._items.push(new ItemOrdenPago(null, f, f.saldo));
				});
				$this.ordenPago.items.forEach(i => {
					
					let item = $this._items.filter(ii => ii.cabeceraFactura.id === i.cabeceraFactura.id)[0];
					if (!item) {
						if (!$this.editable) {
							$this._items.push(i);
							
						}

					} else {
						item.id = i.id;
						item.importe = i.importe;
					}

				});
				$this._items = [...$this.items];
	
				$this.susLoadingCount();
			});
		}
		
	}
	public guardar() {
		let $this = this;
		if (this.validar()) {


			this.addLoadingCount();
			this.ordenPago.pagos.filter(p => !p.centroCosto).forEach(p => p.centroCosto = this.ordenPago.centroCosto);
			this.ordenPagoService.guardar(this.ordenPago).then((res) => {
				$this.susLoadingCount();
				$this.ordenPago = new OrdenPago();
				$this.onOrdenPagoGuardada.emit();
			}).catch(this.errorHandler);
		}

	}
	public volver() {
		
		this.visible = false;
		this.onCancelar.emit();
	}
	public confirmarCancelar() {
		let $this = this;
		this.confirmationService.confirm({
			key: "opConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizados. Desea continuar?',
			accept: () => {
				$this.volver();
			}
		});
	}
	public agregarNuevoPago() {
		this.ordenPago.addPago(new Pago(null, this.tiposPago[0],
			this.ordenPago.centroCosto,
			null,
			this.ordenPago.total - this.ordenPago.totalPagos,
			null));
	}

	public getCuentas(tipoOperacion: Descriptivo) {
		if (tipoOperacion)
			return this.cuentas[tipoOperacion.codigo].filter(c => this.ordenPago.centroCosto.codigo === c.centroCosto.codigo);
		return [];
	}


	public filtrarCheques(event: any) {

		this.chequesSugeridos = this.cheques.filter(c => c.descripcion.toUpperCase().includes(event.query.toUpperCase()));


	}
	public updateTipoPago(op: Pago) {
		if (this.getCuentas(op.tipoOperacion).length === 1) {
			op.cuenta = this.getCuentas(op.tipoOperacion)[0];
		}
		op.comprobante = null;
	}

	public updateCentro(){
		this.actualizarCuentas();
		this.updateFacturas();
	}
	public updateCheque(op: Pago) {
		if (op.tipoOperacion.codigo !== "E" && op.comprobante) {
			if (op.comprobante.importe) {
				op.monto = op.comprobante.importe;
			}
			if (op.tipoOperacion.codigo === 'P') {

				op.cuenta = op.comprobante.cuentaBancaria;
			}


		}
	}
	public autoselectCheque(op: Pago) {
		if (!op.comprobante || !op.comprobante.id) {


			if (this.chequesSugeridos.length == 1) {
				op.comprobante = this.chequesSugeridos[0];

			} else {
				op.comprobante = null;

			}

		}
	}
	public pagosValidos(){

		return this.ordenPago.pagos.length ===  0 || this.ordenPago.totalPagos === this.ordenPago.total;

	}
	public validar(): boolean {
		let estado: boolean = true;
		if(!this.ordenPago.centroCosto){
			estado = false;
			this.error("Indique un Centro de Costo");
		}
		if(!this.ordenPago.proveedor){
			estado = false;
			this.error("Indique un Proveedor");
		}
		if(!this.ordenPago.fecha){
			estado = false;
			this.error("Indique una fecha");
		}
		if(this.ordenPago.items.length === 0 && this.ordenPago.pagoACuenta <= 0){
			estado = false;
			this.error("Debe incluir al menos 1 item a pagar o un pago a cuenta");
		}
		if (this.ordenPago.pagos.some(p => p.tipoOperacion.codigo != 'E' && !p.comprobante)) {
			estado = false;
			this.error("Debe completar todos los cheques");
		}
		if (this.ordenPago.pagos.some(p => !p.monto)) {
			estado = false;
			this.error("Debe completar todos los importes de pagos");
		}
		if (this.ordenPago.pagos.some(p => p.monto <= 0)) {
			estado = false;
			this.error("Los importes deben ser mayores a 0");
		}
		return estado;
	}

	public getChequeColumnStyle(): any{
		return {'width': this.ordenPago.tieneCheques?'40%':'10%'};
	}
}
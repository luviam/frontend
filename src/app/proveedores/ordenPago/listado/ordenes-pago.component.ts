import { CabeceraCliente } from '../../../clientes/lista-clientes/shared/CabeceraCliente';
import { FiltroProveedor } from '../../filtros/FiltroProveedor';
import { CookieServiceHelper } from '../../../common-services/cookieServiceHelper.service';
import { OrdenPago } from '../../../model/OrdenPago';
import { ItemOrdenPago } from '../../../model/ItemOrdenPago';
import { OrdenPagoService } from '../service/OrdenPagoService.service';
import { CabeceraOrdenPago } from './shared/CabeceraOrdenPago';
import { ProveedoresService } from '../../service/ProveedoresService';
import { DescriptivosService } from '../../../common-services/descriptivos.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute } from '@angular/router';
import { Proveedor } from '../../../model/Proveedor';
import { Descriptivo } from '../../../model/Descriptivo';
import { MenuItem } from 'primeng/primeng';
import { SessionComponent } from '../../../session-component.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
	selector: 'ordenes-pago',
	styleUrls: ["./ordenes-pago.component.less"],
	templateUrl: 'ordenes-pago.component.html'
})

export class OrdenPagosComponent extends SessionComponent {


	get proveedor() { return this.filtro.proveedor };

	get titulo() : string{
		if(this.ordenPagoEditada && this.ordenPagoEditada.numero){
			return "Orden de Pago N° " + this.ordenPagoEditada.numero;
		}
		if(this.proveedor && this.proveedor.codigo!== Descriptivo.TODOS().codigo){
			return "Ordenes de Pago de " + this.proveedor.descripcion;
		}else{
			return "Ordenes de Pago";
		}
		
	}
	public estadosSeleccionados: string[] = ["A", "P", "R", "PP"];
	
	
	
	@Input()
	public filtro: FiltroProveedor = new FiltroProveedor();

	@Input()
	public mostrarfiltrosProveedor : boolean = true;

	
	@Output()
	public vistaChange : EventEmitter<string> = new EventEmitter<string>();

	@Output()
	public docsActualizados :EventEmitter<boolean> = new EventEmitter<boolean>();

	public proveedores : Descriptivo[] = [];
	public _ordenesPago: CabeceraOrdenPago[] = [];
	public _filtradas: CabeceraOrdenPago[] = [];
	public ordenPagoEditada: CabeceraOrdenPago = new CabeceraOrdenPago();
	public editandoOrdenPago: boolean = false;
	public _ordenSeleccionada : CabeceraOrdenPago = new CabeceraOrdenPago();
	get ordenSeleccionada(): CabeceraOrdenPago{
		return this._ordenSeleccionada;
	}
	set ordenSeleccionada(val :CabeceraOrdenPago) {
		this.itemsOrden = [];
		if(val){
			let $this = this;
			this.addLoadingCount();
			this.ordenPagoService.getItems(val.id).then((res)=>{
				$this.itemsOrden = res;
				$this.susLoadingCount();
			}).catch(this.errorHandler);
		}
	}
	public itemsOrden : ItemOrdenPago[] = [];
	
	public centros: Descriptivo[] = [];

	
	constructor(cookieService :CookieServiceHelper, 

		private confirmationService: ConfirmationService,
		private router: Router,
		private descriptivosService: DescriptivosService,
		private proveedorService: ProveedoresService,
		private ordenPagoService : OrdenPagoService) {
		super("ops-lista");
	}


	ngOnInit() {
		let $this = this;
		this.finalizado = false;
		//this.filtro.fechaDesde = moment().subtract(3, 'months').startOf('day').toDate();
		this.filtro.fechaDesde = moment().startOf('year').toDate();
		this.filtro.fechaHasta = moment().add(1,'days').startOf('day').toDate();
		if(!this.filtro.proveedor){
			this.filtro.proveedor = Descriptivo.TODOS();
		}
		if(!this.filtro.centro){
			this.filtro.centro = Descriptivo.TODOS();
		}
		$this.addLoadingCount();
		$this.descriptivosService.getProveedores().then((res) =>{
			$this.proveedores = res;
			$this.proveedores.push(Descriptivo.TODOS());
			if(!this.filtro.proveedor)
				$this.filtro.proveedor = Descriptivo.TODOS();
			$this.susLoadingCount();
		}).catch($this.errorHandler);

		$this.addLoadingCount();
		$this.descriptivosService.getCentrosCosto().then((res) => {
			$this.centros = res;
			$this.centros.push(Descriptivo.TODOS());
			if(!this.filtro.centro)
				$this.filtro.centro = Descriptivo.TODOS();
			$this.susLoadingCount();
			
		}).catch($this.errorHandler);

		//$this.updateOrdenPagos();


	}
	public updateOrdenPagos() {
		let $this = this;
		this.filtro.fechaDesde.setHours(0,0,0,0);
		this.filtro.fechaHasta.setHours(23,59,59,0);
		if (this.filtro.proveedor && this.filtro.centro) {
			$this.addLoadingCount();
			$this.proveedorService.getOrdenesPagos(this.filtro).then((res) => {
				$this._ordenesPago = res;
				$this.filtrar();
				if($this.ordenSeleccionada && !$this.ordenesPago.some(s=> s.id === $this.ordenSeleccionada.id)){
					$this.ordenSeleccionada = null;
				}
				$this.susLoadingCount();
			}).catch($this.errorHandler);
		}
	}
	public confirmarCancelar() {
		this.confirmationService.confirm({
			key:"opConf",
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['proveedores/lista-proveedores']);
			}
		});
	}

	get saldo() {
		return this._ordenesPago.map(o => o.importe).reduce((a,b)=> a+ b,0);
		
	}

	public pagable(op : CabeceraOrdenPago): boolean{
		return op.estado === "PP" && this.esAuditor;
	}
	public aprobable(op :CabeceraOrdenPago) : boolean{
		return op.estado === "P" && this.esAuditor;
	}

	public rechazable(op :CabeceraOrdenPago) : boolean{
		return op.estado === "P" && this.esAuditor;
	}

	public eliminable(op : CabeceraOrdenPago) : boolean{
		
		return op.estado !="A" || this.esAdministrador;
	}

	public esEditable(op : CabeceraOrdenPago) : boolean{
		return op.estado !="A";
	}

	public esVisualizable(op : CabeceraOrdenPago) : boolean{
		return !this.esEditable(op);
	}
	

	get ordenesPago() {
		return this._filtradas;
	}

	public updateTipoProveedor(event: any) {

	}

	public updateTipoCuenta(event: any) {

	}
	public editar(ordenPago: CabeceraOrdenPago) {
		this.ordenPagoEditada = ordenPago;
		this.editandoOrdenPago = true;
		
		this.goTo("ops-gestion");
		
	}

	public pagar(ordenPago: CabeceraOrdenPago) {
		this.ordenPagoEditada = ordenPago;
		this.editandoOrdenPago = true;
		this.goTo("ops-gestion");
	}

	public nuevaOrdenPago() {
		this.ordenPagoEditada = new CabeceraOrdenPago();
		this.editandoOrdenPago = true;
		this.goTo("ops-gestion");
	}
	public onOrdenPagoGuardada() {
		this.editandoOrdenPago = false;
		super.volver();
		this.success("OrdenPago Guardada");
		this.updateOrdenPagos();
		this.docsActualizados.emit(true);
	}

	public onEdicionCancelada(){
		super.volver();
		this.editandoOrdenPago = false;
	}
	public aprobar(ordenPago : CabeceraOrdenPago){
		let $this  = this;
		this.addLoadingCount();
		this.ordenPagoService.aprobarOrdenPago(CabeceraOrdenPago.fromData(ordenPago)).then((res)=>{
			$this.success("OrdenPago aprobada");
			$this.updateOrdenPagos();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}

	public rechazar(ordenPago : CabeceraOrdenPago){
		let $this  = this;
		this.addLoadingCount();
		this.ordenPagoService.rechazarOrdenPago(CabeceraOrdenPago.fromData(ordenPago)).then((res)=>{
			$this.success("OrdenPago Rechazada");
			$this.updateOrdenPagos();
			$this.susLoadingCount();
			$this.docsActualizados.emit(true);
		}).catch($this.errorHandler);
		
	}
	public eliminar(ordenPago : CabeceraOrdenPago){
		this.confirmationService.confirm({
			key:"opConf",
			header: "Va a eliminar la OP " + ordenPago.descripcion,
			message: 'Esta operación no puede deshacerse. Desea continuar? ',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.ordenPagoService.eliminarOrdenPago(CabeceraOrdenPago.fromData(ordenPago)).then((res)=>{
					$this.susLoadingCount();
					$this.ordenSeleccionada = null;
					$this.success("Se borró la ordenPago correctamente");
					$this.updateOrdenPagos();
					$this.docsActualizados.emit(true);
				}).catch($this.errorHandler);
			}
		});
	
		
	}

	public filtrar(){
		this.addLoadingCount();
		this._filtradas = this._ordenesPago.filter(f => this.estadosSeleccionados.indexOf(f.estado) > -1);
		this.susLoadingCount();
	}
}


import { ItemOrdenPago } from '../../../../model/ItemOrdenPago';
import { CabeceraFactura } from '../../../facturas/listado/shared/CabeceraFactura';
import { Descriptivo } from '../../../../model/Descriptivo';
import { BasicEntity } from '../../../../model/BasicEntity';
import * as moment from 'moment';
export class CabeceraOrdenPago extends BasicEntity{
    constructor(public id? : number, 
                public fecha? : Date, 
                public numero? : string, 
                public responsable? : string,
                public descripcion? : string,
                public importe? : number, 
                public estado? : string,
                public proveedor? : Descriptivo,
                public centro? : Descriptivo,
            ){
                    super(id,numero,descripcion);
                }
    public static fromData(data : any) : CabeceraOrdenPago{
      if(!data)   return null;
      let o : CabeceraOrdenPago = new CabeceraOrdenPago(
            data.id,
            data.fecha? new Date(data.fecha) : null,
            data.numero,
            data.responsable,
            data.descripcion,
            data.importe,
            data.estado,
            data.proveedor? Descriptivo.fromData(data.proveedor) : Descriptivo.SIN_ASIGNAR(),
            data.centro? Descriptivo.fromData(data.centro) : Descriptivo.SIN_ASIGNAR()
      );
      return o;
    }
    get fechaFormateada(){
        return moment(this.fecha).format('DD/MM/YYYY');
    }
}
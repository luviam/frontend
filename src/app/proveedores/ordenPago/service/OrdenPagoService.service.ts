
import { Pago } from '../../../model/Pago';
import { CabeceraFactura } from '../../facturas/listado/shared/CabeceraFactura';
import { ItemOrdenPago } from '../../../model/ItemOrdenPago';
import { RequestOptions, Http } from '@angular/http';

import { Descriptivo } from '../../../model/Descriptivo';
import { CabeceraOrdenPago } from '../listado/shared/CabeceraOrdenPago';
import { Injectable } from '@angular/core';
import { OrdenPago } from '../../../model/OrdenPago';

import { ServicioAbstract } from '../../../common-services/service.service';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable()
export class OrdenPagoService extends ServicioAbstract {
    
   

    public aprobarOrdenPago(ordenPago: CabeceraOrdenPago): Promise<any> {
        ordenPago.estado = "A";
        return this.cambioEstadoOrdenPago(ordenPago);
    }
    public rechazarOrdenPago(ordenPago: CabeceraOrdenPago): Promise<any> {
        ordenPago.estado = "R";
        return this.cambioEstadoOrdenPago(ordenPago);
    }

    public eliminarOrdenPago(ordenPago : CabeceraOrdenPago) : Promise<any>{
        let params = new HttpParams();
       params = params.append('id',ordenPago.id+"")
        return this.http.delete(this.getApiURL() + 'ordenPago', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    private cambioEstadoOrdenPago(ordenPago: CabeceraOrdenPago): Promise<any> {
        return this.http.post(this.getApiURL() + "ordenPago/cambioEstado", ordenPago).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(idOrdenPago: number): Promise<OrdenPago> {
        return this.http.get(this.getApiURL() + 'ordenPago/' + idOrdenPago).toPromise().then(this.parseOrdenPago, this.handleError);
    }

    public getItems(idOrden : number) : Promise<ItemOrdenPago[]>{
        return this.http.get(this.getApiURL() + 'ordenPago/items/' + idOrden).toPromise().then(this.parseItems, this.handleError);
    }

    public parseItems(response: any) : ItemOrdenPago[]{
        let res = response.respuesta;
        if(res){
            return res.map(i => ItemOrdenPago.fromData(i));
        }
        return [];
    }

    public parseOrdenPago(response: any): OrdenPago {
        let res = response;
        let ordenPago : OrdenPago = null;
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            let c = res.respuesta;
            ordenPago =  OrdenPago.fromData(c);


        }
        return ordenPago;


    }
    public guardar(ordenPago: OrdenPago): Promise<any> {
        console.log(ordenPago);
        ordenPago.importe = ordenPago.total;
        if (ordenPago.id) {
           return this.http.post(this.getApiURL() + 'ordenPago', ordenPago).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'ordenPago', ordenPago).toPromise().then(this.handleOk, this.handleError);
        }
        
    }
}
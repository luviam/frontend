import { FiltroProveedor } from '../filtros/FiltroProveedor';
import { Descriptivo } from '../../model/Descriptivo';
import { CuentaAplicacion } from '../../model/CuentaAplicacion';
import { CentroCosto } from '../../model/CentroCosto';
import { ImputacionCuentaConfig } from '../../model/ImputacionCuentaConfig';

import { ItemOrdenPago } from '../../model/ItemOrdenPago';
import { CabeceraOrdenPago } from '../ordenPago/listado/shared/CabeceraOrdenPago';

import { OrdenPago } from '../../model/OrdenPago';
import { CabeceraFactura } from '../facturas/listado/shared/CabeceraFactura';
import { ListaProveedores } from '../lista-proveedores/shared/lista-proveedores.model';
import { ConvenioSalon } from '../../model/ConvenioSalon';

import { HttpClient, HttpParams } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { Proveedor } from '../../model/Proveedor';
import * as moment from 'moment';

@Injectable()
export class ProveedoresService extends ServicioAbstract {

   

    public getFacturas(filtro: FiltroProveedor): Promise<any> {

        if (filtro.fechaDesde > filtro.fechaHasta) {
            return Promise.reject({ "mensaje": "Fecha Desde debe ser menor a la Fecha Hasta" });
        }

        let url = "facturas/cabeceras/";
        return this.http.post(this.getApiURL() + url, filtro).toPromise().then(this.parseCabecerasFacturas, this.handleError);
    }

    public getFacturasImpagas(idProveedor : number, codigoCentro : string): Promise<any> {
       
        let url = "facturas/cabeceras/impagas/" +idProveedor + "/" + codigoCentro + "/";
        
        return this.http.get(this.getApiURL() + url).toPromise().then(this.parseCabecerasFacturas, this.handleError);
    }

    public getOrdenesPagos(filtro: any): Promise<any> {
        if (filtro.fechaDesde > filtro.fechaHasta) {
            return Promise.reject({ "mensaje": "Fecha Desde debe ser menor a la Fecha Hasta" });
        }

         let url = "ordenPago/cabeceras/";
         return this.http.post(this.getApiURL() + url ,filtro ).toPromise().then(this.parseCabecerasOrdenPago, this.handleError);
      
    }

    public parseCabecerasFacturas(response: any): CabeceraFactura[] {
        let res = response;
        let cabeceras: CabeceraFactura[] = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            cabeceras = res.respuesta.map(c => 
                   CabeceraFactura.fromData(c))
        }
        return cabeceras;

    }

    public parseCabecerasOrdenPago(response: any): CabeceraOrdenPago[] {
        let res = response;
        let cabeceras: CabeceraOrdenPago[] = [];
        if (res !== null && res.respuesta !== null) {
            var $this = this;
            cabeceras = res.respuesta.map(c => CabeceraOrdenPago.fromData(c));
        }
        return cabeceras;

    }
    public getAllList(filtro: any): Promise<any> {
        return this.http.get(this.getApiURL() + "proveedor/cabecera")
            .toPromise()
            .then((response: any) => {
                let res = response;
                let cabeceras: ListaProveedores[] = [];
                if (res !== null && res.respuesta !== null) {
                    var $this = this;
                    cabeceras = res.respuesta.map( v => ListaProveedores.fromData(v));
                }
                return cabeceras;

            }, this.handleError);

    }

    public getSaldo(id:number) : Promise<any>{
        return this.http.get(this.getApiURL() + "proveedor/saldo/" + id )
        .toPromise()
        .then((response: any) => {
            let res = response;
            return res.respuesta;

        }, this.handleError);
    }

    public getById(id: number): Promise<Proveedor> {
        return this.http.get(this.getApiURL() + "proveedor/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let proveedor: Proveedor;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    proveedor = this.parseProveedor(res.respuesta);
                }
                return proveedor;

            }, this.handleError);

    }
    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'proveedor', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public guardar(proveedor: Proveedor): Promise<any> {
        if (proveedor.id) {
            return this.http.post(this.getApiURL() + 'proveedor', proveedor).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'proveedor', proveedor).toPromise().then(this.handleOk, this.handleError);
        }
    }

    private parseProveedor(data: any) : Proveedor{

        return Proveedor.fromData(data);
    }
}
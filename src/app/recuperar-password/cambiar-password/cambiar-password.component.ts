import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Router } from '@angular/router';
import { Message } from 'primeng/primeng';
import { ActivatedRoute } from '@angular/router';
import { SessionComponent } from '../../session-component.component';
import { LoginService } from '../../login/service/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'eventos-cambiar-password',
	templateUrl: './cambiar-password.component.html',
	styleUrls: ['./cambiar-password.component.less']
})
export class CambiarPasswordComponent extends SessionComponent implements OnInit {

	public chequeoRealizado:boolean = false;
	public validToken: boolean = false;
	public email: string;
	public token: string
	public sub: any;
	public msgs: Message[] = [];
	public password:string;
	public password2:string;

	constructor(cookieService :CookieServiceHelper,private route: ActivatedRoute, private loginService: LoginService, private router: Router) { 
		super(); 
	}


	ngOnInit() {
		const $this = this;
		this.loading=true;
		this.sub = this.route.params.subscribe(params => {
			this.chequeoRealizado = true;
			this.email = params['email'];
			this.token = params['token'];
			this.loginService.validarToken(this.email, this.token)
				.then((response) => {
					$this.validToken = response.respuesta;
					if (!$this.validToken) {
						$this.error('La verificación para cambiar la contraseña falló. Por favor revise su mail o solicite la recuperacion de contraseña nuevamente.');
					}
					this.loading=false;
				})
				.catch((response) => this.errorHandler(response, $this.messages));
		});
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	public cambiarPassword = () => {
		const $this = this;

		if (!this.password || !this.password2){
			$this.error('Ingrese su nueva contraseña en ambos campos.');	
			return;
		}

		if (this.password === this.password2){
			this.loading=true;
			this.loginService.cambiarPassword(this.token, this.email, this.password)
				.then((response)=>{
					if (response.respuesta){
						$this.success('Se ha actualizado la contraseña correctamente.');			
						setTimeout(()=>this.router.navigate(["/login"]), 1500);
					}else{
						$this.error('No se puedo actualizar la contraseña, por favor intente nuevamente mas tarde.');			
					}
					this.loading=false;
				}).catch((response)=>this.errorHandler(response));
		}else{
			$this.error('Las contraseñas no son iguales.');
		}
		
	}

}
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { StringUtils } from '../../common-utils/string-utils.class';
import { SessionComponent } from '../../session-component.component';
import {Message} from 'primeng/primeng';
import { LoginService } from '../../login/service/login.service';
import { Component, OnInit } from '@angular/core';
import {Md5} from 'ts-md5';

@Component({
	selector: 'eventos-olvido-password',
	templateUrl: './olvido-password.component.html',
	styleUrls: ['./olvido-password.component.less']
})
export class OlvidoPasswordComponent extends SessionComponent implements OnInit{

	public email:string;
	public mailEnviado:boolean = false;

	constructor(cookieService :CookieServiceHelper,private loginService: LoginService) { super(); }

	ngOnInit() {
	}

	recuperarPassword = () => {
		const $this = this;
		if (!StringUtils.isMailValid($this.email)){
			$this.error("El formato del email ingresado es incorrecto.");
			return;
		}
		$this.loading = true;
		this.loginService.recuperarPassword($this.email)
		.then((response)=>{
			$this.mailEnviado = response.respuesta == "true";
			if (!$this.mailEnviado){
				$this.error("No existe el email ingresado.");
			}
			$this.loading = false;
		})
		.catch((response)=>this.errorHandler(response));
  }

}
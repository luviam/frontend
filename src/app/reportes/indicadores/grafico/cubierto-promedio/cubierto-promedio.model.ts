import { Descriptivo } from './../../../../model/Descriptivo';
export class CubiertoPromedioModel{
    constructor(
        public producto : Descriptivo,
        public minimo : number,
        public maximo : number,
        public promedio : number
    ){

    }

    public static fromData(data : any) : CubiertoPromedioModel{
        if(!data) return null;
        let o : CubiertoPromedioModel = new CubiertoPromedioModel(
            Descriptivo.fromData(data.producto),
            data.minimo,
            data.maximo,
            data.promedio
        );
        return o;
    }
}
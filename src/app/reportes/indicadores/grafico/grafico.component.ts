import { ChartsGlobalData } from '../../model/ChartsGlobalData';
import { Descriptivo } from '../../../model/Descriptivo';
import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { IIndicadores } from '../../model/IIndicadores';

@Component({
	selector: 'grafico',
	templateUrl: 'grafico.component.html',
	styleUrls:["./grafico.component.less"]
})

export class GraficoComponent implements OnInit {

	
	private _indicador : IIndicadores;
	@ViewChild("indicadorV")
	public indicadorV : ElementRef;

	@Input()
	public globalData : ChartsGlobalData;
	
	@Input()
	set indicador(val: IIndicadores){
		this._indicador = val;
		this._indicador
		this.indicadorChange.emit(val);
	}
	get indicador():IIndicadores{
		return this._indicador;
	}

	//@Input()
	public mostrarFiltros : boolean = false;

	public onResize(event : any){
		console.log(event);
	}
	@Output()
	public indicadorChange : EventEmitter<IIndicadores> = new EventEmitter<IIndicadores>();

	
	public graphLoading : boolean = false;

	get tipo(): Descriptivo{
		return this.indicador? this.indicador.tipo : new Descriptivo("SD","SIN DEFINIR")	;
	}

	ngOnInit() { 

	}
}
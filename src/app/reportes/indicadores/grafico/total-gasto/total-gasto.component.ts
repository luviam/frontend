import { Grupo } from './../../../../model/Grupo';
import { FiltroTotalGasto } from './../../../model/FiltroTotalGasto';
import { ChartsGlobalData } from '../../../model/ChartsGlobalData';
import { DescriptivosService } from '../../../../common-services/descriptivos.service';
import { FiltroIndicadores } from '../../../model/FiltroIndicadores';
import { TotalGastoModel } from './total-gasto.model';
import { IIndicadores } from '../../../model/IIndicadores';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IndicadoresService } from '../../../services/indicadores.service';


@Component({
	selector: 'total-gasto',
	templateUrl: 'total-gasto.component.html',
	styleUrls: ['total-gasto.component.less']
})

export class TotalGastoComponent implements OnInit {
	
	get filtro() : FiltroTotalGasto{
		if(!this.indicador){
			return new FiltroTotalGasto();
		}
		if(!this.indicador.filtro){
			this.indicador.filtro =  new FiltroTotalGasto();
		}
		return <FiltroTotalGasto> this.indicador.filtro;
	}

	public data : TotalGastoModel;
	@Input()
	public globalData : ChartsGlobalData;
	
	private _indicador : IIndicadores;

	@Input()
	set indicador(val: IIndicadores){
		this._indicador = val;
		this.indicadorChange.emit(val);
		if(this.indicador.filtro.esValido()){
			this.buscarItem();
		}
	}

	get indicador(): IIndicadores{
		return this._indicador;
	}
	@Output()
	public indicadorChange : EventEmitter<IIndicadores> = new EventEmitter<IIndicadores>();

	@Input()
	set loading(val : boolean){
		this._loading = val;
		this.loadingChange.emit(val);
	}
	get loading() : boolean{
		return this._loading;
	}
	public _loading: boolean = false;

	get mostrarFiltros():boolean{
		return this.indicador.mostrarFiltros;
	}
	@Output()
	public loadingChange : EventEmitter<boolean> = new EventEmitter<boolean>();

	public rangeDates: Date[] = [];
	constructor(private indicadoresService : IndicadoresService){

	}
	
	ngOnInit() { 		
		if(!this.indicador.filtro){
			this.indicador.filtro = new FiltroIndicadores();
		}
	}
	public buscarItem(){
		let $this = this;
		this.indicador.mostrarFiltros = false;
		this.loading = true;
		$this.indicadoresService.getTotalGasto(this.filtro).then(r =>{
			$this.data = r;
			$this.loading = false;
		}).catch(e=>{
			console.log(e);
			$this.loading = false;
		})
	}

	
}
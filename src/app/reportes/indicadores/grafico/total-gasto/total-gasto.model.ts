import { Descriptivo } from '../../../../model/Descriptivo';
export class TotalGastoModel{
    constructor(
        public gasto : Descriptivo,
        public total :number = 0
    ){

    }

    public static fromData(data : any) : TotalGastoModel{
        if(!data) return null;
        let o : TotalGastoModel = new TotalGastoModel(
            Descriptivo.fromData(data.gasto),
           data.total
        );
        return o;
    }
}
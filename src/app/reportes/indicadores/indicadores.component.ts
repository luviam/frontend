import { LayoutIndicadores } from './../model/LayoutIndicadores';
import { Indicador } from './../model/Indicador';
import { MenuItem, Menu } from 'primeng/primeng';
import { ChartsGlobalData } from '../model/ChartsGlobalData';
import { IIndicadores } from '../model/IIndicadores';
import { IndicadoresService } from '../services/indicadores.service';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { Descriptivo } from '../../model/Descriptivo';
import { FiltroCliente } from '../../clientes/filtros/FiltroCliente';
import { DragulaService } from 'ng2-dragula';
import { FiltroIndicadores } from '../model/FiltroIndicadores';

@Component({
	selector: 'indicadores',
	templateUrl: 'indicadores.component.html',
	styleUrls:["./indicadores.component.less"]
})

export class IndicadoresComponent extends SessionComponent implements OnInit {

	public indicadores : IIndicadores[] = [];

	public indicadorSeleccionado : IIndicadores;
	@Input()
	public globalData : ChartsGlobalData;

	@ViewChild("menu")
	private menu : Menu;

	@ViewChild("charts")
	private charts : ElementRef;

	public items: MenuItem[];

	public mostrarFiltros : boolean = false;

	constructor(
				private indicadoresService : IndicadoresService,
				private dragulaService: DragulaService){
		super();
		dragulaService.createGroup("INDICADORES", {
			moves: (el, container, handle) => {
			  return handle.className.includes('handle');
			}
		  });
	}
	ngOnInit() { 
		let $this = this;
		this.items =[
			{label: 'Promedio PU', command: (event)=>{
				this.agregarIndicador(new Indicador(new Descriptivo("C","Promedio Ventas"),"",false, new FiltroIndicadores()))
			}},
			{label: 'Total Ventas x Producto',  command: (event)=>{
				this.agregarIndicador(new Indicador(new Descriptivo("TV","Total Ventas"),"",false, new FiltroIndicadores()))
			}},
			{label: 'Total Gastos',  command: (event)=>{
				this.agregarIndicador(new Indicador(new Descriptivo("TG","Total Gastod"),"",false, new FiltroIndicadores()))
			}}
		];	
		$this.addLoadingCount();
		this.indicadoresService.getLayout().then((r : LayoutIndicadores)=>{
			$this.indicadores = r.indicadores || [];
			$this.susLoadingCount();
			
		}).catch(this.errorHandler);

	}

	public save(event : any){
		let $this = this;
		let indicV = this.charts.nativeElement.children;
		for (let index = 0; index < indicV.length; index++) {
					$this.indicadores[index].width = indicV[index].clientWidth + "px";
					$this.indicadores[index].height = indicV[index].clientHeight + "px";
					
			
		}
		$this.addLoadingCount();
		this.indicadoresService.saveDisplay(new LayoutIndicadores(JSON.stringify(this.indicadores))).then((r)=>{
			$this.susLoadingCount();
			$this.success("Tablero Guardado");
		}).catch(this.errorHandler);
	}
	trackByIndex(index: number, obj: any): any {
		return index;
	  }
	public updateIndicador(ind : IIndicadores){
		this.indicadores.filter(i => i === ind)[0] = ind;
	}
	public mostrarMenu(event:any,indicador ? : IIndicadores){
		this.menu.show(event);
	}
	public deleteIndicador(indicador : IIndicadores){
		this.indicadores = this.indicadores.filter(i => i != indicador);
	}
	public agregarIndicador(indicador : IIndicadores){
		this.indicadores.push(indicador);
	}

	public toggleFiltro(ind : number){
		this.indicadores[ind].mostrarFiltros = !this.indicadores[ind].mostrarFiltros;
		
	}
}
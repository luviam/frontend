import { Grupo } from './../../model/Grupo';
import { Descriptivo } from '../../model/Descriptivo';
export class ChartsGlobalData{

    public productos : Descriptivo[] = [];
    public categorias : Descriptivo[] = [];
    public centros : Descriptivo[] = [];
    public grupos : Grupo[] = [];
    public subGrupos : Descriptivo[] = [];
    public gastos : Descriptivo[] = [];
    public periodos : Descriptivo[] = [
        new Descriptivo("D","Hoy"),
        new Descriptivo("M","Este mes"),
        new Descriptivo("T","Este trimestre"),
        new Descriptivo("S","Este semestre"),
        new Descriptivo("A","Este año"),
        new Descriptivo("R","Por rango")
    ];
    public tiposGraficos : Descriptivo[] =[
        new Descriptivo("C","Promedio de PU")
    ]    
}
import { Descriptivo } from '../../model/Descriptivo';
import * as moment from 'moment';
export class FiltroGastos {

    constructor(public fechaDesde?: Date,
        public fechaHasta?: Date,
        public centro: Descriptivo = Descriptivo.TODOS(),
        public grupo: Descriptivo = Descriptivo.TODOS(),
        public subGrupo: Descriptivo = Descriptivo.TODOS()) {

        if (!fechaDesde) {
            this.fechaDesde = moment().startOf('month').subtract(3, 'month').toDate();
        }
        if (!fechaHasta) {
            this.fechaHasta = new Date();
        }
    }



}
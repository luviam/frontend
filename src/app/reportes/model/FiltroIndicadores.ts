import { IFiltro } from './IFiltro';
import { Descriptivo } from '../../model/Descriptivo';
import * as moment from 'moment';
export class FiltroIndicadores implements IFiltro{

constructor(public rangeDate : Date[] = [],
            public periodo : Descriptivo = new Descriptivo("H","Hoy"),
            public centro : Descriptivo = Descriptivo.TODOS(),
            public categoria : Descriptivo = Descriptivo.TODOS(),
            public producto : Descriptivo = Descriptivo.TODOS()){

    if(!rangeDate) this.rangeDate = [];
}

public esValido() :boolean{
    return this.centro && this.centro.codigo !== Descriptivo.TODOS().codigo &&
           this.producto && this.producto.codigo !== Descriptivo.TODOS().codigo

}
set desde(date : Date){
    if(!this.rangeDate){
        this.rangeDate =[];
    }
    this.rangeDate[0] = date;
}
get desde() : Date{
    if(this.periodo){
        switch (this.periodo.codigo) {
            case "H":
                    return moment().startOf("day").toDate();
            case "M":
                    return moment().startOf("month").toDate();
            case "T":
                return moment().quarter(moment().quarter()).startOf('quarter').toDate();
            case "S":
                if(1 <= moment().month() &&  moment().month() <= 6){
                    return new Date(moment().year(),1,1,0,0,0,0);
                }else {
                    return new Date(moment().year(),7,1,0,0,0,0);
                }                      
            case "A":
                    return moment().startOf("year").toDate();                    
            default:
            return this.rangeDate[0]? this.rangeDate[0] : new Date();
        }
    }
     return this.rangeDate[0];
}


set hasta(date : Date){
    if(!this.rangeDate){
        this.rangeDate =[];
        this.rangeDate[0] = new Date();
    }
    this.rangeDate[1] = date;
}
get hasta() : Date{
    if(this.periodo){
        switch (this.periodo.codigo) {
            case "H":
                    return moment().endOf("day").toDate();
            case "M":
                    return moment().endOf("month").toDate();
            case "T":
                return moment().quarter(moment().quarter()).endOf('quarter').toDate();
            case "S":
                if(1 <= moment().month() &&  moment().month() <= 6){
                    return new Date(moment().year(),6,30,11,59,59,0);
                }else {
                    return new Date(moment().year(),12,31,11,59,59,0);
                }                      
            case "A":
                    return moment().endOf("year").toDate();                    
            default:
                return this.rangeDate[1]? this.rangeDate[1] : new Date();
        }
    }
     return this.rangeDate[1];
}

get rangoStr(){
    if(this.desde && this.hasta){
        return moment(this.desde).format("DD/MM/YY") + " hasta " + moment(this.hasta).format("DD/MM/YY");
    }
    return;
}

public getResumen(): string{
    let centro : string = this.centro.codigo !== Descriptivo.TODOS().codigo? this.centro.codigo : "";
    let producto : string = this.producto.codigo !== Descriptivo.TODOS().codigo? this.producto.codigo : "";
    let fecha : string = this.periodo && this.periodo.codigo === 'R'? this.rangoStr : this.periodo ? this.periodo.descripcion : "";
    return [centro, producto, fecha].filter(function (val) {return val;}).join(' - ');
}


public static fromData(data : any): FiltroIndicadores{
    if(!data) return null;
    let f : FiltroIndicadores = new FiltroIndicadores(
        data.rangeDate? data.rangeDate : [],
        Descriptivo.fromData(data.periodo),
        Descriptivo.fromData(data.centro),
        Descriptivo.fromData(data.categoria),
        Descriptivo.fromData(data.producto)

    );
    return f;
}

}
import { Grupo } from './../../model/Grupo';
import { FiltroIndicadores } from './FiltroIndicadores';
import { IFiltro } from './IFiltro';
import { Descriptivo } from '../../model/Descriptivo';
import * as moment from 'moment';
export class FiltroTotalGasto extends FiltroIndicadores{

constructor( rangeDate : Date[] = [],
            periodo : Descriptivo = new Descriptivo("H","Hoy"),
             centro : Descriptivo = Descriptivo.TODOS(),
             public grupo ?: Grupo,
            public gasto : Descriptivo = Descriptivo.TODOS()){

super(rangeDate,periodo,centro,Descriptivo.TODOS());
  if(!rangeDate) this.rangeDate = [];
}

public esValido() :boolean{
    return this.centro && this.centro.codigo !== Descriptivo.TODOS().codigo &&
           this.gasto && this.gasto.codigo !== Descriptivo.TODOS().codigo 
           && this.grupo && this.grupo.codigo !== Descriptivo.TODOS().codigo ;

}



public static fromData(data : any): FiltroTotalGasto{
    if(!data) return null;
    let f : FiltroTotalGasto = new FiltroTotalGasto(
        data.rangeDate? data.rangeDate : [],
        Descriptivo.fromData(data.periodo),
        Descriptivo.fromData(data.centro),
        Grupo.fromData(data.grupo),
        Descriptivo.fromData(data.gasto)

    );
    return f;
}

}
import { IFiltro } from './IFiltro';
import { Descriptivo } from '../../model/Descriptivo';
import * as moment from 'moment';
export class FiltroTotalProductos implements IFiltro{

constructor(public desde : Date = moment().subtract("days", 60).startOf("day").toDate(),
            public hasta : Date = new Date(),
            public centro : Descriptivo = Descriptivo.TODOS(),
            public categoria : Descriptivo = Descriptivo.TODOS(),
            public producto : Descriptivo = Descriptivo.TODOS()){

}

public esValido() :boolean{
    return this.centro && this.centro.codigo !== Descriptivo.TODOS().codigo &&
           this.producto && this.producto.codigo !== Descriptivo.TODOS().codigo

}

get rangoStr(){
    if(this.desde && this.hasta){
        return moment(this.desde).format("DD/MM/YY") + " hasta " + moment(this.hasta).format("DD/MM/YY");
    }
    return;
}

public getResumen(): string{
    let centro : string = this.centro.codigo !== Descriptivo.TODOS().codigo? this.centro.codigo : "";
    let producto : string = this.producto.codigo !== Descriptivo.TODOS().codigo? this.producto.codigo : "";
    
    return [centro, producto, this.rangoStr].filter(function (val) {return val;}).join(' - ');
}


public static fromData(data : any): FiltroTotalProductos{
    if(!data) return null;
    let f : FiltroTotalProductos = new FiltroTotalProductos(
        data.desde? new Date(data.desde) : null,
        data.hasta? new Date(data.hasta) : null,
        Descriptivo.fromData(data.centro),
        Descriptivo.fromData(data.categoria),
        Descriptivo.fromData(data.producto)

    );
    return f;
}

}
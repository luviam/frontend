export interface IFiltro{
    getResumen(): string;
    esValido():boolean;
}
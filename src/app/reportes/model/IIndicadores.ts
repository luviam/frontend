import { FiltroCliente } from '../../clientes/filtros/FiltroCliente';
import { FiltroIndicadores } from './FiltroIndicadores';
import { Descriptivo } from '../../model/Descriptivo';
import { IFiltro } from './IFiltro';
export interface IIndicadores{
     tipo : Descriptivo;
     titulo : string;
     mostrarFiltros: boolean,
     filtro?:IFiltro;
     getTitulo():string;
     width: string;
     height: string;
     
     
}
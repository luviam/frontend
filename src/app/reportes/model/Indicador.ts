import { FiltroTotalGasto } from './FiltroTotalGasto';
import { FiltroIndicadores } from './FiltroIndicadores';
import { Descriptivo } from './../../model/Descriptivo';
import { IIndicadores } from './IIndicadores';
import { IFiltro } from './IFiltro';
export class Indicador implements IIndicadores{

    constructor(
        public tipo : Descriptivo,
        public titulo : string,
        public mostrarFiltros: boolean,
        public filtro?:IFiltro,
        public width: string = '20em',
        public height: string = '15em'){

        }
    public  getTitulo():string{
        return this.filtro && this.filtro.getResumen()? this.filtro.getResumen() : this.tipo? this.tipo.descripcion : "Sin Titulo";
    }

    public static fromData(data : any): Indicador{
        if(!data) return null;
        let i : Indicador = new Indicador(
            Descriptivo.fromData(data.tipo),
            data.titulo,
            data.mostrarFiltros,
            null,
            data.width,
            data.height

        )
        if(i.tipo.codigo === 'TG'){
            i.filtro = FiltroTotalGasto.fromData(data.filtro);
        }else{
            i.filtro = FiltroIndicadores.fromData(data.filtro);
        }
        return i;
    }
    
}
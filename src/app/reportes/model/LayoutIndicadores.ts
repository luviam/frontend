import { Indicador } from './Indicador';
import { IIndicadores } from './IIndicadores';
export class LayoutIndicadores{
    private _indicadores : IIndicadores[] = [];
    constructor(public indicadoresStr : string = ""){
        this._indicadores = JSON.parse(indicadoresStr).map(i=> Indicador.fromData(i));
    }

    get indicadores(): IIndicadores[]{
        return this._indicadores;
    }
    public static fromData(data : any): LayoutIndicadores{
        if(!data) return null;
        let l = new LayoutIndicadores(
            data.indicadoresStr);
        
        return l;

    }
}
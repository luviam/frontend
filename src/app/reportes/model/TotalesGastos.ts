
export class TotalesGastos{
	constructor(
		public idRubro ?: number,
		public rubro? :string,
		public idGrupo ?: number,
		public grupo? :string,
		public idSubGrupo ?: number,
		public subGrupo? :string,
		public idCuenta ?: number,
		public cuenta? :string,
		public debe : number = 0,
		public haber : number = 0
	){

	}

	public static fromData(data : TotalesGastos) : TotalesGastos{
		if(!data) return null; 
		let o : TotalesGastos  = new TotalesGastos(
			data.idRubro,
			data.rubro,
			data.idGrupo,
			data.grupo,
			data.idSubGrupo,
			data.subGrupo,
			data.idCuenta,
			data.cuenta,
			data.debe? data.debe : 0,
			data.haber? data.haber : 0
		);
		return o; 

	}

}
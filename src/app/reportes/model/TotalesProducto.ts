
export class TotalesProductos{
	constructor(

		public idProducto? : number,
		public categoria? : string,
		public producto? : string,
		public cantidad? : number,
		public ventas? : number,
		public cuPromedio? : number,
		public cobros? : number,
	){}

	public static fromData(data : any) : TotalesProductos{
		if(!data) return null; 
		let o : TotalesProductos  = new TotalesProductos(
		 data.idProducto,
		 data.categoria,
		 data.producto,
		 data.cantidad,
		 data.ventas,
		 data.cuPromedio,
		 data.cobros,	);
		return o; 

	}

}
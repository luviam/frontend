import { PlanCuentaService } from './../../common-services/planCuentaService.service';
import { Descriptivo } from '../../model/Descriptivo';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { ChartsGlobalData } from '../model/ChartsGlobalData';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'panel-control',
	templateUrl: 'panel-control.component.html',
	styleUrls: ["./panel-control.component.less"]
})

export class PanelControlComponent extends SessionComponent implements OnInit {

	public globalData :ChartsGlobalData = new ChartsGlobalData();
	public view : string = "totales";
	get titulo() : string{
		if(this.view ==="totales"){
			return "Totales por Producto";
		}else if(this.view === "graficos"){
			return "Indicadores";
		}
		return "Sin titulo";
	}
	constructor(cookieService : CookieServiceHelper,
				private descService :DescriptivosService,
			private planCuentaService : PlanCuentaService){
		super();
	}
	ngOnInit() { 
		let $this = this;
		this.addLoadingCount();
		this.descService.getAllCategoriasProducto().then(r => {
			$this.globalData.categorias = r;
			$this.globalData.categorias.push(Descriptivo.TODOS());
			
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.planCuentaService.getGruposByRubro("EG").then(r => {
			$this.globalData.grupos = r;
			$this.susLoadingCount();
		}).catch(this.errorHandler);
		
		this.addLoadingCount();
		this.descService.getCentrosCosto().then(r => {
			$this.globalData.centros = r;
			$this.globalData.centros.push(Descriptivo.TODOS());
			
			
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descService.getProductos().then(r => {
			$this.globalData.productos = r;
			$this.globalData.productos.push(Descriptivo.TODOS());
			$this.susLoadingCount();
		}).catch(this.errorHandler);


	}
}
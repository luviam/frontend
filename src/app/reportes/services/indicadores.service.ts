import { Descriptivo } from './../../model/Descriptivo';
import { TotalGastoModel } from './../indicadores/grafico/total-gasto/total-gasto.model';
import { FiltroTotalGasto } from './../model/FiltroTotalGasto';
import { FiltroIndicadores } from '../model/FiltroIndicadores';
import { HttpClient } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';
import { CubiertoPromedioModel } from '../indicadores/grafico/cubierto-promedio/cubierto-promedio.model';

import { LayoutIndicadores } from '../model/LayoutIndicadores';

@Injectable()
export class IndicadoresService extends ServicioAbstract {

   
    public saveDisplay(layout : LayoutIndicadores) : Promise<LayoutIndicadores>{
        return this.http.post(this.getApiURL() + "indicadores/guardarLayout", layout).toPromise().then(this.handleOk,this.handleError);
    }
    public getIndicadorX(filtro : FiltroIndicadores): Promise<any> {
        
        return Promise.resolve({prueba:"asd"});
    }

    public getLayout(): Promise<any> {
        return this.http.get(this.getApiURL() + "indicadores/getLayout").toPromise().then((r) => {
            let res: any = r;
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                 return LayoutIndicadores.fromData(res.respuesta);
            }
            return null;
        }).catch(this.handleError);
    }

    public getPromedioCubierto(filtro: FiltroIndicadores): Promise<CubiertoPromedioModel>{
        
        return this.http.get(this.getApiURL() + "indicadores/precioPromedio/" + filtro.centro.codigo + "/" + filtro.producto.codigo + "/" + filtro.desde + "/" + filtro.hasta).toPromise().then((r) => {
            
            let res: any = r;
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                 return CubiertoPromedioModel.fromData(res.respuesta);
            }
            return null;
        }).catch(this.handleError);
    }

    
    public getTotalGasto(filtro: FiltroTotalGasto): Promise<TotalGastoModel>{
        
       return this.http.get(this.getApiURL() + "indicadores/totalGasto/" + filtro.centro.codigo + "/" + filtro.gasto.codigo + "/" + filtro.desde + "/" + filtro.hasta).toPromise().then((r) => {
            
            let res: any = r;
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                 return TotalGastoModel.fromData(res.respuesta);
            }
            return null;
        }).catch(this.handleError);
    }
}
import { TotalesGastos } from './../model/TotalesGastos';
import { FiltroGastos } from './../model/FiltroGastos';
import { HttpClient } from '@angular/common/http';
import { ServicioAbstract } from '../../common-services/service.service';
import { Injectable } from '@angular/core';
import { TotalesProductos } from '../model/TotalesProducto';

@Injectable()
export class ReporteService extends ServicioAbstract {

   

    public getTotalesPorProducto(
        desde: Date,
        hasta: Date,
        centro: string = "-1",
        cProducto: string = "-1",
        cCategoria: string = "-1"
    ): Promise<TotalesProductos[]> {
        return this.http.get(this.getApiURL() + "totales_producto/buscar/" + desde + "/" + hasta + "/" + centro + "/" + cProducto + "/" + cCategoria).toPromise().then((r) => {
            let items: TotalesProductos[] = [];
            let res: any = r;
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                items = res.respuesta.map(j => TotalesProductos.fromData(j));
            }
            return items;
        }).catch(this.handleError);
    }

    public getTotalesGastos(filtro: FiltroGastos): Promise<TotalesGastos[]> {
        return this.http.post(this.getApiURL() + "totales_gastos/buscar", filtro).toPromise().then((r) => {
            let items: TotalesGastos[] = [];
            let res: any = r;
            if (res !== null && res.respuesta !== null) {
                var $this = this;
                items = res.respuesta.map(j => TotalesGastos.fromData(j));
            }
            return items;
        }).catch(this.handleError);
    }
}
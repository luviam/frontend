import { Grupo } from './../../model/Grupo';
import { PlanCuentaService } from './../../common-services/planCuentaService.service';
import { FiltroGastos } from '../model/FiltroGastos';
import { Descriptivo } from '../../model/Descriptivo';
import { ChartsGlobalData } from '../model/ChartsGlobalData';
import { TotalesGastos } from '../model/TotalesGastos';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { CentroCostoService } from '../../common-services/centroCostoService.service';


import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ReporteService } from '../services/reportes.service';
import * as moment from 'moment';
import { DataTable } from 'primeng/primeng';
@Component({
	selector: 'totales-gastos',
	templateUrl: 'totales-gastos.component.html'
})

export class TotalesGastosComponent extends SessionComponent implements OnInit {

	private _grupo: Grupo;
	private grupoTodos: Grupo = new Grupo(-1, "-1", "Todos", [this.todos]);

	public tableLoading = false;
	@ViewChild("tabla")
	public table: DataTable;
	set grupo(val: Grupo) {
		if (val && val.codigo !== this.todos.codigo) {
			this.filtro.grupo = val;
			this._grupo = val;
		} else {
			this._grupo = this.grupoTodos;
			this.filtro.grupo = this.todos;
		}
	}
	get grupo(): Grupo {
		return this._grupo;
	}

	set centro(val: Descriptivo) {
		if (val && val.codigo !== this.todos.codigo) {
			this.filtro.centro = val;
		} else {
			this.filtro.centro = this.todos;
		}
	}
	get centro(): Descriptivo {
		return this.filtro.centro;
	}

	set subGrupo(val: Descriptivo) {
		if (val && val.codigo !== this.todos.codigo) {
			this.filtro.subGrupo = val;
		} else {
			this.filtro.subGrupo = this.todos;
		}
	}
	get subGrupo(): Descriptivo {
		return this.filtro.subGrupo;
	}


	public filtro: FiltroGastos = new FiltroGastos();

	public titulo: string = "Totales por Gastos";
	get totalGastos(): number {
		this.tableLoading = true;
		let total = this.table ? this.table.dataToRender.reduce((a, b) => a + b.debe - b.haber, 0) : 0;
		this.tableLoading = false;
		return total;
	}


	@Input()
	public globalData: ChartsGlobalData = new ChartsGlobalData();
	get grupos(): Grupo[] {
		return this.globalData.grupos;
	}

	get subGrupos(): Descriptivo[] {
		return (this.grupo && this.grupo.subGrupos) ? [this.todos].concat(this.grupo.subGrupos) : [this.todos];
	}

	get centros(): Descriptivo[] {
		return this.globalData.centros;
	}

	public items: TotalesGastos[] = [];

	constructor(
		private descService: DescriptivosService,
		private planCuentaService: PlanCuentaService,
		private reporteService: ReporteService) {
		super();
	}
	ngOnInit() {

		let $this = this;
		this.addLoadingCount();
		this.planCuentaService.getGruposByRubro("EG").then(r => {
			$this.globalData.grupos = r;
			$this.globalData.grupos.push($this.grupoTodos);
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descService.getCentrosCosto().then(r => {
			$this.globalData.centros = r;
			$this.globalData.centros.push(this.todos);


			$this.susLoadingCount();
		}).catch(this.errorHandler);



		if (!$this.grupo) {
			$this.grupo = this.grupoTodos;
		}
		if (!$this.subGrupo) {
			$this.subGrupo = this.todos;
		}
		if (!$this.centro) {
			$this.centro = this.todos;
		}





	}

	public buscarItems() {
		let $this = this;
		$this.addLoadingCount();
		this.reporteService.getTotalesGastos(this.filtro).then((r) => {
			$this.items = r;
			//$this.totalGastos = $this.items.reduce((a,b)=> a + b.debe - b.haber, 0);
			$this.susLoadingCount();
		}).catch(this.errorHandler)


	}


}
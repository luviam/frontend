import { FiltroTotalProductos } from './../model/FiltroTotalProducto';
import { DataTable } from 'primeng/primeng';
import { FiltroIndicadores } from '../model/FiltroIndicadores';
import { Descriptivo } from '../../model/Descriptivo';
import { ChartsGlobalData } from '../model/ChartsGlobalData';
import { TotalesProductos } from '../model/TotalesProducto';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { CentroCostoService } from '../../common-services/centroCostoService.service';


import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ReporteService } from '../services/reportes.service';
import * as moment from 'moment';
@Component({
	selector: 'totales-producto',
	templateUrl: 'totales-producto.component.html'
})

export class TotalesProductoComponent extends SessionComponent implements OnInit {


	set producto(val: Descriptivo) {
		if (val) {
			this.filtro.producto = val;
		} else {
			this.filtro.producto = Descriptivo.TODOS();
		}
	}
	get producto(): Descriptivo {
		return this.filtro.producto;
	}


	set categoria(val: Descriptivo) {
		if (val) {
			this.filtro.categoria = val;
		} else {
			this.filtro.categoria = Descriptivo.TODOS();
		}
	}
	get categoria(): Descriptivo {
		return this.filtro.categoria;
	}

	set centro(val: Descriptivo) {
		if (val) {
			this.filtro.centro = val;
		} else {
			this.filtro.centro = Descriptivo.TODOS();
		}
	}
	get centro(): Descriptivo {
		return this.filtro.centro;
	}


	@ViewChild("tabla")
	public tabla: DataTable;
	public filtro: FiltroTotalProductos = new FiltroTotalProductos();

	public titulo: string = "Totales por Producto";
	public totalVentas: number = 0;
	public totalCobros: number = 0;

	@Input()
	public globalData: ChartsGlobalData = new ChartsGlobalData();
	get categorias(): Descriptivo[] {
		return this.globalData.categorias;
	}

	get centros(): Descriptivo[] {
		return this.globalData.centros;
	}

	get productos(): Descriptivo[] {
		return this.globalData.productos;
	}
	public items: TotalesProductos[] = [];

	constructor(
		private descService: DescriptivosService,
		private reporteService: ReporteService) {
		super();
	}
	ngOnInit() {
		let $this = this;
		this.addLoadingCount();
		this.descService.getAllCategoriasProducto().then(r => {
			$this.globalData.categorias = r;
			$this.globalData.categorias.push(Descriptivo.TODOS());

			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descService.getCentrosCosto().then(r => {
			$this.globalData.centros = r;
			$this.globalData.centros.push(Descriptivo.TODOS());


			$this.susLoadingCount();
		}).catch(this.errorHandler);

		this.addLoadingCount();
		this.descService.getProductos().then(r => {
			$this.globalData.productos = r;
			$this.globalData.productos.push(Descriptivo.TODOS());
			$this.susLoadingCount();
		}).catch(this.errorHandler);

		if (!$this.categoria) {
			$this.categoria = Descriptivo.TODOS();
		}
		if (!$this.centro) {
			$this.centro = Descriptivo.TODOS();
		}
		if (!$this.producto) {
			$this.producto = Descriptivo.TODOS();
		}




	}
	public onTableFilter(event: any) {
		this.totalCobros = 0;
		this.totalVentas = 0;
		if(this.tabla){
			this.tabla.dataToRender.forEach(a => {
				this.totalCobros +=  a.cobros;
				this.totalVentas +=  a.ventas;
			})
		}
		
	}

	public buscarItems() {
		let $this = this;
		$this.addLoadingCount();
		$this.reporteService.getTotalesPorProducto(this.filtro.desde, this.filtro.hasta,
			this.centro.codigo,
			this.producto.codigo, this.categoria.codigo).then(r => {
				$this.items = r;
				$this.totalCobros = 0;
				$this.totalVentas = 0;
				$this.items.forEach(a => {
					$this.totalCobros +=  a.cobros;
					$this.totalVentas +=  a.ventas;
				})
				
				$this.susLoadingCount();
			}).catch(this.errorHandler);
	}


}
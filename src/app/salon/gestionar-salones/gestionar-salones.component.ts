import { Descriptivo } from '../../model/Descriptivo';
import { ProductoService } from '../../producto/service/producto.service';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Empleado } from '../../model/Empleado';
import { Component, OnInit } from '@angular/core';
import { SalonService } from '../service/salon.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { SessionComponent } from '../../session-component.component';
import { DescriptivosService } from '../../common-services/descriptivos.service';
import { Salon } from '../../model/Salon';


@Component({
  selector: 'app-gestionar-salones',
  templateUrl: './gestionar-salones.component.html',
  styleUrls: ['./gestionar-salones.component.less']
})
export class GestionarSalonesComponent extends SessionComponent implements OnInit {

  public titulo: string = "";
  public salon: Salon = new Salon();
  public finalizado: boolean = false;
  public editando: boolean = false;
  public historicoComisionista: boolean = false;
  public productos: Descriptivo[] =[] ;


  constructor(cookieService :CookieServiceHelper, private service: SalonService,
    private router: Router,
    private confirmationService: ConfirmationService,
	private route: ActivatedRoute,
	private productoService : ProductoService
		) {
    super();
  }

  ngOnInit() {
    let $this = this;
    this.finalizado = false;
	
	this.addLoadingCount();
	this.productoService.getDescriptivosByCategoria("ALQUI").then((prods)=>{
		$this.productos = prods;
		$this.susLoadingCount();
	}).catch(this.errorHandler);
		this.route.params.subscribe(params => {

			let idSalon: string = <string>params['id'];
			if (idSalon) {
				this.addLoadingCount();
				this.service.getById(parseInt(idSalon)).then((sal) => {
					$this.historicoComisionista = sal.esComisionista;
					$this.salon = sal;
					$this.titulo = "Editando - " + $this.salon.nombre;
					$this.susLoadingCount();
					$this.editable = true;
					$this.editando = true;					
				}).catch(this.errorHandler);
			} else {
				this.titulo = "Nuevo Salon";			
				$this.editable = true;

			}

		});
  }

  public confirmarCancelar() {
		this.confirmationService.confirm({
			header: "Cancelar Edición",
			message: 'Se perderán los cambios realizado. Desea continuar?',
			accept: () => {
				this.router.navigate(['salones/lista-salones']);
			}
		});
  }
  
  public guardar(){
	  let $this = this;
	this.addLoadingCount();
	this.service.guardar(this.salon).then((c) => {
		$this.success("El salon fue guardado correctamente");
		$this.finalizado = true;
		setTimeout(() => {
			$this.router.navigate(["salones/lista-salones"]);
		}, 3000)
		$this.susLoadingCount();
	}).catch(this.errorHandler);
  }

  public guardarSalon() {
		let $this = this;
		if (this.salonValido(this.salon)) {
			if(this.historicoComisionista && !this.salon.esComisionista){
				this.confirmationService.confirm({
					header: "Eliminar Comisionista",
					message: 'Esta operación eliminará al comisionista. Desea Continuar?',
					accept: () => {
						$this.guardar();
					}
				});
			}else{
				this.guardar();
			}
			
		} else {
			this.error("El salon no es valido");
		}
	}
	

	public salonValido(salon: Salon): boolean {
		return true;
  }
	

}

import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';
import { Message } from 'primeng/primeng';

import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SessionComponent } from '../../session-component.component';
import { Salon } from '../../model/Salon';
import { SalonService } from '../service/salon.service';
import { Descriptivo } from '../../model/Descriptivo';

@Component({
	selector: 'lista-salones',
	templateUrl: './lista-salones.component.html',
	styleUrls: ['./lista-salones.component.less']
})


export class ListaSalonesComponent extends SessionComponent implements OnInit {

	salones: Salon[];
	msgs: Message[] = [];
	displayDialog: boolean = false;
	tituloDialog: string;
	
	
	constructor(cookieService :CookieServiceHelper, private service: SalonService, private router: Router, private confirmationService: ConfirmationService) {
		super();
	}

	ngOnInit() {
		
		const $this = this;
		this.addLoadingCount();
		this.service.getAll().then(sls => {
			$this.salones = sls;
			$this.susLoadingCount();
		}).catch(this.errorHandler)
		
		
	}

	public eliminar(salon : Salon) {
		this.confirmationService.confirm({
			header: "Eliminación de salon",
			message: 'Desea deshabilitar el salon?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				this.service.delete(salon.id).then((res) => {
					$this.susLoadingCount();
					$this.ngOnInit();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public habilitar(salon : Salon) {
		this.confirmationService.confirm({
			header: "Habilitación de salon",
			message: 'Desea habilitar el salon?',
			accept: () => {
				this.addLoadingCount();
				let $this = this;
				salon.activo = true;
				this.service.guardar(salon).then((res) => {
					$this.susLoadingCount();
					$this.ngOnInit();
				}).catch($this.errorHandler);
				
			}
		});
	}

	public editarSalon(salon: Salon) {
		this.router.navigate(["/salones/editar/" + salon.id + ""]);
	}

	public nuevoSalon() {
		this.router.navigate(["/salones/nuevo"]);
	}
	
}
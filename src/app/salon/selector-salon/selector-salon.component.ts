import { Descriptivo } from '../../model/Descriptivo';
import { SalonService } from '../service/salon.service';
import { Salon } from '../../model/Salon';
import { SessionComponent } from '../../session-component.component';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CookieServiceHelper } from '../../common-services/cookieServiceHelper.service';

@Component({
	selector: 'selector-salon',
	templateUrl: 'selector-salon.component.html',
	styles: ['selecto-salon.component.less']
})

export class SelectorSalonComponent extends SessionComponent {
	private _salonSeleccionado: Salon

	@Input()
	set salon(val: Salon) {
		this._salonSeleccionado = val;
		this.salonChange.emit(val);
	}

	get salon(): Salon {
		return this._salonSeleccionado;
	}

	public salones: Salon[] = [];

	@Output()
	public salonChange: EventEmitter<Salon> = new EventEmitter<Salon>();

	@Output()
	public onSalonSeleccionado: EventEmitter<Salon> = new EventEmitter<Salon>();

	@Input()
	public editable: boolean = true;


	constructor(
		private salonService: SalonService) {
		super();
	}
	ngOnInit() {

		let $this = this;
		this.addLoadingCount();
		this.salonService.getAll().then(salones => {
			let otro : Salon = new Salon(null,"","","");
			otro.codigo = Salon.OTRO;
			otro.descripcion="Otro Salón";
			$this.salones = [...salones, otro];
			$this.susLoadingCount();
		}).catch(this.errorHandler);

	}
	get esAlternativo():boolean{
		return this.salon && this.salon.codigo === Salon.OTRO;
	}
	public salonSeleccionado(event : any){
		if(event.new)
			this.onSalonSeleccionado.emit(event.new);
			this.salonChange.emit(this.salon);
	}
}
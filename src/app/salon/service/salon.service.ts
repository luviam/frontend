import { HttpClient, HttpParams } from '@angular/common/http';
import { DescriptivoHelper } from '../../common-utils/descriptivo-helper.class';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { ServicioAbstract } from '../../common-services/service.service';

import { Salon } from '../../model/Salon';
import { Descriptivo } from '../../model/Descriptivo';


@Injectable()
export class SalonService extends ServicioAbstract {


  

    getAll = (): Promise<Salon[]> => {
        return this.http.get(this.getApiURL()+"salon/all").toPromise().then(
                function (response:any) {
                    return response.respuesta.map(s => Salon.fromData(s));
                }
            )
            .catch(this.handleError);
    }

    public delete(id: any): Promise<any> {
        let params = new HttpParams();
       params = params.append('id', id)
        return this.http.delete(this.getApiURL() + 'salon', {params}).toPromise().then(this.handleOk, this.handleError);
    }

    public getById(id: number): Promise<any> {
        return this.http.get(this.getApiURL() + "salon/" + id)
            .toPromise()
            .then((response: any) => {
                let res = response;
                let salon: Salon;
                if (res !== null && res.respuesta !== null) {
                    var $this = this;

                    salon = this.parseSalon(res.respuesta);
                }
                return salon;

            }, this.handleError);

    }

    public guardar(salon: Salon): Promise<any> {
        if (salon.id) {
            return this.http.post(this.getApiURL() + 'salon', salon).toPromise().then(this.handleOk, this.handleError);
        }
        else {
            return this.http.put(this.getApiURL() + 'salon', salon).toPromise().then(this.handleOk, this.handleError);
        }
    }

    private parseSalon(sl: any) {
        return Salon.fromData(sl);
    
    }

}


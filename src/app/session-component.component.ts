import { CookieServiceHelper } from 'app/common-services/cookieServiceHelper.service';
import { DescriptivosService } from './common-services/descriptivos.service';
import { Descriptivo } from './model/Descriptivo';
import { MessagesService } from './common-utils/messages-data-service.service';

import { JwtHelperService  } from '@auth0/angular-jwt';
import { Message } from 'primeng/primeng';
import { AppUser } from './model/AppUser';
import { LoadingService } from './common-utils/loading-data-service.service';
import { GlobalInjector } from './GlobalInjector';
import { Filtro } from './model/Filtro';


export class SessionComponent {

    userLogged: boolean = false;
    public todos :Descriptivo = Descriptivo.TODOS();
    public finalizado = false;
    public editable = false;
    private loadingService : LoadingService;
    protected descriptivoService : DescriptivosService
    public prevView : string;
    protected cServcie: CookieServiceHelper;
    public static LOCALE_ES:any =
        {
            firstDayOfWeek: 0,
            dayNames: ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
            monthNamesShort: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"]
        }
   ;
   public locale : any = {};
   private cookieService : CookieServiceHelper;
    private jwtHelper: JwtHelperService  = new JwtHelperService ();
    public messagesService: MessagesService;
   

    constructor( public currentView? : string) {
        this.loadingService = LoadingService.injector.get(LoadingService);
        this.messagesService = MessagesService.injector.get(MessagesService);
        this.cookieService = GlobalInjector.InjectorInstance.get<CookieServiceHelper>(CookieServiceHelper);
        this.descriptivoService = DescriptivosService.injector.get(DescriptivosService);
        this.messagesService.life = 5000;
        this.locale.es =SessionComponent.LOCALE_ES;
        
    }

	public onFocus(event : any){
		event.srcElement.select();
	  }
    
    ngDoCheck() {
        // Custom change detection
        this.userLogged = this.isUserLogged();
    }

    addLoadingCount(){
      this.loadingService.addLoadingCount();
    }

    susLoadingCount(){
       this.loadingService.susLoadingCount();
    }
    
    isUserLogged():boolean {
        let token =this.cookieService.getToken();
        const isLogged = token && !this.jwtHelper.isTokenExpired(token);
        if (!isLogged && !(<any>window.location.pathname == "/login" || <any>window.location.pathname == "/")){
            window.location.pathname == "login";
        }
        return isLogged;
    }
    get esAuditor(){
        return this.getCurrentUser() && 
            this.getCurrentUser().user.roles.some(r => r.codigo === "ROLE_ADMIN" || r.codigo === "ROLE_AUDITOR");
        
    }

    get esGerencia(){
        return this.getCurrentUser() && 
        this.getCurrentUser().user.roles.some(r => r.codigo === "ROLE_GERENCIA");
    }

    get esAdministrador(){
      return this.getCurrentUser() && 
            this.getCurrentUser().user.roles.some(r => r.codigo === "ROLE_ADMIN" );
    }

    
    getCurrentUser():AppUser {
        return this.isUserLogged() ? (<AppUser>this.cookieService.getUserApp()) : undefined;
    }
    getUserDescriptivo(): Descriptivo{
        return this.getCurrentUser().user.descriptivo;
    }
    getUserFullName(): string{
        let user = this.getCurrentUser()? this.getCurrentUser().user : null;
        return user? user.nombre + " " + user.apellido : "SIN USUARIO";
        
    }

    get loading(){
        return this.loadingService.loading;
    }
    set loading(val : boolean){
        this.loadingService.loading = val;
    }

    get messages() : Message[]{
        return this.messagesService.messages;
    }
    public errorHandler(error, msgs?:Message[]){
        MessagesService.injector.get(MessagesService).errorHandler(error,msgs);
       
                
    }
    protected clearCredentials(): void {
        this.cookieService.deleteUser();
    }

    public success(mensaje?:string , titulo?:string){
      this.messagesService.success(mensaje,titulo);
    }
    public error(mensaje?:string , titulo?:string){
        this.messagesService.error(mensaje,titulo);
    }
    public info(mensaje?:string , titulo?:string){
        this.messagesService.info(mensaje,titulo);
    }
    public warning(mensaje?:string , titulo?:string){
        this.messagesService.warning(mensaje,titulo);
    }

    set messageLife(val : number){
        this.messagesService.life = val;
    }

    public goTo(vista : string){
        this.prevView = this.currentView;
        this.currentView = vista;
    }

    public volver(){
        this.currentView = this.prevView;
    }

    public currencyInputChanged(value) {
        var num = value.replace(/[$,]/g, "");
        return Number(num);
      }
    protected getFiltro() : Filtro{
        return null;
    }
      protected recargarFiltros(){
          if(this.currentView){
              let o = sessionStorage.getItem(this.currentView);
              if(o) this.parseFiltro(JSON.parse(o));
          }
      }
      protected parseFiltro(o: any){
         
      }
      protected saveFiltro(){
          if(this.getFiltro() && this.currentView){
              sessionStorage.setItem(this.currentView, JSON.stringify(this.getFiltro(),this.getFiltro().replacer));
          }
      }
    
}
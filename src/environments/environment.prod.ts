export const environment = {
  production: true,
  apiUrl: "https://sistemas.cateringluviam.com.ar/luviam-backend/",
  whitelistedDomains: ["sistemas.cateringluviam.com.ar"],
  baseServerUrl:"https://sistemas.cateringluviam.com.ar/"
};

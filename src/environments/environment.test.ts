export const environment = {
  production: false,
  apiUrl: "https://thinkup.com.ar:8443/luviam-backend/",
  whitelistedDomains: ["thinkup.com.ar:8443"],
  baseServerUrl:"https://thinkup.com.ar:8443/"
  
};
